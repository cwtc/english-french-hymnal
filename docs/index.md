# Accueil · Home

Real blurbs will go here at release. For now, this page will host guidelines for the project.

## General motivating principles

1. Anglicans have a big tent and that includes hymnody. Types of hymns considered in scope for this project include:

	* "Classical" text/tune pairings familiar to Anglicans
	* Hymns from non-Anglican sources that have historically been taken up by Anglicans (e.g. many Lutheran hymns, Latin hymns, etc.)
	* Existing French texts that may not be familiar to Anglicans, but that have historically been set to tunes familiar to Anglicans
	* Existing French text/tune pairings that Anglicans have borrowed wholesale (e.g. *Angels we have heard on high*), but that may have fallen out of the francophone repertoire (e.g. *Bring a torch, Jeannette, Isabella*)

2. Text content and musical style should be representative of at least some branch of Anglicanism (hard to pin down though that may be!). Every "flavor" contributes to our hymnody tradition in some way and is fair game to include. The spectrum may run from hymns common among the high Anglo-Catholics (e.g. *Adoro te devote*) to the low Evangelicals (e.g. *Just as I am*).

3. Exceptions can be made for French hymns that are not Anglican whatsoever, but that may be well known to francophones and that fit stylistically with the overall project (e.g. *Dans cette étable*). This is a way of extending a bridge to francophones who may not be as familiar with this kind of hymnody.

4. This project is intended to be in the public domain. We will sort out nitpicky things later (i.e. writers whose work is in the public domain in Canada but not the US, etc.) but a good rule of thumb is to only include content by authors who died before ~1950. The current idea is that any new translations we produce will be put in the public domain.

## Translation guidelines

It is *preferable* to include already-existing translations when available *if* the translations are good. A good translation:

* Is as faithful as possible to the imagery and ideas of the original;
* Is singable, having been written with respect to the rhyme and meter scheme of the original (or a systematically consistent variation thereof).
* Covers the *best* or *most well-known* set of verses as are commonly found in our tradition. (We don't need all 12 verses of a Lutheran chorale, maybe just the 4-5 that made it to our own hymnals... unless they're *really good* or necessary somehow to the integrity of the hymn, e.g. *Noël nouvelet*.)

If no good translation exists (which is the case for many, many hymns), we can have at doing it ourselves. Things to keep in mind when writing a translation:

* French is sung differently than spoken, especially surrounding *e*'s. Sing as you go to make sure things scan. More [here](https://thetranslationcompany.com/resources/language-country/french/french-translation-translators/versification-french-translation.htm).
* Translating poetry is hard and it often takes more syllables to say something in French than in English. If you have to drop some subtlety in the original because it just won't fit, try to make up for it elsewhere with a bonus allusion or relevant construction.

## Musical guidelines

All tunes should have all four lines (soprano, alto, tenor, bass).

It is acceptable to include more than one tune for a given text if the tunes are well-known to the Anglican tradition (e.g. *Saint Louis* and *Forest Green* for *O little town of Bethlehem*.)

Tweaks to the rhythm to accomodate French syllabification should be avoided, but should be notated when made. (Tweaks are more acceptable when made to accommodate the *e* rules; see above.)
