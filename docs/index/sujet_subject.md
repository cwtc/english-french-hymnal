

## Matin · Morning

| Prémiere phrase · First line | Mélodie · Tune | Numéro · Number 
| - | - | - |
| Père, on te loue; la nuit se termine | *Christe sanctorum* | [1](../../hymns\morning\1_pere_on_te_loue_la_nuit_se_termine_CHRISTE_SANCTORUM) |
| Christ, le ciel montre ta gloire | *Ratisbon* | [2](../../hymns\morning\2_christ_le_ciel_montre_ta_gloire_RATISBON) |
| O Dieu, Seigneur de vérité | *Song 34* | [3](../../hymns\morning\3_o_dieu_seigneur_de_verite_SONG_34) |


## Soir · Evening

| Prémiere phrase · First line | Mélodie · Tune | Numéro · Number 
| - | - | - |
| Reste avec moi | *Eventide* | [4](../../hymns\evening\4_reste_avec_moi_EVENTIDE) |
| Seigneur, quand vient l'obscurité | *Tallis Canon* | [5](../../hymns\evening\5_seigneur_quand_vient_l'obscurite_TALLIS_CANON) |
| Le jour décline et la nuit tombe | *St Clement* | [6](../../hymns\evening\6_le_jour_decline_et_la_nuit_tombe_ST_CLEMENT) |
| Le jour de travail prend fin | *Innsbruck* | [7](../../hymns\evening\7_le_jour_de_travail_prend_fin_INNSBRUCK) |
| Avant la fin du jour qui fuit | *Te lucis ante terminum* | [8](../../hymns\evening\8_avant_la_fin_du_jour_qui_fuit_TE_LUCIS_ANTE_TERMINUM) |


## Annonciation · Annunciation

| Prémiere phrase · First line | Mélodie · Tune | Numéro · Number 
| - | - | - |
| Quand Gabriel du ciel est descendu | *Gabriel's Message* | [9](../../hymns\annunciation\9_quand_gabriel_du_ciel_est_descendu_GABRIELS_MESSAGE) |
| O Vièrge Marie | *Paderborn* | [10](../../hymns\annunciation\10_o_vierge_marie_PADERBORN) |


## Avent · Advent

| Prémiere phrase · First line | Mélodie · Tune | Numéro · Number 
| - | - | - |
| Venez, divin Messie | *Venez, divin Messie* | [11](../../hymns\advent\11_venez_divin_messie_VENEZ_DIVIN_MESSIE) |
| Levez vous! La voix appelle | *Wachet auf* | [12](../../hymns\advent\12_levez_vous_la_voix_appelle_WACHET_AUF) |
| Voici, il descend parmi les nuages | *Helmsley* | [13](../../hymns\advent\13_voici_il_descend_parmi_les_nuages_HELMSLEY) |
| O viens, ô viens, Emmanuel | *Veni veni Emmanuel* | [14](../../hymns\advent\14_o_viens_o_viens_emmanuel_VENI_VENI_EMMANUEL) |
| Créateur des cieux étoilés | *Conditor alme siderum* | [15](../../hymns\advent\15_createur_des_cieux_etoiles_CONDITOR_ALME_SIDERUM) |
| Entendez-vous ces cris joyeux | *Richmond* | [16](../../hymns\advent\16_entendez_vous_ces_cris_joyeux_RICHMOND) |
| Une voix retentissante | *Merton* | [17](../../hymns\advent\17_une_voix_retentissante_MERTON) |
| Viens, ô Jésus très attendu | *Stuttgart* | [18](../../hymns\advent\18_viens_o_jesus_tres_attendu_STUTTGART) |
| Consolez mon très cher peuple | *Genevan 42* | [19](../../hymns\advent\19_consolez_mon_tres_cher_peuple_GENEVAN_42) |


## Noël · Christmas

| Prémiere phrase · First line | Mélodie · Tune | Numéro · Number 
| - | - | - |
| Dans l'humble mangeoire | *Cradle Song* | [20](../../hymns\christmas\20_dans_l'humble_mangeoire_CRADLE_SONG) |
| Dans l'humble mangeoire | *Mueller* | [21](../../hymns\christmas\21_dans_l'humble_mangeoire_MUELLER) |
| Petite ville, Bethléem | *Saint Louis* | [22](../../hymns\christmas\22_petite_ville_bethleem_SAINT_LOUIS) |
| Petite ville, Bethléem | *Forest Green* | [23](../../hymns\christmas\23_petite_ville_bethleem_FOREST_GREEN) |
| Douce nuit, sainte nuit | *Stille Nacht* | [24](../../hymns\christmas\24_douce_nuit_sainte_nuit_STILLE_NACHT) |
| Il est né, le divin Enfant | *Il est né* | [25](../../hymns\christmas\25_il_est_ne_le_divin_enfant_IL_EST_NE) |
| Les anges dans nos campagnes | *Gloria* | [26](../../hymns\christmas\26_les_anges_dans_nos_campagnes_GLORIA) |
| Ecoutez le chant du ciel | *Mendelssohn* | [27](../../hymns\christmas\27_ecoutez_le_chant_du_ciel_MENDELSSOHN) |
| Que Dieu vous garde heureux, ô gens | *God rest you merry* | [28](../../hymns\christmas\28_que_dieu_vous_garde_heureux_o_gens_GOD_REST_YOU_MERRY) |
| Montez à Dieu, chants d'allegresse | *Noël* | [29](../../hymns\christmas\29_montez_a_dieu_chants_d'allegresse_NOEL) |
| Les bergers et leurs troupeaux | *Winchester Old* | [30](../../hymns\christmas\30_les_bergers_et_leurs_troupeaux_WINCHESTER_OLD) |
| Cà, bergers | *Cà, bergers* | [31](../../hymns\christmas\31_ca_bergers_CA_BERGERS) |
| Entre le boeuf et l'âne gris | *Entre le boeuf* | [32](../../hymns\christmas\32_entre_le_boeuf_et_l'ane_gris_ENTRE_LE_BOEUF) |
| Quelle est cette odeur agréable | *Quelle est cette odeur* | [33](../../hymns\christmas\33_quelle_est_cette_odeur_agreable_QUELLE_EST_CETTE_ODEUR) |
| Minuit, chrétiens | *Cantique de Noël* | [34](../../hymns\christmas\34_minuit_chretiens_CANTIQUE_DE_NOEL) |
| Dans cette étable | *Dans cette étable* | [35](../../hymns\christmas\35_dans_cette_etable_DANS_CETTE_ETABLE) |
| Un flambeau, Jeannette, Isabelle | *Un flambeau* | [36](../../hymns\christmas\36_un_flambeau_jeannette_isabelle_UN_FLAMBEAU) |
| D'un arbre séculaire | *Es ist ein Ros* | [37](../../hymns\christmas\37_d'un_arbre_seculaire_ES_IST_EIN_ROS) |
| Joie dans le monde | *Antioch* | [38](../../hymns\christmas\38_joie_dans_le_monde_ANTIOCH) |
| O peuple fidèle | *Adeste fideles* | [39](../../hymns\christmas\39_o_peuple_fidele_ADESTE_FIDELES) |
| Des dominions glorieuses | *Regent Square* | [40](../../hymns\christmas\40_des_dominions_glorieuses_REGENT_SQUARE) |
| Chantons Noël, voici le jour | *In dulci jubilo* | [41](../../hymns\christmas\41_chantons_noel_voici_le_jour_IN_DULCI_JUBILO) |
| Bambins et gamines | *Ihr Kinderlein kommet* | [42](../../hymns\christmas\42_bambins_et_gamines_IHR_KINDERLEIN_KOMMET) |
| Chrétiens, prenez courage | *Une jeune pucelle* | [43](../../hymns\christmas\43_chretiens_prenez_courage_UNE_JEUNE_PUCELLE) |
| Qui est cet Enfant endormi | *Greensleeves* | [44](../../hymns\christmas\44_qui_est_cet_enfant_endormi_GREENSLEEVES) |
| La première fois que Noël se disait | *The First Noel* | [45](../../hymns\christmas\45_la_premiere_fois_que_noel_se_disait_THE_FIRST_NOEL) |
| Noël nouvelet | *Noël nouvelet* | [46](../../hymns\christmas\46_noel_nouvelet_NOEL_NOUVELET) |


## Epiphanie · Epiphany

| Prémiere phrase · First line | Mélodie · Tune | Numéro · Number 
| - | - | - |
| Astre brillant qui répands sur la terre | *Liebster Immanuel* | [47](../../hymns\epiphany\47_astre_brillant_qui_repands_sur_la_terre_LIEBSTER_IMMANUEL) |
| Jadis les rois mages heureux | *Dix* | [48](../../hymns\epiphany\48_jadis_les_rois_mages_heureux_DIX) |


## Chandeleur · Candlemas

| Prémiere phrase · First line | Mélodie · Tune | Numéro · Number 
| - | - | - |
| Ouvre tes portes, ô Sion | *Bedford* | [49](../../hymns\candlemas\49_ouvre_tes_portes_o_sion_BEDFORD) |


## Avant le Carême · Shrovetide

| Prémiere phrase · First line | Mélodie · Tune | Numéro · Number 
| - | - | - |
| O sainte et douce harmonie | *Dulce carmen* | [50](../../hymns\shrovetide\50_o_sainte_et_douce_harmonie_DULCE_CARMEN) |


## Carême · Lent

| Prémiere phrase · First line | Mélodie · Tune | Numéro · Number 
| - | - | - |
| Pendant ces quarante jours | *Heinlein* | [51](../../hymns\lent\51_pendant_ces_quarante_jours_HEINLEIN) |


## Passion · Passiontide

| Prémiere phrase · First line | Mélodie · Tune | Numéro · Number 
| - | - | - |
| Tout honneur, toute gloire | *Saint Theodulph* | [52](../../hymns\passion\52_tout_honneur_toute_gloire_SAINT_THEODULPH) |
| Gouttez, mes larmes | *Song 46* | [53](../../hymns\passion\53_gouttez_mes_larmes_SONG_46) |
| La Mère était douloureuse | *Stabat Mater* | [54](../../hymns\passion\54_la_mere_etait_douloureuse_STABAT_MATER) |
| Ah, très cher Jésus | *Herzliebster Jesu* | [55](../../hymns\passion\55_ah_tres_cher_jesus_HERZLIEBSTER_JESU) |
| O front sacré, accablé | *Passion Chorale* | [56](../../hymns\passion\56_o_front_sacre_accable_PASSION_CHORALE) |
| Vois là-bas, mettre le Seigneur en croix | *Were you there* | [57](../../hymns\passion\57_vois_la_bas_mettre_le_seigneur_en_croix_WERE_YOU_THERE) |
| Hors pair est cet amour | *Love unknown* | [58](../../hymns\passion\58_hors_pair_est_cet_amour_LOVE_UNKNOWN) |
| Quelle douleur | *O Traurigkeit* | [59](../../hymns\passion\59_quelle_douleur_O_TRAURIGKEIT) |
| L'étendard vient du Roi des rois | *Vexilla regis prodeunt* | [60](../../hymns\passion\60_l'etendard_vient_du_roi_des_rois_VEXILLA_REGIS_PRODEUNT) |


## Pâques · Easter

| Prémiere phrase · First line | Mélodie · Tune | Numéro · Number 
| - | - | - |
| Le grand combat est terminé | *Victory* | [61](../../hymns\easter\61_le_grand_combat_est_termine_VICTORY) |
| A toi la gloire | *Judas Maccabaeus* | [62](../../hymns\easter\62_a_toi_la_gloire_JUDAS_MACCABAEUS) |
| Jésus Christ, il réssuscite | *Easter Hymn* | [63](../../hymns\easter\63_jesus_christ_il_ressuscite_EASTER_HYMN) |
| Le Christ est ressuscité | *Würtemburg* | [64](../../hymns\easter\64_le_christ_est_ressuscite_WURTEMBURG) |
| Venez, fidèles, chantez | *Gaudeamus Pariter* | [65](../../hymns\easter\65_venez_fideles_chantez_GAUDEAMUS_PARITER) |


## Ascension · Ascension

| Prémiere phrase · First line | Mélodie · Tune | Numéro · Number 
| - | - | - |
| Gloire au Christ victorieux | *Llanfair* | [66](../../hymns\ascension\66_gloire_au_christ_victorieux_LLANFAIR) |
| Le front d'épines couronné | *St Magnus Clarke* | [67](../../hymns\ascension\67_le_front_d'epines_couronne_ST_MAGNUS_CLARKE) |


## Pentecôte · Pentecost

| Prémiere phrase · First line | Mélodie · Tune | Numéro · Number 
| - | - | - |
| Avant de quitter ce bas lieu | *Saint Cuthbert* | [68](../../hymns\pentecost\68_avant_de_quitter_ce_bas_lieu_SAINT_CUTHBERT) |


## Trinité · Trinity

| Prémiere phrase · First line | Mélodie · Tune | Numéro · Number 
| - | - | - |
| Viens, ô Roi tout-puissant | *Italian Hymn* | [69](../../hymns\trinity\69_viens_o_roi_tout_puissant_ITALIAN_HYMN) |


## Rogation · Rogation

| Prémiere phrase · First line | Mélodie · Tune | Numéro · Number 
| - | - | - |
| En ton nom, Seigneur, nous prions | *Sainte Agnes* | [70](../../hymns\rogation\70_en_ton_nom_seigneur_nous_prions_SAINTE_AGNES) |


## Louange · Praise

| Prémiere phrase · First line | Mélodie · Tune | Numéro · Number 
| - | - | - |
| Salut, le Nom du Redempteur | *Coronation* | [71](../../hymns\praise\71_salut_le_nom_du_redempteur_CORONATION) |
| Qu'au saint Nom de Jésus | *King's Weston* | [72](../../hymns\praise\72_qu'au_saint_nom_de_jesus_KINGS_WESTON) |
| Amour dépassant tout autre | *Hyfrydol* | [73](../../hymns\praise\73_amour_depassant_tout_autre_HYFRYDOL) |
| O Dieu, notre aide aux temps anciens | *Saint Anne* | [74](../../hymns\praise\74_o_dieu_notre_aide_aux_temps_anciens_SAINT_ANNE) |
| Loue, âme, le Roi de gloire | *Lauda anima* | [75](../../hymns\praise\75_loue_ame_le_roi_de_gloire_LAUDA_ANIMA) |
| Tant que le monde durera | *Duke Street* | [76](../../hymns\praise\76_tant_que_le_monde_durera_DUKE_STREET) |
| Guide-moi, Berger fidèle | *Cwm Rhondda* | [77](../../hymns\praise\77_guide_moi_berger_fidele_CWM_RHONDDA) |
| Mon Dieu, plus près de toi | *Bethany* | [78](../../hymns\praise\78_mon_dieu_plus_pres_de_toi_BETHANY) |
| Vous qui sur la terre habitez | *Old Hundredth* | [79](../../hymns\praise\79_vous_qui_sur_la_terre_habitez_OLD_HUNDREDTH) |
| Rendez à Dieu louange et gloire | *Rendez à Dieu* | [80](../../hymns\praise\80_rendez_a_dieu_louange_et_gloire_RENDEZ_A_DIEU) |
| Vous, créatures du Seigneur | *Lasst uns erfreuen* | [81](../../hymns\praise\81_vous_creatures_du_seigneur_LASST_UNS_ERFREUEN) |
| Dans ton amour, divin Sauveur | *Sagina* | [82](../../hymns\praise\82_dans_ton_amour_divin_sauveur_SAGINA) |
| Tel que je suis | *Woodworth* | [83](../../hymns\praise\83_tel_que_je_suis_WOODWORTH) |
| Jésus, ô Nom qui surpasse | *Aberystwyth* | [84](../../hymns\praise\84_jesus_o_nom_qui_surpasse_ABERYSTWYTH) |
| Torrents d'amour et de grâce | *Ebenezer* | [85](../../hymns\praise\85_torrents_d'amour_et_de_grace_EBENEZER) |
| Amour profond majestueux | *Deus tuorum militum* | [86](../../hymns\praise\86_amour_profond_majestueux_DEUS_TUORUM_MILITUM) |
| A l'Agneau sur son trône | *Diademata* | [87](../../hymns\praise\87_a_l'agneau_sur_son_trone_DIADEMATA) |
| Beau Seigneur Jésus | *Sainte Elisabeth* | [88](../../hymns\praise\88_beau_seigneur_jesus_SAINTE_ELISABETH) |
| Seigneur, que n'ai-je mille voix | *Azmon* | [89](../../hymns\praise\89_seigneur_que_n'ai_je_mille_voix_AZMON) |
| Louons du Seigneur | *Hanover* | [90](../../hymns\praise\90_louons_du_seigneur_HANOVER) |
| Le grand Dieu d'Abraham | *Leoni* | [91](../../hymns\praise\91_le_grand_dieu_d'abraham_LEONI) |
| Louons le Créateur | *Nun danket* | [92](../../hymns\praise\92_louons_le_createur_NUN_DANKET) |
| Louons le Dieu puissant | *Lobe den Herren* | [93](../../hymns\praise\93_louons_le_dieu_puissant_LOBE_DEN_HERREN) |
| Que l'amour m'enflamme | *Lourdes Hymn* | [94](../../hymns\praise\94_que_l'amour_m'enflamme_LOURDES_HYMN) |
| Protège l'enfance, Jésus, Bon Pasteur | *Hanover* | [95](../../hymns\praise\95_protege_l'enfance_jesus_bon_pasteur_HANOVER) |
| Comme un cerf altéré brâme | *Genevan 42* | [96](../../hymns\praise\96_comme_un_cerf_altere_brame_GENEVAN_42) |
| Sois ma vision | *Slane* | [97](../../hymns\praise\97_sois_ma_vision_SLANE) |


## Communion · Communion

| Prémiere phrase · First line | Mélodie · Tune | Numéro · Number 
| - | - | - |
| Vers toi, divin Père | *Hanover* | [98](../../hymns\communion\98_vers_toi_divin_pere_HANOVER) |
| Orne-toi, âme, de bonheur | *Schmücke dich* | [99](../../hymns\communion\99_orne_toi_ame_de_bonheur_SCHMUCKE_DICH) |
| Saint aliment des âmes | *Innsbruck* | [100](../../hymns\communion\100_saint_aliment_des_ames_INNSBRUCK) |
| Au banquet du Roi on chante | *Salzburg* | [101](../../hymns\communion\101_au_banquet_du_roi_on_chante_SALZBURG) |
| Alléluia, chante à Jésus | *Hyfrydol* | [102](../../hymns\communion\102_alleluia_chante_a_jesus_HYFRYDOL) |
| Attentifs à l'amour qui nous rachète | *Unde et memores* | [103](../../hymns\communion\103_attentifs_a_l'amour_qui_nous_rachete_UNDE_ET_MEMORES) |
| Dans l'amour, il rompit ce Pain | *Rendez à Dieu* | [104](../../hymns\communion\104_dans_l'amour_il_rompit_ce_pain_RENDEZ_A_DIEU) |
| Chante, ô langue, du mystère | *Pange lingua* | [105](../../hymns\communion\105_chante_o_langue_du_mystere_PANGE_LINGUA) |
| Viens silencieux, chair mortelle | *Picardy* | [106](../../hymns\communion\106_viens_silencieux_chair_mortelle_PICARDY) |


## Dedication d'une église · Dedication of a church

| Prémiere phrase · First line | Mélodie · Tune | Numéro · Number 
| - | - | - |
| Nous aimons le saint lieu | *Quam Dilecta* | [107](../../hymns\dedication\107_nous_aimons_le_saint_lieu_QUAM_DILECTA) |
| Le Christ, la pierre angulaire | *Westminster Abbey* | [108](../../hymns\dedication\108_le_christ_la_pierre_angulaire_WESTMINSTER_ABBEY) |
| J'aime ton beau royaume | *St Thomas (Williams)* | [109](../../hymns\dedication\109_j'aime_ton_beau_royaume_ST_THOMAS_(WILLIAMS)) |


## Action de grâce · Thanksgiving

| Prémiere phrase · First line | Mélodie · Tune | Numéro · Number 
| - | - | - |
| Reconnaissants, nous venons | *Saint George Windsor* | [110](../../hymns\thanksgiving\110_reconnaissants_nous_venons_SAINT_GEORGE_WINDSOR) |


## Sur mer · At sea

| Prémiere phrase · First line | Mélodie · Tune | Numéro · Number 
| - | - | - |
| Père éternel dont le pouvoir | *Melita* | [111](../../hymns\sea\111_pere_eternel_dont_le_pouvoir_MELITA) |


## L'Eglise militante · The Church militant

| Prémiere phrase · First line | Mélodie · Tune | Numéro · Number 
| - | - | - |
| L'église universelle a pour roc Jésus Christ | *Aurelia* | [112](../../hymns\militant\112_l'eglise_universelle_a_pour_roc_jesus_christ_AURELIA) |
| Debout, sainte cohorte | *Webb* | [113](../../hymns\militant\113_debout_sainte_cohorte_WEBB) |
| C'est un rempart que notre Dieu | *Ein feste Burg* | [114](../../hymns\militant\114_c'est_un_rempart_que_notre_dieu_EIN_FESTE_BURG) |
| Jésus, la gloire et l'honneur des anges | *Coelites plaudant* | [115](../../hymns\militant\115_jesus_la_gloire_et_l'honneur_des_anges_COELITES_PLAUDANT) |
| O jour de bonheur radiant | *Es flog ein kleins Waldsvögelein* | [116](../../hymns\militant\116_o_jour_de_bonheur_radiant_ES_FLOG_EIN_KLEINS_WALDSVOGELEIN) |


## L'Eglise triomphante · The Church triumphant

| Prémiere phrase · First line | Mélodie · Tune | Numéro · Number 
| - | - | - |
| La foi de nos aieux vivante | *Sainte Catherine* | [117](../../hymns\triumphant\117_la_foi_de_nos_aieux_vivante_SAINTE_CATHERINE) |
| Les dons éternels du grand Roi | *Aeterna Christi Munera* | [118](../../hymns\triumphant\118_les_dons_eternels_du_grand_roi_AETERNA_CHRISTI_MUNERA) |
| Il est aux cieux une joie immortelle | *O Quantia Qualia* | [119](../../hymns\triumphant\119_il_est_aux_cieux_une_joie_immortelle_O_QUANTIA_QUALIA) |
| Pour tous les saints | *Sine Nomine* | [120](../../hymns\triumphant\120_pour_tous_les_saints_SINE_NOMINE) |
