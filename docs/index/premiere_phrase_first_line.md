

## A

| Première phrase · First line | Mélodie · Tune | Sujet · Subject | Numéro · Number 
| - | - | - | - |
| A l'Agneau sur son trône | *Diademata* | Louange · Praise | [87](../../hymns\praise\87_a_l'agneau_sur_son_trone_DIADEMATA) |
| A toi la gloire | *Judas Maccabaeus* | Pâques · Easter | [62](../../hymns\easter\62_a_toi_la_gloire_JUDAS_MACCABAEUS) |
| Ah, très cher Jésus | *Herzliebster Jesu* | Passion · Passiontide | [55](../../hymns\passion\55_ah_tres_cher_jesus_HERZLIEBSTER_JESU) |
| Alléluia, chante à Jésus | *Hyfrydol* | Communion · Communion | [102](../../hymns\communion\102_alleluia_chante_a_jesus_HYFRYDOL) |
| Amour dépassant tout autre | *Hyfrydol* | Louange · Praise | [73](../../hymns\praise\73_amour_depassant_tout_autre_HYFRYDOL) |
| Amour profond majestueux | *Deus tuorum militum* | Louange · Praise | [86](../../hymns\praise\86_amour_profond_majestueux_DEUS_TUORUM_MILITUM) |
| Astre brillant qui répands sur la terre | *Liebster Immanuel* | Epiphanie · Epiphany | [47](../../hymns\epiphany\47_astre_brillant_qui_repands_sur_la_terre_LIEBSTER_IMMANUEL) |
| Attentifs à l'amour qui nous rachète | *Unde et memores* | Communion · Communion | [103](../../hymns\communion\103_attentifs_a_l'amour_qui_nous_rachete_UNDE_ET_MEMORES) |
| Au banquet du Roi on chante | *Salzburg* | Communion · Communion | [101](../../hymns\communion\101_au_banquet_du_roi_on_chante_SALZBURG) |
| Avant de quitter ce bas lieu | *Saint Cuthbert* | Pentecôte · Pentecost | [68](../../hymns\pentecost\68_avant_de_quitter_ce_bas_lieu_SAINT_CUTHBERT) |
| Avant la fin du jour qui fuit | *Te lucis ante terminum* | Soir · Evening | [8](../../hymns\evening\8_avant_la_fin_du_jour_qui_fuit_TE_LUCIS_ANTE_TERMINUM) |


## B

| Première phrase · First line | Mélodie · Tune | Sujet · Subject | Numéro · Number 
| - | - | - | - |
| Bambins et gamines | *Ihr Kinderlein kommet* | Noël · Christmas | [42](../../hymns\christmas\42_bambins_et_gamines_IHR_KINDERLEIN_KOMMET) |
| Beau Seigneur Jésus | *Sainte Elisabeth* | Louange · Praise | [88](../../hymns\praise\88_beau_seigneur_jesus_SAINTE_ELISABETH) |


## C

| Première phrase · First line | Mélodie · Tune | Sujet · Subject | Numéro · Number 
| - | - | - | - |
| C'est un rempart que notre Dieu | *Ein feste Burg* | L'Eglise militante · The Church militant | [114](../../hymns\militant\114_c'est_un_rempart_que_notre_dieu_EIN_FESTE_BURG) |
| Chante, ô langue, du mystère | *Pange lingua* | Communion · Communion | [105](../../hymns\communion\105_chante_o_langue_du_mystere_PANGE_LINGUA) |
| Chantons Noël, voici le jour | *In dulci jubilo* | Noël · Christmas | [41](../../hymns\christmas\41_chantons_noel_voici_le_jour_IN_DULCI_JUBILO) |
| Christ, le ciel montre ta gloire | *Ratisbon* | Matin · Morning | [2](../../hymns\morning\2_christ_le_ciel_montre_ta_gloire_RATISBON) |
| Chrétiens, prenez courage | *Une jeune pucelle* | Noël · Christmas | [43](../../hymns\christmas\43_chretiens_prenez_courage_UNE_JEUNE_PUCELLE) |
| Comme un cerf altéré brâme | *Genevan 42* | Louange · Praise | [96](../../hymns\praise\96_comme_un_cerf_altere_brame_GENEVAN_42) |
| Consolez mon très cher peuple | *Genevan 42* | Avent · Advent | [19](../../hymns\advent\19_consolez_mon_tres_cher_peuple_GENEVAN_42) |
| Créateur des cieux étoilés | *Conditor alme siderum* | Avent · Advent | [15](../../hymns\advent\15_createur_des_cieux_etoiles_CONDITOR_ALME_SIDERUM) |
| Cà, bergers | *Cà, bergers* | Noël · Christmas | [31](../../hymns\christmas\31_ca_bergers_CA_BERGERS) |


## D

| Première phrase · First line | Mélodie · Tune | Sujet · Subject | Numéro · Number 
| - | - | - | - |
| D'un arbre séculaire | *Es ist ein Ros* | Noël · Christmas | [37](../../hymns\christmas\37_d'un_arbre_seculaire_ES_IST_EIN_ROS) |
| Dans cette étable | *Dans cette étable* | Noël · Christmas | [35](../../hymns\christmas\35_dans_cette_etable_DANS_CETTE_ETABLE) |
| Dans l'amour, il rompit ce Pain | *Rendez à Dieu* | Communion · Communion | [104](../../hymns\communion\104_dans_l'amour_il_rompit_ce_pain_RENDEZ_A_DIEU) |
| Dans l'humble mangeoire | *Cradle Song* | Noël · Christmas | [20](../../hymns\christmas\20_dans_l'humble_mangeoire_CRADLE_SONG) |
| | *Mueller* | Noël · Christmas | [21](../../hymns\christmas\21_dans_l'humble_mangeoire_MUELLER) |
| Dans ton amour, divin Sauveur | *Sagina* | Louange · Praise | [82](../../hymns\praise\82_dans_ton_amour_divin_sauveur_SAGINA) |
| Debout, sainte cohorte | *Webb* | L'Eglise militante · The Church militant | [113](../../hymns\militant\113_debout_sainte_cohorte_WEBB) |
| Des dominions glorieuses | *Regent Square* | Noël · Christmas | [40](../../hymns\christmas\40_des_dominions_glorieuses_REGENT_SQUARE) |
| Douce nuit, sainte nuit | *Stille Nacht* | Noël · Christmas | [24](../../hymns\christmas\24_douce_nuit_sainte_nuit_STILLE_NACHT) |


## E

| Première phrase · First line | Mélodie · Tune | Sujet · Subject | Numéro · Number 
| - | - | - | - |
| Ecoutez le chant du ciel | *Mendelssohn* | Noël · Christmas | [27](../../hymns\christmas\27_ecoutez_le_chant_du_ciel_MENDELSSOHN) |
| En ton nom, Seigneur, nous prions | *Sainte Agnes* | Rogation · Rogation | [70](../../hymns\rogation\70_en_ton_nom_seigneur_nous_prions_SAINTE_AGNES) |
| Entendez-vous ces cris joyeux | *Richmond* | Avent · Advent | [16](../../hymns\advent\16_entendez_vous_ces_cris_joyeux_RICHMOND) |
| Entre le boeuf et l'âne gris | *Entre le boeuf* | Noël · Christmas | [32](../../hymns\christmas\32_entre_le_boeuf_et_l'ane_gris_ENTRE_LE_BOEUF) |


## G

| Première phrase · First line | Mélodie · Tune | Sujet · Subject | Numéro · Number 
| - | - | - | - |
| Gloire au Christ victorieux | *Llanfair* | Ascension · Ascension | [66](../../hymns\ascension\66_gloire_au_christ_victorieux_LLANFAIR) |
| Gouttez, mes larmes | *Song 46* | Passion · Passiontide | [53](../../hymns\passion\53_gouttez_mes_larmes_SONG_46) |
| Guide-moi, Berger fidèle | *Cwm Rhondda* | Louange · Praise | [77](../../hymns\praise\77_guide_moi_berger_fidele_CWM_RHONDDA) |


## H

| Première phrase · First line | Mélodie · Tune | Sujet · Subject | Numéro · Number 
| - | - | - | - |
| Hors pair est cet amour | *Love unknown* | Passion · Passiontide | [58](../../hymns\passion\58_hors_pair_est_cet_amour_LOVE_UNKNOWN) |


## I

| Première phrase · First line | Mélodie · Tune | Sujet · Subject | Numéro · Number 
| - | - | - | - |
| Il est aux cieux une joie immortelle | *O Quantia Qualia* | L'Eglise triomphante · The Church triumphant | [119](../../hymns\triumphant\119_il_est_aux_cieux_une_joie_immortelle_O_QUANTIA_QUALIA) |
| Il est né, le divin Enfant | *Il est né* | Noël · Christmas | [25](../../hymns\christmas\25_il_est_ne_le_divin_enfant_IL_EST_NE) |


## J

| Première phrase · First line | Mélodie · Tune | Sujet · Subject | Numéro · Number 
| - | - | - | - |
| J'aime ton beau royaume | *St Thomas (Williams)* | Dedication d'une église · Dedication of a church | [109](../../hymns\dedication\109_j'aime_ton_beau_royaume_ST_THOMAS_(WILLIAMS)) |
| Jadis les rois mages heureux | *Dix* | Epiphanie · Epiphany | [48](../../hymns\epiphany\48_jadis_les_rois_mages_heureux_DIX) |
| Joie dans le monde | *Antioch* | Noël · Christmas | [38](../../hymns\christmas\38_joie_dans_le_monde_ANTIOCH) |
| Jésus Christ, il réssuscite | *Easter Hymn* | Pâques · Easter | [63](../../hymns\easter\63_jesus_christ_il_ressuscite_EASTER_HYMN) |
| Jésus, la gloire et l'honneur des anges | *Coelites plaudant* | L'Eglise militante · The Church militant | [115](../../hymns\militant\115_jesus_la_gloire_et_l'honneur_des_anges_COELITES_PLAUDANT) |
| Jésus, ô Nom qui surpasse | *Aberystwyth* | Louange · Praise | [84](../../hymns\praise\84_jesus_o_nom_qui_surpasse_ABERYSTWYTH) |


## L

| Première phrase · First line | Mélodie · Tune | Sujet · Subject | Numéro · Number 
| - | - | - | - |
| L'église universelle a pour roc Jésus Christ | *Aurelia* | L'Eglise militante · The Church militant | [112](../../hymns\militant\112_l'eglise_universelle_a_pour_roc_jesus_christ_AURELIA) |
| L'étendard vient du Roi des rois | *Vexilla regis prodeunt* | Passion · Passiontide | [60](../../hymns\passion\60_l'etendard_vient_du_roi_des_rois_VEXILLA_REGIS_PRODEUNT) |
| La Mère était douloureuse | *Stabat Mater* | Passion · Passiontide | [54](../../hymns\passion\54_la_mere_etait_douloureuse_STABAT_MATER) |
| La foi de nos aieux vivante | *Sainte Catherine* | L'Eglise triomphante · The Church triumphant | [117](../../hymns\triumphant\117_la_foi_de_nos_aieux_vivante_SAINTE_CATHERINE) |
| La première fois que Noël se disait | *The First Noel* | Noël · Christmas | [45](../../hymns\christmas\45_la_premiere_fois_que_noel_se_disait_THE_FIRST_NOEL) |
| Le Christ est ressuscité | *Würtemburg* | Pâques · Easter | [64](../../hymns\easter\64_le_christ_est_ressuscite_WURTEMBURG) |
| Le Christ, la pierre angulaire | *Westminster Abbey* | Dedication d'une église · Dedication of a church | [108](../../hymns\dedication\108_le_christ_la_pierre_angulaire_WESTMINSTER_ABBEY) |
| Le front d'épines couronné | *St Magnus Clarke* | Ascension · Ascension | [67](../../hymns\ascension\67_le_front_d'epines_couronne_ST_MAGNUS_CLARKE) |
| Le grand Dieu d'Abraham | *Leoni* | Louange · Praise | [91](../../hymns\praise\91_le_grand_dieu_d'abraham_LEONI) |
| Le grand combat est terminé | *Victory* | Pâques · Easter | [61](../../hymns\easter\61_le_grand_combat_est_termine_VICTORY) |
| Le jour de travail prend fin | *Innsbruck* | Soir · Evening | [7](../../hymns\evening\7_le_jour_de_travail_prend_fin_INNSBRUCK) |
| Le jour décline et la nuit tombe | *St Clement* | Soir · Evening | [6](../../hymns\evening\6_le_jour_decline_et_la_nuit_tombe_ST_CLEMENT) |
| Les anges dans nos campagnes | *Gloria* | Noël · Christmas | [26](../../hymns\christmas\26_les_anges_dans_nos_campagnes_GLORIA) |
| Les bergers et leurs troupeaux | *Winchester Old* | Noël · Christmas | [30](../../hymns\christmas\30_les_bergers_et_leurs_troupeaux_WINCHESTER_OLD) |
| Les dons éternels du grand Roi | *Aeterna Christi Munera* | L'Eglise triomphante · The Church triumphant | [118](../../hymns\triumphant\118_les_dons_eternels_du_grand_roi_AETERNA_CHRISTI_MUNERA) |
| Levez vous! La voix appelle | *Wachet auf* | Avent · Advent | [12](../../hymns\advent\12_levez_vous_la_voix_appelle_WACHET_AUF) |
| Loue, âme, le Roi de gloire | *Lauda anima* | Louange · Praise | [75](../../hymns\praise\75_loue_ame_le_roi_de_gloire_LAUDA_ANIMA) |
| Louons du Seigneur | *Hanover* | Louange · Praise | [90](../../hymns\praise\90_louons_du_seigneur_HANOVER) |
| Louons le Créateur | *Nun danket* | Louange · Praise | [92](../../hymns\praise\92_louons_le_createur_NUN_DANKET) |
| Louons le Dieu puissant | *Lobe den Herren* | Louange · Praise | [93](../../hymns\praise\93_louons_le_dieu_puissant_LOBE_DEN_HERREN) |


## M

| Première phrase · First line | Mélodie · Tune | Sujet · Subject | Numéro · Number 
| - | - | - | - |
| Minuit, chrétiens | *Cantique de Noël* | Noël · Christmas | [34](../../hymns\christmas\34_minuit_chretiens_CANTIQUE_DE_NOEL) |
| Mon Dieu, plus près de toi | *Bethany* | Louange · Praise | [78](../../hymns\praise\78_mon_dieu_plus_pres_de_toi_BETHANY) |
| Montez à Dieu, chants d'allegresse | *Noël* | Noël · Christmas | [29](../../hymns\christmas\29_montez_a_dieu_chants_d'allegresse_NOEL) |


## N

| Première phrase · First line | Mélodie · Tune | Sujet · Subject | Numéro · Number 
| - | - | - | - |
| Nous aimons le saint lieu | *Quam Dilecta* | Dedication d'une église · Dedication of a church | [107](../../hymns\dedication\107_nous_aimons_le_saint_lieu_QUAM_DILECTA) |
| Noël nouvelet | *Noël nouvelet* | Noël · Christmas | [46](../../hymns\christmas\46_noel_nouvelet_NOEL_NOUVELET) |


## O

| Première phrase · First line | Mélodie · Tune | Sujet · Subject | Numéro · Number 
| - | - | - | - |
| O Dieu, Seigneur de vérité | *Song 34* | Matin · Morning | [3](../../hymns\morning\3_o_dieu_seigneur_de_verite_SONG_34) |
| O Dieu, notre aide aux temps anciens | *Saint Anne* | Louange · Praise | [74](../../hymns\praise\74_o_dieu_notre_aide_aux_temps_anciens_SAINT_ANNE) |
| O Vièrge Marie | *Paderborn* | Annonciation · Annunciation | [10](../../hymns\annunciation\10_o_vierge_marie_PADERBORN) |
| O front sacré, accablé | *Passion Chorale* | Passion · Passiontide | [56](../../hymns\passion\56_o_front_sacre_accable_PASSION_CHORALE) |
| O jour de bonheur radiant | *Es flog ein kleins Waldsvögelein* | L'Eglise militante · The Church militant | [116](../../hymns\militant\116_o_jour_de_bonheur_radiant_ES_FLOG_EIN_KLEINS_WALDSVOGELEIN) |
| O peuple fidèle | *Adeste fideles* | Noël · Christmas | [39](../../hymns\christmas\39_o_peuple_fidele_ADESTE_FIDELES) |
| O sainte et douce harmonie | *Dulce carmen* | Avant le Carême · Shrovetide | [50](../../hymns\shrovetide\50_o_sainte_et_douce_harmonie_DULCE_CARMEN) |
| O viens, ô viens, Emmanuel | *Veni veni Emmanuel* | Avent · Advent | [14](../../hymns\advent\14_o_viens_o_viens_emmanuel_VENI_VENI_EMMANUEL) |
| Orne-toi, âme, de bonheur | *Schmücke dich* | Communion · Communion | [99](../../hymns\communion\99_orne_toi_ame_de_bonheur_SCHMUCKE_DICH) |
| Ouvre tes portes, ô Sion | *Bedford* | Chandeleur · Candlemas | [49](../../hymns\candlemas\49_ouvre_tes_portes_o_sion_BEDFORD) |


## P

| Première phrase · First line | Mélodie · Tune | Sujet · Subject | Numéro · Number 
| - | - | - | - |
| Pendant ces quarante jours | *Heinlein* | Carême · Lent | [51](../../hymns\lent\51_pendant_ces_quarante_jours_HEINLEIN) |
| Petite ville, Bethléem | *Saint Louis* | Noël · Christmas | [22](../../hymns\christmas\22_petite_ville_bethleem_SAINT_LOUIS) |
| | *Forest Green* | Noël · Christmas | [23](../../hymns\christmas\23_petite_ville_bethleem_FOREST_GREEN) |
| Pour tous les saints | *Sine Nomine* | L'Eglise triomphante · The Church triumphant | [120](../../hymns\triumphant\120_pour_tous_les_saints_SINE_NOMINE) |
| Protège l'enfance, Jésus, Bon Pasteur | *Hanover* | Louange · Praise | [95](../../hymns\praise\95_protege_l'enfance_jesus_bon_pasteur_HANOVER) |
| Père éternel dont le pouvoir | *Melita* | Sur mer · At sea | [111](../../hymns\sea\111_pere_eternel_dont_le_pouvoir_MELITA) |
| Père, on te loue; la nuit se termine | *Christe sanctorum* | Matin · Morning | [1](../../hymns\morning\1_pere_on_te_loue_la_nuit_se_termine_CHRISTE_SANCTORUM) |


## Q

| Première phrase · First line | Mélodie · Tune | Sujet · Subject | Numéro · Number 
| - | - | - | - |
| Qu'au saint Nom de Jésus | *King's Weston* | Louange · Praise | [72](../../hymns\praise\72_qu'au_saint_nom_de_jesus_KINGS_WESTON) |
| Quand Gabriel du ciel est descendu | *Gabriel's Message* | Annonciation · Annunciation | [9](../../hymns\annunciation\9_quand_gabriel_du_ciel_est_descendu_GABRIELS_MESSAGE) |
| Que Dieu vous garde heureux, ô gens | *God rest you merry* | Noël · Christmas | [28](../../hymns\christmas\28_que_dieu_vous_garde_heureux_o_gens_GOD_REST_YOU_MERRY) |
| Que l'amour m'enflamme | *Lourdes Hymn* | Louange · Praise | [94](../../hymns\praise\94_que_l'amour_m'enflamme_LOURDES_HYMN) |
| Quelle douleur | *O Traurigkeit* | Passion · Passiontide | [59](../../hymns\passion\59_quelle_douleur_O_TRAURIGKEIT) |
| Quelle est cette odeur agréable | *Quelle est cette odeur* | Noël · Christmas | [33](../../hymns\christmas\33_quelle_est_cette_odeur_agreable_QUELLE_EST_CETTE_ODEUR) |
| Qui est cet Enfant endormi | *Greensleeves* | Noël · Christmas | [44](../../hymns\christmas\44_qui_est_cet_enfant_endormi_GREENSLEEVES) |


## R

| Première phrase · First line | Mélodie · Tune | Sujet · Subject | Numéro · Number 
| - | - | - | - |
| Reconnaissants, nous venons | *Saint George Windsor* | Action de grâce · Thanksgiving | [110](../../hymns\thanksgiving\110_reconnaissants_nous_venons_SAINT_GEORGE_WINDSOR) |
| Rendez à Dieu louange et gloire | *Rendez à Dieu* | Louange · Praise | [80](../../hymns\praise\80_rendez_a_dieu_louange_et_gloire_RENDEZ_A_DIEU) |
| Reste avec moi | *Eventide* | Soir · Evening | [4](../../hymns\evening\4_reste_avec_moi_EVENTIDE) |


## S

| Première phrase · First line | Mélodie · Tune | Sujet · Subject | Numéro · Number 
| - | - | - | - |
| Saint aliment des âmes | *Innsbruck* | Communion · Communion | [100](../../hymns\communion\100_saint_aliment_des_ames_INNSBRUCK) |
| Salut, le Nom du Redempteur | *Coronation* | Louange · Praise | [71](../../hymns\praise\71_salut_le_nom_du_redempteur_CORONATION) |
| Seigneur, quand vient l'obscurité | *Tallis Canon* | Soir · Evening | [5](../../hymns\evening\5_seigneur_quand_vient_l'obscurite_TALLIS_CANON) |
| Seigneur, que n'ai-je mille voix | *Azmon* | Louange · Praise | [89](../../hymns\praise\89_seigneur_que_n'ai_je_mille_voix_AZMON) |
| Sois ma vision | *Slane* | Louange · Praise | [97](../../hymns\praise\97_sois_ma_vision_SLANE) |


## T

| Première phrase · First line | Mélodie · Tune | Sujet · Subject | Numéro · Number 
| - | - | - | - |
| Tant que le monde durera | *Duke Street* | Louange · Praise | [76](../../hymns\praise\76_tant_que_le_monde_durera_DUKE_STREET) |
| Tel que je suis | *Woodworth* | Louange · Praise | [83](../../hymns\praise\83_tel_que_je_suis_WOODWORTH) |
| Torrents d'amour et de grâce | *Ebenezer* | Louange · Praise | [85](../../hymns\praise\85_torrents_d'amour_et_de_grace_EBENEZER) |
| Tout honneur, toute gloire | *Saint Theodulph* | Passion · Passiontide | [52](../../hymns\passion\52_tout_honneur_toute_gloire_SAINT_THEODULPH) |


## U

| Première phrase · First line | Mélodie · Tune | Sujet · Subject | Numéro · Number 
| - | - | - | - |
| Un flambeau, Jeannette, Isabelle | *Un flambeau* | Noël · Christmas | [36](../../hymns\christmas\36_un_flambeau_jeannette_isabelle_UN_FLAMBEAU) |
| Une voix retentissante | *Merton* | Avent · Advent | [17](../../hymns\advent\17_une_voix_retentissante_MERTON) |


## V

| Première phrase · First line | Mélodie · Tune | Sujet · Subject | Numéro · Number 
| - | - | - | - |
| Venez, divin Messie | *Venez, divin Messie* | Avent · Advent | [11](../../hymns\advent\11_venez_divin_messie_VENEZ_DIVIN_MESSIE) |
| Venez, fidèles, chantez | *Gaudeamus Pariter* | Pâques · Easter | [65](../../hymns\easter\65_venez_fideles_chantez_GAUDEAMUS_PARITER) |
| Vers toi, divin Père | *Hanover* | Communion · Communion | [98](../../hymns\communion\98_vers_toi_divin_pere_HANOVER) |
| Viens silencieux, chair mortelle | *Picardy* | Communion · Communion | [106](../../hymns\communion\106_viens_silencieux_chair_mortelle_PICARDY) |
| Viens, ô Jésus très attendu | *Stuttgart* | Avent · Advent | [18](../../hymns\advent\18_viens_o_jesus_tres_attendu_STUTTGART) |
| Viens, ô Roi tout-puissant | *Italian Hymn* | Trinité · Trinity | [69](../../hymns\trinity\69_viens_o_roi_tout_puissant_ITALIAN_HYMN) |
| Voici, il descend parmi les nuages | *Helmsley* | Avent · Advent | [13](../../hymns\advent\13_voici_il_descend_parmi_les_nuages_HELMSLEY) |
| Vois là-bas, mettre le Seigneur en croix | *Were you there* | Passion · Passiontide | [57](../../hymns\passion\57_vois_la_bas_mettre_le_seigneur_en_croix_WERE_YOU_THERE) |
| Vous qui sur la terre habitez | *Old Hundredth* | Louange · Praise | [79](../../hymns\praise\79_vous_qui_sur_la_terre_habitez_OLD_HUNDREDTH) |
| Vous, créatures du Seigneur | *Lasst uns erfreuen* | Louange · Praise | [81](../../hymns\praise\81_vous_creatures_du_seigneur_LASST_UNS_ERFREUEN) |
