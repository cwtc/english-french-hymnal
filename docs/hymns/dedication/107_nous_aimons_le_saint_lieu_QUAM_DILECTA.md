---
tags:
  - Dedication d'une église · Dedication of a church
---

# Nous aimons le saint lieu



=== ":material-playlist-music: Partition · Score"

	<div id="adobe-dc-view" style="width: 100%;"></div>
	<script src="https://documentcloud.adobe.com/view-sdk/main.js"></script>
	<script type="text/javascript">
    	document.addEventListener("adobe_dc_view_sdk.ready", function(){
        	var adobeDCView = new AdobeDC.View({clientId: "d7518740bf7a4af7b4c3a8b5a7bcff85", divId: "adobe-dc-view"});
        	adobeDCView.previewFile({
            	content:{location: {url: "/livre-de-cantiques-anglicans/assets/pdf/nous_aimons_le_saint_lieu_QUAM_DILECTA.pdf"}},
            	metaData:{fileName: "nous_aimons_le_saint_lieu_QUAM_DILECTA.pdf"}}, {embedMode: "IN_LINE"});
    	});
	</script>

=== ":octicons-list-ordered-16: Paroles seulement · Lyrics only"

	<table style="font-size:16px">
	<tr>
	<td> 1 </td>
	<td> Nous aimons le saint lieu<br>tout plein de ta mémoire;<br>nous y chantons ta gloire,<br>pleins de joie, ô grand Dieu. </td>
	</tr>
	<tr>
	<td> 2 </td>
	<td> Ici tes serviteurs,<br>unis dans la prière,<br>devant toi, tendre Père,<br>viennent ouvrir leur cœur. </td>
	</tr>
	<tr>
	<td> 3 </td>
	<td> Voici les fonts sacrés<br>où l'Esprit Saint lui-même,<br>à Dieu, dans le baptême,<br>nous a tous consacrés. </td>
	</tr>
	<tr>
	<td> 4 </td>
	<td> Nous aimons ton autel,<br>où dans le saint mystère,<br>aux enfants de la terre<br>s'offre le pain du ciel. </td>
	</tr>
	<tr>
	<td> 5 </td>
	<td> Quel bonheur quand nos voix<br>sous ces voûtes bénies<br>t'offrent leurs symphonies,<br>ô Père, ô Roi des rois! </td>
	</tr>
	<tr>
	<td> 6 </td>
	<td> Accorde-nous, Seigneur,<br>du saint amour les flammes,<br>avant-goût pour nos âmes,<br>du céleste bonheur. </td>
	</tr>
	<tr>
	<td> 7 </td>
	<td>  </td>
	</tr>
	</table>

## Information
| | |
| - | - |
| Titre original · Original title | We love the place, O God |
| Texte · Text  | William Bullock (1854);<br/>tr. J. Giraud, *Hymnes à l'usage de l'église pour le service divin*, *Société pour l'avancement des connaissances chrétiennes* (1892) |
| Musique · Music | *Quam Dilecta* <br/> Henry Lascelles Jenner (1861) |