---
tags:
  - Communion · Communion
---

# Au banquet du Roi on chante



=== ":material-playlist-music: Partition · Score"

	<div id="adobe-dc-view" style="width: 100%;"></div>
	<script src="https://documentcloud.adobe.com/view-sdk/main.js"></script>
	<script type="text/javascript">
    	document.addEventListener("adobe_dc_view_sdk.ready", function(){
        	var adobeDCView = new AdobeDC.View({clientId: "d7518740bf7a4af7b4c3a8b5a7bcff85", divId: "adobe-dc-view"});
        	adobeDCView.previewFile({
            	content:{location: {url: "/livre-de-cantiques-anglicans/assets/pdf/au_banquet_du_roi_on_chante_SALZBURG.pdf"}},
            	metaData:{fileName: "au_banquet_du_roi_on_chante_SALZBURG.pdf"}}, {embedMode: "IN_LINE"});
    	});
	</script>

=== ":octicons-list-ordered-16: Paroles seulement · Lyrics only"

	<table style="font-size:16px">
	<tr>
	<td> 1 </td>
	<td> Au banquet du Roi, on chante<br>en victoire nos louanges!<br>L'Agneau, il nous a lavés<br>avec l'eau de son côté.<br>Louange à l'amour divin<br>qui donne son sang en vin,<br>donnant son corps pour la fête,<br>Christ victime, Christ le prêtre. </td>
	</tr>
	<tr>
	<td> 2 </td>
	<td> Où le sang pascal jaillit,<br>la mort, désarmée, fuit.<br>La mer s'ouvre; Israël passe;<br>l'onde l'ennemi écrase.<br>Nous louons ce sang versé,<br>cette chair en pain donnée;<br>avec notre adoration<br>cette Manne nous mangeons. </td>
	</tr>
	<tr>
	<td> 3 </td>
	<td> Victime, tu as conquis<br>en nous restaurant la vie!<br>Tu as gagné dans la guerre,<br>brisant les chaînes d'enfer:<br>Donc, la mort ne nous tient plus;<br>la tombe n'a pas vaincu!<br>Et on ressuscitera<br>en toi, notre Juge et Roi. </td>
	</tr>
	<tr>
	<td> 4 </td>
	<td> La joie pascale, hélas!<br>Seul le péché la menace.<br>De son pouvoir, toi, Seigneur,<br>tu nous libères le cœur.<br>Jésus vivant, nous t'offrons<br>nos cantiques de louange,<br>au Père et au Saint-Esprit;<br>à jamais qu'il soit ainsi. </td>
	</tr>
	</table>

## Information
| | |
| - | - |
| Titre original · Original title | Ad regias Agni dapes |
| Texte · Text  | Latin, IVe siècle;<br/>tr. en anglais Robert Campbell (1849);<br/>tr. en français Arlie Coles (2021) |
| Musique · Music | *Salzburg* <br/> Jakob Hintze (1678) |