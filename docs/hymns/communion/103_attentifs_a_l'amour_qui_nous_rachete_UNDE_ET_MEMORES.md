---
tags:
  - Communion · Communion
---

# Attentifs à l'amour qui nous rachète



=== ":material-playlist-music: Partition · Score"

	<div id="adobe-dc-view" style="width: 100%;"></div>
	<script src="https://documentcloud.adobe.com/view-sdk/main.js"></script>
	<script type="text/javascript">
    	document.addEventListener("adobe_dc_view_sdk.ready", function(){
        	var adobeDCView = new AdobeDC.View({clientId: "d7518740bf7a4af7b4c3a8b5a7bcff85", divId: "adobe-dc-view"});
        	adobeDCView.previewFile({
            	content:{location: {url: "/livre-de-cantiques-anglicans/assets/pdf/attentifs_a_l'amour_qui_nous_rachete_UNDE_ET_MEMORES.pdf"}},
            	metaData:{fileName: "attentifs_a_l'amour_qui_nous_rachete_UNDE_ET_MEMORES.pdf"}}, {embedMode: "IN_LINE"});
    	});
	</script>

=== ":octicons-list-ordered-16: Paroles seulement · Lyrics only"

	<table style="font-size:16px">
	<tr>
	<td> 1 </td>
	<td> Attentifs à l'amour qui nous rachète,<br>accompli une fois sur le Calvaire,<br>et ayant ici lui qui pour nous plaide,<br>nous nous cachons dans ton saint Fils, ô Père:<br>Il s'offra en sacrifice complet,<br>immortel, pur, suffisant, et parfait. </td>
	</tr>
	<tr>
	<td> 2 </td>
	<td> Regarde, ô Père, son parfait visage;<br>ne nous regarde que comme ton Fils;<br>Ignore nos ignorances et nos rages,<br>nos échecs et notre foi affaiblie;<br>Car entre nos péchés et leur salaire,<br>on met la Passion de notre Seigneur. </td>
	</tr>
	<tr>
	<td> 3 </td>
	<td> Ensuite, pour tous ceux que l'on chérit,<br>nous lançons appel par cette Présence;<br>Ramène-les plus proche à ton sein béni!<br>Ô garde leurs âmes de la souffrance!<br>Qu'ils soient lavés, blancs comme la neige,<br>qu'ils soient protégés de chaque piège. </td>
	</tr>
	<tr>
	<td> 4 </td>
	<td> Donc nous venons se mettre à genoux,<br>ô Sauveur patient, qui nous aime encore!<br>Et par cet Aliment terrible et doux,<br>delivre-nous de tout type de mort!<br>Dans ton service on est tous libéré:<br>fais que l'on reste avec toi à jamais. </td>
	</tr>
	</table>

## Information
| | |
| - | - |
| Titre original · Original title | And now, o Father, mindful of the love |
| Texte · Text  | William Bright (1874);<br/>tr. Arlie Coles (2021) |
| Musique · Music | *Unde et memores* <br/> William Henry Monk (1875) |