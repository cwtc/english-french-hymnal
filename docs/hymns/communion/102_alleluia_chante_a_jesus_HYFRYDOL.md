---
tags:
  - Communion · Communion
---

# Alléluia, chante à Jésus



=== ":material-playlist-music: Partition · Score"

	<div id="adobe-dc-view" style="width: 100%;"></div>
	<script src="https://documentcloud.adobe.com/view-sdk/main.js"></script>
	<script type="text/javascript">
    	document.addEventListener("adobe_dc_view_sdk.ready", function(){
        	var adobeDCView = new AdobeDC.View({clientId: "d7518740bf7a4af7b4c3a8b5a7bcff85", divId: "adobe-dc-view"});
        	adobeDCView.previewFile({
            	content:{location: {url: "/livre-de-cantiques-anglicans/assets/pdf/alleluia_chante_a_jesus_HYFRYDOL.pdf"}},
            	metaData:{fileName: "alleluia_chante_a_jesus_HYFRYDOL.pdf"}}, {embedMode: "IN_LINE"});
    	});
	</script>

=== ":octicons-list-ordered-16: Paroles seulement · Lyrics only"

	<table style="font-size:16px">
	<tr>
	<td> 1 </td>
	<td> Alléluia! Chante à Jésus,<br>dont le sceptre et le trône!<br>Alléluia! À lui seul soient<br>la victoire et la triomphe!<br>Ecoutons les chants de Sion<br>tonner comme une inondation:<br>Le Seigneur de chaque nation<br>nous a rachetés par son sang. </td>
	</tr>
	<tr>
	<td> 2 </td>
	<td> Alléluia!<br>Nous ne sommes pas<br>comme des orphélins laissés.<br>Alléluia! C'est par la foi<br>qu'on perçoit qu'il est tout près.<br>Après quarante jours de Pâques,<br>aux cieux il fait son retour,<br>on tient fort à sa promesse:<br>Je suis avec vous tous les jours. </td>
	</tr>
	<tr>
	<td> 3 </td>
	<td> Alléluia! Pain des anges,<br>toi, notre Aliment sur Terre.<br>Alléluia! Ici-bas, on<br>fuit vers toi, nous transgresseurs.<br>Intercesseur, proche aux pécheurs,<br>ô Rédempteur, plaide pour moi,<br>là où la mer cristaline<br>résonne de nombreux chants. </td>
	</tr>
	<tr>
	<td> 4 </td>
	<td> Alléluia! Roi éternel,<br>assis sur ton trône céleste;<br>Alléluia! Né parmi nous,<br>ici, ton marchepied terrestre.<br>De la chair le Roi s'est paré,<br>lui, le Prêtre et la Victime;<br>le Grand Prêtre s'offre lui-mê me,<br>présent dans l'Eucharistie. </td>
	</tr>
	</table>

## Information
| | |
| - | - |
| Titre original · Original title | Alleluia, sing to Jesus |
| Texte · Text  | W. Chatterton Dix (1866);<br/>tr. Arlie Coles (2021) |
| Musique · Music | *Hyfrydol* <br/> Rowland H. Prichard (1811-1887) |