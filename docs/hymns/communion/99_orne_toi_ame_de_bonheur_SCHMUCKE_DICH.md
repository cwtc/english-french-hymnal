---
tags:
  - Communion · Communion
---

# Orne-toi, âme, de bonheur



=== ":material-playlist-music: Partition · Score"

	<div id="adobe-dc-view" style="width: 100%;"></div>
	<script src="https://documentcloud.adobe.com/view-sdk/main.js"></script>
	<script type="text/javascript">
    	document.addEventListener("adobe_dc_view_sdk.ready", function(){
        	var adobeDCView = new AdobeDC.View({clientId: "d7518740bf7a4af7b4c3a8b5a7bcff85", divId: "adobe-dc-view"});
        	adobeDCView.previewFile({
            	content:{location: {url: "/livre-de-cantiques-anglicans/assets/pdf/orne_toi_ame_de_bonheur_SCHMUCKE_DICH.pdf"}},
            	metaData:{fileName: "orne_toi_ame_de_bonheur_SCHMUCKE_DICH.pdf"}}, {embedMode: "IN_LINE"});
    	});
	</script>

=== ":octicons-list-ordered-16: Paroles seulement · Lyrics only"

	<table style="font-size:16px">
	<tr>
	<td> 1 </td>
	<td> Ornetoi, âme, de bonheur;<br>quitte enfin ces endroits sombres;<br>Entre dans le jour radieux,<br>pour le magnifier, joyeux:<br>Magnifiele dont la bonté<br>a ce fameux banquet fondé!<br>Des cieux il a l'autorité,<br>mais il daigne avec toi gîter. </td>
	</tr>
	<tr>
	<td> 2 </td>
	<td> À genoux en révérence,<br>rempli d'une joie profonde,<br>Je réfléchis à ta grandeur;<br>au fond de mon cœur je tremble.<br>Quelles divines mystères<br>dans ce repas qu'on révère!<br>Ton invitation à percer<br>ces secrets vers nous est versée. </td>
	</tr>
	<tr>
	<td> 3 </td>
	<td> Lumière éclairant ma vie,<br>Soleil qui mon âme avive,<br>Joie douce sans quoi j'écroule,<br>Source dont mon être coule:<br>Mon Créateur, je supplie,<br>ô, que tu me fasses digne<br>De manger ce pain très sacré<br>que tu donnes de ta bonté. </td>
	</tr>
	<tr>
	<td> 4 </td>
	<td> Jésus, Pain de Vie, fais que,<br>avec joie, je t'obéisse;<br>Têtu, je nie ton amour;<br>fais que je t'en donne en retour!<br>Je discerne de ce banquet<br>son trésor profond démontré;<br>Par ces dons que tu m'y offres,<br>je suis ton invité aux cieux. </td>
	</tr>
	</table>

## Information
| | |
| - | - |
| Titre original · Original title | Schmücke dich, o liebe Seele |
| Texte · Text  | Johann Franck (1649);<br/>tr. en anglais Catherine Winkworth (1863);<br/>tr. en français Arlie Coles (2021) |
| Musique · Music | *Schmücke dich* <br/> Johann Crüger (1598-1662) |