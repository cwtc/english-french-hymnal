---
tags:
  - Communion · Communion
---

# Dans l'amour, il rompit ce Pain



=== ":material-playlist-music: Partition · Score"

	<div id="adobe-dc-view" style="width: 100%;"></div>
	<script src="https://documentcloud.adobe.com/view-sdk/main.js"></script>
	<script type="text/javascript">
    	document.addEventListener("adobe_dc_view_sdk.ready", function(){
        	var adobeDCView = new AdobeDC.View({clientId: "d7518740bf7a4af7b4c3a8b5a7bcff85", divId: "adobe-dc-view"});
        	adobeDCView.previewFile({
            	content:{location: {url: "/livre-de-cantiques-anglicans/assets/pdf/dans_l'amour_il_rompit_ce_pain_RENDEZ_A_DIEU.pdf"}},
            	metaData:{fileName: "dans_l'amour_il_rompit_ce_pain_RENDEZ_A_DIEU.pdf"}}, {embedMode: "IN_LINE"});
    	});
	</script>

=== ":octicons-list-ordered-16: Paroles seulement · Lyrics only"

	<table style="font-size:16px">
	<tr>
	<td> 1 </td>
	<td> Dans l'amour, il rompit ce Pain,  <br>donnant les paroles de vie.<br>Dans l'amour, il versa ce Vin,  <br>et nos péchés meurent avec lui.<br>Regarde les larmes versées;<br>regarde nos fragiles cœurs;<br>que ce repas soit à jamais  <br>la provision pour nous pécheurs. </td>
	</tr>
	</table>

## Information
| | |
| - | - |
| Titre original · Original title | Bread of the world, in mercy broken |
| Texte · Text  | Reginald Heber (1827);<br/>tr. Arlie Coles (2021) |
| Musique · Music | *Rendez à Dieu* <br/> Louis Bourgeois et/ou Guillaume Franc (1543) |