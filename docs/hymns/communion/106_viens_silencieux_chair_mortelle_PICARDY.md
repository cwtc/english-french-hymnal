---
tags:
  - Communion · Communion
---

# Viens silencieux, chair mortelle



=== ":material-playlist-music: Partition · Score"

	<div id="adobe-dc-view" style="width: 100%;"></div>
	<script src="https://documentcloud.adobe.com/view-sdk/main.js"></script>
	<script type="text/javascript">
    	document.addEventListener("adobe_dc_view_sdk.ready", function(){
        	var adobeDCView = new AdobeDC.View({clientId: "d7518740bf7a4af7b4c3a8b5a7bcff85", divId: "adobe-dc-view"});
        	adobeDCView.previewFile({
            	content:{location: {url: "/livre-de-cantiques-anglicans/assets/pdf/viens_silencieux_chair_mortelle_PICARDY.pdf"}},
            	metaData:{fileName: "viens_silencieux_chair_mortelle_PICARDY.pdf"}}, {embedMode: "IN_LINE"});
    	});
	</script>

=== ":octicons-list-ordered-16: Paroles seulement · Lyrics only"

	<table style="font-size:16px">
	<tr>
	<td> 1 </td>
	<td> Viens silencieux, chair mortelle,<br>avec crainte et tremblement,<br>car les choses temporelles<br>qu'un pâle figure font:<br>il descend, le vrai Messie,<br>sa bénédiction offrant. </td>
	</tr>
	<tr>
	<td> 2 </td>
	<td> Roi des rois, né de Marie,<br>Seigneur des seigneurs puissants,<br>vêtu dans la chair humaine:<br>Par son Corps et par son Sang,<br>il se donne à ses fidèles<br>comme céleste Aliment. </td>
	</tr>
	<tr>
	<td> 3 </td>
	<td> L'entière armée céleste<br>prépare donc son chemin:<br>La Lumière vers nous descend,<br>et son royaume divin,<br>pour que l'enfer disparaisse,<br>tout péché et tout chagrin. </td>
	</tr>
	<tr>
	<td> 4 </td>
	<td> Le séraphin à six ailes,<br>le chérubin remplis d'yeux,<br>ils se cachent leurs visages<br>en criant devant leur Dieu:<br>Alléluia, alléluia,<br>chantons au plus haut des cieux! </td>
	</tr>
	</table>

## Information
| | |
| - | - |
| Titre original · Original title |  Siyisáto pᾶsa sárx vrotía; Let all mortal flesh keep silence |
| Texte · Text  | La Liturgie de Saint Jacques frère-de-Dieu (c. 275)<br/>ad. en anglais Gerard Moultrie (1864)<br/>tr. en français Arlie Coles (2021) |
| Musique · Music | *Picardy* <br/> Air français, XVIIe siècle |