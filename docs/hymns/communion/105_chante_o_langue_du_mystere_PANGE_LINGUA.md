---
tags:
  - Communion · Communion
---

# Chante, ô langue, du mystère



=== ":material-playlist-music: Partition · Score"

	<div id="adobe-dc-view" style="width: 100%;"></div>
	<script src="https://documentcloud.adobe.com/view-sdk/main.js"></script>
	<script type="text/javascript">
    	document.addEventListener("adobe_dc_view_sdk.ready", function(){
        	var adobeDCView = new AdobeDC.View({clientId: "d7518740bf7a4af7b4c3a8b5a7bcff85", divId: "adobe-dc-view"});
        	adobeDCView.previewFile({
            	content:{location: {url: "/livre-de-cantiques-anglicans/assets/pdf/chante_o_langue_du_mystere_PANGE_LINGUA.pdf"}},
            	metaData:{fileName: "chante_o_langue_du_mystere_PANGE_LINGUA.pdf"}}, {embedMode: "IN_LINE"});
    	});
	</script>

=== ":octicons-list-ordered-16: Paroles seulement · Lyrics only"

	<table style="font-size:16px">
	<tr>
	<td> 1 </td>
	<td> Chante, ô langue, du mystère<br>de  ce corps très glorieux <br>que donna le Roi des Nations,<br>et de son sang précieux; <br>le fruit d'un ventre noble<br>paya notre  prix affreux. </td>
	</tr>
	<tr>
	<td> 2 </td>
	<td> D'une Vierge jeune et pure<br>ce  Roi naquit ici-bas, <br>et pour faire sa culture,<br>parmi nous il demeura; <br>il choisit sa sépulture<br>que, luimême, il  ordonna. </td>
	</tr>
	<tr>
	<td> 3 </td>
	<td> Avec ses douze disciples<br>à  la dernière Cène,<br>il mangea l'agneau de Pâques,<br>et, comblant la Loi lui-même,<br>comme aux foules, là il donna<br>son corps de sa  propre main. </td>
	</tr>
	<tr>
	<td> 4 </td>
	<td> Sa vraie chair par son ver be<br>donne<br>le Verbe en chair en pain; <br>et de la même manière,<br>il donne son sang en vin. <br>Par la foi on les discèrne,<br>si nos sens travaillent en vain. </td>
	</tr>
	<tr>
	<td> 5 </td>
	<td> Prosternons-nous donc humblement;<br>tombons devant cette Hostie;<br>et que l'ancienne Alliance<br>cède enfin aux nouveaux rites:<br>Ce que nos faibles sens manquent<br>par la foi est accompli. </td>
	</tr>
	<tr>
	<td> 6 </td>
	<td> Au Géniteur, au Génité<br>soit notre jubilation,<br>notre honneur et nos louanges,<br>et toute bénédiction;<br>Au Saint-Esprit, y procédant,<br>soit égale adoration. </td>
	</tr>
	</table>

## Information
| | |
| - | - |
| Titre original · Original title | Pange lingua gloriosi |
| Texte · Text  | Thomas Aquinas (1225-1274);<br/>tr. Arlie Coles (2021) |
| Musique · Music | *Pange lingua* <br/> Chant grégorien, IIIème mode |