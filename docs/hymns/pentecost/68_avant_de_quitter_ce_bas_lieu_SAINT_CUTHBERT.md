---
tags:
  - Pentecôte · Pentecost
---

# Avant de quitter ce bas lieu



=== ":material-playlist-music: Partition · Score"

	<div id="adobe-dc-view" style="width: 100%;"></div>
	<script src="https://documentcloud.adobe.com/view-sdk/main.js"></script>
	<script type="text/javascript">
    	document.addEventListener("adobe_dc_view_sdk.ready", function(){
        	var adobeDCView = new AdobeDC.View({clientId: "d7518740bf7a4af7b4c3a8b5a7bcff85", divId: "adobe-dc-view"});
        	adobeDCView.previewFile({
            	content:{location: {url: "/livre-de-cantiques-anglicans/assets/pdf/avant_de_quitter_ce_bas_lieu_SAINT_CUTHBERT.pdf"}},
            	metaData:{fileName: "avant_de_quitter_ce_bas_lieu_SAINT_CUTHBERT.pdf"}}, {embedMode: "IN_LINE"});
    	});
	</script>

=== ":octicons-list-ordered-16: Paroles seulement · Lyrics only"

	<table style="font-size:16px">
	<tr>
	<td> 1 </td>
	<td> Avant de quitter ce bas lieu,<br>le bien-aimé Sauveur<br>nous annonça l'Esprit de Dieu<br>Consolateur. </td>
	</tr>
	<tr>
	<td> 2 </td>
	<td> Il vient établir son séjour<br>comme ami ferme et sûr,<br>apportant la paix et l'amour<br>dans un cœur pur. </td>
	</tr>
	<tr>
	<td> 3 </td>
	<td> Dans le secret sa douce voix,<br>comme un souffle du soir<br>calme le pécheur aux abois,<br>parlant d'espoir. </td>
	</tr>
	<tr>
	<td> 4 </td>
	<td> Toute vertu, tout saint désir,<br>qui dans notre âme à lui;<br>douceur, force, innocent plaisir,<br>tout vient de lui. </td>
	</tr>
	</table>

## Information
| | |
| - | - |
| Titre original · Original title | Our blest Redeemer, ere he breathed |
| Texte · Text  | Harriet Auber (1829);<br/>tr. J. Giraud, *Hymnes à l'usage de l'église pour le service divin*, *Société pour l'avancement des connaissances chrétiennes* (1892) |
| Musique · Music | *Saint Cuthbert* <br/> John Bacchus Dykes (1861) |