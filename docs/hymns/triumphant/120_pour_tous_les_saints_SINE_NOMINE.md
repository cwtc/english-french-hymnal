---
tags:
  - L'Eglise triomphante · The Church triumphant
---

# Pour tous les saints



=== ":material-playlist-music: Partition · Score"

	<div id="adobe-dc-view" style="width: 100%;"></div>
	<script src="https://documentcloud.adobe.com/view-sdk/main.js"></script>
	<script type="text/javascript">
    	document.addEventListener("adobe_dc_view_sdk.ready", function(){
        	var adobeDCView = new AdobeDC.View({clientId: "d7518740bf7a4af7b4c3a8b5a7bcff85", divId: "adobe-dc-view"});
        	adobeDCView.previewFile({
            	content:{location: {url: "/livre-de-cantiques-anglicans/assets/pdf/pour_tous_les_saints_SINE_NOMINE.pdf"}},
            	metaData:{fileName: "pour_tous_les_saints_SINE_NOMINE.pdf"}}, {embedMode: "IN_LINE"});
    	});
	</script>

=== ":octicons-list-ordered-16: Paroles seulement · Lyrics only"

	<table style="font-size:16px">
	<tr>
	<td> 1 </td>
	<td> Pour tous les Saints, en repos des labeurs,<br>qui ont confessé par foi le Seigneur:<br>bénissons le Nom de Jésus le Sauveur. </td>
	</tr>
	<tr>
	<td> 2 </td>
	<td> C'est toi qui fus leur rempart icibas;<br>leur Capitaine dans leurs durs combats.<br> Toi, aux ténèbres, tu les éclairas.<br>Alléluia, al léluia! </td>
	</tr>
	<tr>
	<td> 3 </td>
	<td> Que tes soldats fidèles et audacieux<br>combattent comme tes Saints précieux,<br>qu'ils gagnent leur couronne, victorieux. </td>
	</tr>
	<tr>
	<td> 4 </td>
	<td> Ô communion, sainte fraternité!<br>Nous luttons, faibles; ils sont glorifiés: <br>Ensemble on est en toi unifié. </td>
	</tr>
	<tr>
	<td> 5 </td>
	<td> Le combat est feroce et épuisant,<br>mais on entend de loin enfin le chant<br>de sa victoire, nous refortifiant. </td>
	</tr>
	<tr>
	<td> 6 </td>
	<td> Le crepuscule doré vient de l'Ouest;<br>aux guerriers vient leur repos de richesse:<br>Calme et doux, le Paradis céleste. </td>
	</tr>
	<tr>
	<td> 7 </td>
	<td> Mais un jour plus glorieux encore s'en vient:<br>La résurrection des triomphants Saints;<br>le Roi de Gloire se fait son chemin. </td>
	</tr>
	<tr>
	<td> 8 </td>
	<td> Et venant des horizons infinis,<br>aux portes de perle, nombreux, ils arrivent,<br>chantant au Père, au Fils, au Saint-Esprit. </td>
	</tr>
	</table>

## Information
| | |
| - | - |
| Titre original · Original title | For all the saints |
| Texte · Text  | William W. How (1823-1897);<br/>tr. Arlie Coles (2021) |
| Musique · Music | *Sine Nomine* <br/> Ralph Vaughan Williams (1872-1958) |