---
tags:
  - L'Eglise triomphante · The Church triumphant
---

# La foi de nos aieux vivante



=== ":material-playlist-music: Partition · Score"

	<div id="adobe-dc-view" style="width: 100%;"></div>
	<script src="https://documentcloud.adobe.com/view-sdk/main.js"></script>
	<script type="text/javascript">
    	document.addEventListener("adobe_dc_view_sdk.ready", function(){
        	var adobeDCView = new AdobeDC.View({clientId: "d7518740bf7a4af7b4c3a8b5a7bcff85", divId: "adobe-dc-view"});
        	adobeDCView.previewFile({
            	content:{location: {url: "/livre-de-cantiques-anglicans/assets/pdf/la_foi_de_nos_aieux_vivante_SAINTE_CATHERINE.pdf"}},
            	metaData:{fileName: "la_foi_de_nos_aieux_vivante_SAINTE_CATHERINE.pdf"}}, {embedMode: "IN_LINE"});
    	});
	</script>

=== ":octicons-list-ordered-16: Paroles seulement · Lyrics only"

	<table style="font-size:16px">
	<tr>
	<td> 1 </td>
	<td> La foi de nos aïeux vivante<br>malgré le feu et le cachot:<br>Nos cœurs battons joyeusement<br>quand on entend ce glorieux mot:<br>La foi de nos aïeux vivante!<br>Jusqu'à la mort nous la suivons. </td>
	</tr>
	<tr>
	<td> 2 </td>
	<td> Nos aïeux, malgré leurs fers forts,<br>étaient libres dans leurs cœurs;<br>bénis seront donc leurs enfants<br>s'ils, comme leurs parents, pour toi meurent: </td>
	</tr>
	<tr>
	<td> 3 </td>
	<td> La foi de nos aïeux s'étend<br>aux ennemis comme aux amis;<br>malgré nos soufrances présentes,<br>prêchons dans les fruits de l'Esprit. </td>
	</tr>
	</table>

## Information
| | |
| - | - |
| Titre original · Original title | Faith of our fathers |
| Texte · Text  | Frederick William Faber (1849)<br/>tr. Arlie Coles (2021) |
| Musique · Music | *Sainte Catherine* <br/> Henri F. Hemy (1864) |