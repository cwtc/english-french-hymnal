---
tags:
  - L'Eglise triomphante · The Church triumphant
---

# Les dons éternels du grand Roi



=== ":material-playlist-music: Partition · Score"

	<div id="adobe-dc-view" style="width: 100%;"></div>
	<script src="https://documentcloud.adobe.com/view-sdk/main.js"></script>
	<script type="text/javascript">
    	document.addEventListener("adobe_dc_view_sdk.ready", function(){
        	var adobeDCView = new AdobeDC.View({clientId: "d7518740bf7a4af7b4c3a8b5a7bcff85", divId: "adobe-dc-view"});
        	adobeDCView.previewFile({
            	content:{location: {url: "/livre-de-cantiques-anglicans/assets/pdf/les_dons_eternels_du_grand_roi_AETERNA_CHRISTI_MUNERA.pdf"}},
            	metaData:{fileName: "les_dons_eternels_du_grand_roi_AETERNA_CHRISTI_MUNERA.pdf"}}, {embedMode: "IN_LINE"});
    	});
	</script>

=== ":octicons-list-ordered-16: Paroles seulement · Lyrics only"

	<table style="font-size:16px">
	<tr>
	<td> 1 </td>
	<td> Les dons éternels du grand Roi<br>et des saints apôtres la gloire,<br>en chants d'amour et de victoire<br>célébrons du cœur avec foi. </td>
	</tr>
	<tr>
	<td> 2 </td>
	<td> De l'Église ils sont les premiers,<br>triomphants dans la sainte guerre;<br>ils portent partout la lumière,<br>céleste troupe de guerriers. </td>
	</tr>
	<tr>
	<td> 3 </td>
	<td> Ils ont la ferme foi des saints<br>et l'inébranlable espérance;<br>du Christ ils ont l'amour immense<br>qui de Satan rompt les desseins. </td>
	</tr>
	<tr>
	<td> 4 </td>
	<td> Du Père en eux l'honneur brilla,<br>du Fils, la volonté soumise;<br>par eux se réjouit l'Église;<br>en eux l'Esprit Saint triompha. </td>
	</tr>
	</table>

## Information
| | |
| - | - |
| Titre original · Original title | Aeterna Christi munera |
| Texte · Text  | Saint Ambroise (340-397);<br/>tr. en anglais J. M. Neale (1854);<br/>tr. en français J. Giraud, *Hymnes à l'usage de l'église pour le service divin*, *Société pour l'avancement des connaissances chrétiennes* (1892) |
| Musique · Music | *Aeterna Christi Munera* <br/> mélodie d'église (Rouen), XIIIe siècle |