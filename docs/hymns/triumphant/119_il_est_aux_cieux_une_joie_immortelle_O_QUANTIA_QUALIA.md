---
tags:
  - L'Eglise triomphante · The Church triumphant
---

# Il est aux cieux une joie immortelle



=== ":material-playlist-music: Partition · Score"

	<div id="adobe-dc-view" style="width: 100%;"></div>
	<script src="https://documentcloud.adobe.com/view-sdk/main.js"></script>
	<script type="text/javascript">
    	document.addEventListener("adobe_dc_view_sdk.ready", function(){
        	var adobeDCView = new AdobeDC.View({clientId: "d7518740bf7a4af7b4c3a8b5a7bcff85", divId: "adobe-dc-view"});
        	adobeDCView.previewFile({
            	content:{location: {url: "/livre-de-cantiques-anglicans/assets/pdf/il_est_aux_cieux_une_joie_immortelle_O_QUANTIA_QUALIA.pdf"}},
            	metaData:{fileName: "il_est_aux_cieux_une_joie_immortelle_O_QUANTIA_QUALIA.pdf"}}, {embedMode: "IN_LINE"});
    	});
	</script>

=== ":octicons-list-ordered-16: Paroles seulement · Lyrics only"

	<table style="font-size:16px">
	<tr>
	<td> 1 </td>
	<td> Il est aux cieux une joie immortelle<br>qui chaque jour réjouit les fidèles<br>car le Seigneur vit dans toutes les âmes;<br>les cœurs sont tous animés de sa flamme. </td>
	</tr>
	<tr>
	<td> 2 </td>
	<td> Jérusalem, c'est toi, ville céleste,<br>où la paix règne et non les mal funesté,<br>où toutes grâces seront accordées,<br>toutes prières seront exaucées. </td>
	</tr>
	<tr>
	<td> 3 </td>
	<td> Là, plus de deuils, plus de peines cruelles,<br>mais une joie indicible, éternelle:<br>Les bienheureux chanteront des cantiques<br>à l'Éternel, souverain magnifique. </td>
	</tr>
	<tr>
	<td> 4 </td>
	<td> C'est là que règne, entouré de lumière,<br>le tout-puissant Créateur, notre Père;<br>Dieu trois fois saint qui nous a donné l'être,<br>en lui, par lui, nous voulons tous renaître. </td>
	</tr>
	</table>

## Information
| | |
| - | - |
| Titre original · Original title | O quanta qualia sunt illa Sabbata |
| Texte · Text  | Pierre Abelard (1079-1142);<br/>tr. en anglais J. M. Neale (1854);<br/>Flossette du Pasquier (née 1922) |
| Musique · Music | *O Quantia Qualia* <br/> *Antiphonairium Parisiense* (1681);<br/>harm. John Bacchus Dykes (1823-1876) |