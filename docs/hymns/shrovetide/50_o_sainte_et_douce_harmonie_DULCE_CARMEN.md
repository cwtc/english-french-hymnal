---
tags:
  - Avant le Carême · Shrovetide
---

# O sainte et douce harmonie



=== ":material-playlist-music: Partition · Score"

	<div id="adobe-dc-view" style="width: 100%;"></div>
	<script src="https://documentcloud.adobe.com/view-sdk/main.js"></script>
	<script type="text/javascript">
    	document.addEventListener("adobe_dc_view_sdk.ready", function(){
        	var adobeDCView = new AdobeDC.View({clientId: "d7518740bf7a4af7b4c3a8b5a7bcff85", divId: "adobe-dc-view"});
        	adobeDCView.previewFile({
            	content:{location: {url: "/livre-de-cantiques-anglicans/assets/pdf/o_sainte_et_douce_harmonie_DULCE_CARMEN.pdf"}},
            	metaData:{fileName: "o_sainte_et_douce_harmonie_DULCE_CARMEN.pdf"}}, {embedMode: "IN_LINE"});
    	});
	</script>

=== ":octicons-list-ordered-16: Paroles seulement · Lyrics only"

	<table style="font-size:16px">
	<tr>
	<td> 1 </td>
	<td> Ô sainte et douce harmonie!<br>Alléluia! Chant des cieux!<br>Chant de la joie infinie<br>pour toujours dans les hauts lieux,<br>répétée, hymne bénie,<br>par le chœur des bienheureux. </td>
	</tr>
	<tr>
	<td> 2 </td>
	<td> Qu'en tes parvis, cité sainte,<br>retentisse Alléluia!<br>Où toujours, libre, sans crainte,<br>de tes fils l'amour brilla.<br>Ici, loin de ton enceinte,<br>l'exil aux pleurs nous lia. </td>
	</tr>
	<tr>
	<td> 3 </td>
	<td> L'alléluia sur la terre<br>ne peut toujours retentir;<br>de nos péchés la misère<br>se fait, hélas! Trop sentir;<br>et bientôt dans la poussière<br>il faudra nous repentir. </td>
	</tr>
	<tr>
	<td> 4 </td>
	<td> Entends notre humble requête!<br>Bienheureuse Trinité;<br>fais que notre voix répète,<br>dans la céleste cité.<br>Alléluia pour la Fête<br>de l'éternelle bonté. </td>
	</tr>
	</table>

## Information
| | |
| - | - |
| Titre original · Original title | Alleluia, dulce carmen |
| Texte · Text  | Latin, XIe siècle;<br/>tr. en anglais J. M. Neale (1861);<br/>tr. en français J. Giraud, *Hymnes à l'usage de l'église pour le service divin*, *Société pour l'avancement des connaissances chrétiennes* (1892) |
| Musique · Music | *Dulce carmen* <br/> Michael Haydn (1782) |