---
tags:
  - Action de grâce · Thanksgiving
---

# Reconnaissants, nous venons



=== ":material-playlist-music: Partition · Score"

	<div id="adobe-dc-view" style="width: 100%;"></div>
	<script src="https://documentcloud.adobe.com/view-sdk/main.js"></script>
	<script type="text/javascript">
    	document.addEventListener("adobe_dc_view_sdk.ready", function(){
        	var adobeDCView = new AdobeDC.View({clientId: "d7518740bf7a4af7b4c3a8b5a7bcff85", divId: "adobe-dc-view"});
        	adobeDCView.previewFile({
            	content:{location: {url: "/livre-de-cantiques-anglicans/assets/pdf/reconnaissants_nous_venons_SAINT_GEORGE_WINDSOR.pdf"}},
            	metaData:{fileName: "reconnaissants_nous_venons_SAINT_GEORGE_WINDSOR.pdf"}}, {embedMode: "IN_LINE"});
    	});
	</script>

=== ":octicons-list-ordered-16: Paroles seulement · Lyrics only"

	<table style="font-size:16px">
	<tr>
	<td> 1 </td>
	<td> Reconnaisants, nous venons<br>chanter sa grande moisson:<br>La récolte est engrangée,<br>du temps glacés protegée;<br>Dieu, le Créateur, fournit<br>que tout besoin soit rempli:<br>venons à son temple, donc;<br>chantons sa grande moisson. </td>
	</tr>
	<tr>
	<td> 2 </td>
	<td> Le monde est son champ à lui,<br>cultivé pour porter fruit;<br>l'ivraie et le bon grain<br>pousse aux joies ou aux chagrins;<br>D'abord l'herbe, puis l'épi,<br>puis l'épi sera rempli:<br>O Seigneur de la moisson,<br>que du pur grain nous soyons. </td>
	</tr>
	<tr>
	<td> 3 </td>
	<td> Dieu et ses anges viendront<br>ramasser cette moisson;<br>il viendra cette journée<br>toute offense éliminer;<br>L'ivraie il va lier<br>en gerbes pour la brûler,<br>mais il gardera le blé<br>pour toujours dans son grenier. </td>
	</tr>
	<tr>
	<td> 4 </td>
	<td> Maintenant, Seigneur, viens donc<br>faire ta grande moisson;<br>Engrange au final les tiens,<br>libérés de leurs chagrins;<br>qu'on soit enfin purifiés;<br>qu'on puisse avec toi rester<br>ce jour dans ton nouveau champ<br>après ta grande moisson. </td>
	</tr>
	</table>

## Information
| | |
| - | - |
| Titre original · Original title | Come, ye thankful people, come |
| Texte · Text  | Henry Alford (1844);<br/>tr. Arlie Coles (2021) |
| Musique · Music | *Saint George Windsor* <br/> George J. Elvey (1856) |