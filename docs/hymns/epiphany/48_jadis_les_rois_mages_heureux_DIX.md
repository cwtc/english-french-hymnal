---
tags:
  - Epiphanie · Epiphany
---

# Jadis les rois mages heureux



=== ":material-playlist-music: Partition · Score"

	<div id="adobe-dc-view" style="width: 100%;"></div>
	<script src="https://documentcloud.adobe.com/view-sdk/main.js"></script>
	<script type="text/javascript">
    	document.addEventListener("adobe_dc_view_sdk.ready", function(){
        	var adobeDCView = new AdobeDC.View({clientId: "d7518740bf7a4af7b4c3a8b5a7bcff85", divId: "adobe-dc-view"});
        	adobeDCView.previewFile({
            	content:{location: {url: "/livre-de-cantiques-anglicans/assets/pdf/jadis_les_rois_mages_heureux_DIX.pdf"}},
            	metaData:{fileName: "jadis_les_rois_mages_heureux_DIX.pdf"}}, {embedMode: "IN_LINE"});
    	});
	</script>

=== ":octicons-list-ordered-16: Paroles seulement · Lyrics only"

	<table style="font-size:16px">
	<tr>
	<td> 1 </td>
	<td> Jadis les rois mages heureux<br>virent cette étoile aux cieux;<br>splendide était sa lueur<br>qui les menait au Seigneur:<br>Que l'on suive leur chemin,<br>venant à l'Enfant divin. </td>
	</tr>
	<tr>
	<td> 2 </td>
	<td> Aux pas joyeux se hâtant,<br>l'Enfant adoré cherchant,<br>ils fléchirent le genou<br>au vrai Dieu qui les absout:<br>Que l'on voie à la mangeoire<br>leur doux propitiatoire. </td>
	</tr>
	<tr>
	<td> 3 </td>
	<td> Ils offrirent leur encens,<br>l'or, la myrrhe à cet Enfant;<br>Que cette humble crèche nous<br>inspire à lui donner tous<br>nos trésors les plus chéris:<br>à notre Seigneur, le Christ! </td>
	</tr>
	<tr>
	<td> 4 </td>
	<td> Le chemin étroit, Jésus,<br>chaque jour y garde nous!<br>Toutes choses passeront;<br>la nouvelle création<br>te verra sans guide étoile,<br>sans les nuages pour voile. </td>
	</tr>
	</table>

## Information
| | |
| - | - |
| Titre original · Original title | As with gladness men of old |
| Texte · Text  | W. Chatterton Dix;<br/>tr. Arlie Coles (2021) |
| Musique · Music | *Dix* <br/> Conrad Kocher (1838) |