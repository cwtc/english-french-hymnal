---
tags:
  - Pâques · Easter
---

# Venez, fidèles, chantez



=== ":material-playlist-music: Partition · Score"

	<div id="adobe-dc-view" style="width: 100%;"></div>
	<script src="https://documentcloud.adobe.com/view-sdk/main.js"></script>
	<script type="text/javascript">
    	document.addEventListener("adobe_dc_view_sdk.ready", function(){
        	var adobeDCView = new AdobeDC.View({clientId: "d7518740bf7a4af7b4c3a8b5a7bcff85", divId: "adobe-dc-view"});
        	adobeDCView.previewFile({
            	content:{location: {url: "/livre-de-cantiques-anglicans/assets/pdf/venez_fideles_chantez_GAUDEAMUS_PARITER.pdf"}},
            	metaData:{fileName: "venez_fideles_chantez_GAUDEAMUS_PARITER.pdf"}}, {embedMode: "IN_LINE"});
    	});
	</script>

=== ":octicons-list-ordered-16: Paroles seulement · Lyrics only"

	<table style="font-size:16px">
	<tr>
	<td> 1 </td>
	<td> Venez, fidèles, chantez<br>dans votre allégresse;<br>Dieu mena son Israël<br>hors de leur tristesse:<br>Pour les enfants de Jacob<br>la mer Rouge il ouvra;<br>les chariots de Pharaon<br>de ces eaux il couvra. </td>
	</tr>
	<tr>
	<td> 2 </td>
	<td> Des âmes c'est le printemps;<br>Christ a les fers brisés,<br>et après trois jours, il s'est<br>comme un soleil levé:<br>L'hiver de chaque péché,<br>enfin, il s'envole<br>de sa Lumière qu'on perçoit,<br>donnant nos éloges. </td>
	</tr>
	<tr>
	<td> 3 </td>
	<td> Cette reine des saisons<br>amène sa splendeur;<br>cette fête apporte<br>son bonheur à rendre<br>à l'heureux Jérusalem,<br>qui, par son affection,<br>carillonne sur Jésus<br>et sa resurrection. </td>
	</tr>
	<tr>
	<td> 4 </td>
	<td> Ni les portes de la mort,<br>ni la sombre tombe,<br>ni les veilleurs, ni le sceau<br>ne t'ont sous leur ombre!<br>Ce jour, tu donnas, Seigneur,<br>par ta bienfaisance,<br>cette paix qui dépasse<br>toute intelligence. </td>
	</tr>
	</table>

## Information
| | |
| - | - |
| Titre original · Original title | Come ye faithful, raise the strain |
| Texte · Text  | Saint Jean Damascène<br/>tr. en anglais J. M. Neale (1859);<br/>tr. en français Arlie Coles (2021) |
| Musique · Music | *Gaudeamus Pariter* <br/> Jan Roh (1544) |