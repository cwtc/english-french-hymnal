---
tags:
  - Pâques · Easter
---

# Le Christ est ressuscité



=== ":material-playlist-music: Partition · Score"

	<div id="adobe-dc-view" style="width: 100%;"></div>
	<script src="https://documentcloud.adobe.com/view-sdk/main.js"></script>
	<script type="text/javascript">
    	document.addEventListener("adobe_dc_view_sdk.ready", function(){
        	var adobeDCView = new AdobeDC.View({clientId: "d7518740bf7a4af7b4c3a8b5a7bcff85", divId: "adobe-dc-view"});
        	adobeDCView.previewFile({
            	content:{location: {url: "/livre-de-cantiques-anglicans/assets/pdf/le_christ_est_ressuscite_WURTEMBURG.pdf"}},
            	metaData:{fileName: "le_christ_est_ressuscite_WURTEMBURG.pdf"}}, {embedMode: "IN_LINE"});
    	});
	</script>

=== ":octicons-list-ordered-16: Paroles seulement · Lyrics only"

	<table style="font-size:16px">
	<tr>
	<td> 1 </td>
	<td> Le Christ est ressuscité;<br>il a chaque fer brisé!<br>Ceux de la céleste armée<br>chantent en perpetuité: </td>
	</tr>
	<tr>
	<td> 2 </td>
	<td> Lui qui alla au supplice,<br>se donnant en sacrifice,<br>il est notre Agneau pascal;<br>chantons son amour loyal: </td>
	</tr>
	<tr>
	<td> 3 </td>
	<td> Lui qui subit tout effroi,<br>sans réconfort sur la croix,<br>il vit, glorieux, au ciel,<br>l'Intercesseur éternel:<br>Alleluia! </td>
	</tr>
	<tr>
	<td> 4 </td>
	<td> Lui, Descendeur aux enfers,<br>il nous rend le ciel ouvert;<br>que la terre entière croie<br>qu'il est le vrai Roi des rois: </td>
	</tr>
	<tr>
	<td> 5 </td>
	<td> Il donna la Commission:<br>allez dans toute nation;<br>racontez au pénitents<br>qu'ils verront la délivrance: </td>
	</tr>
	<tr>
	<td> 6 </td>
	<td> Tu es notre Agneau, ô Christ:<br>viens, donc, pour qu'on soit nourri;<br>lave-nous de nos péchés,<br>pour que l'on chante à jamais: </td>
	</tr>
	</table>

## Information
| | |
| - | - |
| Titre original · Original title | Christus ist erstanden |
| Texte · Text  | Michael Weisse (1531);<br/>tr. en anglais Catherine Winkworth (1858);<br/>tr. en français Arlie Coles (2021) |
| Musique · Music | *Würtemburg* <br/> Johann Rosenmüller (1655) |