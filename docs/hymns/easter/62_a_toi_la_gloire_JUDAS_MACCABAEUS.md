---
tags:
  - Pâques · Easter
---

# A toi la gloire



=== ":material-playlist-music: Partition · Score"

	<div id="adobe-dc-view" style="width: 100%;"></div>
	<script src="https://documentcloud.adobe.com/view-sdk/main.js"></script>
	<script type="text/javascript">
    	document.addEventListener("adobe_dc_view_sdk.ready", function(){
        	var adobeDCView = new AdobeDC.View({clientId: "d7518740bf7a4af7b4c3a8b5a7bcff85", divId: "adobe-dc-view"});
        	adobeDCView.previewFile({
            	content:{location: {url: "/livre-de-cantiques-anglicans/assets/pdf/a_toi_la_gloire_JUDAS_MACCABAEUS.pdf"}},
            	metaData:{fileName: "a_toi_la_gloire_JUDAS_MACCABAEUS.pdf"}}, {embedMode: "IN_LINE"});
    	});
	</script>

=== ":octicons-list-ordered-16: Paroles seulement · Lyrics only"

	<table style="font-size:16px">
	<tr>
	<td> 1 </td>
	<td> À toi la gloire, Ô Ressuscité!<br>À toi la victoire pour l'éternité!<br>Brillant de lumière, l'ange est descendu;<br>Il roule la pierre du tombeau vaincu. </td>
	</tr>
	<tr>
	<td> 2 </td>
	<td> Vois-le paraître: c'est lui, c'est Jésus,<br>Ton Sauveur, ton Maître, oh! Ne doute plus!<br>Sois dans l'allégresse, peuple du Seigneur,<br>Et redis sans cesse: le Christ est vainqueur!<br>À toi la gloire, Ô Ressuscité!<br>À toi la victoire pour l'éternité! </td>
	</tr>
	<tr>
	<td> 3 </td>
	<td> Craindrais -je encore? Il vit à jamais,<br>Celui que j'adore, le Prince de paix;<br>Il est ma victoire, mon puissant soutien,<br>Ma vie et ma gloire: non, je ne crains rien! </td>
	</tr>
	</table>

## Information
| | |
| - | - |
| Texte · Text  | Edmond L. Budry (1885); |
| Musique · Music | *Judas Maccabaeus* <br/> George F. Handel (1747) |