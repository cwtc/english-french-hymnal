---
tags:
  - Pâques · Easter
---

# Le grand combat est terminé



=== ":material-playlist-music: Partition · Score"

	<div id="adobe-dc-view" style="width: 100%;"></div>
	<script src="https://documentcloud.adobe.com/view-sdk/main.js"></script>
	<script type="text/javascript">
    	document.addEventListener("adobe_dc_view_sdk.ready", function(){
        	var adobeDCView = new AdobeDC.View({clientId: "d7518740bf7a4af7b4c3a8b5a7bcff85", divId: "adobe-dc-view"});
        	adobeDCView.previewFile({
            	content:{location: {url: "/livre-de-cantiques-anglicans/assets/pdf/le_grand_combat_est_termine_VICTORY.pdf"}},
            	metaData:{fileName: "le_grand_combat_est_termine_VICTORY.pdf"}}, {embedMode: "IN_LINE"});
    	});
	</script>

=== ":octicons-list-ordered-16: Paroles seulement · Lyrics only"

	<table style="font-size:16px">
	<tr>
	<td> 1 </td>
	<td> Alléluia, alléluia, alléluia!<br>Le grand combat est terminé;<br>Le Christ Vainqueur a triomphé;<br>Que ses louanges soient chantés: </td>
	</tr>
	<tr>
	<td> 2 </td>
	<td> Il est vivant! La mort  n'est plus!<br>Ses ennemis il a  battu.<br>Chantons la gloire de Jésus! </td>
	</tr>
	<tr>
	<td> 3 </td>
	<td> Après trois jours il s'est  levé,<br>Resplendissant de majesté,<br>Criez de joie et proclamez:<br>Alléluia! </td>
	</tr>
	<tr>
	<td> 4 </td>
	<td> Il a vidé l'infernal prison;<br>Premier-né de la Résurrection;<br>Nous faisons donc ce joyeux son: </td>
	</tr>
	</table>

## Information
| | |
| - | - |
| Titre original · Original title | Finita jam sun proelia |
| Texte · Text  | *Symphonia Sirenum Selectarum* (1695);<br/>tr. en anglais Francis Pott (1859);<br/>tr. en français Richard Paquier (1905-1985) ad. Arlie Coles (2021), str. 1-3;<br/>tr. en français Arlie Coles (2021), str. 4-5 |
| Musique · Music | *Victory* <br/> Giovanni Pierluigi da Palestrina (1591) |