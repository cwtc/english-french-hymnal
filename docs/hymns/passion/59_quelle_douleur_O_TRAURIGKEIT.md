---
tags:
  - Passion · Passiontide
---

# Quelle douleur



=== ":material-playlist-music: Partition · Score"

	<div id="adobe-dc-view" style="width: 100%;"></div>
	<script src="https://documentcloud.adobe.com/view-sdk/main.js"></script>
	<script type="text/javascript">
    	document.addEventListener("adobe_dc_view_sdk.ready", function(){
        	var adobeDCView = new AdobeDC.View({clientId: "d7518740bf7a4af7b4c3a8b5a7bcff85", divId: "adobe-dc-view"});
        	adobeDCView.previewFile({
            	content:{location: {url: "/livre-de-cantiques-anglicans/assets/pdf/quelle_douleur_O_TRAURIGKEIT.pdf"}},
            	metaData:{fileName: "quelle_douleur_O_TRAURIGKEIT.pdf"}}, {embedMode: "IN_LINE"});
    	});
	</script>

=== ":octicons-list-ordered-16: Paroles seulement · Lyrics only"

	<table style="font-size:16px">
	<tr>
	<td> 1 </td>
	<td> Quelle douleur<br>saisit mon cœur!<br>A peu près j'y succombe.<br>Le Fils de Dieu, mon Sauveur,<br>est mis dans la Tombe. </td>
	</tr>
	<tr>
	<td> 2 </td>
	<td> Étrange sort!<br>Dieu même est mort;<br>il laisse en croix la vie.<br>Mais par ce suprême effort,<br>ils nous vivifie. </td>
	</tr>
	<tr>
	<td> 3 </td>
	<td> Homme pécheur,<br>le séducteur<br>t'entraînait dans l'abîme:<br>Pour t'en tirer, le Sauveur<br>expire en victime. </td>
	</tr>
	<tr>
	<td> 4 </td>
	<td> versant pour nous<br>tout le sang de ses veines,<br>et finit nos peines. </td>
	</tr>
	<tr>
	<td> 5 </td>
	<td>  </td>
	</tr>
	</table>

## Information
| | |
| - | - |
| Titre original · Original title | O Traurigkeit |
| Texte · Text  | Johann von Rist (1637);<br/>tr. *Nouvelle édition des Cantiques spirituels* (1747) |
| Musique · Music | *O Traurigkeit* <br/> *Himmlische Harmonie* (1628) |