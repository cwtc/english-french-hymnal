---
tags:
  - Passion · Passiontide
---

# Vois là-bas, mettre le Seigneur en croix



=== ":material-playlist-music: Partition · Score"

	<div id="adobe-dc-view" style="width: 100%;"></div>
	<script src="https://documentcloud.adobe.com/view-sdk/main.js"></script>
	<script type="text/javascript">
    	document.addEventListener("adobe_dc_view_sdk.ready", function(){
        	var adobeDCView = new AdobeDC.View({clientId: "d7518740bf7a4af7b4c3a8b5a7bcff85", divId: "adobe-dc-view"});
        	adobeDCView.previewFile({
            	content:{location: {url: "/livre-de-cantiques-anglicans/assets/pdf/vois_la_bas_mettre_le_seigneur_en_croix_WERE_YOU_THERE.pdf"}},
            	metaData:{fileName: "vois_la_bas_mettre_le_seigneur_en_croix_WERE_YOU_THERE.pdf"}}, {embedMode: "IN_LINE"});
    	});
	</script>

=== ":octicons-list-ordered-16: Paroles seulement · Lyrics only"

	<table style="font-size:16px">
	<tr>
	<td> 1 </td>
	<td> Vois, là-bas, mettre le Seigneur en croix! <br>Vois, là-bas, mettre le Seigneur en croix! <br>Vois, là-bas, mettre le Seigneur en croix! </td>
	</tr>
	<tr>
	<td> 2 </td>
	<td> Vois, là-bas, clouer le Seigneur au bois! <br>Vois, là-bas, clouer le Seigneur au bois! <br>Vois, là-bas, clouer le Seigneur au bois! </td>
	</tr>
	<tr>
	<td> 3 </td>
	<td> Vois, là-bas, de leur lance ils l'ont percé! <br>Vois, là-bas, de leur lance ils l'ont percé! <br>Oh,  le cœur saisi d'effroi,<br>je tremble, tremble, tremble.<br>Vois, là-bas, de leur lance ils l'ont percé! </td>
	</tr>
	<tr>
	<td> 4 </td>
	<td> Vois, là-bas, dans la tombe ils l'ont porté! <br>Vois, là-bas, dans la tombe ils l'ont porté! <br>Vois, là-bas, dans la tombe ils l'ont porté! </td>
	</tr>
	</table>

## Information
| | |
| - | - |
| Titre original · Original title | Were you there when they crucified my Lord |
| Texte · Text  | Spiritual afro-américain, XIXe siècle;<br/>tr. Claire Julien (date inconnue) |
| Musique · Music | *Were you there* <br/> Spiritual afro-américain, XIXe siècle |