---
tags:
  - Passion · Passiontide
---

# Gouttez, mes larmes



=== ":material-playlist-music: Partition · Score"

	<div id="adobe-dc-view" style="width: 100%;"></div>
	<script src="https://documentcloud.adobe.com/view-sdk/main.js"></script>
	<script type="text/javascript">
    	document.addEventListener("adobe_dc_view_sdk.ready", function(){
        	var adobeDCView = new AdobeDC.View({clientId: "d7518740bf7a4af7b4c3a8b5a7bcff85", divId: "adobe-dc-view"});
        	adobeDCView.previewFile({
            	content:{location: {url: "/livre-de-cantiques-anglicans/assets/pdf/gouttez_mes_larmes_SONG_46.pdf"}},
            	metaData:{fileName: "gouttez_mes_larmes_SONG_46.pdf"}}, {embedMode: "IN_LINE"});
    	});
	</script>

=== ":octicons-list-ordered-16: Paroles seulement · Lyrics only"

	<table style="font-size:16px">
	<tr>
	<td> 1 </td>
	<td> Gouttez, mes larmes, et lavez ces beaux pieds<br>apportant les nouvelles de la paix. </td>
	</tr>
	<tr>
	<td> 2 </td>
	<td> Cherchez, mes yeux, à jamais sa clémence;<br>le péché cherche toujours la vengeance. </td>
	</tr>
	<tr>
	<td> 3 </td>
	<td> Inondez donc mes peurs et mes défauts,<br>qu'il voie le péché par mes sanglots. </td>
	</tr>
	</table>

## Information
| | |
| - | - |
| Titre original · Original title | Drop, drop, slow tears |
| Texte · Text  | Phineas Fletcher (1582-1650);<br/>tr. Arlie Coles (2021) |
| Musique · Music | *Song 46* <br/> Orlando Gibbons (1623) |