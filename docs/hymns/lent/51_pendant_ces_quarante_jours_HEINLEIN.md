---
tags:
  - Carême · Lent
---

# Pendant ces quarante jours



=== ":material-playlist-music: Partition · Score"

	<div id="adobe-dc-view" style="width: 100%;"></div>
	<script src="https://documentcloud.adobe.com/view-sdk/main.js"></script>
	<script type="text/javascript">
    	document.addEventListener("adobe_dc_view_sdk.ready", function(){
        	var adobeDCView = new AdobeDC.View({clientId: "d7518740bf7a4af7b4c3a8b5a7bcff85", divId: "adobe-dc-view"});
        	adobeDCView.previewFile({
            	content:{location: {url: "/livre-de-cantiques-anglicans/assets/pdf/pendant_ces_quarante_jours_HEINLEIN.pdf"}},
            	metaData:{fileName: "pendant_ces_quarante_jours_HEINLEIN.pdf"}}, {embedMode: "IN_LINE"});
    	});
	</script>

=== ":octicons-list-ordered-16: Paroles seulement · Lyrics only"

	<table style="font-size:16px">
	<tr>
	<td> 1 </td>
	<td> Pendant ces quarante jours,<br>pendant ces quarante nuits,<br>tu jeûnais, ce sans secours,<br>tenté, sans aucun délit. </td>
	</tr>
	<tr>
	<td> 2 </td>
	<td> Partageons donc ton chagrin;<br>abstenons-nous du plaisir;<br>faisons ce jeûne divin,<br>contents d'avec toi souffrir. </td>
	</tr>
	<tr>
	<td> 3 </td>
	<td> Si Satan, contrarié,<br>notre chair faible assaillit,<br>toi, Vainqueur glorifié,<br>garde-nous de l'ennemi. </td>
	</tr>
	<tr>
	<td> 4 </td>
	<td> Que cette divine paix<br>nous rende donc bienheureux;<br>les anges qui te servaient<br>nous rendront l'aide des cieux. </td>
	</tr>
	</table>

## Information
| | |
| - | - |
| Titre original · Original title | Forty days and forty nights |
| Texte · Text  | George Hunt Smyttan (1856)<br/>tr. Arlie Coles (2021) |
| Musique · Music | *Heinlein* <br/> Martin Herbst (1676) |