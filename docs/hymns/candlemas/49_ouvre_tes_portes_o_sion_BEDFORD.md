---
tags:
  - Chandeleur · Candlemas
---

# Ouvre tes portes, ô Sion



=== ":material-playlist-music: Partition · Score"

	<div id="adobe-dc-view" style="width: 100%;"></div>
	<script src="https://documentcloud.adobe.com/view-sdk/main.js"></script>
	<script type="text/javascript">
    	document.addEventListener("adobe_dc_view_sdk.ready", function(){
        	var adobeDCView = new AdobeDC.View({clientId: "d7518740bf7a4af7b4c3a8b5a7bcff85", divId: "adobe-dc-view"});
        	adobeDCView.previewFile({
            	content:{location: {url: "/livre-de-cantiques-anglicans/assets/pdf/ouvre_tes_portes_o_sion_BEDFORD.pdf"}},
            	metaData:{fileName: "ouvre_tes_portes_o_sion_BEDFORD.pdf"}}, {embedMode: "IN_LINE"});
    	});
	</script>

=== ":octicons-list-ordered-16: Paroles seulement · Lyrics only"

	<table style="font-size:16px">
	<tr>
	<td> 1 </td>
	<td> Ouvre tes portes, ô Sion:<br>L'ombre va disparaître;<br>voici, merveilleuse union!<br>La victime et le prêtre. </td>
	</tr>
	<tr>
	<td> 2 </td>
	<td> Plus d'holocauste à consumer:<br>Par son grand sacrifice,<br>le Fils de Dieu vient désarmer<br>la céleste justice. </td>
	</tr>
	<tr>
	<td> 3 </td>
	<td> L'humble Vierge, dans cet enfant,<br>voit la Déité même;<br>et deux colombes pour présent<br>elle offre au Dieu suprême. </td>
	</tr>
	<tr>
	<td> 4 </td>
	<td> A Siméon paraît enfin<br>l'objet de son attente;<br>Anna peut voir l'enfant divin,<br>et va mourir contente. </td>
	</tr>
	<tr>
	<td> 5 </td>
	<td> La sainte Mère, à deux genoux,<br>en silence l'adore,<br>ce Verbe, petit comme nous,<br>ne parlant pas encore. </td>
	</tr>
	<tr>
	<td> 6 </td>
	<td> À toi gloire, ô Père éternel;<br>gloire à toi, Fils du Père,<br>à toi, Saint-Esprit, gloire au ciel,<br>gloire aussi sur la terre. </td>
	</tr>
	</table>

## Information
| | |
| - | - |
| Titre original · Original title | Templi sacratas pande, Sion, fores |
| Texte · Text  | Jean-Baptiste de Santeul (1630-1697);<br/>tr. en anglais Edward Caswall (1814-1878);<br/>tr. en français J. Giraud, *Hymnes à l'usage de l'église pour le service divin*, *Société pour l'avancement des connaissances chrétiennes* (1892) |
| Musique · Music | *Bedford* <br/> William Wheal (1720) |