---
tags:
  - Avent · Advent
---

# Viens, ô Jésus très attendu



=== ":material-playlist-music: Partition · Score"

	<div id="adobe-dc-view" style="width: 100%;"></div>
	<script src="https://documentcloud.adobe.com/view-sdk/main.js"></script>
	<script type="text/javascript">
    	document.addEventListener("adobe_dc_view_sdk.ready", function(){
        	var adobeDCView = new AdobeDC.View({clientId: "d7518740bf7a4af7b4c3a8b5a7bcff85", divId: "adobe-dc-view"});
        	adobeDCView.previewFile({
            	content:{location: {url: "/livre-de-cantiques-anglicans/assets/pdf/viens_o_jesus_tres_attendu_STUTTGART.pdf"}},
            	metaData:{fileName: "viens_o_jesus_tres_attendu_STUTTGART.pdf"}}, {embedMode: "IN_LINE"});
    	});
	</script>

=== ":octicons-list-ordered-16: Paroles seulement · Lyrics only"

	<table style="font-size:16px">
	<tr>
	<td> 1 </td>
	<td> Viens, ô Jésus très attendu,<br>né pour affranchir les tiens;<br>de nos péchés libère-nous;<br>qu'on soit porté dans ton sein. </td>
	</tr>
	<tr>
	<td> 2 </td>
	<td> D'Israël la consolation,<br>des saints le fervent espoir,<br>cher désir de toute nation,<br>joie ardente de tout cœur: </td>
	</tr>
	<tr>
	<td> 3 </td>
	<td> Né pour délivrer ton peuple,<br>né enfant et pourtant roi,<br>né pour gouverner pour toujours:<br>viens, Jésus; ne tarde pas! </td>
	</tr>
	<tr>
	<td> 4 </td>
	<td> Par ton esprit sécurisant,<br>sois notre Roi et Epoux;<br>par ton mérite suffisant,<br>vers ton trône lève-nous. </td>
	</tr>
	</table>

## Information
| | |
| - | - |
| Titre original · Original title | Come, thou long-expected Jesus |
| Texte · Text  | Charles Wesley (1744);<br/>tr. Arlie Coles (2021) |
| Musique · Music | *Stuttgart* <br/> attr. Christian Friedrich Witt (1715) |