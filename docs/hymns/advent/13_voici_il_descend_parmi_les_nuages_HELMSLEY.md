---
tags:
  - Avent · Advent
---

# Voici, il descend parmi les nuages



=== ":material-playlist-music: Partition · Score"

	<div id="adobe-dc-view" style="width: 100%;"></div>
	<script src="https://documentcloud.adobe.com/view-sdk/main.js"></script>
	<script type="text/javascript">
    	document.addEventListener("adobe_dc_view_sdk.ready", function(){
        	var adobeDCView = new AdobeDC.View({clientId: "d7518740bf7a4af7b4c3a8b5a7bcff85", divId: "adobe-dc-view"});
        	adobeDCView.previewFile({
            	content:{location: {url: "/livre-de-cantiques-anglicans/assets/pdf/voici_il_descend_parmi_les_nuages_HELMSLEY.pdf"}},
            	metaData:{fileName: "voici_il_descend_parmi_les_nuages_HELMSLEY.pdf"}}, {embedMode: "IN_LINE"});
    	});
	</script>

=== ":octicons-list-ordered-16: Paroles seulement · Lyrics only"

	<table style="font-size:16px">
	<tr>
	<td> 1 </td>
	<td> Voici! Il descend parmi les nuages,<br>autrefois pour le salut occis; <br>milliers de milliers de saints lui rendent hommage;<br>en victoire ils s'amplifient:<br>Al léluia!<br>Al léluia!<br>Al léluia!<br>Il revient, le Seigneur, le Christ. </td>
	</tr>
	<tr>
	<td> 2 </td>
	<td> Tout œil doit maintenant  le regarder,<br>revêtu d'un grandeur terrible;<br>tous ceux qui l'ont à la croix cloué<br>après qu'ils le vendirent:<br>Quelles lamentations!<br>Quelles lamentations!<br>Quelles lamentations!<br>Ils verront le vrai Messie. </td>
	</tr>
	<tr>
	<td> 3 </td>
	<td> Ces marques chères qu'il gagna pendant sa Passion,<br>par son corps éblouissant portées, <br>sont toujours source d'ex ultation<br>pour chacun des croyants sauvés: <br>Quelle ecstasie!<br>Quelle ecstasie!<br>Quelle ecstasie!<br>Belles balafres, louons-les! </td>
	</tr>
	<tr>
	<td> 4 </td>
	<td> Amen! Que tout homme t'adore!<br>Viens et à ton trône accède;<br>Sauveur, prends le pouvoir et la gloire,<br>car le Royaume est le tien: <br>Ô,  viens vite!<br>Ô,  viens vite!<br>Ô,  viens vite!<br>C'est à toi qu'appartient le règne. </td>
	</tr>
	</table>

## Information
| | |
| - | - |
| Titre original · Original title | Lo, he comes with clouds descending |
| Texte · Text  | John Cennick (1752);<br/>ad. Charles Wesley (1758);<br/>tr. Arlie Coles (2021) |
| Musique · Music | *Helmsley* <br/> Thomas Augustine Arne (1761);<br/>ad. Martin Madan (1763);<br/>ad. Thomas Olivers (1763) |