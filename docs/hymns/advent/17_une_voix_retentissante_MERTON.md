---
tags:
  - Avent · Advent
---

# Une voix retentissante



=== ":material-playlist-music: Partition · Score"

	<div id="adobe-dc-view" style="width: 100%;"></div>
	<script src="https://documentcloud.adobe.com/view-sdk/main.js"></script>
	<script type="text/javascript">
    	document.addEventListener("adobe_dc_view_sdk.ready", function(){
        	var adobeDCView = new AdobeDC.View({clientId: "d7518740bf7a4af7b4c3a8b5a7bcff85", divId: "adobe-dc-view"});
        	adobeDCView.previewFile({
            	content:{location: {url: "/livre-de-cantiques-anglicans/assets/pdf/une_voix_retentissante_MERTON.pdf"}},
            	metaData:{fileName: "une_voix_retentissante_MERTON.pdf"}}, {embedMode: "IN_LINE"});
    	});
	</script>

=== ":octicons-list-ordered-16: Paroles seulement · Lyrics only"

	<table style="font-size:16px">
	<tr>
	<td> 1 </td>
	<td> Une voix retentissante<br>a crié: “Voici l'Époux!<br>Plus de langueur impuissante;<br>enfants du jour, levez-vous!” </td>
	</tr>
	<tr>
	<td> 2 </td>
	<td> Du lourd sommeil de la terre<br>brisons le fatal pouvoir.<br>Le divin soleil éclaire;<br>ouvrons les yeux pour le voir. </td>
	</tr>
	<tr>
	<td> 3 </td>
	<td> Objet d'une longue attente,<br>l'Agneau pur descend des cieux.<br>Que notre âme pénitente<br>plonge en son sang précieux. </td>
	</tr>
	<tr>
	<td> 4 </td>
	<td> Il reviendra plein de gloire<br>dans ce monde épouvanté<br>nous prendre dans sa victoire,<br>conquis par sa charité. </td>
	</tr>
	</table>

## Information
| | |
| - | - |
| Titre original · Original title | Hark, a thrilling voice is sounding |
| Texte · Text  | Latin, Ve siècle;<br/>tr. en anglais Edward Caswall (1814-1878);<br/>tr. en français J. Giraud, *Hymnes à l'usage de l'église pour le service divin*, *Société pour l'avancement des connaissances chrétiennes* (1892) |
| Musique · Music | *Merton* <br/> William Henry Monk (1850) |