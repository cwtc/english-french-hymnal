---
tags:
  - Avent · Advent
  - Hymne du bréviaire · Office hymn
---

# O viens, ô viens, Emmanuel



=== ":material-playlist-music: Partition · Score"

	<div id="adobe-dc-view" style="width: 100%;"></div>
	<script src="https://documentcloud.adobe.com/view-sdk/main.js"></script>
	<script type="text/javascript">
    	document.addEventListener("adobe_dc_view_sdk.ready", function(){
        	var adobeDCView = new AdobeDC.View({clientId: "d7518740bf7a4af7b4c3a8b5a7bcff85", divId: "adobe-dc-view"});
        	adobeDCView.previewFile({
            	content:{location: {url: "/livre-de-cantiques-anglicans/assets/pdf/o_viens_o_viens_emmanuel_VENI_VENI_EMMANUEL.pdf"}},
            	metaData:{fileName: "o_viens_o_viens_emmanuel_VENI_VENI_EMMANUEL.pdf"}}, {embedMode: "IN_LINE"});
    	});
	</script>

=== ":octicons-list-ordered-16: Paroles seulement · Lyrics only"

	<table style="font-size:16px">
	<tr>
	<td> 1 </td>
	<td> Ô viens, ô viens, Emmanuel,<br>viens pour délivrer Israël:<br>en deuil, en exil démuni<br>jusqu'à ce que le Fils de Dieu arrive.<br>Chantez! Chantez! Emmanuel<br>viendra vers toi, ô Israël. </td>
	</tr>
	<tr>
	<td> 2 </td>
	<td> Ô viens, ô viens, ô sainte Sagesse,<br>qui ordonna le monde sans faiblesse;<br>révèle-nous ta connaissance;<br>que l'on la suive en espérance. </td>
	</tr>
	<tr>
	<td> 3 </td>
	<td> Ô viens, ô viens, Seigneur de pouvoir,<br>qui demontra sur Sinaï sa gloire;<br>aux temps anciens ses douze tribus<br>la loi en révérence reçurent. </td>
	</tr>
	<tr>
	<td> 4 </td>
	<td> Ô viens, Rameau du tronc de Jessé;<br>viens vers ton peuple pour les sauver<br>de l'enfer affreux et ses tréfonds,<br>et gagne la victoire sur la tombe. </td>
	</tr>
	<tr>
	<td> 5 </td>
	<td> Ô viens, Étoile brillant du matin;<br>et réconforte-nous de loin!<br>Chasse les ombres de la nuit<br>et fais que les ténèbres luisent. </td>
	</tr>
	<tr>
	<td> 6 </td>
	<td> Ô viens, ô Roi de toute nation,<br>unis les cœurs de ta création.<br>Guéris nos fractures aggravées,<br>et sois toi-même le Roi de la Paix. </td>
	</tr>
	</table>

## Information
| | |
| - | - |
| Titre original · Original title | Veni veni Emmanuel |
| Texte · Text  | Latin, XIIe siècle;<br/>ad. en anglais J. M. Neale (1861);<br/>tr. en français Arlie Coles (2021) |
| Musique · Music | *Veni veni Emmanuel* <br/> Thomas Augustine Arne (1761);<br/>chant processionel français, XVe siècle;<br/>ad. Thomas Helmore (1811-1890) |