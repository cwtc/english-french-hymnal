---
tags:
  - Avent · Advent
---

# Entendez-vous ces cris joyeux



=== ":material-playlist-music: Partition · Score"

	<div id="adobe-dc-view" style="width: 100%;"></div>
	<script src="https://documentcloud.adobe.com/view-sdk/main.js"></script>
	<script type="text/javascript">
    	document.addEventListener("adobe_dc_view_sdk.ready", function(){
        	var adobeDCView = new AdobeDC.View({clientId: "d7518740bf7a4af7b4c3a8b5a7bcff85", divId: "adobe-dc-view"});
        	adobeDCView.previewFile({
            	content:{location: {url: "/livre-de-cantiques-anglicans/assets/pdf/entendez_vous_ces_cris_joyeux_RICHMOND.pdf"}},
            	metaData:{fileName: "entendez_vous_ces_cris_joyeux_RICHMOND.pdf"}}, {embedMode: "IN_LINE"});
    	});
	</script>

=== ":octicons-list-ordered-16: Paroles seulement · Lyrics only"

	<table style="font-size:16px">
	<tr>
	<td> 1 </td>
	<td> Entendez-vous ces cris joyeux:<br>“Voici le Rédempteur!”<br>Louons-le par nos chants pieux,<br>d'une voix et d'un cœur. </td>
	</tr>
	<tr>
	<td> 2 </td>
	<td> Captifs, il vient vous enlever<br>des mains de l'ennemi.<br>Portes d'airain, chaînes de fer,<br>tout cède devant lui. </td>
	</tr>
	<tr>
	<td> 3 </td>
	<td> Les cœurs meurtris et tout sanglants,<br>il vient pour les guérir;<br>il vient de ses dons consolants<br>les pauvres secourir. </td>
	</tr>
	<tr>
	<td> 4 </td>
	<td> Aux chants que to nous inspiras,<br>descends, Prince de Paix;<br>qu'on répète nos hosannas<br>dans les cieux à jamais. </td>
	</tr>
	</table>

## Information
| | |
| - | - |
| Titre original · Original title | Hark, the glad sound |
| Texte · Text  | Philip Doddridge (1735);<br/>tr. en anglais J. M. Neale (1852);<br/>tr. en français J. Giraud, *Hymnes à l'usage de l'église pour le service divin*, *Société pour l'avancement des connaissances chrétiennes* (1892) |
| Musique · Music | *Richmond* <br/> Thomas Haweis (1792) |