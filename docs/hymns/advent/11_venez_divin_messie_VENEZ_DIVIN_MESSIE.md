---
tags:
  - Avent · Advent
---

# Venez, divin Messie



=== ":material-playlist-music: Partition · Score"

	<div id="adobe-dc-view" style="width: 100%;"></div>
	<script src="https://documentcloud.adobe.com/view-sdk/main.js"></script>
	<script type="text/javascript">
    	document.addEventListener("adobe_dc_view_sdk.ready", function(){
        	var adobeDCView = new AdobeDC.View({clientId: "d7518740bf7a4af7b4c3a8b5a7bcff85", divId: "adobe-dc-view"});
        	adobeDCView.previewFile({
            	content:{location: {url: "/livre-de-cantiques-anglicans/assets/pdf/venez_divin_messie_VENEZ_DIVIN_MESSIE.pdf"}},
            	metaData:{fileName: "venez_divin_messie_VENEZ_DIVIN_MESSIE.pdf"}}, {embedMode: "IN_LINE"});
    	});
	</script>

=== ":octicons-list-ordered-16: Paroles seulement · Lyrics only"

	<table style="font-size:16px">
	<tr>
	<td> 1 </td>
	<td> Venez, divin Messie!<br>Sauvez nous jours infortunés!<br>Venez, source de Vie!<br>Venez, venez, venez!<br>Ah! Descendez, hâtez vos pas;<br>sauvez les hommes du trépas;<br>secoureznous; ne tardez pas. <br>Dans une peine extrême,<br>gémissent nos cœurs affligés.<br>Venez, Bonté Suprême!<br>Venez, venez, venez! </td>
	</tr>
	<tr>
	<td> 2 </td>
	<td> Ah! Désarmez votre courroux;<br>nous soupirons à vos genoux.<br>Seigneur, nous n'espérons qu'en vous. <br>Pour nous livrer la guerre,<br>tous les enfers sont déchaînés;<br>descendez sur la terre!<br>Venez, venez, venez! </td>
	</tr>
	<tr>
	<td> 3 </td>
	<td> Que nos soupirs soient entendus;<br>les biens que nous avons perdus,<br>ne nous serontils point rendus? <br>Voyez couler nos larmes;<br>grand Dieu, si vous nous pardonnez,<br>nous n'aurons plus d'alarmes!<br>Venez, venez, venez! </td>
	</tr>
	<tr>
	<td> 4 </td>
	<td> Si vous venez en ces baslieux,<br>nous vous verrons victorieux,<br>fermer l'enfer, ouvrir les cieux. <br>Nous l'espérons sans cesse:<br>Les cieux nous furent destinés.<br>Tenez votre promesse!<br>Venez, venez, venez! </td>
	</tr>
	</table>

## Information
| | |
| - | - |
| Texte · Text  | Simon-Joseph Pellegrin (1663-1745);<br/>ad. Arlie Coles (2021); str. 5 |
| Musique · Music | *Venez, divin Messie* <br/> air français, XVIe siècle |