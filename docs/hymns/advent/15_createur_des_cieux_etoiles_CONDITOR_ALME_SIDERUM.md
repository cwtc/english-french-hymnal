---
tags:
  - Avent · Advent
  - Hymne du bréviaire · Office hymn
---

# Créateur des cieux étoilés



=== ":material-playlist-music: Partition · Score"

	<div id="adobe-dc-view" style="width: 100%;"></div>
	<script src="https://documentcloud.adobe.com/view-sdk/main.js"></script>
	<script type="text/javascript">
    	document.addEventListener("adobe_dc_view_sdk.ready", function(){
        	var adobeDCView = new AdobeDC.View({clientId: "d7518740bf7a4af7b4c3a8b5a7bcff85", divId: "adobe-dc-view"});
        	adobeDCView.previewFile({
            	content:{location: {url: "/livre-de-cantiques-anglicans/assets/pdf/createur_des_cieux_etoiles_CONDITOR_ALME_SIDERUM.pdf"}},
            	metaData:{fileName: "createur_des_cieux_etoiles_CONDITOR_ALME_SIDERUM.pdf"}}, {embedMode: "IN_LINE"});
    	});
	</script>

=== ":octicons-list-ordered-16: Paroles seulement · Lyrics only"

	<table style="font-size:16px">
	<tr>
	<td> 1 </td>
	<td> Créateur des cieux étoilés,<br>sur le troupeau des désolés,<br>viens, Jésus, viens, ô Rédempteur,<br>porter le jour, chasser l'erreur. </td>
	</tr>
	<tr>
	<td> 2 </td>
	<td> Il fut ému, ton cœur divin<br>à notre malheureux destin;<br>par la vertu de ton amour<br>tu fermas l'infernal séjour. </td>
	</tr>
	<tr>
	<td> 3 </td>
	<td> Céleste Époux, tu es venu<br>quand arrivait le crepuscule;<br>tu es de la Vierge arrivé,<br>Victime pure, immaculée. </td>
	</tr>
	<tr>
	<td> 4 </td>
	<td> À ton grand nom tous prosternés,<br>au salut par toi nouveau-nés,<br>avec les habitants des cieux,<br>adorant, nous t'offrons nos vœux. </td>
	</tr>
	<tr>
	<td> 5 </td>
	<td> Toi qui viendras, terrifiant,<br>juger les morts et les vivants,<br>ô, garde-nous à ton abri<br>de tout assaut de l'ennemi. </td>
	</tr>
	<tr>
	<td> 6 </td>
	<td> Ô Père Éternel, qui nous fis<br>tes enfants par grâce; à ton Fils,<br>au divin Esprit, comme à toi,<br>nous rendons hommage avec foi. </td>
	</tr>
	</table>

## Information
| | |
| - | - |
| Titre original · Original title | Conditor alme siderum |
| Texte · Text  | Latin, IXe siècle;<br/>tr. en anglais J. M. Neale (1852);<br/>tr. en français J. Giraud, *Hymnes à l'usage de l'église pour le service divin*, *Société pour l'avancement des connaissances chrétiennes* (1892), str. 1, 2, 4, 6;<br/>tr. en français Arlie Coles, str. 3, 5 |
| Musique · Music | *Conditor alme siderum* <br/> chant grégorien, IXe siècle |