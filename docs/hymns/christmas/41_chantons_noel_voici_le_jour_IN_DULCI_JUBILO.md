---
tags:
  - Noël · Christmas
---

# Chantons Noël, voici le jour



=== ":material-playlist-music: Partition · Score"

	<div id="adobe-dc-view" style="width: 100%;"></div>
	<script src="https://documentcloud.adobe.com/view-sdk/main.js"></script>
	<script type="text/javascript">
    	document.addEventListener("adobe_dc_view_sdk.ready", function(){
        	var adobeDCView = new AdobeDC.View({clientId: "d7518740bf7a4af7b4c3a8b5a7bcff85", divId: "adobe-dc-view"});
        	adobeDCView.previewFile({
            	content:{location: {url: "/livre-de-cantiques-anglicans/assets/pdf/chantons_noel_voici_le_jour_IN_DULCI_JUBILO.pdf"}},
            	metaData:{fileName: "chantons_noel_voici_le_jour_IN_DULCI_JUBILO.pdf"}}, {embedMode: "IN_LINE"});
    	});
	</script>

=== ":octicons-list-ordered-16: Paroles seulement · Lyrics only"

	<table style="font-size:16px">
	<tr>
	<td> 1 </td>
	<td> Chantons Noël, voici le jour,<br>au front la joie, au cœur l'amour!<br>Là dans une crèche fait homme un Dieu repose enfant;<br>Sur la paille fraîche, il nous regarde en souri ant:<br>c'est de là qu'il prêche<br>au petit, au grand. </td>
	</tr>
	<tr>
	<td> 2 </td>
	<td> Les Anges chantent dans les airs<br>la paix du ciel à l'univers:<br>Douce mélodie! Venez, bergers, pressez le pas;<br>C'est le doux Messie, qui vous appelle et tend les bras:<br>Sa grâce infinie<br>vous attend là-bas. </td>
	</tr>
	<tr>
	<td> 3 </td>
	<td> Venez, ô Sages d'Orient,<br>voir pauvre un Dieu petit enfant!<br>Rendre vos hommages, offrir vos cœurs et vos cadeaux;<br>c'est le Roi des Sages, le Roi des rois des temps nouveaux!<br>Vous serez, Rois Mages,<br>les premiers héraults. </td>
	</tr>
	<tr>
	<td> 4 </td>
	<td> A notre tour, allons joyeux<br>donner nos cœurs au Roi des Cieux!<br>Dans son humble étable, il nous apprend la pauvreté;<br>Roi vainqueur aimable, a l'Homme il rend la liberté:<br>Grâce incomparable!<br>Gloire à sa bonté! </td>
	</tr>
	</table>

## Information
| | |
| - | - |
| Texte · Text  | *Psallam Deo*, Schola Cantorum de Montréal (1922) |
| Musique · Music | *In dulci jubilo* <br/> air allemand (XIVe siècle) |