---
tags:
  - Noël · Christmas
---

# O peuple fidèle



=== ":material-playlist-music: Partition · Score"

	<div id="adobe-dc-view" style="width: 100%;"></div>
	<script src="https://documentcloud.adobe.com/view-sdk/main.js"></script>
	<script type="text/javascript">
    	document.addEventListener("adobe_dc_view_sdk.ready", function(){
        	var adobeDCView = new AdobeDC.View({clientId: "d7518740bf7a4af7b4c3a8b5a7bcff85", divId: "adobe-dc-view"});
        	adobeDCView.previewFile({
            	content:{location: {url: "/livre-de-cantiques-anglicans/assets/pdf/o_peuple_fidele_ADESTE_FIDELES.pdf"}},
            	metaData:{fileName: "o_peuple_fidele_ADESTE_FIDELES.pdf"}}, {embedMode: "IN_LINE"});
    	});
	</script>

=== ":octicons-list-ordered-16: Paroles seulement · Lyrics only"

	<table style="font-size:16px">
	<tr>
	<td> 1 </td>
	<td> Ô peuple fidèle,<br>joyeux et triomphant,<br>Venez à Bethléem, venez en ces lieux!<br>Peuple fidèle,<br>venez voir le Roi des cieux!<br>Que votre amour l'implore,<br>que votre foi l'adore,<br>et qu'elle chante encore<br>ce don précieux! </td>
	</tr>
	<tr>
	<td> 2 </td>
	<td>   Dieu de  Dieu,<br>Lumière de Lumière,<br> il n'abhorre pas le sein de la Vierge.<br>Seul Fils de Dieu, né avant tous les siècles:<br>et qu'ils chantent encore,<br>le ciel et la terre! </td>
	</tr>
	<tr>
	<td> 3 </td>
	<td>    Chantez, les anges,<br>votre exultation,<br> chantez, ô chantez, vous citoyens célestes:<br>Gloire à Dieu, au plus haut des cieux! <br>et qu'elle chante encore<br>nos joies  terrestres! </td>
	</tr>
	<tr>
	<td> 4 </td>
	<td> Dans une humble étable<br>froide et misérable,<br>des bergers remplis d’amour lui forment sa cour!<br>Dans cette étable, accourez à votre tour!<br>et qu’elle chante encore<br>sa gloire en ce jour! </td>
	</tr>
	<tr>
	<td> 5 </td>
	<td> Troublant de mystère,<br>dans la crèche austère<br>est l’enfant qui reçoit notre adoration.<br>Nous l'aimons parce qu'il nous aima le premier!<br>Que votre amour l'implore,<br>que votre foi l'adore,<br>et qu’elle chante encore<br>les jours annoncés! </td>
	</tr>
	<tr>
	<td> 6 </td>
	<td> Saluons le Seigneur,<br>né ce matin joyeux;<br>Jésus, le Christ, que nous te glorifions.<br>Verbe de Dieu, Parole du Père!<br>Que votre amour l’implore,<br>que votre foi l’adore,<br>et qu’elle chante encore<br>qu’il vienne en chair! </td>
	</tr>
	</table>

## Information
| | |
| - | - |
| Titre original · Original title | Adeste fideles |
| Texte · Text  | Attr. John Francis Wade (1750);<br/>tr. en anglais Frederick Oakeley (1841);<br/>tr. en français Jean-François Borderies (1790), ad. Arlie Coles (2021) |
| Musique · Music | *Adeste fideles* <br/> attr. John Francis Wade (1750) |