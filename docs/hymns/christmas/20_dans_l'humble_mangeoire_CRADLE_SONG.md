---
tags:
  - Noël · Christmas
---

# Dans l'humble mangeoire



=== ":material-playlist-music: Partition · Score"

	<div id="adobe-dc-view" style="width: 100%;"></div>
	<script src="https://documentcloud.adobe.com/view-sdk/main.js"></script>
	<script type="text/javascript">
    	document.addEventListener("adobe_dc_view_sdk.ready", function(){
        	var adobeDCView = new AdobeDC.View({clientId: "d7518740bf7a4af7b4c3a8b5a7bcff85", divId: "adobe-dc-view"});
        	adobeDCView.previewFile({
            	content:{location: {url: "/livre-de-cantiques-anglicans/assets/pdf/dans_l'humble_mangeoire_CRADLE_SONG.pdf"}},
            	metaData:{fileName: "dans_l'humble_mangeoire_CRADLE_SONG.pdf"}}, {embedMode: "IN_LINE"});
    	});
	</script>

=== ":octicons-list-ordered-16: Paroles seulement · Lyrics only"

	<table style="font-size:16px">
	<tr>
	<td> 1 </td>
	<td> Dans l'humble mangeoire<br>qui faisait son lit,<br>petit Seigneur Jésus,<br>là, il s'endormit;<br>une étoile dans le ciel<br>l'illuminait:<br>petit Seigneur Jésus,<br>sur la paille en paix. </td>
	</tr>
	<tr>
	<td> 2 </td>
	<td> Les bestiaux beuglent,<br>puis il les entend;<br>petit Seigneur Jésus,<br>il ne pleure point.<br>J'aime Seigneur Jésus:<br>que tu me surveilles,<br>restant à mes côtés<br>jusqu'à mon réveil. </td>
	</tr>
	<tr>
	<td> 3 </td>
	<td> Donc, Seigneur Jésus, j'ose te demander:<br>que tu restes avec moi aussi pour m'aimer;<br>que tout enfant venant vers toi soit béni;<br>que l'on soit fait prêt pour ton règne infini. </td>
	</tr>
	</table>

## Information
| | |
| - | - |
| Titre original · Original title | Away in a manger |
| Texte · Text  | Anonyme (1885), str. 1-2;<br/>John T. McFarland (1892), str. 3;<br/>tr. Arlie Coles (2021) |
| Musique · Music | *Cradle Song* <br/> William J. Kirkpatrick (1895) |