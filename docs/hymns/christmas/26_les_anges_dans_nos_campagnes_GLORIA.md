---
tags:
  - Noël · Christmas
---

# Les anges dans nos campagnes



=== ":material-playlist-music: Partition · Score"

	<div id="adobe-dc-view" style="width: 100%;"></div>
	<script src="https://documentcloud.adobe.com/view-sdk/main.js"></script>
	<script type="text/javascript">
    	document.addEventListener("adobe_dc_view_sdk.ready", function(){
        	var adobeDCView = new AdobeDC.View({clientId: "d7518740bf7a4af7b4c3a8b5a7bcff85", divId: "adobe-dc-view"});
        	adobeDCView.previewFile({
            	content:{location: {url: "/livre-de-cantiques-anglicans/assets/pdf/les_anges_dans_nos_campagnes_GLORIA.pdf"}},
            	metaData:{fileName: "les_anges_dans_nos_campagnes_GLORIA.pdf"}}, {embedMode: "IN_LINE"});
    	});
	</script>

=== ":octicons-list-ordered-16: Paroles seulement · Lyrics only"

	<table style="font-size:16px">
	<tr>
	<td> 1 </td>
	<td> Les anges dans nos campagnes<br>ont entonné l'hymne des cieux.<br>Et l'écho de nos montagnes<br>redit ce chant mélodieux. </td>
	</tr>
	<tr>
	<td> 2 </td>
	<td> Bergers, pour qui cette fête?<br>Quel est l'objet de tous ces chants?<br>Quen vainqueur, quelle conquête<br>mérite ces cris triomphants? </td>
	</tr>
	<tr>
	<td> 3 </td>
	<td> Ils annoncent la naissance<br>du Libérateur d'Israël;<br>et pleins de reconnaissance,<br>chantent, en ce jour solennel:<br>Glo        ria<br>in excelsis Deo!<br>Glo        ria<br>in excelsis Deo! </td>
	</tr>
	<tr>
	<td> 4 </td>
	<td> Cherchons tous l'heureux village<br>qui l'a vu naître sous ses toits;<br>offrons-lui le tendre hommage,<br>et de nos cœurs et de nos voix: </td>
	</tr>
	<tr>
	<td> 5 </td>
	<td> Dans l'humilité profonde<br>où tu apparais à nos yeux;<br>pour te louer, Roi du monde,<br>nous redirons ce chant joyeux: </td>
	</tr>
	<tr>
	<td> 6 </td>
	<td> Toujours remplis du mystère<br>qu'opère aujourd'hui ton amour,<br>notre devoir sur la terre<br>sera de chanter, chaque jour: </td>
	</tr>
	<tr>
	<td> 7 </td>
	<td> Déjà les bienheureux anges,<br>les chérubins, les séraphins,<br>occupés de tes louanges,<br>ont appris à dire aux humains: </td>
	</tr>
	<tr>
	<td> 8 </td>
	<td> Dociles à leur exemple,<br>Seigneur, nous viendrons désormais<br>au milieu de ton saint temple,<br>chanter avec eux tes bienfaits. </td>
	</tr>
	</table>

## Information
| | |
| - | - |
| Texte · Text  | français, XVIIIe siècle |
| Musique · Music | *Gloria* <br/> air français, XVIe siècle |