---
tags:
  - Noël · Christmas
---

# Noël nouvelet



=== ":material-playlist-music: Partition · Score"

	<div id="adobe-dc-view" style="width: 100%;"></div>
	<script src="https://documentcloud.adobe.com/view-sdk/main.js"></script>
	<script type="text/javascript">
    	document.addEventListener("adobe_dc_view_sdk.ready", function(){
        	var adobeDCView = new AdobeDC.View({clientId: "d7518740bf7a4af7b4c3a8b5a7bcff85", divId: "adobe-dc-view"});
        	adobeDCView.previewFile({
            	content:{location: {url: "/livre-de-cantiques-anglicans/assets/pdf/noel_nouvelet_NOEL_NOUVELET.pdf"}},
            	metaData:{fileName: "noel_nouvelet_NOEL_NOUVELET.pdf"}}, {embedMode: "IN_LINE"});
    	});
	</script>

=== ":octicons-list-ordered-16: Paroles seulement · Lyrics only"

	<table style="font-size:16px">
	<tr>
	<td> 1 </td>
	<td> Noël nouvelet, Noël chantons ici:<br>Dévotés gens, rendons à  Dieu merci.<br>Chantons Noël pour le Roi nouvelet:<br>Noël nouvellet, Noël chantons ici! </td>
	</tr>
	<tr>
	<td> 2 </td>
	<td> Quand je m'éveillai, et j'eus assez dormi,<br>ouvris les yeux, vis un ar bre fleuri,<br>dont il sortait un bouton vermeillet: </td>
	</tr>
	<tr>
	<td> 3 </td>
	<td> Quand je le vis, mon cœur fut réjoui,<br>car grande clarté resplendissait de lui,<br>comme le soleil qui luit au matinet: </td>
	</tr>
	<tr>
	<td> 4 </td>
	<td> D'un oiselet après le chant ouï,<br>qui aux pasteurs disait: “par tez d'ici!<br>En Bethléem trouvèrent l'agnelet:” </td>
	</tr>
	<tr>
	<td> 5 </td>
	<td> En Bethléem, Marie et Joseph vis,<br>L'âne et le bœuf, l'Enfant couché au lit:<br>La crèche était au lieu d'un bercelet: </td>
	</tr>
	<tr>
	<td> 6 </td>
	<td> L'Étoile y vis, qui la nuit éclairait,<br>Qui d'Orient, dont il était sorti;<br>En Bethléem, les trois rois amenait: </td>
	</tr>
	<tr>
	<td> 7 </td>
	<td> L'un portait l'or, et l'autre myrrhe aussi,<br>Et l'autre encens, qui faisait bon senti;<br>De paradis semblait le jardinet: </td>
	</tr>
	<tr>
	<td> 8 </td>
	<td> Quarante jours, la nourrice attendit;<br>Entre les bras de Siméon tendit<br>Deux tourterelles dedans un paneret: </td>
	</tr>
	<tr>
	<td> 9 </td>
	<td> Quand Siméon le vit, fit un haut cri:<br>“Voici mon Dieu, mon Sauveur, Jésus-Christ!<br>Voici celui qui gloire au peuple met:” </td>
	</tr>
	<tr>
	<td> 10 </td>
	<td> Un prêtre vint, dont je fus ébahi,<br>Qui les paroles hautement entendit;<br>Puis les mussa dans un petit livret: </td>
	</tr>
	<tr>
	<td> 11 </td>
	<td> Et puis me dit: “Frère, crois-tu ceci?<br>Si tu y crois, au ciel seras ravi;<br>Si tu t'y fermes, crains l'Enfer au gibet:” </td>
	</tr>
	<tr>
	<td> 12 </td>
	<td> Voici mon Dieu, mon Sauveur Jésus-Christ;<br>Voici celui qui gloire au peuple met.<br>En trente jours fut Noël accompli: </td>
	</tr>
	<tr>
	<td> 13 </td>
	<td> En trente jours, Noël fut accompli;<br>Par douze vers, voici le chant fini;<br>Par chaque jour, on chante le couplet: </td>
	</tr>
	</table>

## Information
| | |
| - | - |
| Texte · Text  | français, XVe siècle;<br/>ad. Arlie Coles (2021), str. 11, 13 |
| Musique · Music | *Noël nouvelet* <br/> air français, XVe siècle |