---
tags:
  - Noël · Christmas
---

# Qui est cet Enfant endormi



=== ":material-playlist-music: Partition · Score"

	<div id="adobe-dc-view" style="width: 100%;"></div>
	<script src="https://documentcloud.adobe.com/view-sdk/main.js"></script>
	<script type="text/javascript">
    	document.addEventListener("adobe_dc_view_sdk.ready", function(){
        	var adobeDCView = new AdobeDC.View({clientId: "d7518740bf7a4af7b4c3a8b5a7bcff85", divId: "adobe-dc-view"});
        	adobeDCView.previewFile({
            	content:{location: {url: "/livre-de-cantiques-anglicans/assets/pdf/qui_est_cet_enfant_endormi_GREENSLEEVES.pdf"}},
            	metaData:{fileName: "qui_est_cet_enfant_endormi_GREENSLEEVES.pdf"}}, {embedMode: "IN_LINE"});
    	});
	</script>

=== ":octicons-list-ordered-16: Paroles seulement · Lyrics only"

	<table style="font-size:16px">
	<tr>
	<td> 1 </td>
	<td> Qui est cet Enfant endormi<br>aux genoux de sa Mère?<br>Chantent les anges ses louanges<br>aux bergers sur la terre.<br>Voici le Roi, le Christ,<br>qui les bergers ont prémuni:<br>Vite! Amènez le los<br>du cher Fils de Marie. </td>
	</tr>
	<tr>
	<td> 2 </td>
	<td> Pourquoi dort-il si humblement,<br>en paix parmi les bêtes?<br>Fin des terreurs: pour nous pêcheurs<br>le Verbe sans mot plaide. </td>
	</tr>
	<tr>
	<td> 3 </td>
	<td> De l'or, de l'encens, de la myrrhe:<br>offrons nos choses exquisses;<br>le Roi Jésus rend le salut;<br>que nos cœurs l'intronise. </td>
	</tr>
	</table>

## Information
| | |
| - | - |
| Titre original · Original title | What Child is this, who laid to rest |
| Texte · Text  | W. Chatterton Dix (1865)<br/>tr. Arlie Coles (2021) |
| Musique · Music | *Greensleeves* <br/> air anglais (XVIe siècle) |