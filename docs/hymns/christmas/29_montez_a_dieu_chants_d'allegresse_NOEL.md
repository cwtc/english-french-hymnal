---
tags:
  - Noël · Christmas
---

# Montez à Dieu, chants d'allegresse



=== ":material-playlist-music: Partition · Score"

	<div id="adobe-dc-view" style="width: 100%;"></div>
	<script src="https://documentcloud.adobe.com/view-sdk/main.js"></script>
	<script type="text/javascript">
    	document.addEventListener("adobe_dc_view_sdk.ready", function(){
        	var adobeDCView = new AdobeDC.View({clientId: "d7518740bf7a4af7b4c3a8b5a7bcff85", divId: "adobe-dc-view"});
        	adobeDCView.previewFile({
            	content:{location: {url: "/livre-de-cantiques-anglicans/assets/pdf/montez_a_dieu_chants_d'allegresse_NOEL.pdf"}},
            	metaData:{fileName: "montez_a_dieu_chants_d'allegresse_NOEL.pdf"}}, {embedMode: "IN_LINE"});
    	});
	</script>

=== ":octicons-list-ordered-16: Paroles seulement · Lyrics only"

	<table style="font-size:16px">
	<tr>
	<td> 1 </td>
	<td> Montez à Dieu, chants d'allégresse;<br>ô cœurs brûlés d'un saint amour:<br>chantez Noël; voici le jour!<br>Le ciel entier frémit d'ivresse;<br>que la nuit sombre disparaisse!<br>Montez à Dieu, chants d'allégresse! </td>
	</tr>
	<tr>
	<td> 2 </td>
	<td> Ô Vierge mère, berce encore<br>l'Enfant divin, et dans ses yeux<br>aspire la clarté des cieux!<br>De son regard, céleste aurore;<br>sur ton front pur qui se colore:<br>Voici  le  jour,   voici  le  jour! <br>Une auréolle semble éclore! </td>
	</tr>
	<tr>
	<td> 3 </td>
	<td> Ô Dieu des anges, je t'appelle:<br>Ô Dieu sauveur, j'entends le chœur<br>remplir les cieux d'un chant vainqueur!<br>Laisse à mon âme ouvrir son aile:<br>qu'elle s'envole et sente en elle:<br>Rayonner ta flamme éternelle! </td>
	</tr>
	</table>

## Information
| | |
| - | - |
| Texte · Text  | Jules Barbier (1825-1901)<br/>ad. Arlie Coles (2021) |
| Musique · Music | *Noël* <br/> Charles Gounod (1866) |