---
tags:
  - Noël · Christmas
---

# Que Dieu vous garde heureux, ô gens



=== ":material-playlist-music: Partition · Score"

	<div id="adobe-dc-view" style="width: 100%;"></div>
	<script src="https://documentcloud.adobe.com/view-sdk/main.js"></script>
	<script type="text/javascript">
    	document.addEventListener("adobe_dc_view_sdk.ready", function(){
        	var adobeDCView = new AdobeDC.View({clientId: "d7518740bf7a4af7b4c3a8b5a7bcff85", divId: "adobe-dc-view"});
        	adobeDCView.previewFile({
            	content:{location: {url: "/livre-de-cantiques-anglicans/assets/pdf/que_dieu_vous_garde_heureux_o_gens_GOD_REST_YOU_MERRY.pdf"}},
            	metaData:{fileName: "que_dieu_vous_garde_heureux_o_gens_GOD_REST_YOU_MERRY.pdf"}}, {embedMode: "IN_LINE"});
    	});
	</script>

=== ":octicons-list-ordered-16: Paroles seulement · Lyrics only"

	<table style="font-size:16px">
	<tr>
	<td> 1 </td>
	<td> Que Dieu vous garde heureux, ô gens;<br>Noël a son message:<br>Souvenezvous que Jésus Christ<br>jusqu'à la croix s'engage;<br>à sauver l'Homme de Satan<br>quand le péché ravage.<br>Ô nouvelles de la consolation,<br>consolation!<br>Ô nouvelles de la consolation! </td>
	</tr>
	<tr>
	<td> 2 </td>
	<td> De Dieu le Père céleste,<br>un ange est apparu;  <br>Et jusqu'à certains bergers<br>proclame la venue<br>D'un enfant dans une crèche,<br>le Fils de Dieu, Jésus.   </td>
	</tr>
	<tr>
	<td> 3 </td>
	<td> N'ayez crainte, dit l'ange  aux<br>bergers qui prirent peur;  <br>En ce jour de la Vierge<br>est né l'Enfant Sauveur,  <br>Pour libérer ses fidèles<br>du malin Tentateur.   </td>
	</tr>
	<tr>
	<td> 4 </td>
	<td> En arrivant à Bethléhem,<br>leur trajet accompli,  <br>Les bergers virent le Sauveur;<br>le foin était son lit;  <br>À son côté, à genoux, priait sa Mère Marie. </td>
	</tr>
	</table>

## Information
| | |
| - | - |
| Titre original · Original title | God rest you merry, gentlemen |
| Texte · Text  | Cantique anglais, XVIIe siècle;<br/>tr. en français Thomas W. Lyon (1987), ad. Arlie Coles (2021), str. 1-5;<br/>tr. Arlie Coles (2021), refrain |
| Musique · Music | *God rest you merry* <br/> air anglais, XVIIe siècle |