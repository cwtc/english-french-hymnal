---
tags:
  - Noël · Christmas
  - Enfants · Children
---

# Bambins et gamines



=== ":material-playlist-music: Partition · Score"

	<div id="adobe-dc-view" style="width: 100%;"></div>
	<script src="https://documentcloud.adobe.com/view-sdk/main.js"></script>
	<script type="text/javascript">
    	document.addEventListener("adobe_dc_view_sdk.ready", function(){
        	var adobeDCView = new AdobeDC.View({clientId: "d7518740bf7a4af7b4c3a8b5a7bcff85", divId: "adobe-dc-view"});
        	adobeDCView.previewFile({
            	content:{location: {url: "/livre-de-cantiques-anglicans/assets/pdf/bambins_et_gamines_IHR_KINDERLEIN_KOMMET.pdf"}},
            	metaData:{fileName: "bambins_et_gamines_IHR_KINDERLEIN_KOMMET.pdf"}}, {embedMode: "IN_LINE"});
    	});
	</script>

=== ":octicons-list-ordered-16: Paroles seulement · Lyrics only"

	<table style="font-size:16px">
	<tr>
	<td> 1 </td>
	<td> Bambins et gamines, venez, venez tous:<br>Merveille divine se passe chez nous!<br>Voyez dans la crèche l'Enfant nouveau-né<br>que dans la nuit fraîche Dieu nous a donné. </td>
	</tr>
	<tr>
	<td> 2 </td>
	<td> Une pauvre étable lui sert de maison,<br>ni chaise, ni table, rien que paille et son;<br>une humble chandelle suffit à l'Enfant<br>que le monde appelle le Dieu tout-puissant. </td>
	</tr>
	<tr>
	<td> 3 </td>
	<td> On n'a vu personne monter au clocher,<br>mais la cloche sonne pour le nouveau-né!<br>L'oiseau sur sa branche s'est mis a chanter;<br>l'œil de la pervenche s'est mis à briller. </td>
	</tr>
	<tr>
	<td> 4 </td>
	<td> Bergers et bergères portent leurs présents:<br>Dodo, petit frère! chantent les enfants;<br>mille anges folâtrent dans un rayon d'or;<br>les Mages se hâtent vers Jésus qui dort. </td>
	</tr>
	</table>

## Information
| | |
| - | - |
| Titre original · Original title | Ihr Kinderlein, kommet |
| Texte · Text  | inconnu, attr. chant alsacien |
| Musique · Music | *Ihr Kinderlein kommet* <br/> J. A. P. Schulz (1747-1800) |