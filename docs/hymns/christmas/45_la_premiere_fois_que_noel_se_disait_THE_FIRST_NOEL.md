---
tags:
  - Noël · Christmas
---

# La première fois que Noël se disait



=== ":material-playlist-music: Partition · Score"

	<div id="adobe-dc-view" style="width: 100%;"></div>
	<script src="https://documentcloud.adobe.com/view-sdk/main.js"></script>
	<script type="text/javascript">
    	document.addEventListener("adobe_dc_view_sdk.ready", function(){
        	var adobeDCView = new AdobeDC.View({clientId: "d7518740bf7a4af7b4c3a8b5a7bcff85", divId: "adobe-dc-view"});
        	adobeDCView.previewFile({
            	content:{location: {url: "/livre-de-cantiques-anglicans/assets/pdf/la_premiere_fois_que_noel_se_disait_THE_FIRST_NOEL.pdf"}},
            	metaData:{fileName: "la_premiere_fois_que_noel_se_disait_THE_FIRST_NOEL.pdf"}}, {embedMode: "IN_LINE"});
    	});
	</script>

=== ":octicons-list-ordered-16: Paroles seulement · Lyrics only"

	<table style="font-size:16px">
	<tr>
	<td> 1 </td>
	<td> La première fois que Noël se disait,<br>c'était des anges aux pauvres bergers;<br>ils gardaient aux champs leurs brebis,<br>malgré le vent froid de cet te nuit.<br>Noël, Noël, Noël, Noël!<br>Ici naît le Roi d'Israël! </td>
	</tr>
	<tr>
	<td> 2 </td>
	<td> Au-dessus d'eux, l'étoile brillait;<br>de l'Orient elle resplendissait;<br>et sur la terre elle luait,<br>et jour et nuit elle rayonnait. </td>
	</tr>
	<tr>
	<td> 3 </td>
	<td> Cet astre brillant  de  partout<br>fut par les trois rois mages vu;<br>ils cherchaient enfin un grand roi,<br>donc ils le suivaient où que ce soit. </td>
	</tr>
	<tr>
	<td> 4 </td>
	<td> L'étoile passa au nord-ouest,<br>jusqu'à Bethléem, ce signe céleste;<br>et là au ciel elle s'est figée<br>au-dessus de l'endroit où Jésus dormait. </td>
	</tr>
	<tr>
	<td> 5 </td>
	<td> Chacun des trois rois mages entra,<br>se prosternant devant leur Roi;<br>ils offrirent dans sa présence<br>leur or, leur myrrhe, et leur encens. </td>
	</tr>
	<tr>
	<td> 6 </td>
	<td> Qu'en pur et parfait unisson<br>les louanges du Seigneur nous chantions;<br>lui qui créa la terre et le ciel,<br>lui qui nous accorde la vie éternelle! </td>
	</tr>
	</table>

## Information
| | |
| - | - |
| Titre original · Original title | The first Noel the angel did say |
| Texte · Text  | anglais, c. XVIe siècle;<br/>tr. Arlie Coles (2021) |
| Musique · Music | *The First Noel* <br/> air anglais, c. XVIe siècle |