---
tags:
  - Noël · Christmas
---

# Petite ville, Bethléem



=== ":material-playlist-music: Partition · Score"

	<div id="adobe-dc-view" style="width: 100%;"></div>
	<script src="https://documentcloud.adobe.com/view-sdk/main.js"></script>
	<script type="text/javascript">
    	document.addEventListener("adobe_dc_view_sdk.ready", function(){
        	var adobeDCView = new AdobeDC.View({clientId: "d7518740bf7a4af7b4c3a8b5a7bcff85", divId: "adobe-dc-view"});
        	adobeDCView.previewFile({
            	content:{location: {url: "/livre-de-cantiques-anglicans/assets/pdf/petite_ville_bethleem_SAINT_LOUIS.pdf"}},
            	metaData:{fileName: "petite_ville_bethleem_SAINT_LOUIS.pdf"}}, {embedMode: "IN_LINE"});
    	});
	</script>

=== ":octicons-list-ordered-16: Paroles seulement · Lyrics only"

	<table style="font-size:16px">
	<tr>
	<td> 1 </td>
	<td> Petite ville Bethléem,<br>tu te reposes en paix!<br>Là quand tu dors, l'étoile d'or<br>en silence surviel- le.<br>Sa lumière éternelle<br>éclaire le ciel noir;<br>Nos peurs, nos espoirs sont, en toi,<br>bien exaucés ce soir. </td>
	</tr>
	<tr>
	<td> 2 </td>
	<td> Car Jésus de Marie est né,<br>et tandis que tout dort,<br>Les anges surveillent, assemblés,<br>chantent leurs doux accords.<br>Proclamez sa naisance,<br>étoiles du matin!<br>Chantez sa gloire, c'est le Roi;<br>voici l'Enfant divin. </td>
	</tr>
	<tr>
	<td> 3 </td>
	<td> Ah! Qu'il fut donné simplement<br>ce présent merveilleux!<br>Et Dieu bénit l'homme ainsi:<br>Il les ouvre les cieux.<br>Nul n'entend sa venue<br>dans ce monde pécheur.<br>Mais, bienvenu, entre Jésus,<br>dans les plus humbles cœurs. </td>
	</tr>
	<tr>
	<td> 4 </td>
	<td> Où les enfants heureux et purs<br>se tournent à l'Enfant Saint,<br>Où la misère voit la Mère<br>avec son Fils divin,<br>Où l'on voit la charité,<br>on sait que Dieu est là.<br>Comme au ciel, fêtons Noël<br>encore une fois. </td>
	</tr>
	</table>

## Information
| | |
| - | - |
| Titre original · Original title | O little town of Bethlehem |
| Texte · Text  | Phillips Brooks (1835-1893);<br/>tr. inconnu, ad. Arlie Coles (2021), str. 1-3;<br/>tr. Arlie Coles (2021), str. 4<br/>tr. Thomas W. Lyon (1988), ad. Arlie Coles (2021), str. 5 |
| Musique · Music | *Saint Louis* <br/> Lewis H. Redner (1831-1908) |