---
tags:
  - Noël · Christmas
---

# D'un arbre séculaire



=== ":material-playlist-music: Partition · Score"

	<div id="adobe-dc-view" style="width: 100%;"></div>
	<script src="https://documentcloud.adobe.com/view-sdk/main.js"></script>
	<script type="text/javascript">
    	document.addEventListener("adobe_dc_view_sdk.ready", function(){
        	var adobeDCView = new AdobeDC.View({clientId: "d7518740bf7a4af7b4c3a8b5a7bcff85", divId: "adobe-dc-view"});
        	adobeDCView.previewFile({
            	content:{location: {url: "/livre-de-cantiques-anglicans/assets/pdf/d'un_arbre_seculaire_ES_IST_EIN_ROS.pdf"}},
            	metaData:{fileName: "d'un_arbre_seculaire_ES_IST_EIN_ROS.pdf"}}, {embedMode: "IN_LINE"});
    	});
	</script>

=== ":octicons-list-ordered-16: Paroles seulement · Lyrics only"

	<table style="font-size:16px">
	<tr>
	<td> 1 </td>
	<td> D'un arbre séculaire,<br>durant l'hiver austère,<br>Et sur le sol durci,<br>dans la nuit calme et claire,<br>une Rose a fleuri. </td>
	</tr>
	<tr>
	<td> 2 </td>
	<td> Prévu par Isaïe,<br>la Rose mentionnée:<br>qui le Bouton portait.<br>L'amour de Dieu montré:<br>dans la nuit calme et claire,<br>un Sauveur pour nous naît. </td>
	</tr>
	<tr>
	<td> 3 </td>
	<td> Ô Fleur, dont la fragrance<br>douce comble l'air,<br>en ta splendeur, chasse<br>d'ici les ténèbres.<br>Vrai Homme,  et vrai Dieu,<br>sauve-nous de la mort, et<br>partage nos fardeaux. </td>
	</tr>
	</table>

## Information
| | |
| - | - |
| Titre original · Original title | Es ist ein Ros entsprungen |
| Texte · Text  | allemand (XVe siècle);<br/>tr. en anglais Theodore Baker (1894);<br/>tr. en français inconnu, ad. Arlie Coles (2021), str. 1;<br/>tr. en français Arlie Coles (2021), str. 2-3 |
| Musique · Music | *Es ist ein Ros* <br/> *Alte Catholische Geisliche Kirchengasäng* (1599)<br/>harm. Michael Praetorius (1609) |