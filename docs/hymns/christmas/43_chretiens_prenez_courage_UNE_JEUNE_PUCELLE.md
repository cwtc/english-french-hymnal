---
tags:
  - Noël · Christmas
---

# Chrétiens, prenez courage



=== ":material-playlist-music: Partition · Score"

	<div id="adobe-dc-view" style="width: 100%;"></div>
	<script src="https://documentcloud.adobe.com/view-sdk/main.js"></script>
	<script type="text/javascript">
    	document.addEventListener("adobe_dc_view_sdk.ready", function(){
        	var adobeDCView = new AdobeDC.View({clientId: "d7518740bf7a4af7b4c3a8b5a7bcff85", divId: "adobe-dc-view"});
        	adobeDCView.previewFile({
            	content:{location: {url: "/livre-de-cantiques-anglicans/assets/pdf/chretiens_prenez_courage_UNE_JEUNE_PUCELLE.pdf"}},
            	metaData:{fileName: "chretiens_prenez_courage_UNE_JEUNE_PUCELLE.pdf"}}, {embedMode: "IN_LINE"});
    	});
	</script>

=== ":octicons-list-ordered-16: Paroles seulement · Lyrics only"

	<table style="font-size:16px">
	<tr>
	<td> 1 </td>
	<td> Chrétiens, prenez courage:<br>Jésus Sauveur est né!<br>Du malin les ouvrages<br>à jamais sont ruinés.<br>Quand il chante merveille<br>à ces troublants appas, <br>ne  prêtez plus l'oreille: </td>
	</tr>
	<tr>
	<td> 2 </td>
	<td> Oyez cette nouvelle<br>dont un ange est porteur!<br>Oyez, âmes fidèles,<br>et dilatez vos cœurs.<br>La Vierge dans l'étable<br>entoura de ses bras <br>l'Enfant Dieu adorable: </td>
	</tr>
	<tr>
	<td> 3 </td>
	<td> Voici que trois Rois Mages,<br>perdus en Orient,<br>déchiffrent ce message<br>écrit au firmament:<br>L'Astre nouveau les han te;<br>ils la suivront là-bas, <br>cette étoile marchante:<br>Jésus est né!<br>In excelsis gloria! </td>
	</tr>
	<tr>
	<td> 4 </td>
	<td> Jésus leur met en tête<br>que l'Etoile en la nuit<br>qui jamais ne s'arrête<br>les conduira vers lui.<br>Dans la nuit radieuse,<br>en route ils sont déjà; <br>ils vont l'âme joyeuse: </td>
	</tr>
	</table>

## Information
| | |
| - | - |
| Titre original · Original title | Jesous Ahatonia |
| Texte · Text  | Saint Jean de Brébeuf (1641)<br/>tr. Paul Picard (Tsawenhohi) (date inconnue) |
| Musique · Music | *Une jeune pucelle* <br/> air français (XVIe siècle) |