---
tags:
  - Annonciation · Annunciation
  - Vierge Marie · Virgin Mary
---

# Quand Gabriel du ciel est descendu



=== ":material-playlist-music: Partition · Score"

	<div id="adobe-dc-view" style="width: 100%;"></div>
	<script src="https://documentcloud.adobe.com/view-sdk/main.js"></script>
	<script type="text/javascript">
    	document.addEventListener("adobe_dc_view_sdk.ready", function(){
        	var adobeDCView = new AdobeDC.View({clientId: "d7518740bf7a4af7b4c3a8b5a7bcff85", divId: "adobe-dc-view"});
        	adobeDCView.previewFile({
            	content:{location: {url: "/livre-de-cantiques-anglicans/assets/pdf/quand_gabriel_du_ciel_est_descendu_GABRIELS_MESSAGE.pdf"}},
            	metaData:{fileName: "quand_gabriel_du_ciel_est_descendu_GABRIELS_MESSAGE.pdf"}}, {embedMode: "IN_LINE"});
    	});
	</script>

=== ":octicons-list-ordered-16: Paroles seulement · Lyrics only"

	<table style="font-size:16px">
	<tr>
	<td> 1 </td>
	<td> Quand Gabriel du ciel est descendu,<br>les yeux brûlant, il dit: “Je te salue!<br>Tu es bénie, et le Seigneur est avec toi,<br>Marie, pleine de grâce.”<br>Gloria! </td>
	</tr>
	<tr>
	<td> 2 </td>
	<td> “On te connaîtra, Mère valeureuse;<br>tous les âges te diront bienheureuse!<br>Ton fils sera Emmanuel, Fils du Très-Haut, </td>
	</tr>
	<tr>
	<td> 3 </td>
	<td> Puis Marie dit en humilité:<br>“Qu'il me soit fait selon sa volonté!<br>Mon âme exalte le Seigneur,” proclama-t-elle. </td>
	</tr>
	<tr>
	<td> 4 </td>
	<td> Son fruit fut le Christ, Emmanuel,<br>à Bethléem le matin de Noël.<br>Les chrétiens diront pour des siècles des siècles: </td>
	</tr>
	</table>

## Information
| | |
| - | - |
| Titre original · Original title | The angel Gabriel from heaven came |
| Texte · Text  | basque, XIIIe siècle;<br/>tr. en anglais Sabine Baring-Gould (1834-1924)<br/>tr. en français Arlie Coles (2021) |
| Musique · Music | *Gabriel's Message* <br/> air basque, XIIIe siècle;<br/>ad. Edgar Pettman (1892) |