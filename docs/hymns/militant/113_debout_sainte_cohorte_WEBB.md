---
tags:
  - L'Eglise militante · The Church militant
---

# Debout, sainte cohorte



=== ":material-playlist-music: Partition · Score"

	<div id="adobe-dc-view" style="width: 100%;"></div>
	<script src="https://documentcloud.adobe.com/view-sdk/main.js"></script>
	<script type="text/javascript">
    	document.addEventListener("adobe_dc_view_sdk.ready", function(){
        	var adobeDCView = new AdobeDC.View({clientId: "d7518740bf7a4af7b4c3a8b5a7bcff85", divId: "adobe-dc-view"});
        	adobeDCView.previewFile({
            	content:{location: {url: "/livre-de-cantiques-anglicans/assets/pdf/debout_sainte_cohorte_WEBB.pdf"}},
            	metaData:{fileName: "debout_sainte_cohorte_WEBB.pdf"}}, {embedMode: "IN_LINE"});
    	});
	</script>

=== ":octicons-list-ordered-16: Paroles seulement · Lyrics only"

	<table style="font-size:16px">
	<tr>
	<td> 1 </td>
	<td> Debout, sainte cohorte,<br>soldats du Roi des rois!<br>Tenez d'une main forte<br>l'étendard de la croix!<br>Au sentier de la gloire<br>Jésus-Christ nous conduit;<br>de victoire en victoire<br>il mène qui le suit. </td>
	</tr>
	<tr>
	<td> 2 </td>
	<td> La trompette résonne:<br>Debout, vaillants soldats!<br>L'immortelle couronne<br>est le prix des combats.<br>Si l'ennemi fait rage,<br>soyez fermes et forts;<br>redoublez de courage<br>s'il redouble d'efforts. </td>
	</tr>
	<tr>
	<td> 3 </td>
	<td> Debout pour la bataille!<br>Partez, n'hésitez plus!<br>Si votre bas défaille,<br>regardez à Jésus!<br>De l'armure invincible,<br>soldats, revêtez vous!<br>Le triomphe est possible<br>pour qui lutte à genoux. </td>
	</tr>
	<tr>
	<td> 4 </td>
	<td> Debout, debout encore!<br>Luttez jusqu'au matin;<br>déjà brille l'aurore<br>à l'horizon lointain.<br>Bientôt jetant nos armes<br>au pieds du Roi des rois!<br>Les chants après les larmes,<br>le trône après la croix. </td>
	</tr>
	</table>

## Information
| | |
| - | - |
| Titre original · Original title | The Church's one foundation |
| Texte · Text  | George Duffield (1858);<br/>tr. Ruben Sailens (1855-1942) |
| Musique · Music | *Webb* <br/> George James Webb (1837) |