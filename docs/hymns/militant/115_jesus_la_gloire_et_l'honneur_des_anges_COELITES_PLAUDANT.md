---
tags:
  - L'Eglise militante · The Church militant
---

# Jésus, la gloire et l'honneur des anges



=== ":material-playlist-music: Partition · Score"

	<div id="adobe-dc-view" style="width: 100%;"></div>
	<script src="https://documentcloud.adobe.com/view-sdk/main.js"></script>
	<script type="text/javascript">
    	document.addEventListener("adobe_dc_view_sdk.ready", function(){
        	var adobeDCView = new AdobeDC.View({clientId: "d7518740bf7a4af7b4c3a8b5a7bcff85", divId: "adobe-dc-view"});
        	adobeDCView.previewFile({
            	content:{location: {url: "/livre-de-cantiques-anglicans/assets/pdf/jesus_la_gloire_et_l'honneur_des_anges_COELITES_PLAUDANT.pdf"}},
            	metaData:{fileName: "jesus_la_gloire_et_l'honneur_des_anges_COELITES_PLAUDANT.pdf"}}, {embedMode: "IN_LINE"});
    	});
	</script>

=== ":octicons-list-ordered-16: Paroles seulement · Lyrics only"

	<table style="font-size:16px">
	<tr>
	<td> 1 </td>
	<td> Jésus, la gloire et l'honneur des anges,<br>toi qui nous créa, toi qui sur nous règne:<br>ô, de ta bonté, donne à tes serviteurs<br>de s'approcher au ciel. </td>
	</tr>
	<tr>
	<td> 2 </td>
	<td> Envoie ton saint Michel l'archange,<br>le conciliateur; qu'il expulse de nous<br>tout propos haineux, pour que la paix éclore,<br>et que tout prospère. </td>
	</tr>
	<tr>
	<td> 3 </td>
	<td> Envoie ton saint Gabriel l'archange,<br>le fort hérault; qu'il protège les mortels,<br>chassant le serpent, surveillant les temples<br>où tu es loué. </td>
	</tr>
	<tr>
	<td> 4 </td>
	<td> Envoie ton saint Raphaël l'archange,<br>le restaurateur de tous qui s'égarent,<br>qui, à ton ordre, fortifie nos corps<br>par ton sainte onction. </td>
	</tr>
	<tr>
	<td> 5 </td>
	<td> Que la Vierge Mère du Dieu Sauveur,<br>que l'assemblée de tous les saints triomphants,<br>que les nombreuse armées célestes des anges<br>nous aident à jamais. </td>
	</tr>
	<tr>
	<td> 6 </td>
	<td> Père tout-puissant, Fils, Saint-Esprit égal,<br>Dieu saint et fort, sois toujours notre gardien;<br>à toi la gloire que les anges louent,<br>voilant leurs visages. </td>
	</tr>
	</table>

## Information
| | |
| - | - |
| Titre original · Original title | Christus sanctorum decus angelorum |
| Texte · Text  | Rabanus Maurus (776-856);<br/>tr. en anglais Percy Dearmer (1867-1936);<br/>tr. en français Arlie Coles (2021) |
| Musique · Music | *Coelites plaudant* <br/> *Antiphoner*, Rouen (1728) |