---
tags:
  - L'Eglise militante · The Church militant
---

# L'église universelle a pour roc Jésus Christ



=== ":material-playlist-music: Partition · Score"

	<div id="adobe-dc-view" style="width: 100%;"></div>
	<script src="https://documentcloud.adobe.com/view-sdk/main.js"></script>
	<script type="text/javascript">
    	document.addEventListener("adobe_dc_view_sdk.ready", function(){
        	var adobeDCView = new AdobeDC.View({clientId: "d7518740bf7a4af7b4c3a8b5a7bcff85", divId: "adobe-dc-view"});
        	adobeDCView.previewFile({
            	content:{location: {url: "/livre-de-cantiques-anglicans/assets/pdf/l'eglise_universelle_a_pour_roc_jesus_christ_AURELIA.pdf"}},
            	metaData:{fileName: "l'eglise_universelle_a_pour_roc_jesus_christ_AURELIA.pdf"}}, {embedMode: "IN_LINE"});
    	});
	</script>

=== ":octicons-list-ordered-16: Paroles seulement · Lyrics only"

	<table style="font-size:16px">
	<tr>
	<td> 1 </td>
	<td> L'église universelle<br>a pour roc Jésus Christ;<br>Elle est l'œuvre nouvelle<br>que sa parole fit;<br>Habitant le ciel même<br>il vint se l'attacher,<br>et, par un don suprême,<br>mourut pour la sauver. </td>
	</tr>
	<tr>
	<td> 2 </td>
	<td> L'église en sa prière<br>unit à leur Sauveur;<br>Les peuples de la terre<br>soumis au seul Seigneur.<br>C'est son Nom qu'elle acclamme,<br>son pain, qui la nourrit;<br>Elle verse à tout âme<br>l'espoir qui la guérit. </td>
	</tr>
	<tr>
	<td> 3 </td>
	<td> Honnie et méconnue,<br>menant de durs combats,<br>elle attend la venue<br>de la paix ici-bas.<br>Contemplant par avance<br>la fin de son tourment,<br>la grande délivrance,<br>le repos perma nent. </td>
	</tr>
	<tr>
	<td> 4 </td>
	<td> Aujourd'hui, sur la terre<br>elle est unie à Dieu,<br>et, par un saint mystère,<br>aux élus du saint lieu.<br>Rends-nous, comme eux, fidèles,<br>Et reçois-nous, Seigneur,<br>dans la vie éternel--le,<br>dans l'éternel bonheur. </td>
	</tr>
	</table>

## Information
| | |
| - | - |
| Titre original · Original title | The Church's one foundation |
| Texte · Text  | Samuel J. Stone (1886);<br/>tr. Fernand Barth (date inconnue) |
| Musique · Music | *Aurelia* <br/> Samuel S. Wesley (1864) |