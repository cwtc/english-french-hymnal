---
tags:
  - Louange · Praise
---

# Torrents d'amour et de grâce



=== ":material-playlist-music: Partition · Score"

	<div id="adobe-dc-view" style="width: 100%;"></div>
	<script src="https://documentcloud.adobe.com/view-sdk/main.js"></script>
	<script type="text/javascript">
    	document.addEventListener("adobe_dc_view_sdk.ready", function(){
        	var adobeDCView = new AdobeDC.View({clientId: "d7518740bf7a4af7b4c3a8b5a7bcff85", divId: "adobe-dc-view"});
        	adobeDCView.previewFile({
            	content:{location: {url: "/livre-de-cantiques-anglicans/assets/pdf/torrents_d'amour_et_de_grace_EBENEZER.pdf"}},
            	metaData:{fileName: "torrents_d'amour_et_de_grace_EBENEZER.pdf"}}, {embedMode: "IN_LINE"});
    	});
	</script>

=== ":octicons-list-ordered-16: Paroles seulement · Lyrics only"

	<table style="font-size:16px">
	<tr>
	<td> 1 </td>
	<td> Torrents d'amour et de grâce,<br>amour du Sauveur en croix!<br>À ce grand fleuve qui passe,<br>je m'abandonne et je crois.<br>Je crois à ton sacrifice,<br>Ô Jésus, Agneu de Dieu,<br>et, couvert par ta justice,<br>j'entrerai dans le saint lieu. </td>
	</tr>
	<tr>
	<td> 2 </td>
	<td> Ah! Que partou se répande<br>ce fleuve à la grande voix:<br>Que tout l'univers entende<br>l'appel qui vient de la croix! </td>
	</tr>
	<tr>
	<td> 3 </td>
	<td> Que toute âme condamnée <br>pour qui tu versas ton sang<br>soit au Père ramennée <br>par ton amour tout puissant. </td>
	</tr>
	</table>

## Information
| | |
| - | - |
| Titre original · Original title | O the sweet, sweet love of Jesus |
| Texte · Text  | S. Trevor Francis;<br/>tr. Ruben Sailens (1855-1942) |
| Musique · Music | *Ebenezer* <br/> Thomas John Williams (1890) |