---
tags:
  - Louange · Praise
---

# Le grand Dieu d'Abraham



=== ":material-playlist-music: Partition · Score"

	<div id="adobe-dc-view" style="width: 100%;"></div>
	<script src="https://documentcloud.adobe.com/view-sdk/main.js"></script>
	<script type="text/javascript">
    	document.addEventListener("adobe_dc_view_sdk.ready", function(){
        	var adobeDCView = new AdobeDC.View({clientId: "d7518740bf7a4af7b4c3a8b5a7bcff85", divId: "adobe-dc-view"});
        	adobeDCView.previewFile({
            	content:{location: {url: "/livre-de-cantiques-anglicans/assets/pdf/le_grand_dieu_d'abraham_LEONI.pdf"}},
            	metaData:{fileName: "le_grand_dieu_d'abraham_LEONI.pdf"}}, {embedMode: "IN_LINE"});
    	});
	</script>

=== ":octicons-list-ordered-16: Paroles seulement · Lyrics only"

	<table style="font-size:16px">
	<tr>
	<td> 1 </td>
	<td> Le grand Dieu d'Abraham,<br>le vrai Ancien des Jours,<br>il règne pour l'éternité,<br>ce Dieu d'amour.<br>Il est le grand Je Suis!<br>Seigneur, Adonaï!<br>Adorons donc le Nom béni<br>à l'infini. </td>
	</tr>
	<tr>
	<td> 2 </td>
	<td> Le grand Dieu d'Abraham<br>donna son mot suprême,<br>et je fus formé de la terre, et<br>donc je l'aime.<br>Le pouvoir j'abandonne,<br>et toute sa sagesse;<br>j'accepte tout ce qu'il me donne,<br>ma forteresse. </td>
	</tr>
	<tr>
	<td> 3 </td>
	<td> Mon Sauveur et mon Dieu<br>m'a juré son amour.<br>Je monterai jusques aux cieux<br>plus haut toujours.<br>Là je verrai mon Roi<br>et, louant ses bienfaits,<br>chanterai sa gloire avec foi<br>à tout jamais. </td>
	</tr>
	<tr>
	<td> 4 </td>
	<td> Sur les monts de Sion<br>il demeure à jamais,<br>victorieux des passions<br>en toute paix:<br>Il vit avec les saints,<br>il a la royauté;<br>il accomplit tous ses desseins<br>dans la clarté. </td>
	</tr>
	<tr>
	<td> 5 </td>
	<td> Au Dieu qui règne en haut<br>chantent tous les archanges:<br>“À toi, grand Dieu, toi, trois fois saint,<br>Roi tout-puissant;<br>Comme au commencement,<br>à jamais, maintenant,<br>O Père, Seigneur, grand Je Suis,<br>Nous te louons!” </td>
	</tr>
	<tr>
	<td> 6 </td>
	<td> Le corps des anges uni<br>remercie leur Dieu:<br>“Au Père, Fils, et Saint-Esprit,”<br>dans chaque lieu.<br>Il est Dieu, saint et fort,<br>d'Abraham et de moi;<br>je loue donc pour tous mes jours<br>ce Dieu mon Roi. </td>
	</tr>
	</table>

## Information
| | |
| - | - |
| Titre original · Original title | The God of Abraham praise |
| Texte · Text  | Daniel ben Judah, XIVe siècle;<br/>ad. en anglais Thomas Olivers (1770);<br/>tr. en français Arlie Coles (2022), str. 1-2, 5-6<br/>tr. en français Flossette du Pasquier (née 1922), str. 3-4 |
| Musique · Music | *Leoni* <br/> Meyer Lyon (1770) |