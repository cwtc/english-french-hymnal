---
tags:
  - Louange · Praise
---

# Beau Seigneur Jésus



=== ":material-playlist-music: Partition · Score"

	<div id="adobe-dc-view" style="width: 100%;"></div>
	<script src="https://documentcloud.adobe.com/view-sdk/main.js"></script>
	<script type="text/javascript">
    	document.addEventListener("adobe_dc_view_sdk.ready", function(){
        	var adobeDCView = new AdobeDC.View({clientId: "d7518740bf7a4af7b4c3a8b5a7bcff85", divId: "adobe-dc-view"});
        	adobeDCView.previewFile({
            	content:{location: {url: "/livre-de-cantiques-anglicans/assets/pdf/beau_seigneur_jesus_SAINTE_ELISABETH.pdf"}},
            	metaData:{fileName: "beau_seigneur_jesus_SAINTE_ELISABETH.pdf"}}, {embedMode: "IN_LINE"});
    	});
	</script>

=== ":octicons-list-ordered-16: Paroles seulement · Lyrics only"

	<table style="font-size:16px">
	<tr>
	<td> 1 </td>
	<td> Beau Seigneur Jésus,<br>Roi de la nature,<br>toi, Fils de l'Homme et Fils de Dieu:<br>je te chérirai;<br>ô, je t'honorai,<br>toi, la prunelle de mes yeux. </td>
	</tr>
	<tr>
	<td> 2 </td>
	<td> Beaux sont les grands prés;<br>beaux sont les lieux boisés<br>de nouveau en fleur au printemps;<br>Plus bel est Jésus;<br>plus pur est Jésus,<br>qui redonne aux cœurs leur doux chant. </td>
	</tr>
	<tr>
	<td> 3 </td>
	<td> Bel est le soleil;<br>plus belle est la lune<br>et l'armée étoilée des cieux;<br>Jésus rayonne;<br>Jésus, il brille<br>plus que tous les anges en ce lieu. </td>
	</tr>
	</table>

## Information
| | |
| - | - |
| Titre original · Original title | Schönster Herr Jesu |
| Texte · Text  | inconnu, allemand (1662);<br/>tr. en anglais inconnu (1850)<br/>tr. en français Arlie Coles (2021) |
| Musique · Music | *Sainte Elisabeth* <br/> air silésien, *Schlesische Volkslieder* (1842) |