---
tags:
  - Louange · Praise
---

# Guide-moi, Berger fidèle



=== ":material-playlist-music: Partition · Score"

	<div id="adobe-dc-view" style="width: 100%;"></div>
	<script src="https://documentcloud.adobe.com/view-sdk/main.js"></script>
	<script type="text/javascript">
    	document.addEventListener("adobe_dc_view_sdk.ready", function(){
        	var adobeDCView = new AdobeDC.View({clientId: "d7518740bf7a4af7b4c3a8b5a7bcff85", divId: "adobe-dc-view"});
        	adobeDCView.previewFile({
            	content:{location: {url: "/livre-de-cantiques-anglicans/assets/pdf/guide_moi_berger_fidele_CWM_RHONDDA.pdf"}},
            	metaData:{fileName: "guide_moi_berger_fidele_CWM_RHONDDA.pdf"}}, {embedMode: "IN_LINE"});
    	});
	</script>

=== ":octicons-list-ordered-16: Paroles seulement · Lyrics only"

	<table style="font-size:16px">
	<tr>
	<td> 1 </td>
	<td> Guide moi, Berger fidèle,<br>En ce monde pélerin.<br>Prends à toi mon coeur rebelle,<br>Guide moi, sois mon soutien.<br>Pain de vie, pain de vie,<br>De ta grâce nourris moi!<br>De ta grâce nourris moi! </td>
	</tr>
	<tr>
	<td> 2 </td>
	<td> Christ, tu es la source vive<br>Des biens les plus précieux.<br>Que la colonne de feu je suive;<br>Guide moi, du haut des cieux.<br>Viens, Protecteur; viens, Protecteur;<br>Sois mon roc, mon bouclier!<br>Sois mon roc, mon bouclier! </td>
	</tr>
	<tr>
	<td> 3 </td>
	<td> Du Jourdain, je suis les rives,<br>Quand j'ai peur, rassure-moi.<br>À Sion, qu'enfin j'arrive,<br>Affermis ma faible foi.<br>Tes louanges, tes louanges,<br>À jamais je chanterai!<br>À jamais je chanterai! </td>
	</tr>
	</table>

## Information
| | |
| - | - |
| Titre original · Original title | Arglwydd arwain trwy'r anialwch |
| Texte · Text  | William Williams (1745);<br/>tr. en anglais Peter Williams (1771);<br/>tr. en français Flossette du Pasquier (née 1922), ad. Arlie Coles (2021), str. 2 |
| Musique · Music | *Cwm Rhondda* <br/> attr. John Hughes (1907) |