---
tags:
  - Louange · Praise
---

# Vous, créatures du Seigneur



=== ":material-playlist-music: Partition · Score"

	<div id="adobe-dc-view" style="width: 100%;"></div>
	<script src="https://documentcloud.adobe.com/view-sdk/main.js"></script>
	<script type="text/javascript">
    	document.addEventListener("adobe_dc_view_sdk.ready", function(){
        	var adobeDCView = new AdobeDC.View({clientId: "d7518740bf7a4af7b4c3a8b5a7bcff85", divId: "adobe-dc-view"});
        	adobeDCView.previewFile({
            	content:{location: {url: "/livre-de-cantiques-anglicans/assets/pdf/vous_creatures_du_seigneur_LASST_UNS_ERFREUEN.pdf"}},
            	metaData:{fileName: "vous_creatures_du_seigneur_LASST_UNS_ERFREUEN.pdf"}}, {embedMode: "IN_LINE"});
    	});
	</script>

=== ":octicons-list-ordered-16: Paroles seulement · Lyrics only"

	<table style="font-size:16px">
	<tr>
	<td> 1 </td>
	<td> Vous créatures du Seigneur,<br>chantez toujours en son honneur,<br>Alléluia, alléluia!<br>Car c'est lui seul qu'il faut louer,<br>Il donne au soleil sa clarté,<br>Rendez gloire, rendez gloire,<br>Alléluia, alléluia,<br>alléluia! </td>
	</tr>
	<tr>
	<td> 2 </td>
	<td> Dieu, sois loué pour le soleil,<br>pour ce grand frère sans pareil,<br>Et pour la lune et sa lueur,<br>Pour chaque étoile notre sœur, </td>
	</tr>
	<tr>
	<td> 3 </td>
	<td> Loué soistu pour sire feu,<br>vivant, robuste, glorieux,<br>La terre en maternelle sœur<br>Nous comble de ses mille fleurs, </td>
	</tr>
	<tr>
	<td> 4 </td>
	<td> Dieu trois fois saint, nous te louons,<br>nous te chantons, nous t'adorons,<br>Gloire au Père et louange au Fils,<br>Et loué soit le SaintEsprit, </td>
	</tr>
	</table>

## Information
| | |
| - | - |
| Titre original · Original title | Altissimu, omnipotente bon Signore |
| Texte · Text  | St. François d'Assise (1225);<br/>ad. en anglais William H. Draper (1910);<br/>tr. en français Jean-Jacques Bovet (1904) |
| Musique · Music | *Lasst uns erfreuen* <br/> *Auserlesen Catholische Geistliche Kirchengesäng* (1623) |