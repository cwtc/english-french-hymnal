---
tags:
  - Louange · Praise
---

# Mon Dieu, plus près de toi



=== ":material-playlist-music: Partition · Score"

	<div id="adobe-dc-view" style="width: 100%;"></div>
	<script src="https://documentcloud.adobe.com/view-sdk/main.js"></script>
	<script type="text/javascript">
    	document.addEventListener("adobe_dc_view_sdk.ready", function(){
        	var adobeDCView = new AdobeDC.View({clientId: "d7518740bf7a4af7b4c3a8b5a7bcff85", divId: "adobe-dc-view"});
        	adobeDCView.previewFile({
            	content:{location: {url: "/livre-de-cantiques-anglicans/assets/pdf/mon_dieu_plus_pres_de_toi_BETHANY.pdf"}},
            	metaData:{fileName: "mon_dieu_plus_pres_de_toi_BETHANY.pdf"}}, {embedMode: "IN_LINE"});
    	});
	</script>

=== ":octicons-list-ordered-16: Paroles seulement · Lyrics only"

	<table style="font-size:16px">
	<tr>
	<td> 1 </td>
	<td> Mon Dieu, plus près de toi,<br>plus près de toi!<br>C'est le cri de ma foi:<br>plus près de toi.<br>Dans les jours où l'épreuve<br>déborde comme un fleuve,<br>Tiensmoi plus près de toi,<br>plus près de toi! </td>
	</tr>
	<tr>
	<td> 2 </td>
	<td> Plus près de toi, Seigneur,<br>plus près de toi!<br>Tiensmoi dans ma douleur<br>tout près de toi!<br>Alors que la souffrance<br>fait son œuvre en silence,<br>Toujours, Seigneur, tiensmoi<br>plus près de toi! </td>
	</tr>
	<tr>
	<td> 3 </td>
	<td> Plus près de toi, toujours<br>plus près de toi!<br>Donne moi ton secours;<br>soutiens ma foi!<br>Que Satan se déchaine,<br>ton amour me ramène<br>Toujours plus près de toi,<br>plus près de toi! </td>
	</tr>
	<tr>
	<td> 4 </td>
	<td> Mon Dieu, plus près de toi,<br>dans le désert,<br>J'ai vu plus près de toi<br>ton ciel ouvert.<br>Pèlerin, bon courage!<br>Ton chant brave l'orage:<br>Mon Dieu, plus près de toi,<br>plus près de toi! </td>
	</tr>
	<tr>
	<td> 5 </td>
	<td>  </td>
	</tr>
	</table>

## Information
| | |
| - | - |
| Titre original · Original title | Nearer, my God, to thee |
| Texte · Text  | Sarah F. Adams (1841);<br/>tr. Charles Châtelanat (1833-1907), ad. Arlie Coles (2021), str. 2 |
| Musique · Music | *Bethany* <br/> Lowell Mason (1856) |