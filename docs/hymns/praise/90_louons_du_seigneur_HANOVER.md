---
tags:
  - Louange · Praise
---

# Louons du Seigneur



=== ":material-playlist-music: Partition · Score"

	<div id="adobe-dc-view" style="width: 100%;"></div>
	<script src="https://documentcloud.adobe.com/view-sdk/main.js"></script>
	<script type="text/javascript">
    	document.addEventListener("adobe_dc_view_sdk.ready", function(){
        	var adobeDCView = new AdobeDC.View({clientId: "d7518740bf7a4af7b4c3a8b5a7bcff85", divId: "adobe-dc-view"});
        	adobeDCView.previewFile({
            	content:{location: {url: "/livre-de-cantiques-anglicans/assets/pdf/louons_du_seigneur_HANOVER.pdf"}},
            	metaData:{fileName: "louons_du_seigneur_HANOVER.pdf"}}, {embedMode: "IN_LINE"});
    	});
	</script>

=== ":octicons-list-ordered-16: Paroles seulement · Lyrics only"

	<table style="font-size:16px">
	<tr>
	<td> 1 </td>
	<td> Louons du Seigneur le Nom glorieux,<br>et pour son amour rendons grâce à Dieu;<br>Il est mon refuge, mon seul défenseur,<br>et dans les cieux règne, nimbé de splendeur. </td>
	</tr>
	<tr>
	<td> 2 </td>
	<td> Vantons son pouvoir; chantons sa bonté;<br>il vit dans les cieux, vêtu de beauté.<br>L'ouragan fait rage; tout est déchaîné,<br>et l'orage gronde, s'il est irrité. </td>
	</tr>
	<tr>
	<td> 3 </td>
	<td> Le Dieu tout puissant créa l'univers,<br>puis il l'entoura du manteau des mers.<br>Il régit le monde; tout lui obéit;<br>ses lois immuables règlent l'infini. </td>
	</tr>
	<tr>
	<td> 4 </td>
	<td> Qui chantera donc sa grande bonté?<br>Elle imprègne l'air, luit dans la clarté,<br>et dans les montagnes, dans tous les vallons,<br>s'exhale enrosée, nous comble de dons. </td>
	</tr>
	</table>

## Information
| | |
| - | - |
| Titre original · Original title | O worship the King |
| Texte · Text  | Robert Grant (1833);<br/>tr. Flossette du Pasquier (née 1922), str. 1-4<br/>tr. Arlie Coles (2022), str. 5 |
| Musique · Music | *Hanover* <br/> William Croft (1708) |