---
tags:
  - Louange · Praise
---

# Amour dépassant tout autre



=== ":material-playlist-music: Partition · Score"

	<div id="adobe-dc-view" style="width: 100%;"></div>
	<script src="https://documentcloud.adobe.com/view-sdk/main.js"></script>
	<script type="text/javascript">
    	document.addEventListener("adobe_dc_view_sdk.ready", function(){
        	var adobeDCView = new AdobeDC.View({clientId: "d7518740bf7a4af7b4c3a8b5a7bcff85", divId: "adobe-dc-view"});
        	adobeDCView.previewFile({
            	content:{location: {url: "/livre-de-cantiques-anglicans/assets/pdf/amour_depassant_tout_autre_HYFRYDOL.pdf"}},
            	metaData:{fileName: "amour_depassant_tout_autre_HYFRYDOL.pdf"}}, {embedMode: "IN_LINE"});
    	});
	</script>

=== ":octicons-list-ordered-16: Paroles seulement · Lyrics only"

	<table style="font-size:16px">
	<tr>
	<td> 1 </td>
	<td> Amour dépassant tout autre,<br>Le ciel vers nous descendu,<br>Demeure avec nous les pauvres;<br>Nous accorde ton salut!<br>Jésus, tu es la compassion,<br>L'indefectible Sauveur;<br>Malgré notre trépidation,<br>Entre dans nos faibles cœurs. </td>
	</tr>
	<tr>
	<td> 2 </td>
	<td> SaintEsprit, ô viens insuffler<br>la vie dans nos cœurs troublés,<br>Que nous soyons tous héritiers<br>de ton repos assuré.<br>Tu es Alpha et Oméga;<br>Ôtenous nos tentations.<br>Toi la source et le but de la  foi,<br>Donne ta libération. </td>
	</tr>
	<tr>
	<td> 3 </td>
	<td> Père, rendsnous acceptables<br>de ta vie et ta bonté.<br>Partout est ton tabernacle;<br>reste avec nous à jamais!<br>Comme les anges confessent,<br>te bénissant pour toujours,<br>Nous t'honorerons sans cesse,<br>louant ton parfait amour. </td>
	</tr>
	<tr>
	<td> 4 </td>
	<td> Perfectionne ta création;<br>lavenous de nos péchés.<br>Que l'on voie la restauration<br>de ton monde intentionné.<br>De la gloire vers la gloire<br>changés, jetons nos couronnes;<br>Dans les cieux de sa victoire,<br>prosternonsnous à son trône. </td>
	</tr>
	</table>

## Information
| | |
| - | - |
| Titre original · Original title | Love divine, all loves excelling |
| Texte · Text  | Charles Wesley (1707-1788);<br/>tr. Arlie Coles (2021) |
| Musique · Music | *Hyfrydol* <br/> Rowland H. Prichard (1811-1887) |