---
tags:
  - Louange · Praise
---

# Jésus, ô Nom qui surpasse



=== ":material-playlist-music: Partition · Score"

	<div id="adobe-dc-view" style="width: 100%;"></div>
	<script src="https://documentcloud.adobe.com/view-sdk/main.js"></script>
	<script type="text/javascript">
    	document.addEventListener("adobe_dc_view_sdk.ready", function(){
        	var adobeDCView = new AdobeDC.View({clientId: "d7518740bf7a4af7b4c3a8b5a7bcff85", divId: "adobe-dc-view"});
        	adobeDCView.previewFile({
            	content:{location: {url: "/livre-de-cantiques-anglicans/assets/pdf/jesus_o_nom_qui_surpasse_ABERYSTWYTH.pdf"}},
            	metaData:{fileName: "jesus_o_nom_qui_surpasse_ABERYSTWYTH.pdf"}}, {embedMode: "IN_LINE"});
    	});
	</script>

=== ":octicons-list-ordered-16: Paroles seulement · Lyrics only"

	<table style="font-size:16px">
	<tr>
	<td> 1 </td>
	<td> Jésus, ô nom qui surpasse<br>tout nom qu’on puisse exalter!<br>Que jamais je ne me lasse,<br>Nom béni, de te chanter!<br>Seule clarté qui rayonne<br>sur les gloires du saint lieu,<br>seul nom dont l’écho résonne<br>dans le cœur même de Dieu! </td>
	</tr>
	<tr>
	<td> 2 </td>
	<td> Jésus, c’est l’Amour suprême<br>de son trône descendu,<br>qui ceint de son diadème<br>le front de l’homme perdu.<br>C’est le Roi qui s’humilie<br>pour vaincre le révolté;<br>c’est la divine folie,<br>dans la divine bonté. </td>
	</tr>
	<tr>
	<td> 3 </td>
	<td> Qui pleura sur ceux qui pleurent?<br>C’est Lui, l’homme méprisé!<br>Qui mourut pour ceux qui meurent?<br>C’est Lui, l’homme au cœur brisé!<br>De son sang et de ses larmes<br>il arrosa son chemin,<br>Et c’est par ces seules armes<br>qu’il sauva le genre humain. </td>
	</tr>
	<tr>
	<td> 4 </td>
	<td> Jésus, par qui Dieu pardonne,<br>Roi d’épines couronné,<br>que le monde t’abandonne,<br>à toi mon cœur s’est donné!<br>Ta mort est ma délivrance;<br>je suis heureux sous ta loi;<br>ô Jésus, mon espérance,<br>quelle autre aurais –je que toi? </td>
	</tr>
	<tr>
	<td> 5 </td>
	<td> Ô penche-toi sur ma couche<br>lorsque je devrai mourir,<br>et, ton doux nom sur la bouche,<br>je verrai le ciel s'ouvrir.<br>Mais le ciel que je réclame,<br>c'est ton regard, c'est ta voix,<br>qui s'abaissent sur mon âme<br>assise au pied de ta croix! </td>
	</tr>
	<tr>
	<td> 6 </td>
	<td> Attire, ô Sauveur, attire<br>sur ton sein et dans tes bras<br>le cœur qui tremble et soupire<br>parce qu'il ne te voit pas.<br>Mon frère, il t'appelle, écoute:<br>il t'appelle, ne crains plus;<br>suis ses pas, et sur la route<br>chante ce beau nom: Jésus! </td>
	</tr>
	</table>

## Information
| | |
| - | - |
| Titre original · Original title | Jesus, lover of my soul |
| Texte · Text  | Charles Wesley (1740);<br/>tr. Ruben Sailens (1855-1942) |
| Musique · Music | *Aberystwyth* <br/> Joseph Parry (1879) |