---
tags:
  - Louange · Praise
---

# Tel que je suis



=== ":material-playlist-music: Partition · Score"

	<div id="adobe-dc-view" style="width: 100%;"></div>
	<script src="https://documentcloud.adobe.com/view-sdk/main.js"></script>
	<script type="text/javascript">
    	document.addEventListener("adobe_dc_view_sdk.ready", function(){
        	var adobeDCView = new AdobeDC.View({clientId: "d7518740bf7a4af7b4c3a8b5a7bcff85", divId: "adobe-dc-view"});
        	adobeDCView.previewFile({
            	content:{location: {url: "/livre-de-cantiques-anglicans/assets/pdf/tel_que_je_suis_WOODWORTH.pdf"}},
            	metaData:{fileName: "tel_que_je_suis_WOODWORTH.pdf"}}, {embedMode: "IN_LINE"});
    	});
	</script>

=== ":octicons-list-ordered-16: Paroles seulement · Lyrics only"

	<table style="font-size:16px">
	<tr>
	<td> 1 </td>
	<td> Tel que je suis, sans rien à moi,<br>sinon ton sang versé pour moi<br>et ta voix qui m'appelle à toi:<br>Agneau de Dieu, je viens, je viens! </td>
	</tr>
	<tr>
	<td> 2 </td>
	<td> Tel que je suis, je me relâche<br>de la souillure qui me gâche;<br>ton sang peut sécher chaque tache: </td>
	</tr>
	<tr>
	<td> 3 </td>
	<td> Tel que je suis, bien vacillant,<br>en proie au doute à chaque instant,<br>lutte au dehors, crainte au dedans: </td>
	</tr>
	<tr>
	<td> 4 </td>
	<td> Tel que je suis, pauvre et maudit,<br>tu restitues et tu guéris;<br>chaque besoin, tu rétablis: </td>
	</tr>
	<tr>
	<td> 5 </td>
	<td> Tel que je suis, ton cœur est prêt<br>à prendre le mien tel qu'il est,<br>pour tout changer, Sauveur parfait!<br>Agneau de Dieu, je viens, je viens! </td>
	</tr>
	<tr>
	<td> 6 </td>
	<td> Tel que je suis, ton grand amour<br>a tout pardonné sans retour.<br>Je veux être à toi dès ce jour;<br>Agneau de Dieu, je viens, je viens! </td>
	</tr>
	</table>

## Information
| | |
| - | - |
| Titre original · Original title | Just as I am |
| Texte · Text  | Charlotte Elliot (1789-1871);<br/>tr. inconnu, str. 1, 3, 5-6;<br/>tr. Arlie Coles (2021), str. 2, 4 |
| Musique · Music | *Woodworth* <br/> William B. Bradbury (1849) |