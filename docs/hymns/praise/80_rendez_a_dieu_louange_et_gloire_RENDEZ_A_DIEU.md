---
tags:
  - Louange · Praise
---

# Rendez à Dieu louange et gloire



=== ":material-playlist-music: Partition · Score"

	<div id="adobe-dc-view" style="width: 100%;"></div>
	<script src="https://documentcloud.adobe.com/view-sdk/main.js"></script>
	<script type="text/javascript">
    	document.addEventListener("adobe_dc_view_sdk.ready", function(){
        	var adobeDCView = new AdobeDC.View({clientId: "d7518740bf7a4af7b4c3a8b5a7bcff85", divId: "adobe-dc-view"});
        	adobeDCView.previewFile({
            	content:{location: {url: "/livre-de-cantiques-anglicans/assets/pdf/rendez_a_dieu_louange_et_gloire_RENDEZ_A_DIEU.pdf"}},
            	metaData:{fileName: "rendez_a_dieu_louange_et_gloire_RENDEZ_A_DIEU.pdf"}}, {embedMode: "IN_LINE"});
    	});
	</script>

=== ":octicons-list-ordered-16: Paroles seulement · Lyrics only"

	<table style="font-size:16px">
	<tr>
	<td> 1 </td>
	<td> Rendez à Dieu louange et gloire,<br>car il est bénin et clément:<br>Qui plus est, sa bonté notoire<br>dure perpétuellement.<br>Qu'Israël ores se recorde<br>de chanter solonnellement:<br>Que sa grande miséricorde<br>dure perpétuellement. </td>
	</tr>
	<tr>
	<td> 2 </td>
	<td> La maison d'Aron ancienne<br>vienne tout haut présentement<br>Confesser que la bonté sienne<br>dure perpétuellement.<br>Tous ceux qui du Seigneur ont crainte,<br>viennent aussi chanter comment<br>sa bonté pitoyable et sainte<br>dure perpétuellement. </td>
	</tr>
	<tr>
	<td> 3 </td>
	<td> Ainsi que je sois en détresse,<br>invoquant sa majesté,<br>il m'ouit et de cette presse<br>me met au large, à sauveté.<br>Le Tout-Puissant, qui m'ouit plaindre,<br>mon parti toujours tenir veut:<br>Qu'aije donc que faire de craindre<br>tout ce que l'homme faire peut? </td>
	</tr>
	<tr>
	<td> 4 </td>
	<td> Tu es le seul Dieu que j'honore,<br>ausi sans fin je chanterai:<br>Tu es le seul Dieu que j'adore,<br>aussi sans fin t'exalterai.<br>Rendez à Dieu louange et gloire,<br>Car il est bénin et clément:<br>Qui plus est, sa bonté notoire<br>dure perpétuellement. </td>
	</tr>
	</table>

## Information
| | |
| - | - |
| Texte · Text  | Clément Marot (1497-1544); ad. Arlie Coles (2021) |
| Musique · Music | *Rendez à Dieu* <br/> Louis Bourgeois et/ou Guillaume Franc (1543) |