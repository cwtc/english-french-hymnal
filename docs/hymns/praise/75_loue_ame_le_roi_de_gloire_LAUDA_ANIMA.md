---
tags:
  - Louange · Praise
---

# Loue, âme, le Roi de gloire



=== ":material-playlist-music: Partition · Score"

	<div id="adobe-dc-view" style="width: 100%;"></div>
	<script src="https://documentcloud.adobe.com/view-sdk/main.js"></script>
	<script type="text/javascript">
    	document.addEventListener("adobe_dc_view_sdk.ready", function(){
        	var adobeDCView = new AdobeDC.View({clientId: "d7518740bf7a4af7b4c3a8b5a7bcff85", divId: "adobe-dc-view"});
        	adobeDCView.previewFile({
            	content:{location: {url: "/livre-de-cantiques-anglicans/assets/pdf/loue_ame_le_roi_de_gloire_LAUDA_ANIMA.pdf"}},
            	metaData:{fileName: "loue_ame_le_roi_de_gloire_LAUDA_ANIMA.pdf"}}, {embedMode: "IN_LINE"});
    	});
	</script>

=== ":octicons-list-ordered-16: Paroles seulement · Lyrics only"

	<table style="font-size:16px">
	<tr>
	<td> 1 </td>
	<td>  </td>
	</tr>
	<tr>
	<td> 2 </td>
	<td> Louons sa grande compassion<br>envers nous, pécheurs rigides;<br>Lent au châtiment des nations,<br>aux bénédictions, rapide.<br>Alléluia, alléluia!<br>Sa fidélité splendide! </td>
	</tr>
	<tr>
	<td> 3 </td>
	<td> Comme un père, il nous soulage;<br>renforçant nos faibles corps. <br>Bien que l'on ne soit pas sage,<br>il vient toujours au secours. <br>Alléluia, alléluia!<br>Grande est sa miséricorde! </td>
	</tr>
	<tr>
	<td> 4 </td>
	<td> Joignezvous à nous, les anges,<br>vous qui voyez son visage;<br>Le soleil, fais ses louanges;<br>la lune, à travers les âges.<br>Alléluia, alléluia!<br>À Dieu faisons notre hommage! </td>
	</tr>
	</table>

## Information
| | |
| - | - |
| Titre original · Original title | Praise, my soul, the King of Heaven |
| Texte · Text  | Isaac Watts (1674-1748);<br/>tr. Arlie Coles (2021) |
| Musique · Music | *Lauda anima* <br/> John Goss (1800-1880) |