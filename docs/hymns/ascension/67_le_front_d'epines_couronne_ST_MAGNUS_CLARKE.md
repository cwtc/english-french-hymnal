---
tags:
  - Ascension · Ascension
---

# Le front d'épines couronné



=== ":material-playlist-music: Partition · Score"

	<div id="adobe-dc-view" style="width: 100%;"></div>
	<script src="https://documentcloud.adobe.com/view-sdk/main.js"></script>
	<script type="text/javascript">
    	document.addEventListener("adobe_dc_view_sdk.ready", function(){
        	var adobeDCView = new AdobeDC.View({clientId: "d7518740bf7a4af7b4c3a8b5a7bcff85", divId: "adobe-dc-view"});
        	adobeDCView.previewFile({
            	content:{location: {url: "/livre-de-cantiques-anglicans/assets/pdf/le_front_d'epines_couronne_ST_MAGNUS_CLARKE.pdf"}},
            	metaData:{fileName: "le_front_d'epines_couronne_ST_MAGNUS_CLARKE.pdf"}}, {embedMode: "IN_LINE"});
    	});
	</script>

=== ":octicons-list-ordered-16: Paroles seulement · Lyrics only"

	<table style="font-size:16px">
	<tr>
	<td> 1 </td>
	<td> Le front d'épines couronné Jésus pour nous est mort;<br>Mais glorieux il va regner; il change notre sort. </td>
	</tr>
	<tr>
	<td> 2 </td>
	<td> Car maintenant, ô Roi des rois, tu règnes dans les cieux;<br>C'est le triomphe après la croix du Christ victorieux. </td>
	</tr>
	<tr>
	<td> 3 </td>
	<td> A tous les habitants des cieux, à nous pauvres humains,<br>il donne un bonheur radieux par son amour divin. </td>
	</tr>
	<tr>
	<td> 4 </td>
	<td> Pour les humains tu t'abaissas, puis tu mourus, Seigneur.<br>Et tu nous sauves par ta croix! Ne versons plus de pleurs! </td>
	</tr>
	<tr>
	<td> 5 </td>
	<td> Avec le Christ portons la croix,<br>car un jour dans les cieux,<br>tous ceux qui vivent dans la foi<br>verront enfin leur Dieu. </td>
	</tr>
	<tr>
	<td> 6 </td>
	<td> Ta croix, ô Christ, est le salut;<br>ta mort sauva les tiens;<br>T'aimer, Seigneur, voilà mon but;<br>tu es mon seul vrai bien. </td>
	</tr>
	</table>

## Information
| | |
| - | - |
| Titre original · Original title | The head that once was crowned with thorns |
| Texte · Text  | Thomas Kelly (1820);<br/>tr. Flossette du Pasquier (née 1922) |
| Musique · Music | *St Magnus Clarke* <br/> Jeremiah Clarke (1707) |