% This file contains Lilypond elements common to all
% snippets of music we need to generate.

% Set up the fonts and staff size.
% Note that by convention, the three fonts, in order, are
% Roman, Sans, and Typewriter (so when we need to use Junicode,
% for example, we have to call \sans.)
% The particularities of Lilypond!

\paper {
	#(define fonts
		(make-pango-font-tree "Baskerville"
							  "Junicode"
                      		  "Baskerville"
        	(/ staff-height pt 20)
        )
     )
  line-width = #(+ line-width (* mm 3))
}

% \paper {
%   #(define fonts
%     (set-global-fonts
%      #:roman "Baskerville"
%      #:sans "Junicode"
%      #:typewriter "Baskerville"
%      ; unnecessary if the staff size is default
%      #:factor (/ staff-height pt 20)
%     ))
%   line-width = #(+ line-width (* mm 3))
%   #(define top-margin (* 6 in))
% }

% Define versicle and response
Vbar = \markup{ \fontsize #1.5 \sans "℣   " }
#(define-markup-command (versicle layout props text) (markup?)
  (interpret-markup layout props
    #{\markup \concat { \Vbar #text } #} ))

Rbar = \markup{ \fontsize #1 \sans "℟  " }  % Different sizes, but looks right
#(define-markup-command (response layout props text) (markup?)
  (interpret-markup layout props
    #{\markup \concat { \Rbar #text } #} ))

% Define "placeholder" note so V/R symbols can be placed to the
% left of the real notes
ph = \relative c' { \once \hide NoteHead \once \hide Accidental \once \hide Stem g'4 }

% Define a "bold" environment for lyrics
bold = {
  \override Lyrics.LyricText.font-series = #'bold 
}
normal = {
  \revert Lyrics.LyricText.font-series 
}

% Define large and small sign of the cross
cross = \markup{ \typewriter "✠ "}
scross = \markup{ \typewriter "✛"}

% Define quotes
oq = \markup{ \roman "“" }
cq = \markup{ \roman "”" }

% Define slurs
\sd = \slurDashed
\ss = \slurSolid


% Enable parenthesis around a series of notes
% http://lsr.di.unimi.it/LSR/Snippet?id=902
startParenthesis = {
  \once \override ParenthesesItem.stencils = #(lambda (grob)
        (let ((par-list (parentheses-item::calc-parenthesis-stencils grob)))
          (list (car par-list) point-stencil )))
}

endParenthesis = {
  \once \override ParenthesesItem.stencils = #(lambda (grob)
        (let ((par-list (parentheses-item::calc-parenthesis-stencils grob)))
          (list point-stencil (cadr par-list))))
} 

% Put markup at the end of a line
endOfLine = {
  \once \override Score.RehearsalMark.break-visibility = #end-of-line-visible
  \once \override Score.RehearsalMark.self-alignment-X = #RIGHT
}

% To left align the first word
l=\once \override LyricText #'self-alignment-X = #-1 

% To put space between staff and lyrics
lyricspace = {
  \override VerticalAxisGroup.
    nonstaff-relatedstaff-spacing.padding = #2
  \override VerticalAxisGroup.
    nonstaff-unrelatedstaff-spacing.padding = #2
}