\include "../common_liturgy.ly"

global = {
  \key g \major
  \time 4/4
  \autoBeamOff
  \override Staff.TimeSignature #'stencil = ##f
}

\layout {
  \context {
    \Score
    \omit BarNumber
  }
}

melody = \relative c'' {
  \global
  g4 fis8[ g] a4 g |
  c c b2 |
  e,4 fis g e |
  d d d2 | \break

  g4 fis8[ g] a4 g |
  c c b2 |
  e,4 fis g e |
  d d d2 | \break

  b'4 a g b |
  d4. c8 b2 |
  e,4 fis g c |
  b a g2 \bar "|."
} 

alto = \relative c' {
  \global
  d4 d d d |
  g d d2 |
  c4 d d c |
  b a b2 

  d4 d d d |
  g d d2 |
  c4 d d c |
  b a b2 

  d4 d d d |
  d d d2 |
  c4 d d c |
  d4. c8 b2
}

tenor = \relative c' {
  \global
  b4 c8[ b] a4 b |
  c a g2 |
  g4 a g g |
  g fis g2 

  b4 c8[ b] a4 b |
  c a g2 |
  g4 a g g |
  g fis g2 

  g4 d'8[ c] b4 g |
  a fis g2 |
  g4 a g g |
  g fis g2 
}

bass = \relative c' {
  \global
  g4 a8[ g] fis4 g |
  e fis g2 |
  c,4 c b c |
  d d g,2

  g'4 a8[ g] fis4 g |
  e fis g2 |
  c,4 c b c |
  d d g,2 

  g'4 fis g g |
  fis d g2 |
  c,4 c b e |
  d d g,2 
}

verseOne = \lyricmode {
  % \set ignoreMelismata = ##t
  \set stanza = "1 "
  % As with gladness men of old
  % Did the guiding star behold;
  % As with joy they hailed its light,
  % Leading onward, beaming bright;
  % So, most gracious Lord, may we
  % Evermore be led to Thee.
  \l Ja -- dis les rois mages heu -- reux
  \l vi -- rent cette é -- toile aux cieux;
  \l splen -- dide é -- tait sa lu -- eur
  \l qui les me -- nait au Sei -- gneur:
  \l Que l'on sui -- ve leur che -- min,
  \l ve -- nant à l'En -- fant di -- vin.
}

verseTwo = \lyricmode {
  % \set ignoreMelismata = ##t
  \set stanza = "2 "
  % As with joyful steps they sped
  % To that lowly manger-bed;
  % There to bend the knee before
  % Him Whom heaven and earth adore;
  % So may we with willing feet
  % Ever seek the mercy-seat.
  \l Aux pas joy -- eux se hâ -- tant,
  \l l'En -- fant a -- do -- ré cher -- chant,
  \l ils flé -- chi -- rent le ge -- nou
  \l au vrai Dieu qui les ab -- sout:
  \l Que l'on voie à la man -- geoire  
  \l leur doux pro -- pi -- ti -- a -- toire.
}

verseThree = \lyricmode {
  % \set ignoreMelismata = ##t
  \set stanza = "3 "
  % As they offered gifts most rare
  % At that manger rude and bare;
  % So may we with holy joy,
  % Pure and free from sin's alloy,
  % All our costliest treasures bring,
  % Christ! to Thee our heavenly King.
  \l Ils of -- fri -- rent leur en -- cens,
  \l l'or, la myrrhe à cet En -- fant;
  \l Que cette hum -- ble crè -- che nous
  \l in -- spire à lui don -- ner tous
  \l nos tré -- sors les plus ché -- ris:
  \l à no -- tre Sei -- gneur, le Christ!
}

verseFour = \lyricmode {
  % \set ignoreMelismata = ##t
  \set stanza = "4 "
  % Holy Jesus! every day
  % Keep us in the narrow way;
  % And, when earthly things are past,
  % Bring our ransomed souls at last
  % Where they need no star to guide,
  % Where no clouds Thy glory hide.
  \l Le che -- min é -- troit, Jé -- sus,
  \l cha -- que jour y gar -- de nous!
  \l Tou -- tes cho -- ses pas -- se -- ront;
  \l la nou -- vel -- le cré -- a -- tion
  \l te ver -- ra sans guide é -- toile,
  \l sans les nu -- a -- ges pour voile.
}

verseFive = \lyricmode {
  % \set ignoreMelismata = ##t
  \set stanza = "5 "
  % In the heavenly country bright,
  % Need they no created light;
  % Thou its Light, its Joy, its Crown,
  % Thou its Sun which goes not down,
  % There forever may we sing
  % Alleluias to our King.
  \l Ce pa -- ys sans so -- leil brille,
  \l et il n'y a point de nuit;
  \l car sa lam -- pe, c'est l'A -- gneau,
  \l son très glo -- ri -- eux flam -- beau:
  \l Qu'un jour nous y chan -- ti -- ons
  \l a -- près la Ré -- sur -- rec -- tion.
}


\score {
  <<
    \new Staff  <<
      \new Voice = "soprano" { \voiceOne \melody }
      \new Voice = "alto" { \voiceTwo \alto }
    >>
    \new Lyrics \with{\lyricspace} \lyricsto soprano \verseOne
    \new Lyrics  \lyricsto soprano \verseTwo
    \new Lyrics  \lyricsto soprano \verseThree
    \new Lyrics  \lyricsto soprano \verseFour
    \new Lyrics  \lyricsto soprano \verseFive
    \new Staff  <<
      \clef bass
      \new Voice = "tenor" { \voiceOne \tenor }
      \new Voice = "bass" { \voiceTwo \bass }
    >>
  >>
}
