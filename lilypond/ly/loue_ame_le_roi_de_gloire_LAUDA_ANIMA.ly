\include "../common_liturgy.ly"

global = {
  \key d \major
  \time 2/4
  \autoBeamOff
  \override Staff.TimeSignature #'stencil = ##f
}

\layout {
  \context {
    \Score
    \omit BarNumber
  }
}

melody = \relative c' {
  \global
  
 %verse 2
 a'4 a |
a a|
d cis |
b2 |
a2 |
g4 fis |
b a |
fis g |
\slurDotted
e4( e4) |
\slurSolid
\break

fis4 fis |
fis fis |
b a |
a gis |
a b |
cis d |
fis, gis |
\slurDotted
a4( a4) |
\slurSolid

d4 cis |
b a |
d cis |
b a |
b a |
g e |
d cis |
\slurDotted
d4( d4)
\slurSolid
\bar "|." \break
}

alto = \relative c' {
  \global
  %verse 2
d4 d |
cis cis |
d a' |
a( g) |
fis2 |
d4 d |
d d |
d e |
\slurDotted
cis4( cis4)
\slurSolid |

cis4 e |
d cis |
b b |
d d |
cis e |
e d |
d d |
\slurDotted
cis4( cis4)
\slurSolid |

d4. d8 |
d4 cis |
d e8[ fis] |
g4 g |
fis e |
d b |
a a |
\slurDotted
a4( a4)
\slurSolid
}

tenor = \relative c {
  \global
  
  %verse 2
fis4 fis |
g g |
a a |
b( cis) |
d2 |
g,4 a |
g a |
b b |
a( g) |

fis4 e |
d e |
fis b |
b b |
a d |
cis a |
b b |
\slurDotted
a4( a4)
\slurSolid |

a4. a8 |
g4 g |
fis e8[ d] |
e4 a |
d cis |
b g |
fis e8[ g] |
\slurDotted
fis4( fis4)
\slurSolid
}

bass = \relative c, {
  \global
   
  %verse 2
d'4 d |
e e fis fis |
g2 |
d2 |
b4 d |
g fis |
b e, |
\slurDotted
a,4( a4)
\slurSolid |

ais4 ais |
b cis |
d dis |
e eis |
fis gis |
a fis |
d e |
\slurDotted
a,4( a4)
\slurSolid |

fis'4. fis,8 |
g4 a |
b4. b8 |
cis4 cis |
d e8[ fis] |
g4 g, |
a a |
\slurDotted
d4( d4)
\slurSolid |
}

verseOne = \lyricmode {
  \set stanza = "1 "
%   % \l Praise, my soul, the King of heav -- en,
%   \l Ô mon â -- me, mag -- ni -- fi -- e
%   % \l To his feet your trib -- ute bring;
%   \l le Seig -- neur, le Roi des cieux!
%   % \l Ran -- somed, healed, re -- stored, for -- giv -- en,
%   \l Il t'a sau -- vée, af -- fran -- chi -- e
%   % \l Who, like me, his praise should sing?
%   \l par son par -- don gé -- né -- reux.
%   % \l Praise him! Praise him! Praise him! Praise him!
%   \l Al -- lé -- lu -- ia! Al -- lé -- lu -- ia!
%   % \l Praise the ev -- er -- last -- ing King.
%   \l Chan -- te son nom glo -- ri -- eux.
% }
  % \l Loue, mon âme, le Roi de gloire;
  \l Loue, â -- me, le Roi de gloi -- re;
  \l rend ton hom -- mage à ses pieds. __
  \l Ra -- che -- té par sa vic -- toi -- re;
  \l que son hon -- neur soit chan -- té. __
  \l Al -- lé -- lu -- ia, al -- lé -- lu -- ia!
  \l Le Roi, qu'il soit a -- do -- ré! __
}

verseTwo = \lyricmode {
  \set stanza = "2 "
  % % \l Praise him for his grace and fa -- vor
  % \l Bé -- nis sa main fa -- vo -- ra -- ble
  % % \l To our fa -- thers in dis -- tress;
  % \l Pré -- ve -- nante en ses bon -- tés,
  % % \l Praise him, still the same for ev -- er,
  % \l Et la grâce in -- com -- pa -- ra -- ble
  % % \l Slow to chide, and swift to bless;
  % \l de ce Dieu de cha -- ri -- té!
  % % \l Praise him! Praise him! Praise him! Praise him!
  % \l Al -- lé -- lu -- ia! Al -- lé -- lu -- ia!
  % % \l Glo --  rious in his faith -- ful -- ness.
  % \l Chan -- te sa fi -- dé -- li -- té!
  % \l Lou -- ons tous la grâce et pas -- sion;
  \l Lou -- ons sa gran -- de com -- pas -- sion
  % qu'il a pour le peuple qu’il guide.
  \l en -- vers nous, pé -- cheurs ri -- "gi -- des;"
  \l Lent au châ -- ti -- ment des na -- tions,
  \l aux bé -- né -- dic -- tions, ra -- "pi -- de."
  \l Al -- lé -- lu -- ia, al -- lé -- lu -- ia!
  \l Sa fi -- dé -- li -- té splen -- "di -- de!"
}

verseThree = \lyricmode {
  \set stanza = "3 "
  % \l Fa -- ther -- like, he tends and spares us;
  % \l Well our fee -- ble frame he knows;
  % \l in his hands he gent -- ly bears us, 
  % \l Res -- cues us from all our foes;
  % \l Praise him! Praise him! Praise him! Praise him!
  % \l Wide -- ly as his mer -- cy flows.
  \l Comme un père, il nous sou -- la -- ge;
  \l ren -- for -- çant nos fai -- bles corps. __
  \l Bien que l'on ne soit pas sa -- ge,
  \l il vient tou -- jours au se -- cours. __
  \l Al -- lé -- lu -- ia, al -- lé -- lu -- ia!
  \l Grande est sa mi -- sér -- i -- "cor -- de!"
}


verseFour = \lyricmode {
  \set stanza = "4 "
  % % \l An -- gels, help us to a -- dore him;
  % \l Près des an -- ges qui l'a -- do -- rent
  % % \l Ye be -- hold him face to face;
  % \l Dans l'é -- clat de sa splen -- deur,
  % % \l Sun and moon, bow down be -- fore him,
  % \l Les saints pros -- ter -- nés l'ho -- no -- rent
  % % \l Dwell -- ers all in time and space.
  % \l et con -- tem -- plent sa gran -- deur.
  % % \l Praise him! Praise him! Praise him! Praise him!
  % \l Al -- lé -- lu -- ia! Al -- lé -- lu -- ia!
  % % \l Praise with us the God of grace.
  % \l Tous, a -- dor -- ons le Seig -- neur!
  \l Joig -- nez -- vous à nous, les an -- ges,
  \l vous qui vo -- yez son vi -- "sa -- ge;"
  \l Le so -- leil, fais ses lou -- an -- ges;
  \l la lune, à tra -- vers les "â -- ges."
  \l Al -- lé -- lu -- ia, al -- lé -- lu -- ia!
  \l À Dieu fai -- sons notre hom -- "ma -- ge!"
}

\score {
  <<
    \new Staff  <<
      \new Voice = "soprano" { \voiceOne \melody }
      \new Voice = "alto" { \voiceTwo \alto }
    >>
    \new Lyrics \with{\lyricspace} \lyricsto soprano \verseOne
    \new Lyrics  \lyricsto soprano \verseTwo
    \new Lyrics  \lyricsto soprano \verseThree
    \new Lyrics  \lyricsto soprano \verseFour
    \new Staff  <<
      \clef bass
      \new Voice = "tenor" { \voiceOne \tenor }
      \new Voice = "bass" { \voiceTwo \bass }
    >>
  >>
}
