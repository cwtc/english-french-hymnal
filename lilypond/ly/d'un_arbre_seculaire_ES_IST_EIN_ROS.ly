\include "../common_liturgy.ly"

% https://github.com/magdalenesacredmusic/English-Hymns/blob/master/I-N/Lo,%20How%20a%20Rose%20E'er%20Blooming%20(ES%20IST%20EIN%20ROS).ly

global = {
  \key f \major
  \cadenzaOn
  \autoBeamOff
  \override Staff.TimeSignature #'stencil = ##f
}

\layout {
  \context {
    \Score
    \omit BarNumber
  }
}

melody = \relative c'' {
  \global
  c2 \bar "|"
  c4 c d c \bar "|"
  c2 a bes \bar "|"
  a4 g2 f e4 \bar "|"
  f2. \bar "|" \break

  c'2 \bar "|"
  c4 c d c \bar "|"
  c2 a bes \bar "|"
  a4 g2 f e4 \bar "|"
  f2. \bar "|" \break

  a4 \bar "|"
  g e f d \bar "|"
  c2. \bar "|" \break

  c'4 \bar "|"
  c c d c \bar "|"
  c2 a bes \bar "|"
  a4 g2 f e4 \bar "|"
  f2. \bar "|."
} 

alto = \relative c'' {
  \global
  a2
  a4 f f f
  e2 d d
  c d4. a8 c2
  c2.

  a'2
  a4 f f f
  e2 d d
  c d4. a8 c2
  c2.

  f4
  d c c b
  c8[( d] e2)

  e4
  g f f f
  e2 d d
  f4 d( e) f( g) c,
  c2.
}

tenor = \relative c' {
  \global
  c2
  c4 a bes a
  g2 f f
  a4( c) bes a2 g4
  a2.

  c2
  c4 a bes a
  g2 f f
  a4( c) bes a2 g4
  a2.

  c4
  bes a a g
  g2.

  g4
  g a bes a
  g2 fis g
  c4 bes a2 g
  a2.
}

bass = \relative c {
  \global
  f2
  f4 f bes f
  c2 d bes
  f'4 e d2 c
  f2.

  f2
  f4 f bes f
  c2 d bes
  f'4 e d2 c
  f2.

  f4
  g a f g
  c,2.

  c4
  e f bes, f'
  c2 d g,
  a4 bes c2 c f2.
}

verseOne = \lyricmode {
  % \set ignoreMelismata = ##t
  \set stanza = "1 "
  \l D'un ar -- bre sé -- cu -- lai -- re,
  \l du vieux tronc de Jes -- sé, %d’Isaï,
  \l du -- rant l'hi -- ver au -- stè -- re,
  \l un frais ra -- meau ger -- mait. %jaillit;
  \l Et sur le sol dur -- ci,
  \l dans la nuit calme et clai -- re,
  \l u -- ne Rose a fleur -- i.
}

verseTwo = \lyricmode {
  % \set ignoreMelismata = ##t
  \set stanza = "2 "
  \l Pré -- vu par I -- sa -- ï -- e,
  \l la Ro -- se men -- tion -- née:
  \l on l'ap -- pel -- le Ma -- ri -- e, %avec la Vierge Marie,
  \l qui le Bou -- ton por -- tait.
  % on la contemple et regarde.
  \l L'a -- mour de Dieu mon -- tré:
  \l dans la nuit calme et clai -- re,
  \l un Sau -- veur pour nous naît.
}

verseThree = \lyricmode {
  % \set ignoreMelismata = ##t
  \set stanza = "3 "
  \l Ô Fleur, dont la fra -- gran -- ce
  \l dou -- ce com -- _ ble l'air,
  \l en ta splen -- deur, _ chas -- se
  \l d'i -- ci les té -- nè -- bres.
  \l Vrai Homme, _ __ et vrai Dieu,
  \l sau -- ve -nous de la mort, et
  \l par -- ta -- ge nos far -- deaux.
  
}


\score {
  <<
    \new Staff  <<
      \new Voice = "soprano" { \voiceOne \melody }
      \new Voice = "alto" { \voiceTwo \alto }
    >>
    \new Lyrics \with{\lyricspace} \lyricsto soprano \verseOne
    \new Lyrics  \lyricsto soprano \verseTwo
    \new Lyrics  \lyricsto soprano \verseThree
    \new Staff  <<
      \clef bass
      \new Voice = "tenor" { \voiceOne \tenor }
      \new Voice = "bass" { \voiceTwo \bass }
    >>
  >>
}
