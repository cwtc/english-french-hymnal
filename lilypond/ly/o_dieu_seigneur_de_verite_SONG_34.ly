\include "../common_liturgy.ly"

% https://hymnary.org/hymn/EH1906/page/377

global = {
  \key f \major
  \time 4/4
  \autoBeamOff
  \override Staff.TimeSignature #'stencil = ##f
}

\layout {
  \context {
    \Score
    \omit BarNumber
  }
}

melody = \relative c' {
  \global
  f2 f4 f4 | g2 a2 | bes4 g4 c2 |
  \time 3/4
  f,4 a4 bes4 | c2 g4 | c2 b4 | c2.
  a4 bes4 c4 | d2 c4 | bes2 a4 | g2.
  c4 bes4 a4 | g2 f4 | a4 g2 | f2.
  \bar "|."
} 

alto = \relative c' {
  \global
  c2 c4 c4 | e2 f2 | f4 e4 f2 |
  f4 f4 f4 | g2 g4 | d2 d4 | e2. |
  f4 f4 g8[ a8] | bes2 a4 | a4( g4) f4 | e2. |
  f4 f4 f4 | e2 d4 | f4 f4( e4) | f2.
}

tenor = \relative c' {
  \global
  a2 a4 a4 | c2 c2 | bes4 bes4 a2 |
  a4 c4 d4 | e2 c4 | a4( g4) d'4 | c2. |
  c4 f4 ees4 | d2 e4 | f4( c4) c4 | c2. |
  c4 d4 c4 | c4( bes4) a4 | c4 c2 | a2.
}

bass = \relative c {
  \global
  f2 f4 f4 | c'2 a2 | g4 g4 f2 |
  f4 f4 d4 | c2 e4 | f4( g4) g4 | c,2. |
  f4 d4 c4 | bes2 c4 | d4( e4) f4 | c2( bes4) |
  a4 bes4 f'4 | c2 d4 | a4 c2 | f2.
}

verseOne = \lyricmode {
  % \set ignoreMelismata = ##t
  \set stanza = "1 "
  % O God of truth, O Lord of might,
  % Who, ordering time and change aright,
  % Sendest the early morning ray,
  % Kindling the glow of perfect day;

  \l Ô Dieu, Sei -- gneur de vé -- ri -- té,
  \l le temps par ta main est ran -- gé:
  % \l tu en -- voies l’au -- ro -- re bril -- lant;
  \l En -- voi -- e l'au -- ro -- re bril -- lant,
  % \l tu ral -- lu -- mes le jour lui -- sant.
  \l et ra -- lu -- me le jour lui -- sant.
}

verseTwo = \lyricmode {
  \set ignoreMelismata = ##t
  \set stanza = "2 "
  % Extinguish thou each sinful fire,
  % And banish every ill desire:
  % And, keeping all the body whole,
  % Shed forth Thy peace upon the soul.

  % \l É -- teins cha -- que flam -- me de peine;
  \l É -- teins donc nos pé -- chés brû -- lants;
  % \l pro -- tè -- ge nous des cho -- ses vaines;
  \l en -- lè -- ve cha -- que ten -- ta -- tion;
  \l en gar -- dant no -- tre corps u -- ni,
  \l ver -- se -nous ta paix cu -- ra -- tive.
}

verseThree = \lyricmode {
  \set ignoreMelismata = ##t
  \set stanza = "3 "
  % O Father, that we ask be done,
  % Through Jesus Christ, Thine only Son;
  % Who, with the Holy ghost and thee,
  % Shall live and reign eternally.
  \l O Pè -- re clé -- ment, fait ain -- si,
  \l par Jé -- sus Christ ton Fils ché -- ri,
  \l a -- vec le Saint Es -- prit vi -- vant
  \l et rè -- gnant é -- ter -- nel -- le -- ment.
}



\score {
  <<
    \new Staff  <<
      \new Voice = "soprano" { \voiceOne \melody }
      \new Voice = "alto" { \voiceTwo \alto }
    >>
    \new Lyrics \with{\lyricspace}  \lyricsto soprano \verseOne
    \new Lyrics  \lyricsto soprano \verseTwo
    \new Lyrics  \lyricsto soprano \verseThree
    \new Staff  <<
      \clef bass
      \new Voice = "tenor" { \voiceOne \tenor }
      \new Voice = "bass" { \voiceTwo \bass }
    >>
  >>
}
