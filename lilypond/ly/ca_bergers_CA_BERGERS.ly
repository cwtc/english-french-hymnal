\include "../common_liturgy.ly"

global = {
  \key a \major
  \time 4/4
  \autoBeamOff
  \override Staff.TimeSignature #'stencil = ##f
}

\layout {
  \context {
    \Score
    \omit BarNumber
  }
}

melody = \relative c'' {
  \global
  cis4 cis4 d4 d4 | cis4 cis4 b4. b8 |
  cis4 cis4 d4 d4 | cis2 b2 |
  b4 cis4 b4 a4 | gis4 fis4 e4. b'8 |
  b4. cis8 d4 cis4 | b2 a2 |
  b4 cis4 b4 a4 | b8[ a8] gis8[ fis8] e4. e'8 |
  e4. d8 cis4 d4 | cis4( b4) a2 \bar "|."
} 

alto = \relative c'' {
  \global
  a4 a4 b4 b4 | a4 a4 gis4. gis8 |
  a4 a4 b4 b4 | a2 gis2 |
  gis4 a4 gis4 fis4 | e4 dis4 e4. gis8 |
  gis4. a8 b4 a4 | a4( gis4) a2 |
  gis4 a4 gis4 fis4 | gis8[ fis8] e8[ dis8] e4. cis'8 |
  cis4. b8 a4 b4 | a4( gis4) e2
}

tenor = \relative c' {
  \global
  e4 cis4 b4 gis4 | a4 a4 b4. d8 |
  e4 e4 fis4 fis | e2 e2 |
  e4 dis4 e4 cis4 | b4 a4 gis4. e'8 |
  e4. a8 fis4 e4 | d2 cis2 |
  e4 fis4 e4 cis4 | b8[ cis8] b8[ a8] gis4. e'8 |
  e4. e8 e4 fis4 | e4( d4) cis2
}

bass = \relative c {
  \global
  a4 a4 e'4 e4 | a,4 cis4 e4. e8 |
  a,4 a4 d4 d4 | e2 e2 |
  e4 e4 e4 e4 | b4 b4 e4. e8 |
  e4. a8 d,4 e4 | e2 a2 |
  e4 e4 e4 e4 | b8[ b8] cis8[ dis8] e4. a,8 |
  a4. b8 cis4 d4 | e2 a,2
}

verseOne = \lyricmode {
  \set ignoreMelismata = ##t
  \set stanza = "1 "
  \l Çà, ber -- gers, as -- sem -- blons -- nous;
  \l al -- lons voir le Mes -- si -- e;
  \l Cher -- chons cet en -- fant si doux
  \l dans les bras de Ma -- ri -- e.
  \l Je l'en -- tends; il nous ap -- pel -- le tous:
  \l ô sort dig -- ne d'en -- vi -- _ e!
}

verseTwo = \lyricmode {
  \set ignoreMelismata = ##t
  \set stanza = "2 "
  \l Lais -- sons là tout le trou -- peau;
  \l qu'il erre à l'a -- ven -- tu -- re:
  \l Que sans nous sur ce co -- teau
  \l il cher -- che sa pâ -- tu -- re.
  \l Al -- lons voir dans un pe -- tit ber -- ceau
  \l l'au -- teur de la na -- tu -- _ re.
}

verseThree = \lyricmode {
  \set ignoreMelismata = ##t
  \set stanza = "3 "
  \l Que l'hi -- ver par ses fri -- mas
  \l ait en -- dur -- ci la plai -- ne;
  \l S'il croit ar -- rê -- ter nos pas,
  \l cette es -- pér -- ance est vai -- ne.
  \l Quand on cherche un Dieu rem -- pli d'ap -- pas,
  \l on ne craint point de pei -- _ ne.
}

verseFour = \lyricmode {
  \set ignoreMelismata = ##t
  \set stanza = "4 "
  \l Sa nais -- san -- ce sur ces bords
  \l ra -- mè -- ne l'al -- lé -- gres -- se:
  \l Ré -- pon -- dons par nos trans -- ports
  \l à l'ar -- deur qui le pres -- se.
  \l Se -- con -- dons de nou -- _ veaux ef -- forts
  \l l'ex -- cès de sa ten -- dres -- _ se.
}

verseFive = \lyricmode {
  \set ignoreMelismata = ##t
  \set stanza = "5 "
  \l Dieu nais -- sant, ex -- au -- ce -nous;
  \l dis -- si -- pe nos a -- lar -- mes;
  \l nous tom -- bons à tes ge -- noux;
  \l nous les bai -- gnons des lar -- mes.
  \l Hâ -- te -toi de nous don -- ner à tous
  \l la paix et tous ses char -- _ mes.
}


\score {
  <<
    \new Staff  <<
      \new Voice = "soprano" { \voiceOne \melody }
      \new Voice = "alto" { \voiceTwo \alto }
    >>
    \new Lyrics \with{\lyricspace} \lyricsto soprano \verseOne
    \new Lyrics  \lyricsto soprano \verseTwo
    \new Lyrics  \lyricsto soprano \verseThree
    \new Lyrics  \lyricsto soprano \verseFour
    \new Lyrics  \lyricsto soprano \verseFive
    \new Staff  <<
      \clef bass
      \new Voice = "tenor" { \voiceOne \tenor }
      \new Voice = "bass" { \voiceTwo \bass }
    >>
  >>
}

