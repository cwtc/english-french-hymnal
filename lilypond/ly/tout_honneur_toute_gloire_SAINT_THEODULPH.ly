\include "../common_liturgy.ly"

global = {
  \key c \major
  \time 4/4
  \autoBeamOff
  \override Staff.TimeSignature #'stencil = ##f
}

\layout {
  \context {
    \Score
    \omit BarNumber
  }
}

melody = \relative c' {
  \global
  \partial 4
    c4^\markup { \italic "Refrain" } |
    g' g a b |
    c2 c4 
    e |
    d c c b |
    c2. \bar "|" \break

    c,4 |
    g' g a b |
    c2 c4 
    e |
    d c c b |
    c2. \bar "||" \break

    c4 |
  e4 e d c |
  b4( a4) g 

  b4 |
  c b a a |
  g2. \bar "|" \break

  g4 |
  e4 g4 a g |
  g( f) e 

  g4 |
  f e d d |
  c2.


  \bar "|."
} 

alto = \relative c' {
  \global
  c4 |
  d e f f |
  f( e8[ d]) e4 

    g |
  g8[ f] e4 d d |
  e2. 

  c4 |
  d e f f |
  f( e8[ d]) e4 

    g |
  g8[ f] e4 d d |
  e2. 

  e4 |
  g a8[ g] fis4 g8[ a] | 
  g4( fis) g 
  
    g |
  e8[ fis] g4 g fis |
  g2. 
  
    d4 |
  c c f e |
  d( b) c 
  
    c |
  c c c b |
  c2.
}

tenor = \relative c {
  \global
  e4 |
  g c c g |
  g2 g4 
  
    c |
  b c a g |
  g2.

  e4 |
  g c c g |
  g2 g4 
  
    c |
  b c a g |
  g2.

  g4 |
  c c8[ b] a4 d |
  d( c) b 
  
    e |
  c d e d8[ c] |
  b2.
  
    g4 |
  g c c c |
  g2 g4 
  
    g |
  a g g4. f8 |
  e2.
  
}

bass = \relative c {
  \global
  c4 |
  b c f d |
  c2 c4

    c4 |
  g' a f g |
  c,2.

  c4 |
  b c f d |
  c2 c4

    c4 |
  g' a f g | 
  c,2. 
 
  c4 |
  c c d e8[ fis] |
  g4( d) e 
  
    e |
  a, b c d |
  g2. 
  
    b,4 |
  c e f c |
  b( g) c 
  
    e |
  f c g g |
  c2.
}

verseOne = \lyricmode {
  % \set ignoreMelismata = ##t
  % All glory, laud, and honor
  % To Thee, Redeemer King;
  % To whom the lips of children
  % Made sweet hosannas ring.
  \l Tout hon -- neur, tou -- te gloi -- re au Ré -- demp -- teur, au Roi!
  \l La bou -- che des en -- fants chan -- ta tes doux Ho -- san -- na!

  \set stanza = "1 "
  % Thou art the King of Israel,
  % Thou David's royal Son,
  % Who in the Lord's Name comest,
  % The King and Blessèd One.
  \l C'est toi le Roi d'Is -- ra -- ël,
  \l Fils roy -- al de Da -- vid,
  \l qui vient au Nom du Sei -- gneur,
  \l le Roi et le Bé -- ni.
}

verseTwo = \lyricmode {
  % \set ignoreMelismata = ##t
  _ _ _ _ _ _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _ _ _ _ _ _
  \set stanza = "2 "
  % The company of angels
  % Are praising Thee on high;
  % And mortal men, and all things
  % Created, make reply.
  \l Les an -- ges, les arch -- an -- ges
  \l te ma -- gni -- fient aux cieux;
  \l nous hommes aus -- si, nous of -- frons
  \l nos lou -- anges en tout lieu.
}

verseThree = \lyricmode {
  _ _ _ _ _ _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _ _ _ _ _ _
  \set ignoreMelismata = ##t
  \set stanza = "3 "
  % The people of the Hebrews
  % With palms before Thee went:
  % Our praise and prayer and anthems
  % Before Thee we present.
  \l Les Hé -- breux ont fait ton che -- min,
  \l leurs ra -- meaux ap -- por -- tant;
  \l nous of -- frons nos pri -- è -- _ res
  \l de -- vant toi en chan -- tant.
}

verseFour = \lyricmode {
  % \set ignoreMelismata = ##t
  _ _ _ _ _ _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _ _ _ _ _ _
  \set stanza = "4 "
  % To Thee before Thy passion
  % They sang their hymns of praise:
  % To Thee, now high exalted,
  % Our melody we raise.
  \l Leurs hymnes a -- vant ta Pas -- sion
  \l ils t'a -- vai -- ent chan -- tés;
  \l nos chants aus -- si sont pour toi,
  \l main -- te -- nant ex -- ul -- té.
}

verseFive = \lyricmode {
  % \set ignoreMelismata = ##t
  _ _ _ _ _ _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _ _ _ _ _ _
  \set stanza = "5 "
  % Thou didst accept their praises;
  % Accept the prayers we bring,
  % Who in all good delightest,
  % Thou good and gracious King.
  \l Tu ac -- cep -- tas leurs priè -- res;
  \l en -- tends les nôtres aus -- si,
  \l Au -- teur de tou -- te bon -- té,
  \l no -- tre Roi, notre A -- mi.
}

\score {
  <<
    \new Staff  <<
      \new Voice = "soprano" { \voiceOne \melody }
      \new Voice = "alto" { \voiceTwo \alto }
    >>
    \new Lyrics \with{\lyricspace} \lyricsto soprano \verseOne
    \new Lyrics  \lyricsto soprano \verseTwo
    \new Lyrics  \lyricsto soprano \verseThree
    \new Lyrics  \lyricsto soprano \verseFour
    \new Lyrics  \lyricsto soprano \verseFive
    \new Staff  <<
      \clef bass
      \new Voice = "tenor" { \voiceOne \tenor }
      \new Voice = "bass" { \voiceTwo \bass }
    >>
  >>
}

