\include "../common_liturgy.ly"

% https://github.com/magdalenesacredmusic/English-Hymns/blob/maester/A-C/Christ,%20whose%20glory%20fills%20the%20skies%20(RATISBON).ly

global = {
  \key d \major
  \time 4/4
  \autoBeamOff
  \override Staff.TimeSignature #'stencil = ##f
}

\layout {
  \context {
    \Score
    \omit BarNumber
  }
}

melody = \relative c'' {
  \global
  a4 a b cis |
  d d \slurDashed cis4( cis4) \slurSolid |
  b4 d a fis |
  g e \slurDashed d4( d4) \slurSolid | \bar "|" \break
  
  fis4 fis e fis |
  g g \sd fis4( fis) \ss |
  fis4 b aes b |
  cis cis \sd b4( b) \ss |
  
  d4 b a fis |
  g g fis2 |
  b4 b a fis |
  e e d2
  \bar "|."
} 

alto = \relative c' {
  \global
  d4 d d g |
  fis fis \slurDashed e4( e4) \slurSolid |
  d4 fis e d |
  d cis \sd d4( d) \ss |
  
  d4 d cis d |
  d e \sd d4( d) \ss |
  d4 fis fis8[ e] d4 |
  g fis \sd d4( d) \ss |
  
  d4 d cis d |
  d e8[ d] cis2 |
  fis4 d e d |
  d cis d2
}

tenor = \relative c {
  \global
  fis4 a g g |
  a a \sd a4( a4) \ss |
  fis4 fis a a |
  b a \sd fis4( fis) \ss |
  
  a4 a a a |
  b a \sd a4( a) \ss |
  a4 d cis b |
  b aes \sd b4( b) \ss |
  
  b4 g e b' |
  b cis8[ b] aes2 |
  b4 fis a a |
  b a fis2
}

baess = \relative c {
  \global
  d4 fis g e |
  d d \sd a4( a4) \ss |
  b4 b cis d |
  g, a \sd d4( d4) \ss |
  
  d4 d a d |
  b cis \sd d4( d) \ss |
  d4 b fis' g |
  e fis4 \sd b,4( b) \ss |
  
  g4 g a b |
  e e fis2 |
  d4 b cis d |
  g, a d2
}

verseOne = \lyricmode {
  \set ignoreMelismata = ##t
  \set stanza = "1 "
  % Christ, whose glo -- ry fills the skies,
  % Christ, the true, the on -- ly light,
  % Sun of Right -- eous -- ness, a -- rise,
  % Tri -- umph o'er the shades of night;
  % Day -- spring from on high, be near;
  % Day -- star, in my heart ap -- pear.

  \l Christ, le ciel mon -- tre ta gloi -- re,
  \l Christ, la Vrai -- e Lu -- mi -- è -- re;
  \l Vrai So -- leil, prends ta vic -- to -- ire
  \l sur cet -- te nuit der -- ni -- è -- re.
  \l Au -- be du ciel, mon -- tre -toi;
  \l É -- toi -- le du ma -- tin, Roi!
}

verseTwo = \lyricmode {
  \set ignoreMelismata = ##t
  \set stanza = "2 "
  % Dark and cheer -- less is the morn
  % Un -- ac -- com -- pan -- ied by thee;
  % Joy -- less is the day's re -- turn,
  % Till thy mer -- cy's beams I see;
  % Till they in -- ward light im -- part,
  % Glad my eyes, and warm my heart.

  \l Sombre et morne est le ma -- tin __ _
  \l quand tu n'es pas a -- vec moi; __ _
  \l le jour est plein de cha -- grin __ _
  \l sauf si tes ray -- ons se voi -- ent;
  \l ils é -- cra -- sent tous mes pleurs,
  \l bril -- lant et chauf -- fant mon cœur.
}

verseThree = \lyricmode {
  \set ignoreMelismata = ##t
  \set stanza = "3 "
  % Vis -- it then this soul of mine,
  % Pierce the gloom of sin and grief;
  % Fill me, ra -- dian -- cy di -- vine,
  % Scat -- ter all my un -- be -- lief;
  % More and more thy -- self dis -- play,
  % Shin -- ing to the per -- fect day.
  \l Vi -- si -- te cette âme à moi! __ _
  \l Per -- ce mes dé -- fi -- ci -- en -- ces.
  \l Com -- ble -moi, ray -- on -- ne -- ment; __ _
  \l con -- so -- le mon in -- croy -- an -- ce.
  \l Ain -- si tu t’es ré -- vé -- lé,
  \l lu -- ant jus -- qu’au jour par -- fait.
}


\score {
  <<
    \new Staff  <<
      \new Voice = "soprano" { \voiceOne \melody }
      \new Voice = "alto" { \voiceTwo \alto }
    >>
    \new Lyrics \with{\lyricspace} \lyricsto soprano \verseOne
    \new Lyrics  \lyricsto soprano \verseTwo
    \new Lyrics  \lyricsto soprano \verseThree
    \new Staff  <<
      \clef bass
      \new Voice = "tenor" { \voiceOne \tenor }
      \new Voice = "baess" { \voiceTwo \baess }
    >>
  >>
}

