\include "../common_liturgy.ly"

global = {
  \key f \major
  \time 4/4
  \autoBeamOff
  \override Staff.TimeSignature #'stencil = ##f
}

\layout {
  \context {
    \Score
    \omit BarNumber
  }
}

melody = \relative c' {
  \global
  f2 f4 g4 | a2 f4 f4 | bes2 a4 g4 | g2 f2 | \break
  c'2 a4 f4 | bes2 a4 a4 | a4( g4) a4 c4 | b2 c2 | \break
  f,2 e4 f4 | d2 c4 c4 | f2 g4 a4 | a2 g2 | \break
  c2 f,4 f4 | a4( g4) g4 g4 | a2 f4 g4 | g2 f2
  \bar "|."
} 

alto = \relative c' {
  \global
  c2 d4 e4 | f2 c4 4 | bes2 c4 d4 | c2 c2 |
  f2 f4 c4 | d4( e4) f4 f4 | f4( g4) c,4 f4 | g4( f4) e2 |
  c2 c4 c4 | bes4( a4) g4 c4 | c4( d4) e4 f4 | f2 e2 |
  e2 f4 f4 | f4( e4) d4 c4 | c2 d4 d4 | e2 f2
}

tenor = \relative c' {
  \global
  a2 a4 c4 | c2 a4 a4 | f2 f4 f4 | e4( bes'4) a2 |
  c2 c4 c4 | bes2 d4 d4 | c2 c4 c4 | d2 c2 |
  c2 bes4 a4 | f2 e4 e4 | f2 bes4 c4 | c2 c2 |
  c2 a4 bes4 | c2 bes4 c4 | a2 a4 bes4 | bes2 a2
}

bass = \relative c {
  \global
  f2 d4 c4 | f2 f4 e4 | d2 c4 bes4 | c2 f2 |
  a2 a4 a4 | g2 d4 d4 | f4( e4) f4 a4 | g2 c4( bes4) |
  a2 g4 f4 | bes,2 c4 bes4 | a2 g4 f4 | c'2 c2 |
  a2 d4 d4 | c2 d4 e4 | f4( e4) d4 bes4 | c2 f,2
}

verseOne = \lyricmode {
  % \set ignoreMelismata = ##t
  \set stanza = "1 "
  \l Il est aux cieux u -- ne joie im -- mor -- tel -- le
  \l qui cha -- que jour ré -- jou -- it les fi -- dè -- les
  \l car le Sei -- gneur vit dans tou -- tes les â -- mes;
  \l les cœurs sont tous a -- ni -- més de sa flam -- me.
}

verseTwo = \lyricmode {
  % \set ignoreMelismata = ##t
  \set stanza = "2 "
  \l Jé -- ru -- sa -- lem, c'est toi, vil -- le cé -- les -- te,
  \l où la paix règne et non les mal fu -- nes -- té,
  \l où tou -- tes grâ -- ces se -- ront ac -- cor -- dé -- es,
  \l tou -- tes pri -- è -- res se -- ront ex -- au -- cé -- es.
}

verseThree = \lyricmode {
  % \set ignoreMelismata = ##t
  \set stanza = "3 "
  \l Là, plus de deuils, plus de pei -- nes cru -- el -- les,
  \l mais u -- ne joie in -- di -- cible, é -- ter -- nel -- le:
  \l Les bien -- heu -- reux chan -- te -- ront des can -- ti -- ques
  \l à l'É -- ter -- nel, sou -- ve -- rain ma -- gni -- fi -- que.
}

verseFour = \lyricmode {
  % \set ignoreMelismata = ##t
  \set stanza = "4 "
  \l C'est là que règne, en -- tou -- ré de lu -- miè -- re,
  \l le tout -puis -- sant Cré -- a -- teur, no -- tre Pè -- re;
  \l Dieu trois fois saint qui nous a don -- né l'ê -- tre,
  \l en lui, par lui, nous vou -- lons tous re -- naî -- tre.
}


\score {
  <<
    \new Staff  <<
      \new Voice = "soprano" { \voiceOne \melody }
      \new Voice = "alto" { \voiceTwo \alto }
    >>
    \new Lyrics \with{\lyricspace} \lyricsto soprano \verseOne
    \new Lyrics  \lyricsto soprano \verseTwo
    \new Lyrics  \lyricsto soprano \verseThree
    \new Lyrics  \lyricsto soprano \verseFour
    \new Staff  <<
      \clef bass
      \new Voice = "tenor" { \voiceOne \tenor }
      \new Voice = "bass" { \voiceTwo \bass }
    >>
  >>
}
