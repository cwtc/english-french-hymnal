\include "../common_liturgy.ly"

global = {
  \key g \major
  \time 4/4
  \autoBeamOff
  \override Staff.TimeSignature #'stencil = ##f
}

\layout {
  \context {
    \Score
    \omit BarNumber
  }
}

melody = \relative c'' {
  \global
  b4 b b b8[ d] |
  d4. c8 b4 g4 |
  b4 b8[ a] b4 b8[ d] |
  d4. c8 b2 |

  b4 b b b8[ d] |
  d4. c8 b4 g4 |
  b4 b8[ a] b4 b8[ d] |
  d4. c8 b2 |
  
  \break
  d2^\markup { \italic "Refrain" }( e8[ d c b] |
  c2 d8[ c b a] |
  b2 c8[ b a g] |
  a4.) d,8 d2 |
  
  g4 a b c |
  b2 a | \break
  d2( e8[ d c b] |
  c2 d8[ c b a] |
  
  b2 c8[ b a g] |
  a4.) d,8 d2 |
  g4 a b c |
  b2( a) |
  g1 \bar "|."
}

alto = \relative c'' {
  \global
  g4 g g g |
  fis4. fis8 g2 |
  g4 g g g |
  fis4. fis8 g2 |
  
  g4 fis g g |
  fis4. fis8 g2 |
  g4 g g g |
  fis4. fis8 g2 |

  g4( b8[ a] gis4 e~ |
  e a8[ g] fis4 d~ |
  d g8[ fis] e4 c~ |
  c4.) d8 d2 |
  
  d4 d d e |
  d( g) g( fis) |
  g4( b8[ a] gis4 e~ |
  e a8[ g] fis4 d~ |
  
  d g8[ fis] e4 c~ |
  c4.) d8 d2 |
  d4 d d e |
  d( g2 fis4) |
  d1 \bar "|."
}

tenor = \relative c' {
  \global
  d4 d d d |
  c d d2 |
  d4 c d b |
  d c b2 |
  
  d4 d e8[ d] c[ b] 
  c4 d d2 |
  d4 e d b |
  d c b2 |
  
  b2( e~ |
  e d~ |
  d c |
  a4) g fis2 |
  
  g4 fis g g |
  g( b) d4.( c8) |
  b2( e~ |
  e d~ |
  
  d c |
  a4) g fis2 |
  g4 fis g g |
  g( b d4. c8) |
  b1 \bar "|."
}

bass = \relative c' {
  \global
  g4 g g b |
  a d, g2 |
  g4 g g g |
  d d g2 |
  
  g4 b e8[ d] c[ b] |
  a4 d, g2 |
  g4 c b g |
  d d g2 |
  
  g2( e4 gis |
  a2 d,4 fis |
  g2 c,4 e |
  fis) e d( c) |
  
  b a g c |
  d2 d |
  g2( e4 gis |
  a2 d,4 fis |
  
  g2 c,4 e |
  fis) e d( c) |
  b a g c |
  d1 |
  g1 \bar "|."
}

verseOne = \lyricmode {
  \set stanza = "1 "
  \set ignoreMelismata = ##t
  % \l An -- gels we have heard on high,
  \l Les an -- ges dans _ nos cam -- pa -- gnes
  % \l Sweet -- ly sing -- ing o’er the plains;
  \l ont en -- ton -- né l'hym -- _ ne des cieux.
  % \l And the moun -- tains in re -- ply
  \l Et l'é -- cho de _ nos mon -- ta -- gnes
  % \l Ech -- o -- ing their joy -- ous strains.
  \l re -- dit ce chant mé -- _ lo -- di -- eux.  
}

verseTwo = \lyricmode {
  \set stanza = "2 "
  \set ignoreMelismata = ##t
  % \l Shep -- herds, why this ju -- bi -- lee?
  \l Ber -- gers, pour qui _ cet -- te fê -- te?
  % \l Why your joy -- ous songs pro -- long?
  \l Quel est l'ob -- jet de _ tous ces chants?
  % \l What the glad -- some ti -- dings be
  \l Quen vain -- queur, quel -- _ le con -- quê -- te
  % \l Which in -- spire your heav’n -- ly song?
  \l mé -- ri -- te ces cris _ tri -- om -- phants?
}

verseThree = \lyricmode {
  \set stanza = "3 "
  \set ignoreMelismata = ##t
  \l Ils an -- non -- cent _ la nais -- san -- ce
  \l du Li -- bér -- a -- teur _ d'Is -- ra -- ël;
  \l et pleins de re -- _ con -- nais -- san -- ce,
  \l chantent, en _ ce jour _ so -- len -- nel:

  \l Glo -- _ _ _ _ _ _ _ _ _ _ _ _ _ _ _  ri -- a
  \l in ex -- cel -- sis De -- o!
  \l Glo -- _ _ _ _ _ _ _ _ _ _ _ _ _ _ _  ri -- a
  \l in ex -- cel -- sis De -- _ o!
}

verseFour = \lyricmode {
  \set stanza = "4 "
  \set ignoreMelismata = ##t
  \l Cher -- chons tous l'heu -- _ reux vil -- la -- ge
  \l qui l'a vu naî -- tre _ sous ses toits;
  \l of -- frons -lui le _ tendre hom -- ma -- ge,
  \l et de nos cœurs et _ de nos voix:
}

\score {
  <<
    \new Staff  <<
      \new Voice = "soprano" { \voiceOne \melody }
      \new Voice = "alto" { \voiceTwo \alto }
    >>
    \new Lyrics \with{\lyricspace} \lyricsto soprano \verseOne
    \new Lyrics  \lyricsto soprano \verseTwo
    \new Lyrics  \lyricsto soprano \verseThree
    \new Lyrics  \lyricsto soprano \verseFour
    \new Staff  <<
      \clef bass
      \new Voice = "tenor" { \voiceOne \tenor }
      \new Voice = "bass" { \voiceTwo \bass }
    >>
  >>
}

\markup { \vspace #0.5 } 

\markup {
  \fill-line {
    \hspace #0.1 % moves the column off the left margin;
     % can be removed if space on the page is tight
     \column {
      \line { \bold "5 "
        \column {
          "Dans l'humilité profonde"
          % "Où vous paraissez à nos yeux;"
          "où tu apparais à nos yeux;"
          % "Pour vous louer, roi du monde,"
          "pour te louer, Roi du monde,"
          "nous redirons ce chant joyeux:"
        }
      }
      \combine \null \vspace #0.1 % adds vertical spacing between verses
      \line { \bold "6 "
        \column {
          "Toujours remplis du mystère"
          % "Qu'opère aujourd'hui votre amour,"
          "qu'opère aujourd'hui ton amour,"
          "notre devoir sur la terre"
          "sera de chanter, chaque jour:"
        }
      }
    }
    \hspace #0.1 % adds horizontal spacing between columns;
    \column {
      \line { \bold "7 "
        \column {
          "Déjà les bienheureux anges,"
          "les chérubins, les séraphins,"
          "occupés de tes louanges,"
          "ont appris à dire aux humains:"
        }
      }
      \combine \null \vspace #0.1 % adds vertical spacing between verses
      \line { \bold "8 "
        \column {
          "Dociles à leur exemple,"
          "Seigneur, nous viendrons désormais"
          % "Au milieu de votre temple,"
          "au milieu de ton saint temple,"
          "chanter avec eux tes bienfaits."
        }
      }
    }
  \hspace #0.1 % gives some extra space on the right margin;
  % can be removed if page space is tight
  }
}