\include "../common_liturgy.ly"

% http://openhymnal.org/Pdf/Gabriels_Message-Gabriel.pdf

global = {
  \key c \major
  \time 3/4
  \autoBeamOff
  \override Staff.TimeSignature #'stencil = ##f
}

\layout {
  \context {
    \Score
    \omit BarNumber
  }
}

melody = \relative c' {
  \global
  \partial 4 
  e4 | a2 c4 | b2 d4 | c2 b4 |
  a2. b2. | e,2.~ | e2 \bar "|" \break
  e4 | a2 c4 | b2 d4 | c2 b4 | a2.~ | a2 g4 | a2.~ | a2 \bar "|" \break
  c4 | c2 d4 | c2 b4 | c2 d4 | e2 e4 | d2. | c2. | b2.~ | b2 \bar "||" \break
  c4^\markup { \italic "Refrain" } | d2 c4 | b4 b4 a4 | b2. | e,2. |
  a2.( | c4 b4 c4 a2. | a2) g4 | a2.
  \bar "|."
} 

alto = \relative c' {
  \global
  e4 | c2 e4 | d2 f4 | e2 d4 |
  e2. f2. | d2.~ | d2
  d4 | c2 e4 | d2 f4 | e2 f4 | e2.~ | e2 e4 | e2.~ | e2
  e4 | e2 f4 | e2 f4 | g2 f4 | e2 e4 | a2. | a2. | a2.( | g2)
  g4 | d2 d4 | d4 d4 d4 | d2. | d2. |
  c2.( | f2. | e2. | e2) e4 | c2.
}

tenor = \relative c {
  \global
  e4 | e2 e4 | e2 e4 | e2 e4 |
  c'2. d2. | b2.~ | b2
  b4 | a2 a4 | a2 a4 | a2 b4 | c2.( | b2) b4 | c2.~ | c2
  c4 | c2 g4 | g2 d'4 | c2 b4 | c2 c4 | a2.( | d2.) | d2.( | d2)
  c4 | a2 a4 | a4 a4 a4 | a2. | gis2. |
  a2.( | a2. | c2. | b2) b4 | a2.
}

bass = \relative c {
  \global
  e4 | a,2 a4 | a2 a4 | a2 a4 |
  a'2. a2. | a2. | gis2
  g4 | a2 a,4 | a2 a4 | a2 d4 | c2.( | e2) e4 | a2.~ | a2
  c,4 | c2 b4 | c2 d4 | e2 d4 | c2 c4 | f2.( | fis2.) | g2.( | g2)
  e4 | f2 f4 | f4 f4 f4 | e2. | e2. |
  f2.( | d2. | c2. | e2) e4 | a2.
}

verseOne = \lyricmode {
  % \set ignoreMelismata = ##t
  \set stanza = "1 "
  \l Quand Ga -- bri -- el du ciel est de -- scen -- du,
  \l les yeux brû -- lant, il dit: “Je te sa -- lue!
  \l Tu es bé -- nie, et le Sei -- gneur est a -- vec toi,
  \l Mar -- i -- e, plei -- ne de grâ -- ce.”
  \l Glo -- ri -- a!
}

verseTwo = \lyricmode {
  % \set ignoreMelismata = ##t
  \set stanza = "2 "
  \l “On te con -- naî -- tra, Mè -- re va -- leur -- euse;
  \l tous les â -- ges te di -- ront bien -- heur -- euse!
  \l Ton fils se -- ra Em -- man -- u -- el, Fils du Très -Haut,
}

verseThree = \lyricmode {
  % \set ignoreMelismata = ##t
  \set stanza = "3 "
  \l Puis Ma -- ri -- e dit en hu -- mi -- li -- té:
  \l “Qu'il me soit fait se -- lon sa vo -- lon -- té!
  \l Mon âme ex -- al -- te le Sei -- gneur,” pro -- cla -- ma -t-elle.
}

verseFour = \lyricmode {
  % \set ignoreMelismata = ##t
  \set stanza = "4 "
  \l Son fru -- it fut le Christ, Em -- man -- u -- el,
  \l à Beth -- lé -- em le ma -- tin de No -- ël.
  \l Les chré -- tiens di -- ront pour des siè -- cles des si -- ècles:
}


\score {
  <<
    \new Staff  <<
      \new Voice = "soprano" { \voiceOne \melody }
      \new Voice = "alto" { \voiceTwo \alto }
    >>
    \new Lyrics \with{\lyricspace} \lyricsto soprano \verseOne
    \new Lyrics  \lyricsto soprano \verseTwo
    \new Lyrics  \lyricsto soprano \verseThree
    \new Lyrics  \lyricsto soprano \verseFour
    \new Staff  <<
      \clef bass
      \new Voice = "tenor" { \voiceOne \tenor }
      \new Voice = "bass" { \voiceTwo \bass }
    >>
  >>
}
