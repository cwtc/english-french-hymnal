\include "../common_liturgy.ly"

global = {
  \key f \major
  \time 3/4
  \autoBeamOff
  \override Staff.TimeSignature #'stencil = ##f
}

\layout {
  \context {
    \Score
    \omit BarNumber
  }
}

melody = \relative c' {
  \global
  \partial 4
  c4 |
  f f g8[ a] |
  f4 f a8[ bes] |
  c4 c d |
  bes2 \bar "|" \break
  
  g8[ a] |
  bes4 bes c |
  a a f8[ a] |
  g4 d f |
  e2 \bar "|" \break

  c4 |
  f f g8[ a] |
  f4 f a8[ bes] |
  c4 c d |
  bes2 \bar "|" \break

  g8[ a] |
  bes4 bes c |
  a a f8[ a] |
  g4 d e |
  f2 \bar "|."
} 

alto = \relative c' {
  \global
  \partial 4
  c4 |
  c c c |
  
  c c f |
  f f f |
  d2 

  g4 |
  g4. f8 e4 |
  f f f |
  d d b |
  c2 

  c4 |
  c c c |
  c c f |
  e e fis |
  d2 

  g4 |
  g4. f8 e4 |
  f f f |
  d d c8[ bes] |
  a2 \bar "|."
}

tenor = \relative c' {
  \global
  \partial 4  
  c8[ bes] |
  a4 a bes8[ c] |
  a4 a c |
  c c a |
  bes2 

  bes8[ c] |
  d4 d c |
  c c a8[ c] |
  b4 b g |
  g2 

  c8[ bes] |
  a4 a bes8[ c] |
  a4 a c |
  c c a |
  bes2 
  
  bes8[ c] |
  d4 d c |
  c c c |
  bes4. a8 g4 |
  f2 \bar "|."
}

bass = \relative c {
  \global
  \partial 4
  c4 |
  f f f |
  f f f8[ g] |
  a4 a d, |
  g2 

  g4 |
  g g c, |
  f f f |
  g g g, |
  c2

  c4 |
  f f f |
  f f f8[ g] |
  a4 a d, |
  g2 

  g4 |
  g g c, |
  f f a, |
  bes bes c |
  f2 \bar "|."
}

verseOne = \lyricmode {
  % \set ignoreMelismata = ##t
  \set stanza = "1 "
  % Away in a manger, no crib for a bed,
  % The little Lord Jesus laid down His sweet head;
  % The stars in the bright sky looked down where He lay,
  % The little Lord Jesus asleep on the hay.
  \l Dans l'hum -- ble mange -- oi -- re
  qui fai -- sait son lit,
  \l pe -- tit Sei -- gneur Jé -- sus,
  là, il s'en -- dor -- mit;
  \l une é -- toi -- le dans le ciel
  l'il -- lu -- mi -- nait:
  \l pe -- tit Sei -- gneur Jé -- sus,
  sur la paille en paix.
}

verseTwo = \lyricmode {
  % \set ignoreMelismata = ##t
  \set stanza = "2 "
  % The cattle are lowing, the Baby awakes,
  % But little Lord Jesus, no crying He makes.
  % I love Thee, Lord Jesus, look down from the sky,
  % And stay by my cradle till morning is nigh.
  \l Les bes -- ti -- aux beug -- lent,
  puis il les en -- tend;
  \l pe -- tit Sei -- gneur Jé -- sus,
  il ne pleu -- re point.
  \l J'ai -- me Sei -- gneur Jé -- sus:
  que tu me sur -- veilles, 
  \l res -- tant à mes cô -- tés
  jus -- qu'à mon ré -- veil.
}

verseThree = \lyricmode {
  % \set ignoreMelismata = ##t
  \set stanza = "3 "
  % Be near me, Lord Jesus; I ask Thee to stay
  % Close by me forever, and love me, I pray.
  % Bless all the dear children in Thy tender care,
  % And fit us for heaven to live with Thee there.
  \l Donc, Sei -- gneur Jé -- sus, j'o -- se te de -- man -- der:
  \l que tu restes a -- vec moi aus -- si pour m'ai -- mer;
  \l que tout en -- fant ve -- nant vers toi soit bé -- ni;
  \l que l'on soit fait prêt pour ton règne in -- fi -- ni.
}

\score {
  <<
    \new Staff  <<
      \new Voice = "soprano" { \voiceOne \melody }
      \new Voice = "alto" { \voiceTwo \alto }
    >>
    \new Lyrics \with{\lyricspace} \lyricsto soprano \verseOne
    \new Lyrics  \lyricsto soprano \verseTwo
    \new Lyrics  \lyricsto soprano \verseThree
    \new Staff  <<
      \clef bass
      \new Voice = "tenor" { \voiceOne \tenor }
      \new Voice = "bass" { \voiceTwo \bass }
    >>
  >>
}
