\include "../common_liturgy.ly"

global = {
  \key ees \major
  \time 2/4
  \autoBeamOff
  \override Staff.TimeSignature #'stencil = ##f
}

\layout {
  \context {
    \Score
    \omit BarNumber
  }
}

melody = \relative c'' {
  \global
  \partial 8 bes8 |
  bes4 g8 bes8\noBeam |
  bes4 g8 bes\noBeam |
  aes4 f8\noBeam f |
  g4 bes8\rest \bar"|"\break bes |
  
  bes4 g8 bes8\noBeam |
  bes4 g8 bes\noBeam |
  aes4 f8\noBeam f |
  g4 bes8\rest \bar"|"\break g |
  
  f4 f8\noBeam f |
  aes4 aes8\noBeam aes |
  g4 g8\noBeam g |
  c4. \bar"|"\break c8 |
  
  bes4 bes8\noBeam bes |
  ees4 bes8\noBeam g |
  aes4 f8\noBeam f |
  \partial 4. ees4 bes'8\rest \bar "|."
} 

alto = \relative c'' {
  \global
  g8 |
  g4 ees8\noBeam g |
  g4 ees8\noBeam des |
  c[ ees] ees\noBeam d |
  ees4 s8 g8 |
  
  g4 ees8\noBeam g |
  g4 ees8\noBeam des |
  c[ ees] ees\noBeam d |
  ees4 s8 ees |
  
  ees4 d8\noBeam d |
  c4 c8\noBeam d |
  ees4 ees8\noBeam ees |
  ees4. ees8 |
  
  f[ d] ees\noBeam f |
  ees4 ees8\noBeam ees |
  f[ ees] ees\noBeam d |
  bes4 s8 \bar "|."
}

tenor = \relative c' {
  \global
  bes8 |
  bes4 bes8\noBeam bes |
  bes[ ees] bes\noBeam g |
  aes4 bes8\noBeam bes |
  bes4 s8 bes8 |
  
  bes4 bes8\noBeam bes |
  bes[ ees] bes\noBeam g |
  aes4 bes8\noBeam bes |
  bes4 s8 bes8 |
  
  bes4 bes8\noBeam bes |
  aes4 c8\noBeam bes |
  bes4 c8\noBeam des |
  c4. c8 |
  
  d[ bes] c\noBeam d |
  bes[ aes] g\noBeam c |
  c[ aes] bes\noBeam aes |
  g4 s8 \bar "|."
}

bass = \relative c {
  \global
  ees8 |
  ees4 ees8\noBeam ees |
  ees4 ees8\noBeam e |
  f4 bes,8\noBeam bes |
  ees4 d8\rest ees8 |
  
  ees4 ees8\noBeam ees |
  ees4 ees8\noBeam e |
  f4 bes,8\noBeam bes |
  ees4 d8\rest ees8 |
  
  bes4 bes8\noBeam bes |
  f'4 f8\noBeam bes, |
  ees4 c8\noBeam bes |
  aes4. aes'8 |
  
  aes4 aes8\noBeam aes |
  g[ f] ees\noBeam c |
  f4 bes,8 bes |
  ees4 d8\rest \bar "|."
}

verseOne = \lyricmode {
  % \set ignoreMelismata = ##t
  \set stanza = "1 "
  \l Bam -- bins et ga -- mi -- nes, ve -- nez, ve -- nez tous:
  \l Mer -- veil -- le di -- vi -- ne se pas -- se chez nous!
  \l Voy -- ez dans la crè -- che l'En -- fant nou -- veau -né
  \l que dans la nuit fraî -- che Dieu nous a don -- né.
}

verseTwo = \lyricmode {
  % \set ignoreMelismata = ##t
  \set stanza = "2 "
  \l U -- ne pauvre é -- ta -- ble lui sert de mai -- son,
  \l ni chai -- se, ni ta -- ble, rien que paille et son;
  \l une hum -- ble chan -- del -- le suf -- fit à l'En -- fant
  \l que le monde ap -- pel -- le le Dieu tout -puis -- sant.
}

verseThree = \lyricmode {
  % \set ignoreMelismata = ##t
  \set stanza = "3 "
  \l On n'a vu per -- son -- ne mon -- ter au clo -- cher,
  \l mais la clo -- che son -- ne pour le nou -- veau -né!
  \l L'oi -- seau sur sa bran -- che s'est mis a chan -- ter;
  \l l'œil de la per -- ven -- che s'est mis à bril -- ler.
}

verseFour = \lyricmode {
  % \set ignoreMelismata = ##t
  \set stanza = "4 "
  \l Ber -- gers et ber -- gè -- res por -- tent leurs pré -- sents:
  \l Do -- do, pe -- tit frè -- re! chan -- tent les en -- fants;
  \l mille an -- ges fo -- lâ -- trent dans un ray -- on d'or;
  \l les Ma -- ges se hâ -- tent vers Jé -- sus qui dort.
}


\score {
  <<
    \new Staff  <<
      \new Voice = "soprano" { \voiceOne \melody }
      \new Voice = "alto" { \voiceTwo \alto }
    >>
    \new Lyrics \with{\lyricspace} \lyricsto soprano \verseOne
    \new Lyrics  \lyricsto soprano \verseTwo
    \new Lyrics  \lyricsto soprano \verseThree
    \new Lyrics  \lyricsto soprano \verseFour
    \new Staff  <<
      \clef bass
      \new Voice = "tenor" { \voiceOne \tenor }
      \new Voice = "bass" { \voiceTwo \bass }
    >>
  >>
}
