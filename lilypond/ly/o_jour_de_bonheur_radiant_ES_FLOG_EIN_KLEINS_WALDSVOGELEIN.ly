\include "../common_liturgy.ly"

% https://hymnary.org/hymn/HPEC1940/page/624

global = {
  \key g \major
  \time 4/4
  \autoBeamOff
  \override Staff.TimeSignature #'stencil = ##f
}

\layout {
  \context {
    \Score
    \omit BarNumber
  }
}

melody = \relative c'' {
  \global
  \partial 4 g4 |
  g4 d'4 b4 g4 | fis4.( g8) a4 d,4 |
  e4 g4 g4 fis4 | g2. \bar "|" \break

  g4 | g4 d'4 b4 g4 | fis4.( g8) a4 d,4 |
  e4 g4 g4 fis4 | g2. \bar "|" \break

  b4 | g4 d'4 d8[ c8] b4 | a2 a4
  c4 | b4 a4 b4 g4 | a2. \bar "|" \break

  d4 | c4 b4 a4 g4 | fis4.( g8) a4 d,4 |
  e4 g4 g4 fis4 | g1
  \bar "|."
} 

alto = \relative c' {
  \global
  d4 |
  e4 d4 d4 g4 | d2 d4 d4 |
  d4 c8[ b8] a4 d4 | d2.

  d4 |
  e4 d4 d4 g4 | d2 d4 d4 |
  d4 c8[ b8] a4 d4 | d2.

  d4 | d4 a'4 g4 g4 | e2 fis4
  e4 | d4 d4 d4 b4 | cis4( e2)

  d4 |
  e4 d4 c4 d4 | d2 d4 d4 |
  d4 c8[ b8] a4 d4 | d1
}

tenor = \relative c' {
  \global
  b4 |
  b4 a4 b4 d4 | a4.( g8) fis4 g8[ fis8] |
  e4 e'4 d4 a4 | c4( b2)

  b4 |
  b4 a4 b4 d4 | a4.( g8) fis4 g8[ fis8] |
  e4 e'4 d4 a4 | b2.

  g4 | b4 d4 e4. d8 | d4( cis4) d4
  g,4 | g4 fis4 fis4 g4 | e4( fis2)

  g4 | g4 g4 e8[ fis8] g4 | a4.( g8) fis4 g8[ fis8] |
  e4 e'4 d4 a4 | b1
}

bass = \relative c' {
  \global
  g4 | e4 fis4 g4 b,4 | d2 d4 b4 |
  c4 a4 d4 d4 | d2.
  g4 | d4 fis4 g4 b,4 | d2 d4 b4 |
  c4 a4 d4 d4 | g2.
  g4 | g4 fis4 e4 e4 | a,2 d4
  c8[ e8] | g4 d4 b4 d4 | a4( d2)
  b4 | c4 g4 a4 b4 | d2 d4
  b4 | c4 a4 d4 d4 | g1
}

verseOne = \lyricmode {
  \set ignoreMelismata = ##t
  \set stanza = "1 "
  % O day of rest and gladness,
  % O day of joy and light,
  % O balm of care and sadness,
  % Most beautiful, most bright;
  % On thee, the high and lowly,
  % Through ages join'd in tune,
  % Sing, Holy, Holy, Holy,
  % To the great God Triune.
  \l Ô jour de bon -- heur ra -- di -- ant,
  \l jour de fé -- li -- ci -- té,
  \l ô bau -- me ré -- con -- ci -- li -- ant,
  \l d’une u -- ni -- que beau -- té;
  \l les hum -- bles com -- me les puis -- sants,
  \l ce jour, ils ont chan -- té
  \l le Saint, _ saint, _ saint! _ _
  \l vers Dieu en Tri -- ni -- té.

}

verseTwo = \lyricmode {
  % \set ignoreMelismata = ##t
  \set stanza = "2 "
  % On thee, at the Creation,
  % The light first had its birth;
  % On thee, for our salvation,
  % Christ rose from depths of earth;
  % On thee our Lord victorious
  % The Spirit sent from heaven;
  % And thus on thee most glorious
  % A triple light was given.
  \l Ce jour à la cré -- a -- tion,
  \l la lu -- mi -- ère na -- quit;
  \l ce jour pour la ré -- demp -- tion
  \l res -- sus -- ci -- ta le Christ;
  \l no -- tre Sei -- gneur vic -- to -- rieux
  \l nous en -- voy -- a l’Es -- prit,
  \set ignoreMelismata = ##t
  \l donc c’est ce jour très glo -- ri -- eux 
  \l qu’on voit la lu -- mière triple.
}

verseThree = \lyricmode {
  % \set ignoreMelismata = ##t
  \set stanza = "3 "
  % To-day, on weary nations,
  % The heavenly manna falls;
  % To holy convocations
  % The silver trumpet calls,
  % Where gospel light is glowing
  % With pure and radiant beams,
  % And living water flowing
  % With soul-refreshing streams.
  \l Au -- jour -- d'hui tombe aux na -- tions
  \l cet -- te Man -- ne du ciel;
  \l aux sain -- tes con -- vo -- ca -- tions
  \l cet -- te trom -- pette ap -- pelle.
  \l Le don de l'É -- van -- gi -- le
  \l ray -- on -- ne pu -- re -- ment;
  \l de la Sour -- ce de Vi -- e
  \l cou -- le cette eau vi -- vante.
}

verseFour = \lyricmode {
  % \set ignoreMelismata = ##t
  \set stanza = "4 "
  % New graces ever gaining
  % From this one day of rest,
  % We reach the rest remaining
  % To spirits of the blest:
  % To Holy Ghost be praises,
  % To Father and to Son,
  % The church her voice upraises,
  % To Thee, blest Three in One.
  \l Fais -nous voir un a -- per -- çu
  \l dans ce jour de re -- pos
  \l du re -- pos si at -- ten -- du
  \l de ton grand saint trou -- peau!
  \l À toi nous ren -- dons gloi -- re,
  \l toi, trois en un u -- nit,
  \l en chan -- tant ta vic -- toi -- re,
  \l Pè -- re, Fils, Saint -Es -- prit.
}


\score {
  <<
    \new Staff  <<
      \new Voice = "soprano" { \voiceOne \melody }
      \new Voice = "alto" { \voiceTwo \alto }
    >>
    \new Lyrics \with{\lyricspace} \lyricsto soprano \verseOne
    \new Lyrics  \lyricsto soprano \verseTwo
    \new Lyrics  \lyricsto soprano \verseThree
    \new Lyrics  \lyricsto soprano \verseFour
    \new Staff  <<
      \clef bass
      \new Voice = "tenor" { \voiceOne \tenor }
      \new Voice = "bass" { \voiceTwo \bass }
    >>
  >>
}
