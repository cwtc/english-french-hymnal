\include "../common_liturgy.ly"

global = {
  \key g \major
  \time 4/4
  \autoBeamOff
  \override Staff.TimeSignature #'stencil = ##f
}

\layout {
  \context {
    \Score
    \omit BarNumber
  }
}

melody = \relative c'' {
  \global
  g4 g b b |
  d c8[ b] a2 | \bar "||"
  d4.( c8 b4) c8[ b] |
  a2 g \bar "|" \break

  g4 g b b |
  d c8[ b] a2 | \bar "||"
  d4.( c8 b4) c8[ b] |
  a2 g \bar "|" \break

  b4 b d d |
  e8[ d] c[ b] a2 | \bar "||"
  b4.( a8 b4) cis |
  d2 d \bar "|" \break

  g,4 g b b |
  d c8[ b] a2 | \bar "||"
  d4.(  c8 b4) c8[ b] |
  a2 g \bar "|."
}

alto = \relative c' {
  \global
  d4 d g g |
  g a8[ g] fis2 |
  d4( e8[ fis] g4) g |
  g( fis) g2 

  d4 d g g |
  g a8[ g] fis2 |
  d4( e8[ fis] g4) g |
  g( fis) g2 

  g4 g f f |
  e e fis2 |
  g4.( a8 g4) g |
  fis2 fis 

  g4 d d g |
  g a8[ g] fis2 |
  d4 e8[ fis] g4 g |
  g4( fis) d2 
}

tenor = \relative c' {
  \global
  b4 b g g |
  d' e a,2 |
  b4.( c8 d4) c8[ d] |
  e4( d) b2 

  b4 b g g |
  d' e a,2 |
  b4.( c8 d4) c8[ d] |
  e4( d) b2 

  g4 g a b |
  c c d2 |
  d2. g,4 |
  a2 d 

  d4 d8[ c] b4 g |
  d' e a,2 |
  g4. a8 b4 c8[ d] |
  e4( d) b2 
}

bass = \relative c' {
  \global
  g4 g e e |
  b c d2 |
  b'4.( a8 g4) e8[ d] |
  c4( d) g2 

  g4 g e e |
  b c d2 |
  b'4.( a8 g4) e8[ d] |
  c4( d) g2 

  e4 e d d |
  c a d2 |
  g4.( fis8 g4) e |
  d2 d'4( c!) 

  b b8[ a] g4 e |
  b c d2 |
  b4. a8 g4 a8[ b] |
  c4( d) g,2 
}

verseOne = \lyricmode {
  \set ignoreMelismata = ##t
  \set stanza = "1 "
  % ADAPTED
  % \l Hail the day that sees Him rise, Al -- le -- lu -- ia!
  \l Gloire au Christ vic -- to -- ri -- _ eux, _ _ _ _ _ _ _
  % \l To His throne a -- bove the skies, Al -- le -- lu -- ia!
  \l Qui monte au plus haut des _ cieux, _ _ _ _ _ _ _
  % \l Christ, a -- while to mor -- tals giv'n, Al -- le -- lu -- ia!
  % \l Tout ray -- on -- nant de beau -- té, Al -- lé -- lu -- ia!
  \l Christ a -- près sa Ré -- sur -- rec -- _ tion, _ _ _ _ _ _
  % \l Re -- as -- cends His na -- tive heav'n, Al -- le -- lu -- ia!
  % \l Et de splen -- deur cou -- ron -- né. Al -- lé -- lu -- ia!
  \l Ce jour fait son As -- cen -- _ sion. _ _ _ _ _ _ _
}

verseTwo = \lyricmode {
  \set ignoreMelismata = ##t
  \set stanza = "2 "
  % \l There the glo -- rious tri -- umph waits, Al -- le -- lu -- ia!
  \l Ou -- vrez -- vous, por -- tes du _ ciel, Al -- _ _ lé -- _ lu -- ia!
  % \l Lift your heads, e -- ter -- nal gates, Al -- le -- lu -- ia!
  \l Pour ce vain -- queur im -- mor -- _ tel, Al -- _ _ lé -- _ lu -- ia!
  % \l Christ hath con -- quered death and sin, Al -- le -- lu -- ia!
  \l Car il a dé -- truit _ la _ mort, Al -- _ _ lé -- lu -- ia!
  % \l Take the King of glo -- ry in, Al -- le -- lu -- ia!
  \l Lui le Dieu saint, le Dieu _ fort. Al -- _ _ lé -- _ lu -- ia!
}

verseThree = \lyricmode {
  \set ignoreMelismata = ##t
  \set stanza = "3 "
  % NEW 
  % Lo! the heaven its Lord receives,
  % Yet He loves the earth He leaves;
  % Though returning to His throne,
  % Still He calls mankind His own.
  \l Le ciel re -- çoit son Seign -- _ eur;  _ _ _ _ _ _ _
  \l Et la terre a son Plai -- _ deur;  _ _ _ _ _ _ _
  \l Mal -- gré son re -- tour _ di -- _ vin, _ _ _ _ _ _
  \l L'Hom -- me de -- meu -- re le _ sien.  _ _ _ _ _ _ _
}

verseFour = \lyricmode {
  \set ignoreMelismata = ##t
  \set stanza = "4 "
  % See! He lifts His hands above;
  % See! He shows the prints of love;
  % Hark! His gracious lips bestow
  % Blessings on His Church below.
  \l De -- vant le trô -- ne di -- _ vin, _ _ _ _ _ _ _
  \l Pour nous il lè -- ve ses _ mains, _ _ _ _ _ _ _
  \l Où se gra -- vent pour _ tou -- _ jours, _ _ _ _ _ _
  \l les mar -- ques de son a -- _ mour. _ _ _ _ _ _ _
}

% verseFour = \lyricmode {
%   \set ignoreMelismata = ##t
%   \set stanza = "4 "
%   % \l Still for us He in -- ter -- cedes,  Al -- le -- lu -- ia!
%   \l Tout -pui -- sant In -- ter -- ces -- seur,
%   % \l His pre -- vail -- ing death he pleads,  Al -- le -- lu -- ia!
%   \l Qui plai -- des pour nous pé -- cheurs,
%   % \l Near Him -- self pre -- pares our place,  Al -- le -- lu -- ia!
%   \l Fais -- nous place au -- près de toi,
%   % \l Har -- bing -- er of hu -- man race,  Al -- le -- lu -- ia!
%   \l Dans le ciel, ô Roi des rois.
% }

\score {
  <<
    \new Staff  <<
      \new Voice = "soprano" { \voiceOne \melody }
      \new Voice = "alto" { \voiceTwo \alto }
    >>
    \new Lyrics \with{\lyricspace} \lyricsto soprano \verseOne
    \new Lyrics  \lyricsto soprano \verseTwo
    \new Lyrics  \lyricsto soprano \verseThree
    \new Lyrics  \lyricsto soprano \verseFour
    \new Staff  <<
      \clef bass
      \new Voice = "tenor" { \voiceOne \tenor }
      \new Voice = "bass" { \voiceTwo \bass }
    >>
  >>
}

\markup { \vspace #0.5 } 

\markup {
  \fill-line {
    \hspace #0.1 % moves the column off the left margin;
     % can be removed if space on the page is tight
     \column {
      \line { \bold "5 "
        \column {
          % Still for us He intercedes,
          % His prevailing death He pleads,
          % Near Himself prepares our place,
          % He the first-fruits of our race.
          "Tout-puissant Intercesseur, Alléluia!"
          "Qui plaides pour nous pécheurs, Alléluia!"
          "Fais-nous place auprès de toi, Alléluia!"
          "Dans le ciel, ô Roi des rois. Alléluia!"
        }
      }
    }
    \hspace #0.1 % adds horizontal spacing between columns;
    \column {
      \line { \bold "6 "
        \column {
          % NEW
          % Lord, though parted from our sight
          % Far above the starry height,
          % Grant our hearts may thither rise,
          % Seeking Thee above the skies.
          "Malgré ton départ, Seigneur, Alléluia!"
          "Tourne-nous à ta hauteur, Alléluia!"
          "Qu'on fasse la volonté de Dieu, Alléluia!"
          "Sur la terre comme aux cieux. Alléluia!"
        }
      }
    }
  \hspace #0.1 % gives some extra space on the right margin;
  % can be removed if page space is tight
  }
}

