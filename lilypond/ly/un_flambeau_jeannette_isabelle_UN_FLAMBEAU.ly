\include "../common_liturgy.ly"
% https://ks4.imslp.info/files/imglnks/usimg/e/e0/IMSLP597869-PMLP961835-1_Nunn_4french_carols.pdf_1.pdf

global = {
  \key g \major
  \time 3/8
  \autoBeamOff
  \override Staff.TimeSignature #'stencil = ##f
}

\layout {
  \context {
    \Score
    \omit BarNumber
  }
}

melody = \relative c' {
  \global
  d'4 g,8 | g8[ fis8] g8 | a8 b8 c8 | b4 a8 | \break
  d4 g,8 | g8[ fis8] g8 | a8 b8 a8 | g4. | \break
  d'4 d8 | d8[ c8] b8 | b8 a8 g8 | g4 fis8 | \break
  e8 fis8 g8 | d4 d8 | c'4 c8 | b4 a8 | \break

  g4 b8\rest | a4 b8\rest |
  b8[ c8] b8 | a4 d8 | b4 a8 | \break
  g4 b8\rest | a4 b8\rest |
  b8[ c8] b8 | a4 d8 | g,4.
  \bar "|."
} 

alto = \relative c' {
  \global
  g'4 d8 | e4 e8 | e8 e8 e8 | d4 d8 |
  d4 d8 | e4 e8 | fis8 fis8 fis8 | g4. |
  g4 g8 | g8[ fis8] g8 | d8 d8 cis8 | d4 d8 |
  e8 e8 e8 | d4 d8 | e4 e8 | d4 fis8 |

  g4 s8 | fis4 s8 |
  g4 g8 | a4 fis8 | g4 fis8 |
  g4 s8 | e4 s8 |
  d4 e8 | fis4 fis8 | g4.
}

tenor = \relative c {
  \global
  b'4 b8 | b8[ a8] b8 | a8 gis8 a8 | fis4 fis8 |
  g4 g8 | b8[ a8] b8 | c8 c8 c8 | b4. |
  d4 d8 | d4 d8 | g,8 a8 a8 | a4 a8 |
  c8 c8 g8 | g4 g8 | g4 g8 | fis4 c'8 |

  b4 d,8\rest | d'4 d,8\rest |
  d'8[ e8] d8 | d4 d8 | d4 c8 |
  b4 d,8\rest | e4 d8\rest |
  g4 g8 | c4 c8 | b4.
}

bass = \relative c {
  \global
  g'4 g8 | e4 e8 | c8 b8 a8 | d4 c8 |
  b4 b8 | e4 e8 | d8 d8 d8 | g4. |
  b4 b8 | b8[ a8] g8 | g8 fis8 e8 | d4 d8 |
  c8 c8 c8 | b4 b8 | a4 a8 | d4 d8 |

  e4 s8 | d4 s8 |
  g4 g8 | fis4 d8 | g4 d8 |
  e4 s8 | c4 s8 |
  b4 c8 | d4 d8 | g4.
}

verseOne = \lyricmode {
  \set ignoreMelismata = ##t
  \set stanza = "1 "
  \l Un flam -- beau, _ Jean -- nette, Is -- a -- bel -- le;
  \l un flam -- beau, _ cou -- rons au ber -- ceau.
  \l C'est Jé -- sus, _ bon -- nes gens du ha -- meau;
  \l le Christ est né! Ma -- rie ap -- pel -- le:
  \l Ah! Ah! Que _ la Mère est bel -- le!
  \l Ah! Ah! Que l'En -- _ fant est beau.
}

verseTwo = \lyricmode {
  \set ignoreMelismata = ##t
  \set stanza = "2 "
  \l C'est un tort _ quand l'En -- fant som -- meil -- le,
  \l c'est un tort _ de cri -- er si fort.
  \l Tai -- sez vous _ l'un et l'au -- tre d'a -- bord;
  \l Au moin -- dre bruit, Jé -- sus s'é -- veil -- le.
  \l Chut! Chut! Chut, il dort à mer -- veil -- le!
  \l Chut! Chut! Chut, voy -- ez comme il dort.
}

verseThree = \lyricmode {
  \set ignoreMelismata = ##t
  \set stanza = "3 "
  % ADAPTED
  \l Dou -- ce -- ment _ dans l'é -- ta -- ble clo -- se,
  \l dou -- ce -- ment _ ve -- nez un mo -- ment.
  \l Ap -- pro -- chez, _ que Jé -- sus est char -- mant!
  \l Comme il est pur, comme il est ro -- se!
  \l Do! Do! Do, que l'En -- fant re -- po -- se!
  \l Do! Do! Do, qu'il rit en dor -- mant.  
}


\score {
  <<
    \new Staff  <<
      \new Voice = "soprano" { \voiceOne \melody }
      \new Voice = "alto" { \voiceTwo \alto }
    >>
    \new Lyrics \with{\lyricspace} \lyricsto soprano \verseOne
    \new Lyrics  \lyricsto soprano \verseTwo
    \new Lyrics  \lyricsto soprano \verseThree
    \new Staff  <<
      \clef bass
      \new Voice = "tenor" { \voiceOne \tenor }
      \new Voice = "bass" { \voiceTwo \bass }
    >>
  >>
}
