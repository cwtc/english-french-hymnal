\include "../common_liturgy.ly"

global = {
  \key g \major
  \time 4/4
  \autoBeamOff
  \override Staff.TimeSignature #'stencil = ##f
}

\layout {
  \context {
    \Score
    \omit BarNumber
  }
}

melody = \relative c'' {
  \global
  b4. b8 d4 b |
  g a b2 |

  b4. b8 d4 b |
  g a b2 |

  b4. b8 c4 c |
  a4. a8 b2 |

  b4 cis d g, |
  fis e d2 |

  fis4. fis8 a4 fis |
  g a b2 |

  b4. b8 d4 b |
  c d e2 |

  e4. e8 c4 a |
  d4. d8 b2 |

  c4 e d g, |
  b a g2 \bar "|."
} 

alto = \relative c' {
  \global
  d4. d8 d4 d |
  e fis g2 

  d4. d8 d4 d |
  e e dis2 

  e4. e8 e4 e |
  d d d2 

  d4 e d e |
  d cis d2 

  d4. d8 d4 d |
  d c d2 

  d4. d8 g4 g |
  g f e2 

  e4 gis a e |
  d fis g2 

  g4 g g g |
  g fis g2 
}

tenor = \relative c' {
  \global
  g4. g8 a4 b |
  b d d2 

  g,4. g8 a4 b |
  b c fis,2 

  g4. g8 a4 a |
  fis fis g2 

  g4 g a b |
  a4. g8 fis2 

  a4 a fis a |
  g4. fis8 g2 

  g4. g8 b4 d |
  c4. b8 c2 

  b4 e e c |
  a d d2 

  c4 c d b |
  d4. c8 b2 
}

bass = \relative c' {
  \global
  g4. g8 fis4 g |
  e d g2 

  g4. g8 fis4 g |
  e c b2 

  e4. e8 a,4 a |
  d d g,2 

  g'4 e fis g |
  a a, d2 

  d4. d8 d4 c |
  b a g2 

  g4. g8 g'4 f |
  e d c2 

  gis'4 e a4. a8 |
  fis4 d g2 

  e4 c b e |
 
 d d g,2 

}

verseOne = \lyricmode {
  % \set ignoreMelismata = ##t
  \set stanza = "1 "
  % Come, ye thankful people, come,
  % Raise the song of harvest-home:
  % All is safely gathered in,
  % Ere the winter storms begin;
  % God, our Maker, doth provide
  % For our wants to be supplied:
  % Come to God's own temple, come,
  % Raise the song of harvest-home.
  \l Re -- con -- nai -- sants, nous ve -- nons
  \l chan -- ter sa gran -- de mois -- son:
  \l La ré -- colte est en -- gran -- gée,
  \l du temps gla -- cés pro -- te -- gée;
  \l Dieu, le Cré -- a -- teur, four -- nit
  \l que tout be -- soin soit rem -- pli:
  \l ve -- nons à son tem -- ple, donc;
  \l chan -- tons sa gran -- de mois -- son.
}

verseTwo = \lyricmode {
  \set ignoreMelismata = ##t
  \set stanza = "2 "
  % All the world is God's own field,
  % Fruit unto His praise to yield,
  % Wheat and tares together sown,
  % Unto joy or sorrow grown;
  % First the blade, and then the ear,
  % Then the full corn shall appear:
  % Lord of harvest, grant that we
  % Wholesome grain and pure may be.
  \l Le monde est son champ à lui,
  \l cul -- ti -- vé pour por -- ter fruit;
  \l l'i -- vra -- ie et le bon grain
  \l pousse aux joies ou aux cha -- grins;
  \l D'a -- bord l'her -- be, puis l'é -- pi,
  \l puis l'é -- pi se -- ra rem -- pli:
  \l O Sei -- gneur de la mois -- son,
  \l que du pur grain nous soy -- ons.
}

verseThree = \lyricmode {
  % \set ignoreMelismata = ##t
  \set stanza = "3 "
  % For the Lord our God shall come,
  % And shall take His harvest home;
  % From His field shall in that day
  % All offenses purge away;
  % Give His angels charge at last
  % In the fire the tares to cast,
  % But the fruitful ears to store
  % In His garner evermore.
  \l Dieu et ses an -- ges vien -- dront
  \l ra -- mas -- ser cet -- te mois -- son;
  \l il vien -- dra cet -- te jour -- née
  \l toute of -- fense é -- li -- mi -- ner;
  \l L'i -- vra -- ie il va li -- er
  \l en ger -- bes pour la brû -- ler,
  \l mais il gar -- de -- ra le blé
  \l pour tou -- jours dans son gre -- nier.
}

verseFour = \lyricmode {
  % \set ignoreMelismata = ##t
  \set stanza = "4 "
  % Even so, Lord, quickly come
  % To Thy final harvest-home;
  % Gather Thou Thy people in,
  % Free from sorrow, free from sin;
  % There, forever purified,
  % In Thy presence to abide:
  % Come, with all Thine angels, come,
  % Raise the glorious harvest-home.
  \l Main -- te -- nant, Sei -- gneur, viens donc
  \l fai -- re ta gran -- de mois -- son;
  \l En -- grange au fi -- nal les tiens,
  \l li -- bé -- rés de leurs cha -- grins;
  \l qu'on soit en -- fin pu -- ri -- fiés;
  \l qu'on puisse a -- vec toi res -- ter
  \l ce jour dans ton nou -- veau champ
  \l a -- près ta gran -- de mois -- son.
}


\score {
  <<
    \new Staff  <<
      \new Voice = "soprano" { \voiceOne \melody }
      \new Voice = "alto" { \voiceTwo \alto }
    >>
    \new Lyrics \with{\lyricspace} \lyricsto soprano \verseOne
    \new Lyrics  \lyricsto soprano \verseTwo
    \new Lyrics  \lyricsto soprano \verseThree
    \new Lyrics  \lyricsto soprano \verseFour
    \new Staff  <<
      \clef bass
      \new Voice = "tenor" { \voiceOne \tenor }
      \new Voice = "bass" { \voiceTwo \bass }
    >>
  >>
}


