\include "../common_liturgy.ly"

global = {
  \key g \major
  \time 3/4
  \autoBeamOff
  \override Staff.TimeSignature #'stencil = ##f
}

\layout {
  \context {
    \Score
    \omit BarNumber
  }
}

melody = \relative c' {
  \global
  \partial 4
  d4 |
  g g a |
  b4 b4 d4 |
  g, a fis |
  g2 \bar "|" \break

  a4 |
  b a g |
  fis4 fis4 g4 |
  a g8[ fis] e4 |
  d2 \bar "|" \break

  fis4^\markup { \italic "Refrain" } |
  g a b |
  g e c' |
  b a g |
  d'2 \bar "|" \break

  d,4 |
  e fis g |
  a d, b' |
  e, a fis |
  g2 \bar "|."
} 

alto = \relative c' {
  \global
  \partial 4
  b4 |
  b d d |
  d4 d4 d4 |
  e e d |
  d2

  d4 |
  d d d8[ e] |
  a,4 a4 b4 |
  d e8[ d] cis4 |
  d2

  dis4 |
  e e dis |
  b b d |
  d d g |
  fis2

  d4 |
  d c d |
  d b d |
  c e d |
  d2
}

tenor = \relative c' {
  \global
 \partial 4
  g4 |
  g g fis |
  g4 g4 a4 |
  b c a |
  b2

  fis4 |
  g a b8[ cis] |
  d4 d4 d4 |
  a b e, |
  fis2

  b4 |
  b e, fis |
  g g a |
  g d'4. cis8 |
  d2

  g,4 |
  g c b |
  a fis g |
  g c a |
  b2 
}

bass = \relative c {
  \global
  \partial 4
  g4 |
  g b d |
  g4 g4 fis4 |
  e c d |
  g,2

  d'4 |
  g fis e |
  d4 d4 b4 |
  fis g a |
  d2

  b4 |
  e c b |
  e e fis |
  g fis e |
  d2

  b4 |
  c a g |
  fis b g |
  c a d |
  g,2
}

verseOne = \lyricmode {
  % \set ignoreMelismata = ##t
  \set stanza = "1 "
  % O worship the King, all-glorious above,
  % And gratefully sing His wonderful love;
  % Our Shield and Defender, the Ancient of Days,
  % Pavilioned in splendor and girded with praise.

  \l Pro -- tè -- ge l'en -- fan -- ce, Jé -- sus, bon Pas -- teur;
  \l de son in -- no -- cen -- ce, con -- ser -- ve la fleur.
}

verseTwo = \lyricmode {
  % \set ignoreMelismata = ##t
  \set stanza = "2 "
  % O tell of His might and sing of His grace,
  % Whose robe is the light, whose canopy space;
  % His chariots of wrath the deep thunder-clouds form,
  % And dark is His path on the wings of the storm.

  \l Lor -- sque, va -- ga -- bon -- de, la bre -- bis s'en -- fuit,
  \l à tra -- vers le mon -- de, ton a -- mour la suit.
}

verseThree = \lyricmode {
  % \set ignoreMelismata = ##t
  \set stanza = "3 "
  % The earth with its store of wonders untold,
  % Almighty, Thy power hath founded of old:
  % Hast stablished it fast by a changeless decree,
  % And round it hast cast like a mantle the sea.
  
  \l Trop long -- temps re -- bel -- le au di -- vin Pas -- teur,
  \l bre -- bis in -- fi -- dè -- le, re -- viens sur son cœur.

  \l De la dent cru -- el -- le des loups ra -- vis -- sants,
  \l ô Gar -- dien fi -- dè -- le, Toi seul nous dé -- fends.
}

verseFour = \lyricmode {
  % \set ignoreMelismata = ##t
  \set stanza = "4 "
  % Thy bountiful care what tongue can recite?
  % It breathes in the air, it shines in the light,
  % It streams from the hills, it descends to the plain
  % And sweetly distills in the dew and the rain.

  \l Vois comme il s'em -- pres -- se pour te re -- ce -- voir,
  \l lui dont la ten -- dres -- se est ton seul es -- poir.
}

\score {
  <<
    \new Staff  <<
      \new Voice = "soprano" { \voiceOne \melody }
      \new Voice = "alto" { \voiceTwo \alto }
    >>
    \new Lyrics \with{\lyricspace} \lyricsto soprano \verseOne
    \new Lyrics  \lyricsto soprano \verseTwo
    \new Lyrics  \lyricsto soprano \verseThree
    \new Lyrics  \lyricsto soprano \verseFour
    \new Staff  <<
      \clef bass
      \new Voice = "tenor" { \voiceOne \tenor }
      \new Voice = "bass" { \voiceTwo \bass }
    >>
  >>
}

\markup { \vspace #0.5 } 

\markup {
  \fill-line {
    \hspace #0.1 % moves the column off the left margin;
     % can be removed if space on the page is tight
     \column {
      \line { \bold "5 "
        \column {
          "O Pasteur suprême! Soumis à ta loi,"
          "Pour toujours je t'aime et me donne à toi."
        }
      }
    }
    \hspace #0.1 % adds horizontal spacing between columns;
    \column {
      \line { \bold "6 "
        \column {
          "Au gras paturages, conduis tes brebis"
          "sous les frais ombrages de ton paradis."
        }
      }
    }
  \hspace #0.1 % gives some extra space on the right margin;
  % can be removed if page space is tight
  }
}
