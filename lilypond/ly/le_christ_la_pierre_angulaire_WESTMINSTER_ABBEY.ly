\include "../common_liturgy.ly"

global = {
  \key g \major
  \time 3/4
  \autoBeamOff
  \override Staff.TimeSignature #'stencil = ##f
}

\layout {
  \context {
    \Score
    \omit BarNumber
  }
}

melody = \relative c'' {
  \global
  b2 d4 |
  b2 g4 |
  c2 a4 |
  fis2 d4 |
  g2 a4 |
  b4.( a8) g4 | \break
  fis e2 |
  d2. |

  fis2 a4 |
  fis2 d4 |
  g2 a4 |
  b2 g4 | \break
  c2 b4 |
  a4.( b8) g4 |
  g8[ fis] fis2 |
  e2. |

  gis2 gis4 |
  a2 a4 |  \break
  fis2 fis4 |
  g2 g4 |
  e2 e4 |
  fis2 g4 |
  g2 fis4 |
  g2. \bar "|."
} 

alto = \relative c' {
  \global
  d2 d4 |
   d2 d4 |
   e2 e4 |
   d2 a4 |
   d2 c4 |
   b( d) cis |
   d2 cis4 |
   d2. | 
   
   d2 e4 |
   d2 a4 |
   b2 c4 |
   d2 d4 |
   e( fis8[ g]) fis4 |
   e8[( d] e4) b4 |
   c b2 |
   b2. |
   
   e2 e4 |
   e2 e4 |
   d2 d4 |
   d2 d4 |
   c2 c4 |
   d( c) b |
   a2 a8[ b16 c] |
   b2. |
}

tenor = \relative c' {
  \global
  g2 a4 |
    b2 b8[ a] |
    g2 a4 |
    a2 fis4 |
    b2 fis4 |
    g4.( fis8) e4 |
    fis8[ g] a4( g) |
    fis2. |
    a2 a4 |
    a2 fis4 |
    d4.( g8) fis4 |
    g2 b4 |
    g2 a8[ b] |
    c4( e,) dis |
    e2 dis4 |
    e2. |
    
    b'2 b4 |
    a2 a4 |
    a2 a4 |
    g2 g4 |
    g2 a4 |
    a2 g4 |
    fis8[( e] d4) d4 |
    d2.
}

bass = \relative c' {
  \global
  g2 fis4 |
  g2 g8[ fis] |
  e2 c4 |
  d2 d8[ c] |
  b2 a4 |
  g4.( d'8) e4 |
  d4 a2 |
  d2. |

  d2 cis4 |
  d2 d8[ c] |
  b2 a4 |
  g2 g'4 |
  e2 d4 |
  c2 b4 |
  a4 b2 |
  e2. |

  e2 d4 |
  cis2 cis4 |
  d2 c4 |
  b2 b4 |
  c2 a4 |
  d2 g4 |
  d2 d4 |
  g,2.
}

verseOne = \lyricmode {
  % \set ignoreMelismata = ##t
  \set stanza = "1 "
  \l Le Christ, la pierre an -- gu -- lai -- re,
  \l il est le fer -- me sou -- tien 
  \l des murs pré -- cieux qu'on vé -- nè -- re 
  \l de Si -- on, ce lieu très saint;
  \l lui, l'in -- é -- bran -- lable a -- si -- le
  \l du fi -- dè -- le chré -- ti -- en.
}

verseTwo = \lyricmode {
  % \set ignoreMelismata = ##t
  \set stanza = "2 "
  \l Cet -- te ci -- té, dans son zè -- le,
  \l a con -- sa -- cré tous ses soins
  \l à la gloi -- re im -- mor -- tel -- le
  \l du vrai Dieu qui la dé -- fend;
  \l el -- le chante en al -- lé -- gres -- se
  \l de son pou -- voir tri -- om -- phant.
}

verseThree = \lyricmode {
  % \set ignoreMelismata = ##t
  \set stanza = "3 "
  \l Toi, ob -- jet de nos lou -- an -- ges,
  \l viens et des -- cends donc du ciel
  \l ré -- gner par -- mi tous tes an -- ges 
  \l sur cha -- que sac -- ré au -- tel;
  \l toi, le flam -- beau sal -- u -- tai -- re,
  \l viens luire aux fai -- bles mor -- tels.
}

verseFour = \lyricmode {
  % \set ignoreMelismata = ##t
  \set stanza = "4 "
  \l Que l'on, par nos pures of -- fran -- des,
  \l puisse at -- ti -- rer les ef -- fets,
  \l sur nos très hum -- bles de -- man -- des. 
  \l de ton im -- men -- se bon -- té;
  \l qu'on voie en nous l'hé -- ri -- ta -- ge
  \l de cha -- cun de tes bien -- faits.
}


\score {
  <<
    \new Staff  <<
      \new Voice = "soprano" { \voiceOne \melody }
      \new Voice = "alto" { \voiceTwo \alto }
    >>
    \new Lyrics \with{\lyricspace} \lyricsto soprano \verseOne
    \new Lyrics  \lyricsto soprano \verseTwo
    \new Lyrics  \lyricsto soprano \verseThree
    \new Lyrics  \lyricsto soprano \verseFour
    \new Staff  <<
      \clef bass
      \new Voice = "tenor" { \voiceOne \tenor }
      \new Voice = "bass" { \voiceTwo \bass }
    >>
  >>
}
