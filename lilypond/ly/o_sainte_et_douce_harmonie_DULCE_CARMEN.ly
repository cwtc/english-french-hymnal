\include "../common_liturgy.ly"

% https://www.pgdp.net/wiki/The_English_Hymnal_-_Wiki_Music_Experiment/Hymns52-200/240

global = {
  \key a \major
  \time 4/4
  \autoBeamOff
  \override Staff.TimeSignature #'stencil = ##f
}

\layout {
  \context {
    \Score
    \omit BarNumber
  }
}

melody = \relative c' {
  \global
  a'4 b4 cis4 d4 | e4 d4 cis4 b4 | a4 a4 a4 d4 | cis4 b4 a2 | \break
  fis4 fis4 e4 e4 | a4 a4 a4 gis4 | b4 e4 dis8[ cis8] b8[ a8] | gis4 fis4 e2 | \break
  gis4 gis4 a4 e4 | fis4 gis4 a4 b4 | cis8[ b8] a8[ gis8] fis4 d'4 | cis4 b4 a2
  \bar "|."
} 

alto = \relative c' {
  \global
  e4 e4 e4 a4 | gis4 fis8[ e8] e4 e4 | cis4 e4 fis4 fis4 | e4. d8 cis2 |
  d4 d4 cis4 cis4 | cis4 e4 e4 e4 | e4 e4 e4 fis4 | e4 dis4 \once \override NoteColumn #'force-hshift = #-0.02 e2 |
  e4 e4 e4 cis4 | fis4 eis4 fis4 gis4 || a4 e4 d4 fis4 | e4. d8 cis2
}

tenor = \relative c {
  \global
  cis'4 b4 a4 a4 | b4 a8[ b8] a4 gis4 | a4 a4 a4 a4 | a4 gis4 a2 |
  a4 a4 a8[ b8] cis8[ d8] | e4 cis4 cis4 b4 | gis4 b4 a4 cis4 | b4. a8 gis2 |
  b4 b4 a4 a4 | a4 cis4 cis4 e4 | e4 a,4 a4 a4 | a4 gis4 a2
}

bass = \relative c {
  \global
  a'4 gis4 a4 fis4 | e4 fis8[ gis8] a4 e4 | fis4 cis4 d4 b4 | e4 e4 a,2 |
  d4 d4 a'4 a4 | a,8[ b8] cis8[ d8] e4 e4 | e4 gis4 a4 fis4 | b,4 b4 e2 |
  e4. d8 cis4 a4 | d4 cis4 fis4 e4 | a4 cis,4 d4 b4 | cis8[ d8] e4 a,2
}

verseOne = \lyricmode {
  % \set ignoreMelismata = ##t
  \set stanza = "1 "
  \l Ô sainte et douce har -- mo -- ni -- e!
  \l Al -- lé -- lu -- ia! Chant des cieux!
  \l Chant de la joie in -- fi -- ni -- e
  \l pour tou -- jours dans les hauts lieux,
  \l ré -- pé -- tée, hym -- ne bé -- ni -- e,
  \l par le chœur des bien -- heu -- reux.
}

verseTwo = \lyricmode {
  % \set ignoreMelismata = ##t
  \set stanza = "2 "
  \l Qu'en tes par -- vis, ci -- té sain -- te,
  \l re -- ten -- tisse Al -- lé -- lu -- ia!
  \l Où tou -- jours, li -- bre, sans crain -- te,
  \l de tes fils l'a -- mour bril -- la.
  \l I -- ci, loin de ton en -- cein -- te,
  \l l'ex -- il aux pleurs nous li -- a.
}

verseThree = \lyricmode {
  % \set ignoreMelismata = ##t
  \set stanza = "3 "

  \l L'al -- lé -- lu -- ia sur la ter -- re
  \l ne peut tou -- jours re -- ten -- tir;
  \l de nos pé -- chés la mi -- sè -- re
  \l se fait, hé -- las! Trop sen -- tir;
  \l et bien -- tôt dans la pous -- siè -- re
  \l il fau -- dra nous re -- pen -- tir.
}

verseFour = \lyricmode {
  % \set ignoreMelismata = ##t
  \set stanza = "4 "

  \l En -- tends no -- tre humble re -- quê -- te!
  \l Bien -- heu -- reu -- se Tri -- ni -- té;
  \l fais que no -- tre voix ré -- pè -- te,
  \l dans la cé -- les -- te ci -- té.
  \l Al -- lé -- lu -- ia pour la Fê -- te
  \l de l'é -- ter -- nel -- le bon -- té.
}


\score {
  <<
    \new Staff  <<
      \new Voice = "soprano" { \voiceOne \melody }
      \new Voice = "alto" { \voiceTwo \alto }
    >>
    \new Lyrics \with{\lyricspace} \lyricsto soprano \verseOne
    \new Lyrics  \lyricsto soprano \verseTwo
    \new Lyrics  \lyricsto soprano \verseThree
    \new Lyrics  \lyricsto soprano \verseFour
    \new Staff  <<
      \clef bass
      \new Voice = "tenor" { \voiceOne \tenor }
      \new Voice = "bass" { \voiceTwo \bass }
    >>
  >>
}
