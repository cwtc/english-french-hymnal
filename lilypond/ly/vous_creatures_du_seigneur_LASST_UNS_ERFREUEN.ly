% https://github.com/bbloomf/lilypond-songs/blob/master/ly/Nearer%2C%20My%20God%2C%20to%20Thee.ly
% http://www.hymntime.com/tch/non/fr/m/d/p/l/mdpluspr.htm
\include "../common_liturgy.ly"

global = {
  \key ees \major
  \time 3/2
  \partial 2*1
  % \autoBeamOff
  \override Staff.TimeSignature #'stencil = ##f
}

\layout {
  \context {
    \Score
    \omit BarNumber
  }
}

melody = \relative c' {
  \global
  ees2 |
  ees4 f g ees g aes |
  bes1 ees,2 |
  ees4 f g ees g aes |
  bes1  \bar "||" \break

  ees4 d |
  c2 bes ees4 d |
  c2 bes2 \bar "||" \break
  
  ees2 |
  ees4 bes bes aes g aes |
  bes1 \bar "|" %\break 
  ees2 |
  ees4 bes bes aes g aes |
  bes1 \bar "||" \break
  
  aes4 g |
  f2 ees \bar "|"
  aes4 g |
  f2 ees \bar "|"
  ees'4 d |
  c2 bes \bar "|" \break
  ees4 d |
  c2 bes \bar "|"
  aes4 g |
  f1. |
  ees1 \bar "|."
}

alto = \relative c' {
  \global
  bes2 |
  ees1 ees2 |
  ees( d) ees |
  bes4 d ees2 ees |
  ees2( d) bes' |
  bes4 aes g2 ees4 f |
  g f d2 bes'4 aes |
  g2 ~ g4 f ees2 |
  ees( d) ees4 f |
  ees d ees ees2. |
  ees4 d g f ees2 ~ ees4 d ees2 ees ~ |
  ees4 d c2 g'4 f |
  g f d2 g ~ g4 f2 c4 c ees |
  ees2 d1 |
  bes1
}

tenor = \relative c' {
  \global
  g2 |
  g4 aes bes g c2 |
  bes1 bes4 c |
  bes aes bes2 c |
  bes1 ees2 |
  ees ees g,4 bes |
  bes a bes2 ees |
  ees ~ ees4 bes ~ bes aes |
  f2 bes bes |
  bes ~ bes4 ees d c |
  bes1 c2 ~ |
  c4 bes bes2 c4 bes |
  c aes g2 g4 bes ~ |
  bes a bes2 bes2( |
  g4) aes f g aes bes |
  c2 bes aes |
  g1
}

bass =  \relative c {
  \global
  ees2 |
  ees2 ~ ees4 ees8 d c4 f |
  bes,2 bes'4 aes g aes |
  g f ees d c f |
  bes,2 bes'4 aes g2 |
  aes2 ees c4 d |
  ees f bes,2 g'4 f | 
  ees d c d ees c |
  bes2 bes'4 aes g aes |
  g f ees c' bes aes |
  g f ees d c bes |
  aes2 g f4 g |
  aes bes c2 c4 d |
  ees f bes,2 c4 d |
  ees f d e f g |
  aes2 bes bes, |
  ees1
}

verseOne = \lyricmode {
  \set stanza = "1 "
  % All creatures of our God and King,
  % Lift up your voice and with us sing
  % Alleluya, allelyia!
  % Thou burning sun with golden beam,
  % Thou silver moon with silver gleam:
  % O praise him, O praise him,
  % Alleluya, alleluya, alleluya!

  % TO ADAPT?
  \l Vous cré -- a -- tu -- res du Seig -- neur,
  \l chan -- tez tou -- jours en son hon -- neur,
  \l Al -- lé -- lu -- ia, al -- lé -- lu -- ia!
  \l Car c'est lui seul qu'il faut lou -- er,
  \l Il donne au so -- leil sa clar -- té,
  \l Ren -- dez gloi -- re, ren -- dez gloi -- re,
  \l Al -- lé -- lu -- ia, al -- lé -- lu -- ia,
  \l al -- lé -- lu -- ia!
}

verseTwo = \lyricmode {
  \set stanza = "2 "
  % Thou rushing wind that art so strong,
  % Ye clouds that sail in heav'n along,
  % O praise Him, alleluya!
  % Thou rising morn in praise rejoice,
  % Ye lights of ev'ning, find a voice;
  % O praise him, O praise him,
  % Alleluya, alleluya, alleluya!

  % TO ADAPT
  \l Dieu, sois lou -- é pour le so -- leil,
  \l pour ce grand frè -- re sans pa -- reil,
  % \l Al -- lé -- lu -- ia, al -- lé -- lu -- ia!
  _ _ _ _ _ _ _ _
  \l Et pour la lune et sa lu -- eur,
  \l Pour chaque é -- toi -- le no -- tre sœur,
  % \l Ren -- dez gloi -- re, ren -- dez gloi -- re,
  % \l Al -- lé -- lu -- ia, al -- lé -- lu -- ia,
  % \l al -- lé -- lu -- ia!
}

verseThree = \lyricmode {
  \set stanza = "3 "
  % Thou flowing water, pure and clear,
  % Make music for Thy Lord to hear,
  % Alleluya, alleluya!
  % Thou fire so masterful and bright,
  % That givest man both warmth and light;
  % O praise him, O praise him,
  % Alleluya, alleluya, alleluya!
  
  % TO ADAPT?
  \l Lou -- é sois -- tu pour si -- re feu,
  \l vi -- vant, ro -- bus -- te, glo -- ri -- eux,
  % \l Al -- lé -- lu -- ia, al -- lé -- lu -- ia!
  _ _ _ _ _ _ _ _
  \l La terre en ma -- ter -- nel -- le sœur
  \l Nous com -- ble de ses mil -- le fleurs,
  % \l Ren -- dez gloi -- re, ren -- dez gloi -- re,
  % \l Al -- lé -- lu -- ia, al -- lé -- lu -- ia,
  % \l al -- lé -- lu -- ia!
}

verseFour = \lyricmode {
  \set stanza = "4 "
  % Let all things their Creator bless,
  % And worship Him in humbleness,
  % O praise Him, alleluya!
  % Praise, praise the Father, praise the Son,
  % And praise the Spirit, Three in One;
  % O praise him, O praise him,
  % Alleluya, alleluya, alleluya!

  % TO ADAPT?
  \l Dieu trois fois saint, nous te lou -- ons,
  \l nous te chan -- tons, nous t'a -- do -- rons,
  % \l Al -- lé -- lu -- ia, al -- lé -- lu -- ia!
  _ _ _ _ _ _ _ _
  \l Gloire au Père et lou -- ange au Fils,
  \l Et lou -- é soit le Saint -- Es -- prit,
  % \l Ren -- dez gloi -- re, ren -- dez gloi -- re,
  % \l Al -- lé -- lu -- ia, al -- lé -- lu -- ia,
  % \l al -- lé -- lu -- ia!
}


\score {
  <<
    \new Staff  <<
      \new Voice = "soprano" { \voiceOne \melody }
      \new Voice = "alto" { \voiceTwo \alto }
    >>
    \new Lyrics \with{\lyricspace} \lyricsto soprano \verseOne
    \new Lyrics  \lyricsto soprano \verseTwo
    \new Lyrics  \lyricsto soprano \verseThree
    \new Lyrics  \lyricsto soprano \verseFour
    \new Staff  <<
      \clef bass
      \new Voice = "tenor" { \voiceOne \tenor }
      \new Voice = "bass" { \voiceTwo \bass }
    >>
  >>
}
