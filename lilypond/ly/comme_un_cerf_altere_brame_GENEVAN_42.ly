\include "../common_liturgy.ly"

global = {
  \key f \major
  \cadenzaOn
  \autoBeamOff
  \override Staff.TimeSignature #'stencil = ##f
}

\layout {
  \context {
    \Score
    \omit BarNumber
  }
}

melody = \relative c' {
  \global
  f2 g4 a2 g4 \bar "|"
  f e d2 c \bar "|"
  f2 g4 a2 bes4 \bar "|"
  a2 g2 f1 \bar "|"

  f2 g4 a2 g4 \bar "|"
  f e d2 c \bar "|"
  f2 g4 a2 bes4 \bar "|"
  a2 g2 f1 \bar "|"

  a2 a4 c2 bes4 \bar "|"
  a g a1 \bar "|"
  c2 c4 d2 c4 \bar "|"
  bes4 a g1 \bar "|"

  a2 c4 bes2 a4 \bar "|"
  f4 g a2 f \bar "|"
  a2 a4 bes2 a4 \bar "|"
  g f e2 f2 \bar "|."
} 

alto = \relative c' {
  \global
  c2 e4 f2 e4
  d c bes2 a
  c2 e4 f2 f4
  f2 e c1

  c2 e4 f2 e4
  d c bes2 a
  c e4 f2 f4
  f2 e c1

  f2 f4 f2 f4
  f e f1
  f2 f4 f2 f4
  f f e1

  a2 g4 g2 f4
  d c c2 d
  c2 f4 f2 f4
  e a, c2 c2
}

tenor = \relative c' {
  \global
  a2 c4 c2 c4
  a a f2 f
  a2 c4 c2 d4
  c2 c a1

  a2 c4 c2 c4
  a a f2 f
  a2 c4 c2 d4
  c2 c a1

  c2 c4 c2 d4
  c c c1

  a2 a4 bes2 c4
  d c c1
  c2 ees4 d2 d4
  a g f2 f

  f c'4 d2 c4
  c f, g2 a
}

bass = \relative c {
  \global
  f2 c4 f2 c4
  d a bes2 f
  f'2 c4 f2 bes,4
  f'2 c f,1

  f'2 c4 f2 c4
  d a bes2 f
  f' c4 f2 bes,4
  f'2 c f,1

  f'2 f4 a,2 bes4
  f' c f1
  f2 f4 bes,2 a4
  bes4 f' c1

  f2 c4 g'2 d4
  d e f2 bes,
  f'2 f4 bes,2 f'4
  c d c2 f
}

verseOne = \lyricmode {
  % \set ignoreMelismata = ##t
  \set stanza = "1 "
  \l Comme un cerf al -- té -- ré brâ -- me
  \l a -- près le cou -- rant des eaux,
  \l ain -- si sou -- spi -- re mon â -- me,
  \l Sei -- gneur, a -- près tes ruis -- seaux.
  \l Elle a soif du Dieu vi -- vant,
  \l et s'é -- crie en le sui -- vant:
  \l ô mon Dieu, quand donc se -- ra -ce
  \l que mes yeux ver -- ront ta fa -- ce?
}

verseTwo = \lyricmode {
  % \set ignoreMelismata = ##t
  \set stanza = "2 "
  \l Pour pain je n'ai que mes lar -- mes,
  \l et nuit et jour en tout lieu,
  \l lors -- qu'en mes du -- res a -- lar -- mes,
  \l on me dit: Que fait ton Dieu?
  \l Je re -- gret -- te la sai -- son
  \l où j'al -- lais en ta mai -- son
  \l chan -- tant a -- vec les fi -- dè -- les
  \l tes lou -- an -- ges im -- mor -- tel -- les.
}

verseThree = \lyricmode {
  % \set ignoreMelismata = ##t
  \set stanza = "3 "
  \l Mais quel cha -- grin te dé -- vo -- re?
  \l Mon â -- me, ras -- su -- re -toi:
  \l Es -- père en Dieu, car en -- co -- re
  \l il se -- ra lou -- é de moi,
  \l quand, d'un re -- gard seu -- le -- ment,
  \l il gué -- ri -- ra mon tour -- ment.
  \l Mon Dieu, je sens que mon â -- me
  \l d'un ar -- dent dé -- sir se pâ -- me.
  
}

verseFour = \lyricmode {
  % \set ignoreMelismata = ##t
  \set stanza = "4 "
  \l Je me sou -- viens à toute heu -- re
  \l du temps que, vers le Jour -- dain,
  \l j'a -- vais pour tri -- ste de -- meu -- re
  \l Her -- mon, où j'er -- rais en vain.
  \l Et Mi -- shar, et tous ces lieux,
  \l où j'é -- tais loin des tes yeux!
  \l Par -- tout mes maux me pour -- sui -- vent,
  \l com -- me des flots qui me sui -- vent.
}

verseFive = \lyricmode {
  % \set ignoreMelismata = ##t
  \set stanza = "5 "
  \l Les tor -- rents de ta co -- lè -- re
  \l sur moi cent fois ont pas -- sé,
  \l mais, par ta grâ -- ce, j'es -- pè -- re
  \l qu'en -- fin l'o -- rage est ces -- sé.
  \l Tu me con -- dui -- ras le jour,
  \l et moi la nuit, à mon tour,
  \l lou -- ant ta ma -- jes -- té sain -- te,
  \l je t'ad -- dres -- se -- rai ma plain -- te.
}

\score {
  <<
    \new Staff  <<
      \new Voice = "soprano" { \voiceOne \melody }
      \new Voice = "alto" { \voiceTwo \alto }
    >>
    \new Lyrics \with{\lyricspace} \lyricsto soprano \verseOne
    \new Lyrics  \lyricsto soprano \verseTwo
    \new Lyrics  \lyricsto soprano \verseThree
    \new Lyrics  \lyricsto soprano \verseFour
    \new Lyrics  \lyricsto soprano \verseFive
    \new Staff  <<
      \clef bass
      \new Voice = "tenor" { \voiceOne \tenor }
      \new Voice = "bass" { \voiceTwo \bass }
    >>
  >>
}

\markup { \vspace #0.5 } 

\markup {
  \fill-line {
    \hspace #0.1 % moves the column off the left margin;
     % can be removed if space on the page is tight
     \column {
      \line { \bold "6 "
        \column {
          "Dieu, ma force et ma puissance,"
          "dirai-je, as-tu donc permis"
          "qu’une si longue souffrance"
          "m’expose à mes ennemis?"
          "Leurs fiers, leurs malins propos"
          "me pénètrent jusqu’aux os,"
          "quand ils disent à toute heure:"
          "Où fait ton Dieu sa demeure?"
        }
      }
    }
    \hspace #0.1 % adds horizontal spacing between columns;
    \column {
      \line { \bold "7 "
        \column {
          "Mais pourquoi, mon âme, encore"
          "t’abattre avec tant d’effroi?"
          "Espère au Dieu que j’adore;"
          "il sera loué de moi."
          "Un regard dans sa fureur,"
          "me dit qu’il est mon Sauveur;"
          "et c’est aussi lui, mon âme,"
          "qu’en tous mes maux je réclame."

        }
      }
    }
  \hspace #0.1 % gives some extra space on the right margin;
  % can be removed if page space is tight
  }
}
