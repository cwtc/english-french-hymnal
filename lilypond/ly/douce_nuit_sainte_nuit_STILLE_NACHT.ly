\include "../common_liturgy.ly"

global = {
  \key bes \major
  \time 6/8
  \autoBeamOff
  \override Staff.TimeSignature #'stencil = ##f
}

\layout {
  \context {
    \Score
    \omit BarNumber
  }
}

melody = \relative c' {
  \global
  f8.[ g16] f8 d4. |
  f8.[ g16] f8 d4. |
  c'8.[ b16] c8 a4. | \break
  bes8.[ a16] bes8 f4. |
  
  g4 g8 bes8.[ a16] g8 |
  f8.[ g16] f8 d4. | \break
  g4 g8 bes8.[ a16] g8 |
  f8.[ g16] f8 d4. |
  
  c'4 c8 ees8.[ c16] a8 | \break
  bes4.( d4.) |
  bes8[ f] d8 f8.[ ees16] c8 |
  bes4.~bes4.\bar "|."
}

alto = \relative c' {
  \global
  d8.[ ees16] d8 bes4. |
  d8.[ ees16] d8 bes4. |
  ees8.[ d16] ees8 c4. |
  d8.[ c16] d8 d4. |
  
  ees4 ees8 g8.[ f16] ees8 |
  d8.[ ees16] d8 bes4 s8 |
  ees4 ees8 g8.[ f16] ees8 |
  d8.[ ees16] d8 bes4 s8 |
  
  ees4 ees8 c8.[ ees16] c8 |
  d4.( f4.) |
  d4 bes8 d8.[ c16] a8 |
  bes4.~ bes4. \bar "|."
}

tenor = \relative c' {
  \global
  bes4 bes8 f4. |
  bes4 bes8 f4. |
  a4 a8 f4. |
  f4 f8 bes4. |
  
  bes4 bes8 g8.[ a16] bes8 |
  bes8.[ bes16] bes8 f4. |
  bes4 bes8 g8.[ a16] bes8 |
  bes8.[ bes16] bes8 f4. |
  
  a4 a8 a8.[ a16] f8 |
  f4.( bes4.) |
  f4 f8 f8.[ f16] ees8 |
  d4.~d4. \bar "|."
}

bass = \relative c {
  \global
  bes4 bes8 bes4. |
  bes4 bes8 bes4. |
  f'4 f8 f4. |
  bes,4 bes8 bes4. |
  
  ees4 ees8 ees4 ees8 |
  bes8.[ bes16] bes8 bes4. |
  ees4 ees8 ees8.[ ees16] ees8 |
  bes8.[ bes16] bes8 bes4. |
  
  f'4 f8 f8.[ f16] f8 |
  bes,4.~ bes4. |
  f4 f8 f8.[ f16] f8 |
  bes4.~ bes4. \bar "|."
}

verseOne = \lyricmode {
  \set stanza = "1 "
  \set ignoreMelismata = ##t
  % % \l Si -- _ lent night!
  % \l Ô nuit de paix,
  \l Dou -- _ ce nuit, sain -- _ te nuit!
  % % \l Ho -- _ ly night!
  % \l sain -- _ te nuit!
  % % \l All __ _ is calm,
  % \l Dans _ le ciel
  % % \l all __ _ is bright.
  % \l l'as -- _ tre luit;
  \l Tout _ s'é -- tant en -- _ dor -- mi,
  % % \l Round yon Vir -- _ gin
  % \l Dans les champs tout re -- po -- se en paix.
  % % \l Moth -- er and Child,
  \l Veille un cou -- ple sur leur nou -- veau -né.
  % % \l Ho -- ly In -- fant, so ten -- der and mild,
  % \l Mais sou -- dain, dans l'air pur _ et frais,
  \l En -- fant Saint, _ gen -- til et lan -- gé:
  % % \l Sleep in heav -- en -- ly peace, __ _
  % \l le bril -- lant chœur des an -- ges
  % % \l Sleep _ in heav -- en -- ly peace. __ _
  % \l aux ber -- gers ap -- pa -- rait.
  \l Dors en paix _ cé -- les -- te!
  \l Dors _ en paix _ cé -- les -- te!
}

verseTwo = \lyricmode {
  \set stanza = "2 "
  \set ignoreMelismata = ##t
    % \l Si -- _ lent night!
    % \l Ô nuit de paix,
    % % \l Ho -- _ ly night!
    % \l sain -- _ te nuit!
    \l Dou -- _ ce nuit, sain -- _ te nuit!
    % \l Shep -- _ herds quake at __ _ the sight;
    % \l Les _ ber -- gers sont _ in -- struits:
    \l Aux _ ber -- gers on _ l'a dit:
    % % \l Glo -- ries stream _ from heav -- en a -- far,
    % \l Con -- fi -- ants dans la voix _ des cieux,
    \l A -- lé -- lu -- i -- a! L'ange a chan -- té,
    % % \l Heav’n -- ly hosts __ _ sing Al -- le -- lu -- ia;
    % \l ils s'en vont a -- do -- rer _ leur Dieu.
    \l Les nou -- vel -- les par -- tout pro -- pa -- gées.
    % % \l Christ, the Sav -- ior is born! __ _
    % \l Et Jé -- sus en é -- chan -- ge
    % % \l Christ, _ the Sav -- ior is born! __ _
    % \l Leur sou -- rit ra -- di -- eux.
    \l Christ le Sau -- veur est né! __ _
    \l Christ _ le Sau -- veur est né! __ _
}

verseThree = \lyricmode {
  \set stanza = "3 "
  \set ignoreMelismata = ##t
  % % \l Si -- _ lent night!
  %   \l Ô nuit de paix,
  %   % \l Ho -- _ ly night!
  %   \l sain -- _ te nuit!
  \l Dou -- _ ce nuit, sain -- _ te nuit!
  % % \l Son __ _ of God, love’s __ _ pure light!
  % \l L'es -- per -- ance a re -- lui:
  \l Fils _ de Dieu; ô _ qu'il rit!
  % % \l Ra -- diant beams _ from Thy ho -- ly face,
  % \l Le Sau -- veur de la terre est né;
  \l De ta bou -- che l'a -- mour est ve -- nu;
  % % \l With the dawn of re -- deem -- _ ing grace,
  % \l c'est à nous que Dieu l'a don -- né.
  \l Main -- ten -- ant, _ l'heu -- re de sa -- lut.
  % % \l Je -- sus, Lord, at Thy birth! __ _
  % \l Cé -- lé -- brons ses lou -- an -- ges:
  % % \l Je -- _ sus, Lord, at Thy birth! __ _
  % \l Gloire au Verbe In -- car -- né!
  \l Seig -- neur dès ta nais -- san -- ce!
  \l Seig -- _ neur dès ta nais -- san -- ce!
}


\score {
  <<
    \new Staff  <<
      \new Voice = "soprano" { \voiceOne \melody }
      \new Voice = "alto" { \voiceTwo \alto }
    >>
    \new Lyrics \with{\lyricspace} \lyricsto soprano \verseOne
    \new Lyrics  \lyricsto soprano \verseTwo
    \new Lyrics  \lyricsto soprano \verseThree
    \new Staff  <<
      \clef bass
      \new Voice = "tenor" { \voiceOne \tenor }
      \new Voice = "bass" { \voiceTwo \bass }
    >>
  >>
}
