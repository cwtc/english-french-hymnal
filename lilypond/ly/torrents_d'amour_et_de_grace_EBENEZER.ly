\include "../common_liturgy.ly"

% https://github.com/magdalenesacredmusic/English-Hymns/blob/master/T-Z/Who%20is%20this%20so%20weak%20and%20helpless%20(EBENEZER).ly
% https://topmusic.topchretien.com/chant/t-j-williams-torrents-damour-et-de-grace/
% ADAPT it all!


global = {
  \key aes \major
  \time 4/2
  \autoBeamOff
  \override Staff.TimeSignature #'stencil = ##f
}

\layout {
  \context {
    \Score
    \omit BarNumber
  }
}

melody = \relative c' {
  \global
  f2 \tuplet 3/2 {f4 \melisma  g aes \melismaEnd} g2 f |
  g \tuplet 3/2 {g4  \melisma aes bes \melismaEnd} aes4.( g8) f2 |
  c' \tuplet 3/2 {bes4  \melisma c des \melismaEnd} c4.( bes8) aes2 |
  bes4.( aes8) g2 f1 \bar "|"

  f2 \tuplet 3/2 {f4  \melisma g aes \melismaEnd} g2 f |
  g \tuplet 3/2 {g4  \melisma aes bes \melismaEnd} aes4.( g8) f2 |
  c' \tuplet 3/2 {bes4  \melisma c des \melismaEnd} c4.( bes8) aes2 |
  bes4.( aes8) g2 f1 \bar "||"

  c'2^\markup { \italic "Refrain" } \tuplet 3/2 {aes4  \melisma bes c \melismaEnd} bes2 bes |
  aes \tuplet 3/2 {f4  \melisma g aes \melismaEnd} g2 g |
  f \tuplet 3/2 {f4  \melisma g aes \melismaEnd} bes2 bes |
  aes \tuplet 3/2 {bes4  \melisma aes bes \melismaEnd} c1 \bar "||"

  f,2 \tuplet 3/2 {f4  \melisma g aes \melismaEnd} g2 f |
  g \tuplet 3/2 {g4  \melisma aes bes \melismaEnd} aes4.( g8) f2 |
  c'2 \tuplet 3/2 {bes4  \melisma c des \melismaEnd} c4.( bes8) aes2 |
  bes4.( aes8) g2 f1 \bar "|."
} 

alto = \relative c' {
  \global
  c2 c e f
  f e f c
  c \tuplet 3/2 {ees2 f4} g2 f 
  f e f1

  c2 c e f
  f e f c
  c \tuplet 3/2 {ees2 f4} g2 f 
  f e f1

  ees2 ees ees ees 
  c f f e
  f c f ees
  c f e1

  aes,2 f' e f
  f e f f 
  ees ees ees f 
  f e f1
}

tenor = \relative c' {
  \global
  aes2 c c aes 
  des c c4.( bes8) aes2
  ees' \tuplet 3/2 {bes2 aes4} c2 c 
  des4.( c8) bes2 aes1

  aes2 c c aes 
  des c c4.( bes8) aes2
  ees' \tuplet 3/2 {bes2 aes4} c2 c 
  des4.( c8) bes2 aes1

  aes2 aes aes g 
  aes c c c 
  f, aes aes g
  aes f g1

  f2 c' c aes 
  des c c4.( bes8) aes2
  aes \tuplet 3/2 {g4 aes bes} aes4.( g8) f2 
  des'4.( c8) bes2 aes1
}

bass = \relative c {
  \global
  f,2 aes c des
  bes c f f 
  aes \tuplet 3/2 {g2 f4} e2 f
  bes, c f1

  f,2 aes c des
  bes c f f 
  aes \tuplet 3/2 {g2 f4} e2 f
  bes, c f1

  aes,2 c ees ees 
  f aes, c c 
  f ees des ees 
  f4( ees) des2 c1

  des2 aes c des 
  bes c f f 
  aes ees aes, des
  bes c f1
}

verseOne = \lyricmode {
  % \set ignoreMelismata = ##t
  \set stanza = "1 "
  % O the deep, deep love of Jesus,
  % Vast, unmeasured, boundless, free,
  % Rolling as a mighty ocean
  % In its fulness over me!
  % Underneath me, all around me,
  % Is the current of thy love;
  % Leading onward, leading homeward,
  % To my glorious rest above.
  \l Tor -- rents d'a -- mour et de grâ -- ce,
  \l a -- mour du Sau -- veur en croix!
  \l À ce grand fleu -- ve qui pas -- se,
  \l je m'a -- ban -- donne et je crois.

  \l Je crois à ton sa -- cri -- fi -- ce,
  \l Ô Jé -- sus, A -- gneu de Dieu,
  \l et, cou -- vert par ta jus -- ti -- ce,
  \l j'en -- tre -- rai dans le saint lieu.

}

verseTwo = \lyricmode {
  % \set ignoreMelismata = ##t
  \set stanza = "2 "
  % O the deep, deep love of Jesus,
  % Spread his praise from shore to shore;
  % How he loveth, ever loveth,
  % Changeth never, nevermore;
  % How he watcheth o’er his loved ones,
  % Died to call them all his own;
  % How for them he intercedeth,
  % Watcheth o’er them from the throne.
  \l Ah! Que par -- tou se ré -- pan -- de
  \l ce fleuve à la gran -- de voix:
  \l Que tout l'u -- ni -- vers en -- ten -- de
  \l l'ap -- pel qui vient de la croix!

}

verseThree = \lyricmode {
  % \set ignoreMelismata = ##t
  \set stanza = "3 "
  % O the deep, deep love of Jesus,
  % Love of every love the best;
  % ‘Tis an ocean vast of blessing,
  % ‘Tis a haven sweet of rest.
  % O the deep, deep love of Jesus,
  % ‘Tis a Heaven of heavens to me;
  % And it lifts me up to Glory,
  % For it lifts me up to thee.
  \l Que toute â -- me con -- dam -- née _
  \l pour qui tu ver -- sas ton sang
  \l soit au Pè -- re ra -- men -- née _
  \l par ton a -- mour tout puis -- sant.
}


\score {
  <<
    \new Staff  <<
      \new Voice = "soprano" { \voiceOne \melody }
      \new Voice = "alto" { \voiceTwo \alto }
    >>
    \new Lyrics \with{\lyricspace} \lyricsto soprano \verseOne
    \new Lyrics  \lyricsto soprano \verseTwo
    \new Lyrics  \lyricsto soprano \verseThree
    \new Staff  <<
      \clef bass
      \new Voice = "tenor" { \voiceOne \tenor }
      \new Voice = "bass" { \voiceTwo \bass }
    >>
  >>
}

