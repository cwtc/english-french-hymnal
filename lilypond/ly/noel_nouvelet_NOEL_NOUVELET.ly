\include "../common_liturgy.ly"
% https://ks4.imslp.info/files/imglnks/usimg/c/c2/IMSLP89405-SIBLEY1802.13684.5b83-39087011285469score.pdf

global = {
  \key bes \major
  \time 2/4
  \autoBeamOff
  \override Staff.TimeSignature #'stencil = ##f
}

\layout {
  \context {
    \Score
    \omit BarNumber
  }
}

melody = \relative c'' {
  \global
  g8[ d'8] e8[ c8] | d4 bes4 |
  c8 c16[ d16] bes8[ a8] | g2

  g8[ d'8] e8[ c8] | d4 bes4 |
  c8 c16[ d16] bes8[ a8] | g2

  bes4 a8[ g8] | a4. d,8 |
  bes'8[ bes8] a8[ g8] | a2 \bar "||" \break

  g8^\markup { \italic "Refrain" }[ d'8] e8[ c8] | d4 bes4 |
  c8 c16[ d16] bes8[ a8] | g2
  \bar "|."
} 

alto = \relative c'' {
  \global
  g8[ bes8] c8[ a8] | bes4 g4 |
  a8[ g8] fis8[ e8] | d2

  g8[ bes8] a8[ g8] | bes8[ a8] g8[ f8] |
  e8[ e8] fis8[ fis8] | d2

  g4 e8[ e8] | f4. d8 |
  g8[ f8] e8[ g8] | fis2

  g8[ bes8] a8[ c8] | bes4 g4 |
  e8[ e8] fis8[ fis8] | g2
}

tenor = \relative c' {
  \global
  d8[ d8] d8[ d8] | g4 d4 |
  c8[ g8] d'8[ c8] | bes2 |

  bes8[ d8] c8[ e8] | d4 d4 |
  g,8[ g8] d'8[ c8] | bes2 |

  d4 c8[ c8] | c4. a8 |
  bes8[ d8] cis8[ e8] | d2

  bes8[ d8] c8[ e8] | d4 d4 |
  c8[ a8] d8[ d8] | bes2
}

bass = \relative c' {
  g8[ g8] g8[ g8] | g4 g4 |
  c,8[ c8] d8[ d8] | g2 |

  g4 c,8[ c8] | g'4 g,4 |
  c8[ c8] d8[ d8] | g2 |

  g4 c,8[ c8] | f4. fis8 |
  g8[ g8] a8[ a8] | d,2 |

  g8[ g8] g8[ g8] | g4 bes4 |
  a8[ a8] d,8[ d8] | g2
}

verseOne = \lyricmode {
  \set ignoreMelismata = ##t
  \set stanza = "1 "
  \l No -- ël nou -- vel -- et, No -- ël chan -- _ tons i -- ci:
  \l Dé -- vo -- tés gens, ren -- dons à _ _ Dieu mer -- ci.
  \l Chan -- tons No -- ël pour le Roi nou -- vel -- et:
  \l No -- ël nou -- vel -- let, No -- ël chan -- _ tons i -- ci!
}

verseTwo = \lyricmode {
  \set ignoreMelismata = ##t
  \set stanza = "2 "
  \l Quand je m'é -- veil -- lai, et j'eus as -- _ sez dor -- mi,
  \l ou -- vris les yeux, vis un ar -- _ _ bre fleu -- ri,
  \l dont il sor -- tait un bou -- ton ver -- meil -- let:
}

verseThree = \lyricmode {
  \set ignoreMelismata = ##t
  \set stanza = "3 "
  \l Quand je le vis, mon cœur fut ré -- _ jou -- _ i,
  \l car gran -- de clar -- té re -- splen -- dis -- _ sait de lui,
  \l comme le so -- leil qui luit au ma -- tin -- et:
}

verseFour = \lyricmode {
  \set ignoreMelismata = ##t
  \set stanza = "4 "
  \l D'un oi -- se -- let a -- près le chant _ ou -- _ ï,
  \l qui aux pas -- teurs di -- sait: “par -- _ _ tez d'i -- ci!
  \l En Beth -- lé -- em trou -- vè -- rent l'a -- gne -- let:”
}


\score {
  <<
    \new Staff  <<
      \new Voice = "soprano" { \voiceOne \melody }
      \new Voice = "alto" { \voiceTwo \alto }
    >>
    \new Lyrics \with{\lyricspace} \lyricsto soprano \verseOne
    \new Lyrics  \lyricsto soprano \verseTwo
    \new Lyrics  \lyricsto soprano \verseThree
    \new Lyrics  \lyricsto soprano \verseFour
    \new Staff  <<
      \clef bass
      \new Voice = "tenor" { \voiceOne \tenor }
      \new Voice = "bass" { \voiceTwo \bass }
    >>
  >>
}

\markup { \vspace #0.5 } 

\markup {
  \fill-line {
    \hspace #0.1 % moves the column off the left margin;
     % can be removed if space on the page is tight
     \column {
      \line { \bold "5 "
        \column {
        "En Bethléem, Marie et Joseph vis,"
        "L'âne et le bœuf, l'Enfant couché au lit:"
        "La crèche était au lieu d'un bercelet:"
        }
      }
      \combine \null \vspace #0.1 % adds vertical spacing between verses
      \line { \bold "6 "
        \column {
          "L'Étoile y vis, qui la nuit éclairait,"
          "Qui d'Orient, dont il était sorti;"
          "En Bethléem, les trois rois amenait:"
        }
      }
      \combine \null \vspace #0.1 % adds vertical spacing between verses
      \line { \bold "7 "
        \column {
          "L'un portait l'or, et l'autre myrrhe aussi,"
          "Et l'autre encens, qui faisait bon senti;"
          "De paradis semblait le jardinet:"
        }
      }
      \combine \null \vspace #0.1 % adds vertical spacing between verses
      \line { \bold "8 "
        \column {
          "Quarante jours, la nourrice attendit;"
          "Entre les bras de Siméon tendit"
          "Deux tourterelles dedans un paneret:"
        }
      }
      \combine \null \vspace #0.1 % adds vertical spacing between verses
      \line { \bold "9 "
        \column {
          "Quand Siméon le vit, fit un haut cri:"
          "“Voici mon Dieu, mon Sauveur, Jésus-Christ!"
          "Voici celui qui gloire au peuple met:”"
        }
      }
    }
    \hspace #0.1 % adds horizontal spacing between columns;
    \column {
      \line { \bold "10 "
        \column {
        "Un prêtre vint, dont je fus ébahi,"
        "Qui les paroles hautement entendit;"
        "Puis les mussa dans un petit livret:"
        }
      }
      \combine \null \vspace #0.1 % adds vertical spacing between verses
      \line { \bold "11 "
        \column {
          % ADAPTED
          "Et puis me dit: “Frère, crois-tu ceci?"
          "Si tu y crois, au ciel seras ravi;"
          "Si tu t'y fermes, crains l'Enfer au gibet:”"
        }
      }
      \combine \null \vspace #0.1 % adds vertical spacing between verses
      \line { \bold "12 "
        \column {
          "Voici mon Dieu, mon Sauveur Jésus-Christ;"
          "Voici celui qui gloire au peuple met."
          "En trente jours fut Noël accompli:"
        }
      }
      \combine \null \vspace #0.1 % adds vertical spacing between verses
      \line { \bold "13 "
        \column {
          % ADAPTED
          "En trente jours, Noël fut accompli;"
          "Par douze vers, voici le chant fini;"
          "Par chaque jour, on chante le couplet:"
        }
      }
    }
  \hspace #0.1 % gives some extra space on the right margin;
  % can be removed if page space is tight
  }
}
