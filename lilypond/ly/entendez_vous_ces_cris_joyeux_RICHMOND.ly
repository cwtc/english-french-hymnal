\include "../common_liturgy.ly"

% https://hymnary.org/hymn/EH1906/page/562

global = {
  \key g \major
  \time 3/4
  \autoBeamOff
  \override Staff.TimeSignature #'stencil = ##f
}

\layout {
  \context {
    \Score
    \omit BarNumber
  }
}

melody = \relative c' {
  \global
  d4 g4 b4 | d2 c4 | b4( c4) a4 | g2
  b4 | a4( d4) g,4 | fis4( g4) e4 | d2 \bar "|" \break
  a'4 | b4( a4) g4 | c4( b4) a4 | d4( c4) b4 | e2
  d4 | e4( c4) b4 | b8( [ a8] g4) fis4 | g2. 
  \bar "|."
} 

alto = \relative c' {
  \global
  b4 d4 g4 | a4( fis4) e4 | d4( e4) c4 | b2
  cis4 | d2 e4 | d2 cis4 | d2
  d4 | d2 e4 | c2 d8[ e8] | f2 f4 | e2
  g4 | e4( fis4) g4 | e4( d4) d4 | d2. 
}

tenor = \relative c' {
  \global
  g4 b4 d4 | d2 g,8[ a8] | b4( g4) fis4 | g2
  g4 | a2 b4 | a2 g4 | fis2
  fis4 | g4( fis4) e4 | a4( g4) fis4 | g4( a4) b4 | g2
  d'4 | c2 d4 | c4( b4) a4 | b2. 
}

bass = \relative c' {
  \global
  g4 g4 g4 | fis4( d4) e8[ fis8] | g4( c,4) d4 | g,2
  e'4 | fis2 g4 | a2 a,4 | d2
  c4 | b2 e4 | a,2 c4 | b4( a4) g4 | c2
  b4 | b4( a4) g4 | c4( d4) d4 | g2. 
}

verseOne = \lyricmode {
  % \set ignoreMelismata = ##t
  \set stanza = "1 "
  % Hark! the glad sound! the Savior comes,
  % The Savior promised long:
  % Let every heart prepare a throne,
  % And every voice a song.

  \l En -- ten -- dez -vous ces cris jo -- yeux:
  \l “Voi -- ci le Ré -- demp -- teur!”
  \l Lou -- ons -le par nos chants pi -- eux,
  \l d'u -- ne voix et d'un cœur.
}

verseTwo = \lyricmode {
  % \set ignoreMelismata = ##t
  \set stanza = "2 "
  % He comes, the prisoners to release,
  % In Satan's bondage held:
  % The gates of brass before him burst,
  % The iron fetters yield.

  \l Cap -- tifs, il vient vous en -- le -- ver
  \l des mains de l'en -- ne -- mi.
  \l Por -- tes d'ai -- rain, chaî -- nes de fer,
  \l tout cè -- de de -- vant lui.
}

verseThree = \lyricmode {
  % \set ignoreMelismata = ##t
  \set stanza = "3 "
  % He comes, the broken heart to bind,
  % The bleeding soul to cure:
  % And with the treasures of his grace
  % To enrich the humble poor.
  
  \l Les cœurs meur -- tris et tout san -- glants,
  \l il vient pour les gué -- rir;
  \l il vient de ses dons con -- so -- lants
  \l les pau -- vres se -- cou -- rir.
}

verseFour = \lyricmode {
  % \set ignoreMelismata = ##t
  \set stanza = "4 "
  % Our glad hosannas, Prince of Peace,
  % Thy welcome shall proclaim:
  % And heaven's eternal arches ring
  % With thy beloved Name.

  \l Aux chants que to nous in -- spi -- ras,
  \l des -- cends, Prin -- ce de Paix;
  \l qu'on ré -- pè -- te nos ho -- san -- nas
  \l dans les cieux à ja -- mais.
}


\score {
  <<
    \new Staff  <<
      \new Voice = "soprano" { \voiceOne \melody }
      \new Voice = "alto" { \voiceTwo \alto }
    >>
    \new Lyrics \with{\lyricspace} \lyricsto soprano \verseOne
    \new Lyrics  \lyricsto soprano \verseTwo
    \new Lyrics  \lyricsto soprano \verseThree
    \new Lyrics  \lyricsto soprano \verseFour
    \new Staff  <<
      \clef bass
      \new Voice = "tenor" { \voiceOne \tenor }
      \new Voice = "bass" { \voiceTwo \bass }
    >>
  >>
}
