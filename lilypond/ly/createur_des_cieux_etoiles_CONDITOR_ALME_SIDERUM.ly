\include "../common_liturgy.ly"

% https://github.com/magdalenesacredmusic/English-Hymns/blob/master/A-C/Creator%20of%20the%20stars%20of%20night%20(MODE%20IV%20PLAINSONG).ly

global = {
  \key ees \major
  \cadenzaOn
  \autoBeamOff
  \override Staff.TimeSignature #'stencil = ##f
}

\layout {
  \context {
    \Score
    \omit BarNumber
  }
}

melody = \relative c'' {
  \global
  g8 ees g bes bes c aes bes \bar "|" \noBreak
  bes8 c aes bes aes g f g4 \once \override ChoirStaff.BarLine.bar-extent = #'( -1 . 1 )
  \bar "|"
  bes8 aes f g aes g f ees \bar "|" 
  ees8 g aes bes aes g f g4 \bar "|."
  
  % g8[ aes g] f4( g) \bar "||"
} 

alto = \relative c' {
  \global
  d8 ees4~ ees2~ ees8
  d8 c2 d4 ees
  ees8 c2 d4 ees8
  c8~ c2~ c4 bes4
  
  % ees4. c4 d
}

tenor = \relative c {
  \global
  g8 c4 g'~ g8[ aes] g8~ 
  g8 aes4 f bes~ bes 
  g8 f2 bes4 g8~ 
  g8 ees4 f c d

  % g4. aes4 bes
}

bass = \relative c {
  \global
  s4.ees4 c ees8~ 
  ees8 aes,2 bes4 ees
  ees8 f2
  bes,4 c8~
  c8 c4 f,4 aes g
 
  % c4. aes4 g
}

verseOne = \lyricmode {
  \set ignoreMelismata = ##t
  \set stanza = "1 "
  % Creator of the stars of night,
  %  Thy people’s everlasting light,
  %  Jesu, Redeemer, save us all,
  %  And hear Thy servants when they call.
  \l Cré -- a -- teur des cieux é -- toi -- lés,
  \l sur le trou -- peau des dé -- so -- lés,
  \l viens, Jé -- sus, viens, ô Ré -- demp -- teur,
  \l por -- ter le jour, chas -- ser l'er -- reur.
}

verseTwo = \lyricmode {
  \set ignoreMelismata = ##t
  \set stanza = "2 "
  % Thou, grieving that the ancient curse
  %  Should doom to death a universe,
  %  Hast found the medicine, full of grace,
  %  To save and heal a ruined race.
  \l Il fut é -- mu, ton cœur di -- vin
  \l à no -- tre mal -- heu -- reux des -- tin;
  \l par la ver -- tu de ton a -- mour
  \l tu fer -- mas l'in -- fer -- nal sé -- jour.
}

verseThree = \lyricmode {
  \set ignoreMelismata = ##t
  \set stanza = "3 "
  % Thou cam’st, the Bridegroom of the bride,
  %  As drew the world to evening-tide;
  %  Proceeding from a virgin shrine,
  %  The spotless victim all divine.
  % \l Tu es ve -- nu, cé -- leste É -- poux,
  \l Cé -- leste É -- poux, tu es ve -- nu
  \l quand ar -- ri -- vait le cre -- pus -- cule;
  % \l de la Vierge tu es ar -- ri -- vé,
  \l tu es de la Vierge ar -- ri -- vé,
  \l Vic -- ti -- me pure, im -- ma -- cu -- lée.
}

verseFour = \lyricmode {
  \set ignoreMelismata = ##t
  \set stanza = "4 "
  % At whose dread name, majestic now,
  %  All knees must bend, all hearts must bow;
  %  And things celestial Thee shall own,
  %  And things terrestrial, Lord alone.
  \l À ton grand nom tous pros -- ter -- nés,
  \l au sa -- lut par toi nou -- veau -nés,
  \l a -- vec les ha -- bi -- tants des cieux,
  \l a -- do -- rant, nous t'of -- frons nos vœux.
}


\score {
  <<
    \new Staff  <<
      \new Voice = "soprano" { \voiceOne \melody }
      \new Voice = "alto" { \voiceTwo \alto }
    >>
    \new Lyrics \with{\lyricspace} \lyricsto soprano \verseOne
    \new Lyrics  \lyricsto soprano \verseTwo
    \new Lyrics  \lyricsto soprano \verseThree
    \new Lyrics  \lyricsto soprano \verseFour
    \new Staff  <<
      \clef bass
      \new Voice = "tenor" { \voiceOne \tenor }
      \new Voice = "bass" { \voiceTwo \bass }
    >>
  >>
}

\markup { \vspace #0.5 } 

\markup {
  \fill-line {
    \hspace #0.1 % moves the column off the left margin;
     % can be removed if space on the page is tight
     \column {
      \line { \bold "5 "
        \column {
         %  O Thou whose coming is with dread
         % To judge and doom the quick and dead,
         % Preserve us, while we dwell below,
         % From every insult of the foe.
         "Toi qui viendras, terrifiant,"
         "juger les morts et les vivants,"
         "ô, garde-nous à ton abri"
         "de tout assaut de l'ennemi."
        }
      }
    }
    \hspace #0.1 % adds horizontal spacing between columns;
    \column {
      \line { \bold "6 "
        \column {
         %  To God the Father, God the Son,
         % And God the Spirit, Three in One,
         % Laud, honor, might, and glory be
         % From age to age eternally.
         "Ô Père Éternel, qui nous fis"
         "tes enfants par grâce; à ton Fils,"
         "au divin Esprit, comme à toi,"
         "nous rendons hommage avec foi."
        }
      }
    }
  \hspace #0.1 % gives some extra space on the right margin;
  % can be removed if page space is tight
  }
}
