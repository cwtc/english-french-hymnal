\include "../common_liturgy.ly"

global = {
  \key c \major
  \time 12/8
  \autoBeamOff
  \override Staff.TimeSignature #'stencil = ##f
}

\layout {
  \context {
    \Score
    \omit BarNumber
  }
}

melody = \relative c'' {
  \global 
    \partial 4. g4. | 
    % \partial 3 g4. |
    % \partial 4 g4. \bar "|"
    c4.( c4) b8 b4. a4. | d4. c4. c4. b4. | 
    a4. a4 g8 g4. c4. | \bar "|" \break g4.( g4) f8 e4.
    c'4. | b4. a4. g4. fis4. | d'4.( d4) fis,8 g2. | \bar "|" \break

    g4. a4 b8 c4. g4. | e'4.( e4) c8 c4. b4. |
    b4\rest g8 a4 b8 c4. g4. | \bar "|" \break e'4.( e4) c8 c4. b4 \bar "||"
    g8 |
    g4. c4. a4.( a4) a8 | a4. d4. b4.( b4) \bar "||"
    b8 |
    b4. e4. c4 c8 f4 e8 | e4.( d4.) c2. \bar "|."

    % \partial 4*3 e2. \break
} 

alto = \relative c' {
  \global
  \partial 4.
  e4. |
  c4.( c4) c8 c4. c4. | f4. e4. e4. d4. |
  f4. f4 f8 e4. c4. | e4. d4. c4. 
  e4. | e4. e4. e4. d4. | e4. fis4( d8) d2. |

  b4. c4 d8 c4. f4. | e4.( e4) e8 fis4. g4. |
  s4 b,8 c4 d8 c4. f4. | e4.( e4) e8 fis4. g4. |
  s4. s4 c,8 c4. f4. | d4.( d4) d8 d4.( g4.) |
  e4. g4. e4 e8 a4. | g4.( f4.) e2.

}

tenor = \relative c' {
  \global
  \partial 4.
  g4. | 
  g4.( g4) g8 g4. a4. | a4. a4. g4. g4. |
  c4. d4 d8 c4. g4. | c4.( c4) c8 b4. 
  e,4. | gis4. a4. c4. c4. | c4. c4. b2.  
 
  g4. g4 g8 g4. b4. | c4. g4. a4. b4. |
  c4. b4 a8 g4. b4. | c4. g4. a4. b4. |
  % b4.\rest b4.\rest b4.\rest b4.\rest %| b1\rest \
  R1*12/8 |
  % b4.\rest b4.\rest b4.\rest b4.\rest %| b1\rest \
  R1*12/8 |
  % s1 | s1 | 
  d,2.\rest c'4 c8 d4 c8 | c4.( b4.) c2.
}

bass = \relative c {
  \global
  \partial 4.
  c4. |
  e4.( e4) e8 f4. f4. | d4. e4( f8) g4. g,4.|
  a4. b4 b8 c4. e4. | g4. g,4. c4. 
  a4. | b4. c4. d4. d4. | d4. d4. g2. |

  g4. f4 f8 e4. d4. | c4. e4. d4. g4. |
  g4. f4 f8 e4. d4. | c4. e4. d4. g4. |
  s1*12/8 |
  s1*12/8 |
  s2. a4 a8 d,4 d8 | g4.( g,4.) c2.
}

verseOne = \lyricmode {
  \set stanza = "1 "
  \l Mon -- tez à Dieu, chants d'al -- lé -- gres -- se;
  \l ô cœurs brû -- lés d'un saint a -- mour:
  \l chan -- tez No -- ël; voi -- ci le jour!
  \l Le ciel en -- tier fré -- mit d'iv -- res -- se;
  \l que la nuit som -- bre dis -- par -- ais -- se!
  \l _ _ _ _ _ _ _ _
  \l Mon -- tez à Dieu, chants d'al -- lé -- gres -- se!
}

verseTwo = \lyricmode {
  \set stanza = "2 "
  \l Ô Vier -- ge mè -- re, berce en -- co -- re
  \l l'En -- fant di -- vin, et dans ses yeux
  \l as -- pi -- re la clar -- té des cieux!
  \l De son re -- gard, cé -- leste au -- ro -- re;
  \l sur ton front pur qui se co -- lo -- re:
  \l Voi -- ci __ le __ jour, __  voi -- ci __ le __ jour! __
  \l Une au -- ré -- ol -- le semble é -- clo -- re!
  
}

verseThree = \lyricmode {
  \set stanza = "3 "
  % ADAPTED
  \l Ô Dieu des an -- ges, je t'ap -- pel -- le:
  \l Ô Dieu sau -- veur, j'en -- tends le chœur
  \l rem -- plir les cieux d'un chant vain -- queur!
  \l Laisse à mon âme ou -- vrir son ai -- le:
  \l qu'el -- le s'en -- vole et sente en el -- le:
  \l _ _ _ _ _ _ _ _
  \l Ray -- on -- ner ta flamme é -- ter -- nel -- le!
  
}

chorusEcho = \lyricmode {
  _ _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _ _
  \l Voi -- ci __  le __ jour! __
  \l Voi -- ci! __
}


\score {
  <<
    \new Staff  <<
      \new Voice = "soprano" { \voiceOne \melody }
      \new Voice = "alto" { \voiceTwo \alto }
    >>
    \new Lyrics \with{\lyricspace}  \lyricsto soprano \verseOne
    \new Lyrics  \lyricsto soprano \verseTwo
    \new Lyrics  \lyricsto alto \chorusEcho
    \new Lyrics  \lyricsto soprano \verseThree
    % \new Lyrics  \lyricsto soprano \verseFour
    \new Staff  <<
      \clef bass
      \new Voice = "tenor" { \voiceOne \tenor }
      \new Voice = "bass" { \voiceTwo \bass }
    >>
  >>
}
