\include "../common_liturgy.ly"

global = {
  \key g \major
  \time 6/8
  \autoBeamOff
  \override Staff.TimeSignature #'stencil = ##f
}

\layout {
  \context {
    \Score
    \omit BarNumber
  }
}

melody = \relative c' {
  \global
  \partial 8 d8^\markup { \italic "Antiphon" } |
  g4 a8 b4 b8 |
  c4. b4 a8 |
  g4 fis8 e4 a8 |
  g[ fis] e d4 \bar "|" \break
  d8 |
  g4 a8 b4 b8 |
  
  c4. b4 d8 |
  b4 b8 a[ g] a |
  g2. | \bar "||" \break


  a4. g4 fis8 |
  g2. |
  a4. g4 fis8 |
  
  g4 g8 c4 b8 |
  a4 d8 c4 b8 |
  a4 d8 c4 b8 |
  a4 g8 fis4 g8 |
  a4.~ a4 \bar "|" \break
  d,8 |
  g4 a8 b4 b8 |
  
  c4. b4 a8 |
  g4 fis8 e4 a8 |
  g[ fis] e d4 \bar "|" \break
  d8 |
  d'4 d8 b4 g8 |
  
  c4. b4 d8 |
  b4 b8 a[ g] a |
  g2. \bar "|."
} 

alto = \relative c' {
  \global
  d8 |
  b4 c8 d4 d8 |
  e4. d4 d8 |
  d4 d8 c4 e8 |
  d4 cis8 d4 d8 |
  d4 c8 d4 d8 |
  
  e4. e4 d8 |
  d4 d8 d4 d8 |
  c( a4 b4.) |

  e4. d4 a8 |
  b2. |
  e4. d4 a8 |
  
  e'4 e8 e[ fis] g |
  fis[ e] fis e4 g8 |
  fis4 fis8 g4 g8 |
  e4 e8 e4 e8 |
  d4.~ d4 d8 |
  b4 c8 d4 d8 |
  
  e4. d4 d8 |
  d4 d8 c4 e8 |
  d4 cis8 d4 d8 |
  d[ e] fis g4 d8 |
  
  e4( fis8) g4 fis8 |
  d4 d8 d4 c8 |
  d8( c4 b4.) \bar "|."
}

tenor = \relative c {
  \global
  d8 |
  d4 g8 b4 b8 |
  g4. g4 fis8 |
  b4 b8 c[ b] a |
  a4 g8 fis[ g] a |
  b4 g8 b4 b8 |
  
  g4. g4 a8 |
  b4 g8 fis4 fis8 |
  g2. |

  a4. a4 d8 |
  d2. |
  a8[ b c] b4 d8 |
  
  b4 b8 c4 b8 |
  d4 a8 c4 b8 |
  d4 d8 e4 d8 |
  c4 c8 a4 a8 |
  fis4.~ fis4 d8 |
  d4 g8 b4 b8 |
  
  g4. g4 fis8 |
  b4 b8 c[ b] a |
  a4 g8 fis[ g] a |
  fis[ g] a d4 b8 |
  
  g4( fis8) e4 a8 |
  g4 g8 fis[ g] fis |
  b([ a8 fis] g4.) \bar "|."
}

bass = \relative c {
  \global
  d8 |
  g,4 g8 g4 g8 |
  c4. g4 a8 |
  b4 b8 c4 cis8 |
  d4 a8 d[ e] fis |
  g4 g8 g4 g,8 |
  
  c4( d8) e4 fis8 |
  g4 b,8 d4 d8 |
  g,2. |

  c4( a8) d4 d8 |
  g,2. |
  c8[ b a] b4 d8 |
  
  e4 e8 c[ d] e |
  d[ e] d c[ d] e |
  d[ c] b a4 b8 |
  c4 c8 cis4 cis8 |
  d4.~ d4 d8 |
  g,4 g8 g4 g8 |
  
  c4. g4 a8 |
  b4 b8 c4 cis8 |
  d4 a8 d4 fis8 |
  b,4 b8 g4 g'8 |
  
  c,4. e4 d8 |
  g,8[ a] b d8[ e] d |
  g,2. \bar "|."
}

verseOne = \lyricmode {
  \set ignoreMelismata = ##t
  \l Ve -- nez, di -- vin Mes -- si -- e!
  \l Sau -- vez nous jours in -- for -- _ tu -- nés!
  \l Ve -- nez, sour -- ce de Vi -- e!
  \l Ve -- nez, ve -- nez, _ ve -- nez!

  \set stanza = "1 "
  \l Ah! Des -- cen -- dez, hâ -- tez vos pas;
  \l sau -- vez les hom -- mes du tré -- pas;
  \l se -- cou -- rez -- nous; ne tar -- dez pas. _
  \l Dans un -- e peine ex -- trê -- me,
  \l gé -- mis -- sent nos cœurs af -- _ fli -- gés.
  \l Ve -- nez, Bon -- té Su -- prê -- me!
  \l Ve -- nez, ve -- nez, _ ve -- nez!
}

verseTwo = \lyricmode {
  \set ignoreMelismata = ##t
  _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ 
  _ _ _ _ _ _ _
  \set stanza = "2 "
  \l Ah! Dés -- ar -- mez vo -- tre cour -- roux;
  \l nous sou -- pi -- rons à vos ge -- noux.
  \l Sei -- gneur, nous n'es -- pé -- rons qu'en vous. _
  \l Pour nous li -- vrer la guer -- re,
  \l tous les en -- fers sont dé -- _ chaî -- nés;
  \l des -- cen -- dez sur la ter -- re!
  \l Ve -- nez, ve -- nez, _ ve -- nez!
}

verseThree = \lyricmode {
  \set ignoreMelismata = ##t
  _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ 
  _ _ _ _ _ _ _
  \set stanza = "3 "
  \l Que nos sou -- pirs soient en -- ten -- dus;
  \l les biens que nous a -- vons per -- dus,
  \l ne nous se -- ront -- ils point ren -- dus? _
  \l Vo -- yez cou -- ler nos lar -- mes;
  \l grand Dieu, si vous nous par -- _ don -- nez,
  \l nous n'au -- rons plus d'a -- lar -- mes!
  \l Ve -- nez, ve -- nez, _ ve -- nez!
}

verseFour = \lyricmode {
  \set ignoreMelismata = ##t
  _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ 
  _ _ _ _ _ _ _
  \set stanza = "4 "
  \l Si vous ve -- nez en ces bas -- lieux,
  \l nous vous ver -- rons vic -- to -- ri -- eux,
  \l fer -- mer l'en -- fer, ou -- vrir les cieux. _
  \l Nous l'es -- pé -- rons sans ces -- se:
  \l Les cieux nous fu -- rent des -- _ tin -- és.
  \l Te -- nez vo -- tre pro -- mes -- se!
  \l Ve -- nez, ve -- nez, _ ve -- nez!
}

verseFive = \lyricmode {
  \set ignoreMelismata = ##t
  _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ 
  _ _ _ _ _ _ _
  \set stanza = "5 "
  % ADAPTED
  \l Ah! Puis -- sions -nous chan -- ter un jour
  \l dans vo -- tre bien -- heu -- reu -- se cour,
  \l et vo -- tre gloire et votre a -- mour! _ 
  \l C'est là l'heu -- reux pré -- sa -- ge
  % "De ceux que vous prédestinez,"
  \l de ceux que vous au -- rez _ sau -- vés;
  \l Don -- nez -nous en un ga -- ge!
  \l Ve -- nez, ve -- nez, _ ve -- nez!
}

\score {
  <<
    \new Staff  <<
      \new Voice = "soprano" { \voiceOne \melody }
      \new Voice = "alto" { \voiceTwo \alto }
    >>
    \new Lyrics \with{\lyricspace} \lyricsto soprano \verseOne
    \new Lyrics  \lyricsto soprano \verseTwo
    \new Lyrics  \lyricsto soprano \verseThree
    \new Lyrics  \lyricsto soprano \verseFour
    \new Lyrics  \lyricsto soprano \verseFive
    \new Staff  <<
      \clef bass
      \new Voice = "tenor" { \voiceOne \tenor }
      \new Voice = "bass" { \voiceTwo \bass }
    >>
  >>
}

\markup { \vspace #0.5 } 
