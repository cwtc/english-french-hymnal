\include "../common_liturgy.ly"
% https://www.pgdp.net/wiki/The_English_Hymnal_-_Wiki_Music_Experiment/Hymns51-100/161

global = {
  \key ees \major
  \cadenzaOn
  \autoBeamOff
  \override Staff.TimeSignature #'stencil = ##f
}

\layout {
  \context {
    \Score
    \omit BarNumber
  }
}

ln = {\once \override NoteHead #'duration-log = #0 
           \once \override Stem #'stencil = ##f
           \once \override NoteColumn #'force-hshift = #'0.4 }
push = \once \override NoteHead #'X-offset = #'1

melody = \relative c' {
  \global
  aes'8 bes[ des] des c[ bes aes] bes bes[ c bes] aes g[ f] \bar "|" \break
  bes bes c aes8[ g f] aes g[ aes] f[ ees] ees[ f] \bar "|" \break
  f f aes f[ ees] aes aes[ bes c] bes bes[ aes] \bar "|" \break
  aes aes[ c des! c bes] c aes[ g f] aes g[ aes] f[ ees] ees[ f]
  \bar "|."
} 

alto = \relative c' {
  \global
  c8 ees[ f] f \ln ees4. ees8 \ln ees4. ees8 ees8[ c] \bar "|" d! \ln ees4

  \ln c4. ees8 des[ ees] des[ c] bes[ c] \bar "|" des! c ees \ln  c4. \ln ees4.

  f8 g[ ees] \bar "|" f \ln ees4( \ln des!4.) ees8  \ln ees4( des!8) ees8 \ln ees4 des!8[ c] \ln c4
}

tenor = \relative c {
  \global
  aes'8 g[ aes] aes aes[ bes c] bes g[ aes g] aes bes[ aes] \bar "|" f \ln g4 

  f8[ g aes] aes bes[ c] \ln aes4 bes8[ aes] \bar "|" \ln aes4 ~ aes8 \ln aes4. \ln \push c4.

  des8 bes[ c] \bar "|" aes \ln \push aes4( \ln f4.) aes8 \ln \push aes4. aes8 bes[ aes] \ln aes4 g8[ aes]
}

bass = \relative c {
  \global
  f8 ees[ des] des \ln aes'4. g8 \ln ees4. c8 ees[ f] \bar "|" bes, ees c
  \ln f4. c8 bes[ aes] des![ aes] g[ f] \bar "|"
  des'! f c \ln  aes'4. \ln \push aes,4.

des!8 ees[ aes] \bar "|" des,!8 
     \once\override Slur  #'control-points = #'((2 . -1.5) (4 . -2.5) (7 . -2.5 ) (9 . -1.8 ))   
     \ln \push  c4( \ln bes4.) aes8 
     % \once\override Slur  #'control-points = #'((2 . -1.5) (4 . -2.5) (7 . -2.0 ) (9 . -1.2 ))
     \ln \push c4 des!8 c ees[ c] des![ aes] c[ f,]
}

verseOne = \lyricmode {
  % \set ignoreMelismata = ##t
  \set stanza = "1 "
  \l L'é -- ten -- dard vient __ du Roi __ des rois; __
  \l le my -- stè -- re __ vient de __ la __ croix, __
  \l où pend en chair sainte et __ 
  \set ignoreMelismata = ##t
  sa -- cré -- e,
  \set ignoreMelismata = ##f
  \l lui qui __ tou -- te __ chair a __ cré -- ée. __
}

verseTwo = \lyricmode {
  % \set ignoreMelismata = ##t
  \set stanza = "2 "
  \l Où de __ plus est __ à mort __ bles -- sé __
  \l le flanc par la __ lan -- ce __ per -- cé; __
  \l pour nous ren -- dre __ nets de __
  \set ignoreMelismata = ##t
  souil -- lu -- re,
  \set ignoreMelismata = ##f
  \l le sang __ sort et __ l'eau tout __ à __
  \set ignoreMelismata = ##t
  l'heu -- re.
}

verseThree = \lyricmode {
  % \set ignoreMelismata = ##t
  \set stanza = "3 "
  \l O -- res __ on voit __ vé -- ri -- fi -- é __
  \l ce que Da -- vid __ a -- vait __ cri -- é: __
  \l que Dieu, par le __ bois qui __ le
  \set ignoreMelismata = ##t
  ser -- re,
  \set ignoreMelismata = ##f
  \l ré -- gne -- rait un __ jour sur __ la __
  \set ignoreMelismata = ##t
  ter -- re.
  
}

verseFour = \lyricmode {
  % \set ignoreMelismata = ##t
  \set stanza = "4 "
  \l Ar -- bre __ beau, tout __ re -- splen -- dis -- sant __
  \l de la pour -- pre __ du Roi __ puis -- sant, __
  \l Ar -- bre sur tous __ au -- tres __
  \set ignoreMelismata = ##t
  in -- sig -- ne,
  \set ignoreMelismata = ##f
  \l par l'at -- tou -- cher __ de chair __  si __
  \set ignoreMelismata = ##t
  di -- gne!
}

verseFive = \lyricmode {
  % \set ignoreMelismata = ##t
  \set stanza = "5 "
  % \l Ar -- bre __ beau, tout __ re -- splen -- dis -- sant __
  % \l de la pour -- pre __ du Roi __ puis -- sant, __
  % \l Ar -- bre sur tous __ au -- tres __
  % \set ignoreMelismata = ##t
  % in -- sig -- ne,
  % \set ignoreMelismata = ##f
  % \l par l'at -- tou -- cher __ de chair __  si __
  % \set ignoreMelismata = ##t
  % di -- gne!
  \l Heu -- reux __ qui tiens __ ès bras pen -- du __
  \l le prix du mon -- de 
  \set ignoreMelismata = ##t
  com -- me
  \set ignoreMelismata = ##f
  per -- du! __
  \l Le corps de -- ça, __ tout en __  
  \set ignoreMelismata = ##t
  ba -- lan -- ce,
  \set ignoreMelismata = ##f
  \l de là, __ l'en -- fer __ et sa __ puis -- 
  \set ignoreMelismata = ##t san -- ce.
  \set ignoreMelismata = ##t
}

\score {
  <<
    \new Staff  <<
      \new Voice = "soprano" { \voiceOne \melody }
      \new Voice = "alto" { \voiceTwo \alto }
    >>
    \new Lyrics \with{\lyricspace} \lyricsto soprano \verseOne
    \new Lyrics  \lyricsto soprano \verseTwo
    \new Lyrics  \lyricsto soprano \verseThree
    \new Lyrics  \lyricsto soprano \verseFour
    \new Lyrics  \lyricsto soprano \verseFive
    \new Staff  <<
      \clef bass
      \new Voice = "tenor" { \voiceOne \tenor }
      \new Voice = "bass" { \voiceTwo \bass }
    >>
  >>
}

\markup { \vspace #0.5 } 

\markup {
  \fill-line {
    \hspace #0.1 % moves the column off the left margin;
     % can be removed if space on the page is tight
     \column {
      \line { \bold "6 "
        \column {
          "Je te salue, ô sainte croix,"
          "notre espoir seul en ces endroits!"
          "Donne aux bons accroît de justice;"
          "pardonne aux pécheurs leur malice."
        }
      }
    }
    \hspace #0.1 % adds horizontal spacing between columns;
    \column {
      \line { \bold "7 "
        \column {
          "Dieu grand, haute Trinité,"
          "tout esprit loue ta bonté."
          "Si la croix sauve les coupables,"
          "rends-nous les perdus perdurables."
        }
      }
    }
  \hspace #0.1 % gives some extra space on the right margin;
  % can be removed if page space is tight
  }
}