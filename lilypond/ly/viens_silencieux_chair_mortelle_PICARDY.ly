\include "../common_liturgy.ly"

global = {
  \key f \major
  \time 4/4
  \autoBeamOff
  \override Staff.TimeSignature #'stencil = ##f
}

\layout {
  \context {
    \Score
    \omit BarNumber
  }
}

melody = \relative c' {
  \global
  d4 e f g |
  a2 a4( g) |
  a2 a \bar "|"

  a4 a bes c |
  bes2 a4( g) |
  a1 \bar "|" \break

  d,4 e f g |
  a2 a4( g) |
  a2 a \bar "|"

  a4 a bes c |
  bes2 a4( g) |
  a1 \bar "|"

  a4 a d a |
  g2. f4 |
  d( f a f) |
  e1 \bar "|"

  a4 a d a |
  g2 e4( f) |
  d1 \bar "|."
} 

alto = \relative c' {
  \global
  a1~ |
  a2 d2
  f1 |

  f2 ees |
  d2. e4 |
  cis1 |

  d1 |
  c2 d |
  f1 |

  f2 ees |
  d2. e4 |
  c1 |

  d1 |
  bes |
  a2. bes4 |
  c1 |

  d |
  d2 c2 |
  s1 \bar "|."
}

tenor = \relative c {
  \global
  f1~ |
  f2 a2 |
  c1 |

  f,1~ |
  f2. e4~ |
  e1 |

  f1~ |
  f2. e4 |
  d1 |

  f1~ |
  f2. e4 |
  f1 |

  d1~ |
  d1 |
  f |
  g |

  d1 |
  bes'2 g4 a |
  f1 \bar "|."
}

bass = \relative c {
  \global
  d1~ |
  d2 bes |
  f1 |

  d'2 c4 a |
  bes1 |
  a1 \bar "|"

  bes1 |
  f2 g |
  d1 |

  d'2 c4 a |
  bes1 |
  f |

  fis |
  g |
  d' |
  c |

  f, |
  g2 c4 a |
  d,1 \bar "|."
}

verseOne = \lyricmode {
  % \set ignoreMelismata = ##t
  \set stanza = "1 "
  % Let all mortal flesh keep silence,
  % And with fear and trembling stand;
  % Ponder nothing earthly-minded,
  % For with blessing in his hand,
  % Christ our God to earth descendeth,
  % Our full homage to demand.
  \l Viens si -- len -- cieux, chair mor -- tel -- le,
  \l a -- vec crainte et trem -- ble -- ment,
  \l car les cho -- ses tem -- por -- el -- les
  \l qu'un pâ -- le fi -- gu -- re font:
  \l il des -- cend, le vrai Mes -- si -- e,
  \l sa bé -- né -- dic -- tion of -- frant.
}

verseTwo = \lyricmode {
  % \set ignoreMelismata = ##t
  \set stanza = "2 "
  % King of kings, yet born of Mary,
  % As of old on earth he stood
  % Lord of lords, in human vesture --
  % In the Body and the Blood --
  % He will give to all the faithful
  % His own self for heavenly food.
  \l Roi des rois, né de Ma -- ri -- e,
  \l Sei -- gneur des sei -- gneurs puis -- sants, 
  \l vê -- tu dans la chair hu -- mai -- ne:
  \l Par son Corps et par son Sang,
  \l il se donne à ses fi -- dè -- les
  \l com -- me cé -- leste A -- li -- ment.
}

verseThree = \lyricmode {
  % \set ignoreMelismata = ##t
  \set stanza = "3 "
  % Rank on rank the host of heaven
  % Spreads its vanguard on the way,
  % As the Light of Light descendeth
  % From the realms of endless day,
  % That the powers of hell may vanish
  % As the darkness clears away.
  \l L'en -- tière ar -- mé -- e cé -- les -- te
  \l pré -- pa -- re donc son che -- min:
  \l La Lu -- miè -- re vers nous des -- cend, 
  \l et son roy -- au -- me di -- vin,
  \l pour que l'en -- fer dis -- pa -- rais -- se,
  \l tout pé -- ché et tout cha -- grin.
}

verseFour = \lyricmode {
  % \set ignoreMelismata = ##t
  \set stanza = "4 "
  % At his feet the six-winged seraph;
  % Cherubim with sleepless eye,
  % Veil their faces to the Presence,
  % As with ceaseless voice they cry,
  % Alleluia, alleluia,
  % Alleluia, Lord most high.
  \l Le sé -- ra -- phin à six ai -- les,
  \l le ché -- ru -- bin rem -- plis d'yeux,
  \l ils se ca -- chent leurs vi -- sa -- ges 
  \l en cri -- ant de -- vant leur Dieu:
  \l Al -- lé -- lu -- ia, al -- lé -- lu -- ia,
  \l chan -- tons au plus haut des cieux!
}


\score {
  <<
    \new Staff  <<
      \new Voice = "soprano" { \voiceOne \melody }
      \new Voice = "alto" { \voiceTwo \alto }
    >>
    \new Lyrics \with{\lyricspace} \lyricsto soprano \verseOne
    \new Lyrics  \lyricsto soprano \verseTwo
    \new Lyrics  \lyricsto soprano \verseThree
    \new Lyrics  \lyricsto soprano \verseFour
    \new Staff  <<
      \clef bass
      \new Voice = "tenor" { \voiceOne \tenor }
      \new Voice = "bass" { \voiceTwo \bass }
    >>
  >>
}
