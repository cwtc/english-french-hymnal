\include "../common_liturgy.ly"

% TO CONSIDER:
% http://www.hymntime.com/tch/non/fr/r/e/s/a/resavmoi.htm
% http://www.hymntime.com/tch/non/fr/r/e/s/a/resanous.htm

global = {
  \key ees \major
  \time 4/4
  \autoBeamOff
  \override Staff.TimeSignature #'stencil = ##f
}

\layout {
  \context {
    \Score
    \omit BarNumber
  }
}

melody = \relative c'' {
  \global
  g2 g4 f |
  ees2 bes'2 |
  c4 bes bes aes |
  g2 g2 \bar "|" \break
  
  g2 aes4 bes |
  c2 bes2 |
  aes4 f g a |
  bes1 \bar "|" \break

  g2 g4 f |
  ees2 bes'2 |
  bes4 aes aes g |
  f2 f2 \bar "|" \break

  f2 g4 aes |
  g f ees aes |
  g2 f2 |
  ees1 \bar "|."
}

alto = \relative c' {
  \global
  ees2 d4 d |
  ees2 ees |
  ees4 d ees f |
  ees2 ees2 |

  ees2 ees4 ees |
  ees2 ees |
  ees4 f ees ees |
  d1 |

  ees2 d4 d | 
  ees2 ees |
  ees4 ees e e |
  f2 f2 |

  d2 ees4 d |
  ees d ees f |
  ees2 d |
  ees1
}

tenor = \relative c' {
  \global
  bes2 bes4 aes |
  g2 ees |
  ees4 bes' bes bes |
  bes2 bes2 |

  bes2 aes4 g |
  aes2 g |
  c4 bes bes ees, |
  f1 |

  g4( aes) bes aes |
  g2 ees'4( d) |
  c4 c c bes |
  aes2 aes2 |

  bes2 bes4 bes |
  bes aes g c |
  bes2. aes4 |
  g1
}

bass = \relative c {
  \global
  ees2 bes4 bes |
  c2 g |
  aes4 bes c d |
  ees2 ees2 |

  ees4( d) c bes |
  aes2 ees' |
  f4 d ees c |
  bes1 |

  ees2 bes4 bes |
  c2 g |
  aes4. bes8 c4 c |
  f2 f2 |

  aes2 g4 f |
  ees bes c aes |
  bes2 bes |
  ees1
}

verseOne = \lyricmode {
  \set stanza = "1 "
  % \l A -- bide with me: fast falls the e -- ven -- tide;
  \l Reste a -- vec moi: c'est l'heure où le jour bais -- se.
  % \l The dark -- ness deep -- ens; Lord, with me a -- bide:
  \l L'om -- bre gran -- dit; Seig -- neur, at -- tar -- de toi.
  % \l When oth -- er help -- ers fail and com -- forts flee,
  \l Tous les ap -- puis man -- quent à ma fai -- bles -- se:
  % \l Help of the help -- less, O a -- bide with me.
  \l For -- ce du fai -- "ble, ô" Christ, reste a -- vec moi.
}

verseTwo = \lyricmode {
  \set stanza = "2 "
  % \l Swift to its close ebbs out life's lit -- tle day,
  \l Le flot des jours ra -- pi -- de -- ment s'é -- cou -- le;
  % \l Earth's joys grow dim, its glo -- ries pass a -- way,
  \l Leur gloire est vaine et leur bon -- heur dé -- çoit.
  % \l Change and de -- cay in all a -- round I see;
  \l Tout change et meurt, tout chan -- celle et s'é -- crou -- le:
  % \l O thou who chang -- est not, a -- bide with me.
  \l Toi qui ne chan -- ges point, reste a -- vec moi.
}

verseThree = \lyricmode {
  \set stanza = "3 "
  % Rewrite needed

  % \l I fear no foe, with thee at hand to bless;
  \l J'ose im -- plo -- rer plus qu'un re -- gard qui pas -- se;
  % \l Ills have no weight, and tears no bit -- ter -- ness.
  \l Viens, comme à tes di -- sci -- ples au -- tre -- fois.
  % \l Where is death's sting? where, grave, thy vic -- to -- ry?
  \l Plein de dou -- ceur, de ten -- dresse et de grâ -- ce,
  % \l I tri -- umph still, if thou a -- bide with me.
  \l et pour tou -- jours, Seig -- neur, reste a -- vec moi.
}

verseFour = \lyricmode {
  \set stanza = "4 "
  \l Viens, mais non pas ar -- mé de ta co -- lè -- re!
  \l Parle à mon cœur, a -- pai -- se mon é -- moi!
  \l É -- tends sur moi ton ai -- le tu -- té -- lai -- re;
  \l A -- mi des pé -- a -- gers, reste av -- ec moi.
}

\score {
  <<
    \new Staff  <<
      \new Voice = "soprano" { \voiceOne \melody }
      \new Voice = "alto" { \voiceTwo \alto }
    >>
    \new Lyrics \with{\lyricspace}  \lyricsto soprano \verseOne
    \new Lyrics  \lyricsto soprano \verseTwo
    \new Lyrics  \lyricsto soprano \verseThree
    \new Lyrics  \lyricsto soprano \verseFour
    \new Staff  <<
      \clef bass
      \new Voice = "tenor" { \voiceOne \tenor }
      \new Voice = "bass" { \voiceTwo \bass }
    >>
  >>
}

\markup { \vspace #0.5 } 

\markup {
  \fill-line {
    \hspace #0.1 % moves the column off the left margin;
     % can be removed if space on the page is tight
     \column {
      \line { \bold "5 "
        \column {
          "Heure après heure, il me faut ta présence;"
          "Le tentateur ne redoute que toi."
          "Qui donc prendrait contre lui ma défense?"
          "Dans l'ombre ou la clarté, reste avec moi."
        }
      }
    }
    \hspace #0.1 % adds horizontal spacing between columns;
    \column {
      \line { \bold "6 "
        \column {
          "Montre ta Croix à ma vue expirante!"
          "Et que ton ciel s'illumine à ma foi!"
          "L'ombre s'enfuit, voici l'aube éclatante!"
          "Dans la vie et la mort, reste avec moi."
        }
      }
    }
  \hspace #0.1 % gives some extra space on the right margin;
  % can be removed if page space is tight
  }
}
