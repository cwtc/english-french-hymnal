\include "../common_liturgy.ly"

global = {
  \key d \minor
  \time 4/4
  \autoBeamOff
  \override Staff.TimeSignature #'stencil = ##f
}

\layout {
  \context {
    \Score
    \omit BarNumber
  }
}

melody = \relative c'' {
  \global
  a4 a d, e |
  f g a2 |
  a4 a8[ b] c4 c |
  b4. a8 a2 |
  a4 a bes! bes |
  g g a2 |
  a4 a g f |
  e4. e8 d2 \bar "|."
} 

alto = \relative c' {
  \global
  d4 d d cis |
  d4. e8 d4( cis) |
  d d8[ f] e4 f |
  f8[ e] d4 cis2 |
  fis4 fis g g8[ f] |
  e[ d] c4 c2 |
  c8[ bes] c[ d] e4 d |
  d cis d2 |
}

tenor = \relative c {
  \global
  f4 f8[ g] a4 a |
  a4. g8 e2 |
  f8[ g] a[ d] c4 a |
  a gis a2 |
  a4 d d d |
  c4. bes8 a2 |
  a8[ g] a[ bes] c4 a |
  a8[ g] e4 f2 \bar "|."
}

bass = \relative c {
  \global
  d4 d8[ e] f4 a4 |
  d,8[ c] bes4 a2 |
  d8[ e] f[ g] a4 f8[ e] |
  d4 e a,2 |
  d8[ c] bes[ a] g4 bes |
  c e f2 |
  f4 f c d |
  a a d2 |
}

verseOne = \lyricmode {
  % \set ignoreMelismata = ##t
  \set stanza = "1 "
  % Forty days and forty nights
  % Thou wast fasting in the wild;
  % Forty days and forty nights
  % Tempted, and yet undefiled.
  \l Pen -- dant ces qua -- ran -- te jours,
  \l pen -- dant ces qua -- ran -- te nuits,
  \l tu jeû -- nais, ce sans se -- cours,
  \l ten -- té, sans au -- cun dé -- lit.
}

verseTwo = \lyricmode {
  % \set ignoreMelismata = ##t
  \set stanza = "2 "
  % Shall not we Thy sorrow share,
  % And from earthly joys abstain,
  % Fasting with unceasing prayer,
  % Glad with Thee to suffer pain?
  \l Par -- ta -- geons donc ton cha -- grin;
  \l ab -- ste -- nons -nous du plai -- sir;
  \l fai -- sons ce jeû -- ne di -- vin,
  \l con -- tents d'a -- vec toi souf -- frir.
}

verseThree = \lyricmode {
  % \set ignoreMelismata = ##t
  \set stanza = "3 "
  % And if Satan vexing sore,
  % Flesh or spirit should assail,
  % Thou, his Vanquisher before,
  % Grant we may not faint or fail.
  \l Si Sa -- tan, con -- tra -- ri -- é,
  \l no -- tre chair faible as -- sail -- lit,
  \l toi, Vain -- queur glo -- ri -- fi -- é,
  \l gar -- de -nous de l'en -- ne -- mi.
}

verseFour = \lyricmode {
  % \set ignoreMelismata = ##t
  \set stanza = "4 "
  % So shall we have peace divine;
  % Holier gladness ours shall be;
  % Round us, too, shall angels shine,
  % Such as ministered to Thee.
  \l Que cet -- te di -- vi -- ne paix
  \l nous ren -- de donc bien -- heu -- reux;
  \l les an -- ges qui te ser -- vaient
  \l nous ren -- dront l'ai -- de des cieux.
}

verseFive = \lyricmode {
  % \set ignoreMelismata = ##t
  \set stanza = "5 "
  % Keep, O keep us, Saviour dear,
  % Ever constant by Thy side;
  % That with Thee we may appear
  % At th' eternal Eastertide.
  \l Gar -- de -nous, ô cher Sei -- gneur,
  \l tou -- jours proche à tes cô -- tés,
  \l qu'a -- vec toi, nos corps, nos cœurs
  \l soient en -- fin res -- sus -- ci -- tés.
}

\score {
  <<
    \new Staff  <<
      \new Voice = "soprano" { \voiceOne \melody }
      \new Voice = "alto" { \voiceTwo \alto }
    >>
    \new Lyrics \with{\lyricspace} \lyricsto soprano \verseOne
    \new Lyrics  \lyricsto soprano \verseTwo
    \new Lyrics  \lyricsto soprano \verseThree
    \new Lyrics  \lyricsto soprano \verseFour
    \new Lyrics  \lyricsto soprano \verseFive
    \new Staff  <<
      \clef bass
      \new Voice = "tenor" { \voiceOne \tenor }
      \new Voice = "bass" { \voiceTwo \bass }
    >>
  >>
}

