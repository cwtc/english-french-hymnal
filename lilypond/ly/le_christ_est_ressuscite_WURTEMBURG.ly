\include "../common_liturgy.ly"

global = {
  \key e \major
  \time 4/4
  \autoBeamOff
  \override Staff.TimeSignature #'stencil = ##f
}

\layout {
  \context {
    \Score
    \omit BarNumber
  }
}

melody = \relative c'' {
  \global
  gis4 gis b b |
  e, fis gis2 \bar "|"

  cis4 dis e gis, |
  fis fis e2 \bar "|" \break

  b'4 fis gis gis |
  cis gis ais2 \bar "|"

  b4 fis gis8[ ais] b4 |
  cis cis b2 \bar "|" \break

  cis4^\markup { \italic "Refrain" }( dis e) gis,4 |
  fis2 e \bar "|."
} 

alto = \relative c' {
  \global
  e4 e fis dis |
  e dis e2
  e4 fis e e |
  e dis e2
  dis4 fis e fis |
  eis gis fis2

  fis4 dis e dis |
  g fis8[ e] dis2 |

  fis2( e4) e |
  e( dis) e2 |

}

tenor = \relative c' {
  \global
  b4 b b a |
  gis b b2 |

  cis4 a gis cis |
  cis b gis2 |

  fis4 b b bis |
  cis cis cis2 |
  b4 b b b |
  b ais b2 |
  a2( gis4) cis4 |
  cis( b) gis2 |

}

bass = \relative c {
  \global
  e4 e dis b |
  cis b e2 |

  a4 fis cis cis |
  a b e2 |

  b4 dis e dis |
  cis eis fis2 |

  dis4 b e gis |
  e fis b,2 |

  fis4( b cis4.) b8 |
  a4( b) e2 |

}

verseOne = \lyricmode {
  % \set ignoreMelismata = ##t
  \set stanza = "1 "
  % Christ, the Lord, is risen again;
  % Christ hath broken every chain
  % Hark, angelic voices cry,
  % Singing evermore on high,
  % Alleluia!
  \l Le Christ est res -- sus -- ci -- té;
  \l il a cha -- que fer bri -- sé!
  \l Ceux de la cé -- leste ar -- mée
  \l chantent en per -- pe -- tu -- i -- té:
}

verseTwo = \lyricmode {
  % \set ignoreMelismata = ##t
  \set stanza = "2 "
  % He Who gave for us His life,
  % Who for us endured the strife,
  % Is our Paschal Lamb to-day;
  % We too sing for joy, and say:
  % Alleluia!
  \l Lui qui al -- la au sup -- plice,
  \l se don -- nant en sac -- ri -- fice,
  \l il est notre A -- gneau pas -- cal;
  \l chan -- tons son a -- mour loy -- al:
}

verseThree = \lyricmode {
  % \set ignoreMelismata = ##t
  \set stanza = "3 "
  % He who bore all pain and loss
  % Comfortless upon the Cross,
  % Lives in glory now on high,
  % Pleads for us, and hears our cry;
  % Alleluia!
  \l Lui qui su -- bit tout ef -- froi,
  \l sans ré -- con -- fort sur la croix,
  \l il vit, glo -- ri -- eux, au ciel,
  \l l'In -- ter -- ces -- seur é -- ter -- nel:
  \l Al -- le -- lu -- ia!
}

verseFour = \lyricmode {
  % \set ignoreMelismata = ##t
  \set stanza = "4 "
  % He Who slumbered in the grave,
  % Is exalted now to save;
  % Now through Christendom it rings
  % That the Lamb is King of kings.
  % Alleluia!
  \l Lui, Des -- cen -- deur aux en -- fers,
  \l il nous rend le ciel ou -- vert;
  \l que la terre en -- tiè -- re croie
  \l qu'il est le vrai Roi des rois:
}


\score {
  <<
    \new Staff  <<
      \new Voice = "soprano" { \voiceOne \melody }
      \new Voice = "alto" { \voiceTwo \alto }
    >>
    \new Lyrics \with{\lyricspace} \lyricsto soprano \verseOne
    \new Lyrics  \lyricsto soprano \verseTwo
    \new Lyrics  \lyricsto soprano \verseThree
    \new Lyrics  \lyricsto soprano \verseFour
    \new Staff  <<
      \clef bass
      \new Voice = "tenor" { \voiceOne \tenor }
      \new Voice = "bass" { \voiceTwo \bass }
    >>
  >>
}

\markup { \vspace #0.5 } 

\markup {
  \fill-line {
    \hspace #0.1 % moves the column off the left margin;
     % can be removed if space on the page is tight
     \column {
      \line { \bold "5 "
        \column {
          % Now He bids tell abroad,
          % How the lost may be restored,
          % How the penitent forgiven,
          % How we, too, may enter heaven.
          % Alleluia!
          "Il donna la Commission:"
          "allez dans toute nation;"
          "racontez au pénitents"
          "qu'ils verront la délivrance:"
        }
      }
    }
    \hspace #0.1 % adds horizontal spacing between columns;
    \column {
      \line { \bold "6 "
        \column {
          % Thou our Paschal Lamb indeed,
          % Christ, Thy ransomed people feed:
          % Take our sins and guilt away,
          % That we all may sing for aye:
          % Alleluia!
          "Tu es notre Agneau, ô Christ:"
          "viens, donc, pour qu'on soit nourri;"
          "lave-nous de nos péchés,"
          "pour que l'on chante à jamais:"
        }
      }
    }
  \hspace #0.1 % gives some extra space on the right margin;
  % can be removed if page space is tight
  }
}
