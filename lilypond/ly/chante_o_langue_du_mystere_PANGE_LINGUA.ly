\include "../common_liturgy.ly"

global = {
  \key c \major
  \cadenzaOn
  \autoBeamOff
  \override Staff.TimeSignature #'stencil = ##f
}

\layout {
  \context {
    \Score
    \omit BarNumber
  }
}

noacci = { \once \override Accidental #'transparent = ##t }
ln = {\once \override NoteHead #'duration-log = #0 
           \once \override Stem #'stencil = ##f
           \once \override NoteColumn #'force-hshift = #'0.4 }

melody = \relative c' {
  \global
  e8 e e d g g a[c] c \bar "|"c [d] c c c a c b[a g] \bar "|" \break
  g a c b a g a a \bar "|" a b g fis! e a a[d,] \bar "|" \break
  g g g e g a a g \bar "|" a b g a[g] \noacci f d e \bar "|"
} 

alto = \relative c' {
  \global
  \ln b4 c8 b \ln d4 \ln f4 e8 \bar "|" \ln g4  ~ g8 a g f e \ln d4. \bar "|"
  d8 \ln e4( \ln e) e8 e e \bar "|" fis fis e d b c c[a] \bar "|"
  \ln c4 ~ \ln c4 ~ c8 c8  ~ c8 c8 \bar "|" c d e \ln e4 d8 d b \bar "|"
}

tenor = \relative c {
  \global
  \ln g'4 ~ g8 g8 \ln b4 c8[a]c \bar "|" e[d] e ~ e \ln c4 ~ c8 d[c b] \bar "|"
  b8 c e d c b c c \bar "|" d d b ~ b g a e[f!] \bar "|"
  \ln e4 ~ e8 g e e ~ e e \bar "|" e g b \ln c4 a8 a g \bar "|"
}

bass = \relative c {
  \global
  \ln e4 c8 g' \ln g4 \ln f c8 \bar "|" c'[b]c a e f c \ln g'4. \bar "|"
  b8 \ln a4 ~ \ln a4 e8 a, a \bar "|" d b e b e a, a[d] \bar "|"
  \ln c4 ~ \ln c4 ~ c8 a8 ~ a8 c8 \bar "|" a g e' \ln c4 d8 f e \bar "|"
}

verseOne = \lyricmode {
  % \set ignoreMelismata = ##t
  \set stanza = "1 "
  \l Chante, ô lan -- gue, du my -- stè -- re
  \l de __ ce corps très glo -- ri -- eux __
  \l que don -- na le Roi des Na -- tions,
  \l et de son sang pré -- ci -- eux; __
  \l le fru -- it d'un ven -- tre no -- ble
  \l pa -- ya no -- tre __ prix af -- freux.
}

verseTwo = \lyricmode {
  % \set ignoreMelismata = ##t
  \set stanza = "2 "
  \l D'u -- ne Vier -- ge jeune et pu -- re
  \l ce __ Roi na -- quit i -- ci -bas, __
  \l et pour fai -- re sa cul -- tu -- re,
  \l par -- mi nous il de -- meu -- ra; __
  \l il choi -- sit sa sé -- pul -- tu -- re
  \l que, lui -- même, il __ or -- don -- na.
}

verseThree = \lyricmode {
  % \set ignoreMelismata = ##t
  \set stanza = "3 "
  \l A -- vec ses dou -- ze dis -- ci -- ples
  \set ignoreMelismata = ##t
  \l à _ __ la der -- ni -- è -- re Cè -- _ ne,
  \l il man -- gea l'a -- gneau de Pâ -- ques,
  \l et, com -- blant la Loi lui -mê -- me,
  \l comme aux fou -- les, là il don -- na
  \set ignoreMelismata = ##f
  \l son corps de sa __ pro -- pre main. 
}

verseFour = \lyricmode {
  \set ignoreMelismata = ##f
  \set stanza = "4 "
  \l Sa vrai -- e chair par son ver  -- be
  \set ignoreMelismata = ##t
  \l don -- ne
  \set ignoreMelismata = ##f
  le Verbe en chair en pain; __
  \l et de la mê -- me ma -- niè -- re,
  \l il don -- ne son sang en vin. __
  \l Par la foi on les di -- scèr -- ne,
  \l si nos sens tra -- vaillent en vain.
}


\score {
  <<
    \new Staff  <<
      \new Voice = "soprano" { \voiceOne \melody }
      \new Voice = "alto" { \voiceTwo \alto }
    >>
    \new Lyrics \with{\lyricspace} \lyricsto soprano \verseOne
    \new Lyrics  \lyricsto soprano \verseTwo
    \new Lyrics  \lyricsto soprano \verseThree
    \new Lyrics  \lyricsto soprano \verseFour
    \new Staff  <<
      \clef bass
      \new Voice = "tenor" { \voiceOne \tenor }
      \new Voice = "bass" { \voiceTwo \bass }
    >>
  >>
}

\markup { \vspace #0.5 } 

\markup {
  \fill-line {
    \hspace #0.1 % moves the column off the left margin;
     % can be removed if space on the page is tight
     \column {
      \line { \bold "5 "
        \column {
          "Prosternons-nous donc humblement;"
          % "devant ce grand Sacrement,"
          "tombons devant cette Hostie;"
          "et que l'ancienne Alliance"
          "cède enfin aux nouveaux rites:"
          "Ce que nos faibles sens manquent"
          "par la foi est accompli."
          % "chaque défaut de nos sens."
        }
      }
    }
    \hspace #0.1 % adds horizontal spacing between columns;
    \column {
      \line { \bold "6 "
        \column {
          "Au Géniteur, au Génité"
          "soit notre jubilation,"
          "notre honneur et nos louanges,"
          "et toute bénédiction;"
          "Au Saint-Esprit, y procédant,"
          "soit égale adoration."
        }
      }
    }
  \hspace #0.1 % gives some extra space on the right margin;
  % can be removed if page space is tight
  }
}
