% https://github.com/bbloomf/lilypond-songs/blob/master/ly/Nearer%2C%20My%20God%2C%20to%20Thee.ly
% http://www.hymntime.com/tch/non/fr/m/d/p/l/mdpluspr.htm
\include "../common_liturgy.ly"

global = {
  \key g \major
  \time 6/4
  \autoBeamOff
  \override Staff.TimeSignature #'stencil = ##f
}

\layout {
  \context {
    \Score
    \omit BarNumber
  }
}

melody = \relative c' {
  \global
  b'2. a2 g4 |
  g2 e4 e2. |
  d g2 b4 |
  a2.~ a2. | \break

  b2. a2 g4 |
  g2 e4 e2. |
  d2( g4) fis2 a4 |
  g2.~ g2. | \break

  d'2. e2 d4 |
  d2 b4 d2. |
  d2. e2 d4 |
  d2 b4 a2. | \break

  b2. a2 g4 |
  g2 e4 e2. |
  d2( g4) fis2 a4 |
  g2.~ g2. \bar"|."
}

alto = \relative c' {
  \global
  d2. c2 b4 |
  e2 c4 c2. |
  d d2 d4 |
  d2.~ d2. |

  d2. c2 b4 |
  e2 c4 c2. |
  b2( d4) d2 d4 |
  d2.~ d2. |

  g2. g2 g4 |
  g2 g4 g2. |
  g g2 g4 |
  d2 d4 d2. |
  d c2 b4 |
  e2 c4 c2. |
  b2( d4) d2 d4 |
  d2.~ d2. \bar"|."
}

tenor = \relative c' {
  \global
  g2. fis2 g4 |
  c2 g4 g2. |
  b g2 g4 |
  fis2.~ fis2. |
  g2. fis2 g4 |
  c2 g4 g2. |
  g2( b4) a2 c4 |
  b2.~ b2. |

  b2. c2 b4 |
  b2 g4 b2. |
  b c2 b4 |
  a2 g4 fis2. |
  g fis2 g4 |
  c2 g4 g2. |
  g2( b4) a2 c4 |
  b2.~ b2. \bar"|."
}

bass =  \relative c' {
  \global
  g2. d2 e4 |
  c2 c4 c2. |
  g b2 g4 |
  d'2.~ d2. |
  g2. d2 e4 |
  c2 c4 c2. |
  d d2 d4 |
  g2.~ g2. |

  g2. g2 g4 |
  g2 g4 g2. |
  g c,2 g'4 |
  fis2 g4 d2. |
  g d2 e4 |
  c2 c4 c2. |
  d d2 d4 |
  g,2.~ g2. \bar"|."
}

verseOne = \lyricmode {
  \set stanza = "1 "
  % Nearer, my God, to Thee,
  % Nearer to Thee!
  % E'en though it be a cross
  % That raiseth me;
  % Still all my song shall be,
  % Nearer, my God, to Thee,--
  % Nearer to Thee! 
  \l Mon Dieu, plus près de toi,
  \l plus près de toi!
  \l C'est le cri de ma foi:
  \l plus près de toi.
  \l Dans les jours où l'é -- preuve
  \l dé -- bor -- de comme un fleuve,
  \l Tiens -- moi plus près de toi,
  \l plus près de toi!
}

verseTwo = \lyricmode {
  \set stanza = "2 "
  % ADAPTED
  % Though like the wanderer,
  % The sun gone down,
  % Darkness be over me,
  % My rest a stone;
  % Yet in my dreams I'd be
  % Nearer, my God, to Thee,--
  % Nearer to Thee! 
  \l Plus près de toi, Seig -- neur,
  \l plus près de toi!
  \l Tiens -- moi dans ma dou -- leur
  \l tout près de toi!
  \l A -- lors que la souf -- france
  \l fait son œuvre en si -- lence,
  \l Tou -- jours, Seig -- neur, tiens -- moi
  \l plus près de toi!
}

verseThree = \lyricmode {
  \set stanza = "3 "
  % There let the way appear,
  % Steps unto heaven;
  % All that Thou sendest me,
  % In mercy given;
  % Angels to beckon me
  % Nearer, my God, to Thee,--
  % Nearer to Thee! 
  \l Plus près de toi, tou -- jours
  \l plus près de toi!
  \l Don -- ne moi ton se -- cours;
  \l sou -- tiens ma foi!
  \l Que Sa -- tan se dé -- chaine,
  \l ton a -- mour me ra -- mène
  \l Tou -- jours plus près de toi,
  \l plus près de toi!
}

verseFour = \lyricmode {
  \set stanza = "4 "
  % Then, with my waking thoughts
  % Bright with Thy praise,
  % out of my stony griefs
  % Bethel I'll raise;
  % So by my woes to be
  % Nearer, my God, to Thee,--
  % Nearer to Thee! 
  \l Mon Dieu, plus près de toi,
  \l dans le dé -- sert,
  \l J'ai vu plus près de toi
  \l ton ciel ou -- vert.
  \l Pè -- le -- rin, bon cou -- rage!
  \l Ton chant bra -- ve l'o -- rage:
  \l Mon Dieu, plus près de toi,
  \l plus près de toi!
}


\score {
  <<
    \new Staff  <<
      \new Voice = "soprano" { \voiceOne \melody }
      \new Voice = "alto" { \voiceTwo \alto }
    >>
    \new Lyrics \with{\lyricspace} \lyricsto soprano \verseOne
    \new Lyrics  \lyricsto soprano \verseTwo
    \new Lyrics  \lyricsto soprano \verseThree
    \new Lyrics  \lyricsto soprano \verseFour
    \new Staff  <<
      \clef bass
      \new Voice = "tenor" { \voiceOne \tenor }
      \new Voice = "bass" { \voiceTwo \bass }
    >>
  >>
}

% \markup { \vspace #0.5 } 

% \markup {
%   \fill-line {
%     \hspace #0.1 % moves the column off the left margin;
%      % can be removed if space on the page is tight
%      \column {
%       \line { \bold "5 "
%         \column {
%           % Or if on joyful wing
%           % Cleaving the sky,
%           % Sun, moon, and stars forgot,
%           % Upward I fly,
%           % Still all my song shall be,--
%           % Nearer, my God, to Thee,--
%           % Nearer, my God, to Thee,
%           % Nearer to Thee.
%         }
%       }
%     }
%     \hspace #0.1 % adds horizontal spacing between columns;
%   \hspace #0.1 % gives some extra space on the right margin;
%   % can be removed if page space is tight
%   }
% }
