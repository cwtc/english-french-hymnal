\include "../common_liturgy.ly"

global = {
  \key g \major
  \time 4/4
  \autoBeamOff
  \override Staff.TimeSignature #'stencil = ##f
}

\layout {
  \context {
    \Score
    \omit BarNumber
  }
}

melody = \relative c' {
  \global
  d4 e d4. g8 |
  g[ fis] g[ a] b4 a |

  b g e c' |
  b a g2

  d4 e d4. g8 |
  g[ fis] g[ a] b4 a |

  b4 c d c8[ a] |
  g4 fis g2 \break

  a4. b8 c4 a |
  b4. c8 d4 b |
  d4. d8 d d d d |
  d1 |

  d4. c8 b[ d] c[ a] |
  g4 fis g2 \bar "|."
}

alto = \relative c' {
  \global
  b4 c d4. d8 |
  d4 d8[ e] d4 d |

  d c e e d c b2 |

  b4 c d4. d8 |
  d4 d8[ e] d4 d |

  d4 e d e |
  d d d2 |

  fis4. g8 a4 d, |
  g4. a8 b[ a] g4 |
  g4. g8 fis g d g |
  << {\voiceTwo fis 1} \\ {\voiceFour \teeny \once \override NoteColumn #'force-hshift = #1 fis4 d8[ fis]  a2 } >> |
  g4. fis8 g4 e |
  d d d2
}

tenor = \relative c' {
  \global
  g4 g g4. b8 |
  b[ a] g4 g fis |

  g4 g g g |
  g fis g2 |

  g4 g g4. b8 |
  b[ a] g4 g fis |

  g4 g g g8[ c] |
  b4 a b2 |

  c4. b8 a[ g] fis[ a] |
  d4. c8 b4 d |

  d4. d8 c b a g |
  d'1 |

  d4. d8 d[ b] a[ c] |
  b4 a8[ c] b2
}

bass = \relative c {
  \global
  g4 c b a |
  g8[ a] b[ c] d4 d |

  g4 e c a |
  d d g,2

  g4 c b a |
  g8[ a] b[ c] d4 d |

  g4 e b c |
  d d g,2 |

  d'4. d8 d[ e] fis4 |
  g4. d8 g[ a] b4 |
  b4. b8 a g fis e |
  <<
    { \voiceTwo d1 } \\ {
      \voiceFour \teeny
      \once \override NoteColumn #'force-hshift = #1 d4 fis8[ a] c2
    }
  >> |
  b4. a8 g4 c, |
 
 d d g2

}

verseOne = \lyricmode {
  \set stanza = "1 "
  % \l Guide me, O thou great Re -- dee -- mer,
  \l Gui -- de moi, Ber -- ger fi -- dè -- le,
  % \l Pil -- grim through this bar -- ren land.
  \l En ce mon -- de pé -- le -- rin.
  % \l I am weak, but thou art might -- y;
  \l Prends à toi mon coeur re -- bel -- le,
  % \l Hold me with thy pow'r -- ful hand.
  \l Gui -- de moi, sois mon sou -- tien.
  % \l Bread of heav -- en, bread of heav -- en,
  \l Pain de vi -- e, pain de vi -- e,
  % \l Feed me till I want no more;
  \l De ta grâ -- ce nour -- ris moi!
  % \l Feed me till I want no more.
  \l De ta grâ -- ce nour -- ris moi!
}

verseTwo = \lyricmode {
  \set ignoreMelismata = ##t
  \set stanza = "2 "
  % ADAPTED
  % \l O -- pen now the crys -- tal foun -- tain,
  \l Christ, tu es la sour -- _ ce _ vi -- ve
  % \l Whence the heal -- ing stream doth flow;
  \l Des biens les plus pré -- ci -- eux.
  % \l Let the fire and cloud -- y pil -- lar
  % \l Fais que pour toi seul je vi -- ve
  \l Que la co -- lon -- ne de feu je sui -- ve;
  % \l Lead me all my jour -- ney through.
  \l Gui -- de moi, du _ haut des cieux.
  % \l Strong de -- liv -- erer, strong de -- liv -- erer,
  % \l Viens, pro -- tè -- ge, viens, pro -- tè -- ge,
  \l Viens, Pro -- tec -- teur; viens, Pro -- tec -- teur;
  % \l Be thou still my strength and shield;
  \l Sois mon roc, mon bou -- cli -- er!
  % \l Be thou still my strength and shield.
  \l Sois mon roc, _ mon _ bou -- cli -- er!
}

verseThree = \lyricmode {
  \set stanza = "3 "
  % \l When I tread the verge of Jor -- dan,
  \l Du Jour -- dain, je suis les ri -- ves,
  % \l Bid my anx -- ious fears sub -- side;
  \l Quand j'ai peur, ras -- su -- re -moi.
  % \l Death of death and hell's de -- struc -- tion,
  \l À Si -- on, qu'en -- fin j'ar -- ri -- ve,
  % \l Land me safe on Ca -- naan's side.
  \l Af -- fer -- mis ma fai -- ble foi.
  % \l Songs of prais -- es, songs of prais -- es,
  \l Tes lou -- an -- ges, tes lou -- an -- ges,
  % \l I will ev -- er give to thee;
  \l À ja -- mais je chan -- te -- rai!
  % \l I will ev -- er give to thee.
  \l À ja -- mais je chan -- te -- rai!
}

\score {
  <<
    \new Staff  <<
      \new Voice = "soprano" { \voiceOne \melody }
      \new Voice = "alto" { \voiceTwo \alto }
    >>
    \new Lyrics \with{\lyricspace} \lyricsto soprano \verseOne
    \new Lyrics  \lyricsto soprano \verseTwo
    \new Lyrics  \lyricsto soprano \verseThree
    \new Staff  <<
      \clef bass
      \new Voice = "tenor" { \voiceOne \tenor }
      \new Voice = "bass" { \voiceTwo \bass }
    >>
  >>
}
