\include "../common_liturgy.ly"

global = {
  \key d \major
  \time 3/4
  \autoBeamOff
  \override Staff.TimeSignature #'stencil = ##f
}

\layout {
  \context {
    \Score
    \omit BarNumber
  }
}

melody = \relative c' {
  \global
  \partial 4
  fis8[ e] |
  d4.( e8) fis[ g] |
  a2 b8[ cis] |
  \set melismaBusyProperties = #'()
  d4( cis) b |
  a2 \bar "|" \break 
  b8[ cis] |
  d4( cis) b |
  a( b) cis |
  d( a) g |
  fis2 \bar "|" \break

  fis8[ e] |
  d4.( e8) fis[ g] |
  a2 b8[ cis] |
  d4( cis) b |
  a2  \bar "|" \break
  b8[ cis] |
  d4( cis) b |
  a( b) cis |
  d( a) g |
  fis2 \bar "||" \break

  fis8^\markup { \italic "Refrain" }[ e] |
  d4.( e8) fis[ g] |
  a2 d8[ cis] |
  b2 b4 a2. |
  d4 cis b |
  a( b) cis |
  d( a) g |
  fis2 \bar "|."
} 

alto = \relative c' {
  \global
  \partial 4
  d4 |
  a2 d4 |
  e2 e4 |
  a~ a g |
  fis2 d8[ e] |
  a4( fis) g |
  a( d,) g |
  fis~ fis e |
  d2
  cis4 |
  a2 d4 |
  e2 e4 |
  a~ a g |
  fis2
  d8[ e] |
  a4( fis) g |
  a( g) g |
  fis~ fis e |
  d2
  cis4 |
  a2 d4 |
  cis2 fis4 |
  g2 g4 |
  fis2( a4) |
  fis fis g
  fis2 g4 |
  fis2 e4 |
  d2 \bar "|."
}

tenor = \relative c' {
  \global
  \partial 4
  a8[ g] |
  fis2 d'4 |
  cis( a) b |
  a~ a b8[ cis] |
  d2 b8[ a] |
  a4~ a cis |
  d( b) g |
  a( d) a |
  a2 a8[ g] |
  fis2 d'4 |
  d( cis) b |
  a~ a b8[ cis] |
  d2 b8[ a] |
  a4~ a cis |
  d~ d e |
  a,( d) a |
  a2
  a8[ g] |
  fis2 b4 |
  a2 a4 |
  b4.( cis8) d[ e] |
  fis2( e4) |
  d d d |
  d2 g,4 |
  a2 a4 |
  a2 \bar "|."
}

bass = \relative c {
  \global
  \partial 4
  d4 |
  d2 b4 |
  a2 g'4 |
  fis~ fis g |
  d2 g8[ g] |
  fis4( d) e |
  fis( g) e |
  fis8[( g] a4) a |
  d,2
  d4 |
  d2 b4 |
  a( a') g |
  fis~ fis g |
  d2 g8[ g] |
  fis4( d) e |
  fis( g) e |
  fis8[( g] a4) a, |
  d2 a4 |
  d2 b4 |
  fis'2 d4 |
  g4.( a8) b[ cis] |
  d2( cis4) |
  b a g |
  d'( d,) e |
  fis8[( g] a4) a, |
  d2 \bar "|."
}

verseOne = \lyricmode {
  \set ignoreMelismata = ##t
  \set stanza = "1 "
  % The first Nowell the angel did say
  % Was to certain poor shepherds in fields as they lay;
  % In fields as they lay, keeping their sheep,
  % On a cold winter's night that was so deep.
  \l La _ pre -- mi -- è -- re fois que No -- ël se di -- sait,
  \l c'é -- tait des an -- ges aux _ pau -- vres _ ber -- gers;
  \l ils _ gar -- _ daient _ aux champs _ leurs _ bre -- bis,
  \l mal -- _ gré _ le vent froid de cet -- _  te nuit.

  % Noel, noel, noel, noel
  % Born is the king of Israel
  \l No -- _ ël, _ No -- _ ël, No -- _ ël, No -- ël!
  \l I -- ci naît le Roi _ d'Is -- _ ra -- ël! 
}

verseTwo = \lyricmode {
  % \set ignoreMelismata = ##t
  \set stanza = "2 "
  % They lookèd up and saw a star
  % Shining in the east, beyond them far,
  % And to the earth it gave great light,
  % And so it continued both day and night.
  \l Au -des -- sus d'eux, l'é -- toi -- le bril -- lait;
  \l de _ l'O -- ri -- ent el -- le res -- plen _ -- dis -- sait;
  \l et _ sur _ la _ terre el -- _ le _ lu -- ait,
  \l et _ jour _ et nuit el -- le ray -- _ on -- nait.
}

verseThree = \lyricmode {
  \set ignoreMelismata = ##t
  \set stanza = "3 "
  % And by the light of that same star
  % Three wise men came from country far;
  % To seek for a king was their intent,
  % And to follow the star wherever it went.
  \l Cet _ as -- _ tre _ bril -- lant  _ de  _ par -- tout
  \l fut _ par _ les trois _ rois ma -- _ ges vu;
  \l ils _ cher -- _ chaient _ en -- fin _ un _ grand roi,
  \l donc _ ils _ le sui -- _ vaient où que ce soit. 
}

verseFour = \lyricmode {
  \set ignoreMelismata = ##t
  \set stanza = "4 "
  % This star drew nigh to the northwest,
  % O'er Bethlehem it took its rest,
  % And there it did both stop and stay
  % Right over the place where Jesus lay.
  \l L'é -- _ toi -- _ le _ pas -- sa _ au _ nord -ouest,
  \l jusqu' -- à Beth -- _ lé -- em, _ ce si -- gne cé -- leste;
  \l et _ là _ au _ ciel el -- le s'est _ fi -- gée
  \l au -des -- sus _ de l'en -- droit où Jé -- sus dor -- mait.
}


\score {
  <<
    \new Staff  <<
      \new Voice = "soprano" { \voiceOne \melody }
      \new Voice = "alto" { \voiceTwo \alto }
    >>
    \new Lyrics \with{\lyricspace} \lyricsto soprano \verseOne
    \new Lyrics  \lyricsto soprano \verseTwo
    \new Lyrics  \lyricsto soprano \verseThree
    \new Lyrics  \lyricsto soprano \verseFour
    \new Staff  <<
      \clef bass
      \new Voice = "tenor" { \voiceOne \tenor }
      \new Voice = "bass" { \voiceTwo \bass }
    >>
  >>
}

\markup { \vspace #0.5 } 

\markup {
  \fill-line {
    \hspace #0.1 % moves the column off the left margin;
     % can be removed if space on the page is tight
     \column {
      \line { \bold "5 "
        \column {
          % Then entered in those wise men three
          % Full reverently upon their knee,
          % And offered there in his presence
          % Their gold, and myrrh, and frankincense.
          "Chacun des trois rois mages entra,"
          "se prosternant devant leur Roi;"
          "ils offrirent dans sa présence"
          "leur or, leur myrrhe, et leur encens."
        }
      }
    }
    \hspace #0.1 % adds horizontal spacing between columns;
    \column {
      \line { \bold "6 "
        \column {
          % Then let us all with one accord
          % Sing praises to our heavenly Lord;
          % That hath made heaven and earth of naught,
          % And with his blood mankind hath bought.
          "Qu'en pur et parfait unisson"
          "les louanges du Seigneur nous chantions;"
          "lui qui créa la terre et le ciel,"
          "lui qui nous accorde la vie éternelle!"
        }
      }
    }
  \hspace #0.1 % gives some extra space on the right margin;
  % can be removed if page space is tight
  }
}
