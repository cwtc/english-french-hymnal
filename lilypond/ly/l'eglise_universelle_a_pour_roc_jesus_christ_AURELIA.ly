\include "../common_liturgy.ly"

% http://www.hymntime.com/tch/non/fr/l/e/g/l/legliseu.htm

global = {
  \key ees \major
  \time 4/4
  \autoBeamOff
  \set Staff.midiInstrument = "church organ"
}

\layout {
  \context {
    \Score
    \omit BarNumber
  }
}

melody = \relative c'' {
  \global
  g4 |
  g g aes g |
  g2 f4 ees |
  ees c' bes aes |
  g2. \bar "|"  \break
  
    aes4 |
  bes ees ees d |
  d2 c4 bes |
  aes bes g ees |
  f2. \bar "|" \break
  
    f4 |
  g aes bes c |
  c2 bes4 ees4 |
  ees4. d8 c4 g |
  aes2. \bar "|" \break
  
    f4 |
  g g aes g |
  g2 f4 ees |
  ees f ees d |
  ees2. \bar "|."
}

alto = \relative c' {
  \global
  \partial 4
  ees4 |
  ees ees ees ees |
  ees2 d4 ees |
  c ees ees d |
  ees2. d4 |
  ees ees ees f |
  f2 ees4 g |
  g f ees ees |
  d2. d4 |
  ees d ees ees |
  ees2 ees4 g |
  g4. g8 g4 g4 |
  f2. f4 |
  ees ees ees ees |
  c2 c4 c |
  c c bes bes |
  bes2. \bar "|."
}

tenor = \relative c' {
  \global
  \partial 4  
  bes4 |
  bes bes c bes |
  bes2 bes4 bes |
  f f g bes |
  bes2. bes4 |
  bes bes b b |
  b2 c4 d |
  ees f bes, a |
  bes2. bes4 |
  bes bes bes aes |
  aes2 bes4 c |
  c4. b8 c4 c |
  c2. bes4 |
  bes bes c bes |
  bes2 aes4 aes |
  aes aes f aes |
  g2. \bar "|."
}

bass = \relative c {
  \global
  \partial 4
  ees4 |
  ees ees ees ees |
  bes2 aes4 g |
  aes aes bes bes |
  ees2. f4 |
  g g g g, |
  aes2 aes4 bes! |
  c d ees c |
  bes2. bes4 |
  ees f g aes |
  aes2 g4 c |
  g4. f8 ees4 e |
  f2. d4 |
  ees ees ees ees |
  aes,2 aes4 aes |
  f f bes bes |
  ees2. \bar "|."
}

verseOne = \lyricmode {
  \set stanza = "1 "
  % \l The Church’s _ one foun -- da -- tion
  %% LC: L'é -- glise u -- ni -- ver -- sel -- le
  \l L'é -- glise u -- ni -- ver -- sel -- le 
  % \l Is Je -- sus Christ her Lord, 
  %% LC: fon -- dée en Jé -- sus Christ
  \l a pour roc Jé -- sus Christ;
  % \l She is His new cre -- a -- tion
  %% LC: est la mai -- son nou -- vel -- le
  \l Elle est l'œuv -- re nou -- vel -- le 
  % \l By wa -- ter and the Word:
  %% LC: vi -- vant de son es -- prit.
  \l que sa pa -- ro -- le fit;
  % \l From heav'n He came and sought her
  %% LC: Du ciel il vint lui mê -- me
  \l Hab -- i -- tant le ciel mê -- me 
  % \l To be His ho -- ly bride, 
  %% LC: pour ê -- tre son É -- poux;
  \l il vint se l'at -- ta -- cher,
  % \l With His own blood He bought her
  %% LC: le Ré -- demp -- teur su -- prê -- me
  \l et, par un don su -- prê -- me, 
  %% LC: don -- nant son sang pour nous.
  % \l And for her life He died. 
  \l mour -- ut pour la sau -- ver.
}

verseTwo = \lyricmode {
  \set stanza = "2 "
  % \l She is from ev' -- ry na -- tion 
  %% LC: Par -- tout dans la pri -- è -- re,
  \l L'é -- glise en sa pri -- è -- re
  % \l Yet one o'er all the earth,
  %% LC: l'É -- glise u -- nit les coeurs 
  \l u -- nit à leur Sau -- veur;
  % \l Her char -- ter of sal -- va -- tion
  %% LC: Pou in -- vo -- quer le Pè -- re, 
  \l Les peu -- ples de la ter -- re
  % \l One Lord, one faith, one birth, 
  %% LC: un même et seul Seig -- neur.
  \l sou -- mis au seul Seig -- neur.
  % \l One Ho -- ly Name she bles -- ses, 
  %% LC: Aux or -- dres de son Maî -- tre
  \l C'est son Nom qu'elle ac -- clam -- me,
  % \l Par -- takes one Ho -- ly Food, 
  %% LC: rom -- pant le mê -- me pain;
  \l son pain, qui la nour -- rit;
  % \l And to one Hope she pres -- ses
  %% LC: L'É -- gli -- se fait con -- naî -- tre 
  \l Elle ver -- se à tout â -- me
  % \l With ev' -- ry grace en -- dued. 
  %% LC: à tous l'a -- mour di -- vin.
  \l l'es -- poir qui la gué -- rit.
}

verseThree = \lyricmode {
  \set stanza = "3 "
  % \l The Church shall nev -- er per -- ish!
  % Still, schisms, tribulation
  %% LC: Le mon -- de la mé -- pri -- se,
  \l Hon -- nie et mé -- con -- nu -- e, 
  % and hatred fuel our war;
  % \l Her dear Lord to de -- fend, 
  %% LC: il cherche à la bri -- ser,
  \l me -- nant de durs com -- bats,
  % \l To guide, sus -- tain, and cher -- ish, 
  % we wait the consummation
  %% LC: Des lut -- tes la di -- vi -- sent,
  \l elle at -- tend la ve -- nu -- e
  % \l Is with her to the end: 
  % of peace forevermore.
  %% LC: son temps pa -- raît comp -- té.
  \l de la paix i -- ci -bas.
  % \l Though there be those who hate her, 
  % The saints their watch are keeping;
  %% LC: Mais el -- le, dans l'é -- preu-- ve,
  \l Con -- tem -- plant par a -- van -- ce
  % Their cry goes up, "how long?"
  % \l And false sons in her pale, 
  %% LC: re -- çoit de Jé -- sus Christ
  \l la fin de son tour -- ment,
  % And soon the night of weeping
  % \l A -- gainst or foe or trai -- tor 
  %% LC: des for -- ces tou -- jours neu -- ves
  \l la gran -- de dé -- li -- vran -- ce,
  % \l She ev -- er shall pre -- vail. 
  % shall be the morn of song.
  %% LC: le don de son es -- prit.
  \l le re -- pos per -- ma nent.
}

verseFour = \lyricmode {
  \set stanza = "4 "
  % Yet we on earth have union
  %% LC: Dé -- jà sur cet -- te ter -- re
  \l Au -- jourd' -- hui, sur la ter -- re
  % with God, the Three in One,
  \l elle est u -- nie à Dieu,
  % and mystic, sweet communion
  %% LC: et, par un grand my -- stè -- re,
  \l et, par un saint my -- stè -- re,
  % with those whose rest is won.
  %% LC: aux ra -- che -- tés des cieux,
  \l aux é -- lus du saint lieu.
  % Oh, happy ones and holy!
  %% LC: Car la vie é -- ter -- nel -- le
  \l Rends -- -nous, comme eux, fi -- dè -- les,
  % God, give us grace that we,
  %% LC: est pour tous ceux de nous
  \l Et re -- çois -nous, Seig -- neur,
  % like them, the meek and lowly,
  %% LC: dont Christ est le mo -- dè -- le
  \l dans la vie é -- ter -- nel --le,
  %% LC: dans un coeur humble et doux.
  % may live eternally.
  \l dans l'é -- ter -- nel bon -- heur.
}


\score {
  <<
    \new Staff  <<
      \new Voice = "soprano" { \voiceOne \melody }
      \new Voice = "alto" { \voiceTwo \alto }
    >>
    \new Lyrics \with{\lyricspace} \lyricsto soprano \verseOne
    \new Lyrics  \lyricsto soprano \verseTwo
    \new Lyrics  \lyricsto soprano \verseThree
    \new Lyrics  \lyricsto soprano \verseFour
    \new Staff  <<
      \clef bass
      \new Voice = "tenor" { \voiceOne \tenor }
      \new Voice = "bass" { \voiceTwo \bass }
    >>
  >>
}
