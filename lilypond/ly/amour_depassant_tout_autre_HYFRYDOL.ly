\include "../common_liturgy.ly"

global = {
  \key f \major
  \time 3/4
  \autoBeamOff
  \override Staff.TimeSignature #'stencil = ##f
}

\layout {
  \context {
    \Score
    \omit BarNumber
  }
}

melody = \relative c' {
  \global
  f2 g4 |
  f4.( g8) a4 |
  bes2 a4 |
  g( f) g |
  c2 bes4 |
  a2 a4 |
  g( f) g |
  f2. \bar "|"

  f2 g4 |
  f4.( g8) a4 |
  bes2 a4 |
  g( f) g |
  c2 bes4 |
  a2 a4 |
  g( f) g |
  f2. \bar "|"
  
  c'2 c4 |
  c( bes) a |
  bes2 bes4 |
  bes( a) g |
  a2 a4 |
  a( bes) c |
  c( bes) a |
  g2. \bar "|"

  c4( a) c |
  bes( g) bes |
  a( f) a |
  g8[( a] bes[ a]) g4 |
  c2 c4 |
  d( c) bes |
  a2 g4 |
  f2. \bar "|."
}

alto = \relative c' {
  \global
  c2 e4 |
  f2 f4 |
  g2 f4 |
  e( d) e |
  f( c) d8[ e] |
  f2 f8[ e] |
  d2 e4 |
  f2. |

  c2 e4 |
  f2 f4 |
  g2 f4 |
  e( d) e |
  f( c) d8[ e] |
  f2 f8[ e] |
  d2 e4 |
  f2. |

  e2 e4 |
  f2 f4 |
  f( e) d |
  e( f) c |
  c2 f4 |
  f( g) a |
  a( g) f |
  f2( e4) |
  
  f2 a4 |
  g2 g4 |
  f( c) f |
  g8[ e] f4 c |
  c( f) ees |
  d( f) f |
  f2 e4 |
  f2. \bar "|." 
}

tenor = \relative c' {
  \global
  a2 g4 |
  a4.( bes8) c4 |
  d( c) c |
  c( a) c |
  c2 c4 |
  c2 c4 |
  bes( a) c |
  a2. |

  a2 g4 |
  a4.( bes8) c4 |
  d( c) c |
  c( a) c |
  c2 c4 |
  c2 c4 |
  bes( a) c |
  a2. |

  c2 c4 |
  f,( g) a |
  g2 g4 |
  g( c) bes |
  a2 c4 |
  d2 c4 |
  d2 d4 |
  g,2. |
  c2 d4 |
  d2 c4 |
  c( a) b |
  c bes bes |
  a2 c4 |
  bes( c) d |
  c2 bes4 |
  a2. \bar "|."
}

bass = \relative c, {
  \global
  f4( a) c |
  f2 f4 |
  f( e) f |
  c( d) c |
  a'2 g4 |
  f2 a,4 |
  bes( d) c |
  f2. |

  f,4( a) c |
  f2 f4 |
  f( e) f |
  c( d) c |
  a'2 g4 |
  f2 a,4 |
  bes( d) c |
  f2. |

  a2 a4 |
  d,2 d4 |
  g2 g4 |
  c,( d) e |
  f2 e4 |
  d2 a4 |
  g2 bes4 |
  c2. |
  
  a'2 f4 |
  f( e8[ d]) e4 |
  f( e) d |
  e8[ c] d4 e |
  f2 a,4 |
  bes( a) bes |
  c2 c4 |
  f2. \bar "|."
}

verseOne = \lyricmode {
  \set stanza = "1 "
  % % \l Love di -- vine, all loves ex -- cel -- ling,
  % \l Cha -- ri -- té de Dieu le Pè -- re,
  % % \l Joy of heav'n to earth come down;
  % \l Saint a -- mour de Jé -- sus Christ.
  % % \l Fix in us thy hum -- ble dwel -- ling;
  % \l Oh! re -- viens sur cet -- te ter -- re,
  % % \l All thy faith -- ful mer -- cies crown!
  % \l rem -- plis nous de ton Es -- prit.
  % % \l Je -- sus, Thou art all com -- pas -- sion,
  % \l Ta mi -- sé -- ri -- corde im -- men -- se
  % % \l Pure un -- bound -- ed love Thou art;
  % \l af -- fer -- mit nos coeurs trem -- blants,
  % % \l Vis -- it us with Thy sal -- va -- tion;
  % \l Fais -nous sen -- tir ta puis -- san -- ce,
  % % \l En -- ter ev' -- ry tremb -- ling heart.
  % \l Prends pi -- tié de tes en -- fants.
  \l A -- mour dé -- pas -- sant tout au -- tre,
  \l Le ciel vers nous de -- scen -- du,
  \l De -- meure a -- vec nous les pau -- vres;
  \l Nous ac -- cor -- de ton sa -- lut!
  \l Jé -- sus, tu es la com -- pas -- sion,
  \l L'in -- def -- ect -- i -- ble Sau -- veur;
  \l Mal -- gré no -- tre tré -- pi -- da -- tion,
  \l En -- tre dans nos fai -- bles cœurs.
}

verseTwo = \lyricmode {
  \set ignoreMelismata = ##t
  \set stanza = "2 "
  % % \l Come, Al -- migh -- ty to de -- liv -- er,
  % \l Saint Es -- prit, Es -- prit de vie,
  % % \l Let us all Thy life re -- ceive;
  % \l sois vain -- queur de notre ef -- froi!
  % % \l Sud -- den -- ly re -- turn and nev -- er,
  % \l Ton peuple en toi se con -- fi -- e,
  % % \l Nev -- er more Thy tem -- ples leave.
  % \l no -- tre re -- pos est en toi.
  % % \l Thee we would be al -- ways bles -- sing,
  % \l Sau -- ve nous, cé -- les -- te Frè -- re,
  % % \l Serve Thee as Thy hosts a -- bove,
  % \l Nous es -- pé -- rons ton re -- tour,
  % % \l Pray and praise Thee with -- out ceas -- ing,
  % \l Re -- çois -nous dès cet -- te ter -- re
  % % \l Glo -- ry in Thy per -- fect love.
  % \l à l'au -- tel de ton a -- mour.

  % Breathe, O breathe thy loving Spirit
  % into ev’ry troubled breast.
  % Let us all in thee inherit,
  % let us find the promised rest.
  % Take away the love of sinning;
  % Alpha and Omega be.
  % End of faith, as its beginning,
  % set our hearts at liberty.
  \l Saint -- Es -- prit, _ ô viens in -- suf -- _ fler
  \l la vie dans nos cœurs _ trou -- blés,
  \l Que nous soy -- _ ons tous hér -- i -- _ tiers
  \l de ton re -- pos as -- _ su -- ré.
  \l Tu es Al -- _ pha et O -- mé -- _ ga;
  \l Ô -- te -- nous _ nos ten -- _ ta -- tions.
  \l Toi _ la source et le but _ de la _ _ _ foi,
  \l Don -- ne ta _ li -- bé -- ra -- tion.
}

verseThree = \lyricmode {
  % \set ignoreMelismata = ##t
  \set stanza = "3 "
  % Come, Almighty, to deliver,
  % let us all thy life receive.
  % Suddenly return, and never,
  % nevermore thy temples leave.
  % Thee we would be always blessing,
  % serve thee as thy hosts above,
  % pray, and praise thee without ceasing,
  % glory in thy perfect love.
  \l Pè -- re, rends -- nous ac -- cep -- ta -- bles
  \l de ta vie et ta bon -- té.
  \l Par -- tout est ton ta -- ber -- na -- cle;
  \l reste a -- vec nous à ja -- mais!
  \l Com -- me les an -- ges con -- fes -- sent,
  \l te bé -- nis -- sant pour tou -- jours,
  \l Nous t'hon -- or -- er -- ons sans ces -- se,
  \l lou -- ant ton par -- fait a -- mour.
}

verseFour = \lyricmode {
  \set stanza = "4 "
  % \l Fin -- ish, then, Thy new cre -- a -- tion;
  % \l Pure and spot -- less let us be.
  % \l Let us see Thy great sal -- va -- tion
  % \l Per -- fect -- ly re -- stored in Thee;
  % \l Changed from glo -- ry in -- to glo -- ry,
  % \l Till in heav'n we take our place,
  % \l Till we cast our crowns be -- fore Thee,
  % \l Lost in won -- der, love, and praise.
  \l Per -- fec -- tion -- ne ta cré -- a -- tion;
  \l la -- ve -- nous de nos pé -- chés.
  \l Que l'on voie la re -- stau -- ra -- tion
  \l de ton monde in -- ten -- tion -- né.
  \l De la gloi -- re vers la gloi -- re 
  \l chan -- gés, je -- tons nos cou -- ronnes;
  \l Dans les cieux de sa vic -- toi -- re,
  \l pro -- ster -- nons -- nous à son trône.
}

\score {
  <<
    \new Staff  <<
      \new Voice = "soprano" { \voiceOne \melody }
      \new Voice = "alto" { \voiceTwo \alto }
    >>
    \new Lyrics \with{\lyricspace} \lyricsto soprano \verseOne
    \new Lyrics  \lyricsto soprano \verseTwo
    \new Lyrics  \lyricsto soprano \verseThree
    \new Lyrics  \lyricsto soprano \verseFour
    \new Staff  <<
      \clef bass
      \new Voice = "tenor" { \voiceOne \tenor }
      \new Voice = "bass" { \voiceTwo \bass }
    >>
  >>
}
