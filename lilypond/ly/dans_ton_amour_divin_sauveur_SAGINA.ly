\include "../common_liturgy.ly"

% https://www.conducteurdelouange.com/chants/consulter/71
% TO ADAPT?

global = {
  \key g \major
  \time 4/4
  \autoBeamOff
  \override Staff.TimeSignature #'stencil = ##f
}

\layout {
  \context {
    \Score
    \omit BarNumber
  }
}

melody = \relative c'' {
  \global
  g2 g4. a8 | b2. d4 | c4( e,4) fis4( a4) | g2.
  b,4 | c4( a'4) fis4( d4) | g2 a4( g4) | fis2 e2 | d1 |
  d2 fis4 g4 | g2( fis4) d4 | g2 b2 | c2. d,4 |
  e4( g2) e4 | d4( g2) a4 | b2 a2 | g1 |

  b2 a4 d,4 | g2 a4( b4) | c4( e,4) a4( g4) |
  fis4.( e8 d4) d4 |
  b4( d4 g4) d4 | e4( g4 c4) e,4 | fis4( a4 d4) fis,4 | g2.

  \bar "|" \break
  g4^\markup { \italic "Refrain" } |
  b2 b2 | a2. a4 | c2 c2 | b2.
  b4 | e2 e2 | d2 c4( a4) | g2 fis2 | g1
  \bar "|."
} 

alto = \relative c' {
  \global
  b2 d4. d8 | d2. d4 | e4( c4) c2 | b2. 
  b4 | c2 a2 | d2 d2 | d2 cis2 | s1 |
  d2 d4 d4 | d2. d4 | d2 d2 | d2.
  d4 | c4( e2) c4 | d2. e4 | d2 c2 | b1

  d2 d4 d4 | d2 d2 | e2 e2 | d2. a4 |
  b2. b4 | c4( e2) d4 | d2. c4 | b2.

  s4 | s2 d2 | d4 d4 d4 s4 |
  s2 d2 | d4 d4 d4 s4 |
  s4 g4 g4 g4 | g2 e2 | d2 c2 | b1
}

tenor = \relative c {
  \global
  d2 g4. fis8 | g2. b4 | a2 a4(fis4) | g2. d4 |
  e2 d4( fis4) | g2 c4( b4) | a2 g2 | fis1 |
  fis2 a4 b4 | b2( a4) fis4 | g2 g2 | fis2. g4 |
  g2. g4 | g2. g4 | g2 fis2 | g1 |
  
  g2 fis4 fis4 | g2 fis4( g4) | g2 e4( a4) a4.( g8 fis4) fis4 |
  g2. g4 | g2. a4 | a4( fis2) a4 | g2.

  d4\rest | d2\rest  g2 |
  fis4 fis4 fis4 d4\rest |
  d2\rest fis2 | g4 b4 b4 d4\rest |
  d4\rest c4 c4 c4 | b2 a4( c4) |
  b2 a2 | g1 
}

bass = \relative c {
  \global
  g2 b4. d8 | g4( d4 b4) g4 | c2 d2 | g,2. g4 |
  c2 d4( c4) | b2 fis4( g4) | a2 a2 | d1 |
  d2 d4 d4 | d2. c4 | b2 g2 | d'2. b4 |
  c2. c4 | b2. c4 | d2 d2 | g,1 |

  g2 d'4 c4 | b2 a4( g4) | c2 cis2 | d2. d4 |
  g,2. g4 | c2. c4 | d2. d4 | g,2.

  s4 | s2 g2 | d'4 d4 d4 s4 |
  s2 fis2 | g,4 g4 g4 s4 |
  s4 c4 e4 c4 | g'2 c,2 | d2 d2 | g,1
}

verseOne = \lyricmode {
  % \set ignoreMelismata = ##t
  \set stanza = "1 "
  % And can it be that I should gain
  % An int'rest in the Saviour's blood?
  % Died he for me, who caused his pain?
  % For me, who him to death pursued?
  % Amazing love! how can it be
  % That thou, my Lord, should'st die for me?

  \l Dans ton a -- mour, di -- vin Sau -- veur,
  \l toi, les dé -- li -- ces de ton Père,
  \l te dé -- pouil -- lant de ta splen -- deur,
  \l tu vins t'u -- nir à ma mi -- sère!
  \l A -- mour di -- vin, le Roi des rois
  \l pour moi se don -- ne sur la croix!


  % Amazing love! how can it be
  % That thou, my Lord, should'st die for me?
  \l A -- mour di -- vin, le Roi des rois
  \l pour moi se don -- ne sur la croix!
}

verseTwo = \lyricmode {
  % \set ignoreMelismata = ##t
  \set stanza = "2 "
  % 'Tis mystery all, th'Immortal dies!
  % Who can explore his strange design?
  % In vain the first born seraph tries
  % To sound the depth of love divine;
  % 'Tis mercy all! let earth adore:
  % Let angel minds inquire no more.

  \l En vain, mon cœur, de ton a -- mour,
  \l vou -- drait son -- der le grand my -- stère;
  \l Sa -- voir pour -- quoi tu fus, un jour,
  \l frap -- pé pour moi sur le Cal -- vaire!
  \l Sub -- lime amour, don mer -- veil -- leux,
  \l plus grand que l'in -- fi -- ni des cieux!
}

chorusEcho = \lyricmode {
  _ _ _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _ _ _
  _ _ _ _ _ 

  _ _ \l A -- mour di -- vin,
  \l le Roi des rois 
  \l pour moi se
}

verseThree = \lyricmode {
  % \set ignoreMelismata = ##t
  \set stanza = "3 "
  % He left his Father's throne above;
  % (So free, so infinite his grace!)
  % Emptied himself of all but love,
  % And bled for Adam's helpless race;
  % 'Tis mercy all, immense and free,
  % For, O my God, it found out me!

  \l Le cré -- a -- teur, l'être im -- mor -- tel,
  \l l'au -- teur, la sour -- ce de la vie;
  \l Le Fils de Dieu, le Roi du ciel,
  \l grâce in -- ef -- fab -- le, in -- fi -- nie,
  \l vou -- lant sau -- ver l'Hom -- me pé -- cheur
  \l Souffre à sa place et pour lui meurt!

}

verseFour = \lyricmode {
  % \set ignoreMelismata = ##t
  \set stanza = "4 "
  % Long my imprisoned spirit lay
  % Fast bound in sin and nature's night:
  % Thine eye diffused a quickening ray,
  % I woke: the dungeon flamed with light;
  % My chain fell off, my heart was free—
  % I rose, went forth, and followed thee.

  \l Mais, ô bon -- heur, tu as vain -- cu!
  \l la mort a dû lâ -- cher sa proie,
  \l Et ta vic -- toire est mon sa -- lut,
  \l oui, mon bon -- heur, ma paix, ma joie!
  \l Aus -- si je veux, tout à nou -- veau,
  \l bé -- nir ton Nom, ô saint A -- gneau!
}


\score {
  <<
    \new Staff  <<
      \new Voice = "soprano" { \voiceOne \melody }
      \new Voice = "alto" { \voiceTwo \alto }
    >>
    \new Lyrics \with{\lyricspace} \lyricsto soprano \verseOne
    \new Lyrics  \lyricsto soprano \verseTwo
    \new Lyrics  \lyricsto alto \chorusEcho
    \new Lyrics  \lyricsto soprano \verseThree
    \new Lyrics  \lyricsto soprano \verseFour
    \new Staff  <<
      \clef bass
      \new Voice = "tenor" { \voiceOne \tenor }
      \new Voice = "bass" { \voiceTwo \bass }
    >>
  >>
}

