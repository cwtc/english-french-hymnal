\include "../common_liturgy.ly"

% https://hymnary.org/hymn/GRH1911/page/195

global = {
  \key aes \major
  \time 4/4
  \autoBeamOff
  \override Staff.TimeSignature #'stencil = ##f
}

\layout {
  \context {
    \Score
    \omit BarNumber
  }
}

melody = \relative c' {
  \global
  \partial 2 ees4 aes4 |
  c2 c4 c4 | bes4 aes4 c4. bes8 | aes1~ | aes2 \bar "|" \break
  aes4 c4 | ees2 ees4 ees4 | f4 ees4 ees4. c8 | bes1~ | bes1 \bar "||" \break

  ees2( aes4 f4 | ees1) |
  f4 ees2 aes,4 | aes4. aes8 aes4 aes4 |
  aes4 aes2. | aes4 aes2. | f4 ees2.~ | ees2 \bar "||" \break

  ees4 des'4 | c2 c4 c4 | bes4 aes4 c4. bes8 | aes1
  \bar "|."
} 

alto = \relative c' {
  \global
  ees4 ees4 |
  aes2 aes4 aes4 | ees4 ees4 aes4. ees8 | ees1~ | ees2
  aes4 aes4 | c2 c4 c4 | des4 c4 c4. aes8 | g1~ | g1

  c2( c4 des4 | c1) |
  des4 c2 aes4 | aes4. aes8 aes4 aes4 |
  des,4 c2. | c4 c2. | aes4 aes2.~ | aes2
  ees'4 aes4 | aes2 aes4 aes4 | g4 aes4 g4. ees8 | ees1
}

tenor = \relative c {
  \global
  c4 c4 |
  ees2 ees4 ees4 | des4 c4 ees4. des8 | c2( ees4 f4 | ees2)
  c4 ees4 | aes2 aes4 aes4 | aes4 aes4 aes4. ees8 | ees1~ | ees1

  aes2( f4 des4 | aes'1) |
  aes4 aes2 ees4 | ees4.( ees8) f4 f4 |
  f4 f2. | f4 f2. | des4 c2.~ | c2
  ees4 f4 | ees2 ees4 ees4 | des4 c4 ees4. des8 | c1
}

bass = \relative c {
  \global
  aes4 aes4 |
  aes2 aes4 aes4 | ees4 ees4 ees4. ees8 | aes1~ | aes2
  aes4 aes4 | aes2 aes4 aes4 | aes4 aes4 aes4. c8 | ees,1~ | ees1

  aes'2( f4 des4 | aes1) |
  aes4 aes2 aes4 | aes4.( aes8) f4 f4 |
  bes4 f2. | des'4 des2. | aes4 aes2.~ | aes2
  aes4 aes4 | aes2 aes4 aes4 | ees4 f4 ees4. ees8 | aes1
}

verseOne = \lyricmode {
  % \set ignoreMelismata = ##t
  \set stanza = "1 "
  % Were you there when they crucified my Lord? Oh!
  % Sometimes it causes me to tremble, tremble, tremble.
  % Were you there when they crucified my Lord?
  \l Vois, là -bas, met -- tre le Sei -- gneur en croix! __
  \l Vois, là -bas, met -- tre le Sei -- gneur en croix! __
  "" _ _ _ _ _ _
  _ _ _ _ _ _ _
  \l Vois, là -bas, met -- tre le Sei -- gneur en croix!
}

verseTwo = \lyricmode {
  % \set ignoreMelismata = ##t
  \set stanza = "2 "
  % Were you there when they nailed Him to the tree? (to the tree?)
  % Were you there when they nailed Him to the tree? Oh!
  % Sometimes it causes me to tremble, tremble, tremble.
  % Were you there when they nailed Him to the tree?
  \l Vois, là -bas, clou -- er le Sei -- gneur au bois! __
  \l Vois, là -bas, clou -- er le Sei -- gneur au bois! __
  "" _ _ _ _ _ _
  _ _ _ _ _ _ _
  \l Vois, là -bas, clou -- er le Sei -- gneur au bois!
}

verseThree = \lyricmode {
  % \set ignoreMelismata = ##t
  \set stanza = "3 "
  % Were you there when they pierced Him in the side? (in the side?)
  % Were you there when they pierced Him in the side? Oh!
  % Sometimes it causes me to tremble, tremble, tremble.
  % Were you there when they pierced Him in the side?
  \l Vois, là -bas, de leur lance ils l'ont per -- cé! __
  \l Vois, là -bas, de leur lance ils l'ont per -- cé! __
  \l Oh, __ le cœur sai -- si d'ef -- froi,
  \l je trem -- ble, trem -- ble, trem -- ble.
  \l Vois, là -bas, de leur lance ils l'ont per -- cé!
}

verseFour = \lyricmode {
  % \set ignoreMelismata = ##t
  \set stanza = "4 "
  % Were you there when they laid Him in the tomb? (in the tomb?_
  % Were you there when they laid Him in the tomb? Oh!
  % Sometimes it causes me to tremble, tremble, tremble.
  % Were you there when they laid Him in the tomb?
  \l Vois, là -bas, dans la tombe ils l'ont por -- té! __
  \l Vois, là -bas, dans la tombe ils l'ont por -- té! __
  "" _ _ _ _ _ _
  _ _ _ _ _ _ _
  \l Vois, là -bas, dans la tombe ils l'ont por -- té!
}


\score {
  <<
    \new Staff  <<
      \new Voice = "soprano" { \voiceOne \melody }
      \new Voice = "alto" { \voiceTwo \alto }
    >>
    \new Lyrics \with{\lyricspace} \lyricsto soprano \verseOne
    \new Lyrics  \lyricsto soprano \verseTwo
    \new Lyrics  \lyricsto soprano \verseThree
    \new Lyrics  \lyricsto soprano \verseFour
    \new Staff  <<
      \clef bass
      \new Voice = "tenor" { \voiceOne \tenor }
      \new Voice = "bass" { \voiceTwo \bass }
    >>
  >>
}
