\include "../common_liturgy.ly"

global = {
  \key d \major
  \time 4/4
  \autoBeamOff
  \override Staff.TimeSignature #'stencil = ##f
}

\layout {
  \context {
    \Score
    \omit BarNumber
  }
}

melody = \relative c'' {
  \global
  a4 d a b |
  a4. g8 fis2 |
  a4 a g fis |
  e e d2 | \break
  
  a'4 d a b |
  a4. g8 fis2 |
  a4 a g fis |
  e e d2 | \break
  
  e4 e fis8[ gis] a4 |
  a4 gis a2 |
  b4. cis8 d4 d |
  cis4 cis b2 | \break
  fis4 fis b a |
  a gis a2 |
  b4 a g fis |
  e e d2 \bar "|."
} 

alto = \relative c' {
  \global
  d4 d d d |
  d cis d2 |
  d4 d8[ cis] b[ cis] d4 |
  d cis d2 |
  d4 d d d |
  d cis d2 |
  d4 d8[ cis] b[ cis] d4 |
  d cis d2 |
  cis4 cis d cis |
  fis e8[ d] cis2 |
  e8[ fis] g4 fis fis |
  fis fis8[ e] d2 |
  d4 d d cis |
  d d cis2 |
  d4 d8[ cis] b[ cis] d4 |
  d cis d2 |
}

tenor = \relative c {
  \global
  fis4 fis8[ g] a4 g8[ fis] |
  e[ d] e4 d2 |
  fis4 fis g a |
  b a fis2 |
  
  fis4 fis8[ g] a4 g8[ fis] |
  e[ d] e4 d2 |
  fis4 fis g a |
  b a fis2 |
  
  a4 a a a |
  b b a2 |
  g4. a8 b4 b |
  b as b2 |
  
  a4 a g8[ fis] e4 |
  d8[ fis] e[ d] e2 |
  g4 fis g a |
  b a8[ g] fis2
}

bass = \relative c {
  \global
  d4 b fis g |
  a a d2 |
  d4 d e fis |
  g a d,2 |
  d4 b fis g |
  a a d2 |
  d4 d e fis |
  g a d,2 |
  a4 a d fis8[ e] |
  d4 e a,2 |
  e'4 e b8[ cis] d[ e] |
  fis4 fis b,2 |
  d4 d g, a |
  b b a2 |
  g4 d' e fis |
  g
}

verseOne = \lyricmode {
  % \set ignoreMelismata = ##t
  \set stanza = "1 "
  % At the Lamb's high feast we sing
  % Praise to our victorious King,
  % Who hath washed us in the tide
  % Flowing from His piercèd Side;
  % Praise we Him, whose love divine,
  % Gives His sacred Blood for wine,
  % Gives His Body for the feast,
  % Christ the Victim, Christ the Priest.
  \l Au ban -- quet du Roi, on chante
  \l en vic -- toi -- re nos lou -- anges!
  \l L'A -- gneau, il nous a la -- vés
  % \l dans l'eau cou -- lant du cô -- té.
  \l a -- vec l'eau de son cô -- té.
  \l Lou -- ange à l'a -- mour di -- vin
  \l qui don -- ne son sang en vin,
  \l don -- nant son corps pour la fête,
  \l Christ vic -- ti -- me, Christ le prêtre.
}

verseTwo = \lyricmode {
  % \set ignoreMelismata = ##t
  \set stanza = "2 "
  % Where the Paschal blood is poured,
  % Death's dark angel sheathes his sword
  % Israel's hosts triumphant go
  % Through the wave that drowns the foe.
  % Praise we Christ, Whose Blood was shed,
  % Paschal Victim, Paschal Bread;
  % With sincerity and love
  % Eat we manna from above.
  \l Où le sang pas -- cal jail -- lit,
  \l la mort, dés -- ar -- mé -- e, fuit.
  \l La mer s'ouvre; Is -- ra -- ël passe;
  \l l'on -- de l'en -- ne -- mi é -- crase.
  \l Nous lou -- ons ce sang ver -- sé,
  \l cet -- te chair en pain don -- née;
  \l a -- vec notre a -- do -- ra -- tion
  \l cet -- te Man -- ne nous man -- geons.

}

verseThree = \lyricmode {
  % \set ignoreMelismata = ##t
  \set stanza = "3 "
  % Mighty Victim from the sky!
  % Hell's fierce powers beneath Thee lie;
  % Thou hast conquer'd in the fight,
  % Thou hast brought us life and light:
  % Now no more can death appall,
  % Now no more the grave enthrall;
  % Thou hast opened Paradise,
  % And in Thee Thy saints shall rise.
  \l Vic -- ti -- me, tu as con -- quis
  \l en nous res -- tau -- rant la vie!
  \l Tu as ga -- gné dans la guerre,
  \l bri -- sant les chaî -- nes d'en -- fer:
  \l Donc, la mort ne nous tient plus;
  \l la tom -- be n'a pas vain -- cu!
  \l Et on res -- sus -- ci -- ter -- a
  \l en toi, no -- tre Juge et Roi.
}

verseFour = \lyricmode {
  % \set ignoreMelismata = ##t
  \set stanza = "4 "
  % Easter triumph, Easter joy,
  % Sin alone can this destroy;
  % From sin's power do Thou set free
  % Souls new-born, O Lord, in Thee.
  % Hymns of glory and of praise,
  % Risen Lord, to Thee we raise;
  % Holy Father, praise to Thee,
  % With the Spirit, ever be.
  \l La joie pas -- ca -- le, hé -- las!
  \l Seul le pé -- ché la me -- nace.
  \l De son pou -- voir, toi, Sei -- gneur,
  \l tu nous li -- bè -- res le cœur.
  \l Jé -- sus vi -- vant, nous t'of -- frons
  \l nos can -- ti -- ques de lou -- ange,
  \l au Père et au Saint -Es -- prit;
  \l à ja -- mais qu'il soit ain -- si.
}


\score {
  <<
    \new Staff  <<
      \new Voice = "soprano" { \voiceOne \melody }
      \new Voice = "alto" { \voiceTwo \alto }
    >>
    \new Lyrics \with{\lyricspace} \lyricsto soprano \verseOne
    \new Lyrics  \lyricsto soprano \verseTwo
    \new Lyrics  \lyricsto soprano \verseThree
    \new Lyrics  \lyricsto soprano \verseFour
    \new Staff  <<
      \clef bass
      \new Voice = "tenor" { \voiceOne \tenor }
      \new Voice = "bass" { \voiceTwo \bass }
    >>
  >>
}

