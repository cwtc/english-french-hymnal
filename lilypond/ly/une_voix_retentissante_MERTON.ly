\include "../common_liturgy.ly"

global = {
  \key e \major
  \time 4/4
  \autoBeamOff
  \override Staff.TimeSignature #'stencil = ##f
}

\layout {
  \context {
    \Score
    \omit BarNumber
  }
}

melody = \relative c' {
  \global
  e4 gis b b |
  a cis cis b \bar "|"

  gis ais b b |
  cis cis b2 \bar "|"

  b4 a gis b |
  a gis fis e \bar "|"

  fis4 gis a gis |
  fis fis e2 \bar "|."
} 

alto = \relative c' {
  \global
  b4 b b dis |
  e e e e \bar "|"

  e e dis fis |
  gis fis8[ e] dis2 \bar "|"

  dis4 e8[ fis] e4 fis |
  e e dis e \bar "|"

  cis eis fis e |
  e dis e2 \bar "|."
}

tenor = \relative c' {
  \global
  gis4 b fis fis |
  e a a gis \bar "|"

  b e, fis b |
  b ais b2 \bar "|"

  dis4 cis8[ b] b4 b |
  cis b b8[ a] gis4 \bar "|"

  a b cis b |
  cis b8[ a] gis2 \bar "|."
}

bass = \relative c {
  \global
  e4 e dis b |
  cis a8[ b] cis dis e4 \bar "|"

  e cis b dis |
  e fis b,2 \bar "|"

  b4 cis8[ dis] e4 dis |
  cis e b cis \bar "|"

  a gis fis gis |
  a b e2 \bar "|."
}

verseOne = \lyricmode {
  % \set ignoreMelismata = ##t
  \set stanza = "1 "
  \l U -- ne voix re -- ten -- tis -- san -- te
  \l a cri -- é: “Voi -- ci l'É -- poux!
  \l Plus de lan -- gueur im -- puis -- san -- te;
  \l en -- fants du jour, le -- vez -vous!”
}

verseTwo = \lyricmode {
  % \set ignoreMelismata = ##t
  \set stanza = "2 "
  \l Du lourd som -- meil de la ter -- re
  \l bri -- sons le fa -- tal pou -- voir.
  \l Le di -- vin so -- leil é -- clai -- re;
  \l ou -- vrons les yeux pour le voir.
}

verseThree = \lyricmode {
  % \set ignoreMelismata = ##t
  \set stanza = "3 "
  \l Ob -- jet d'u -- ne longue at -- ten -- te,
  \l l'A -- gneau pur des -- cend des cieux.
  \l Que notre â -- me pé -- ni -- ten -- te
  \l plonge en son sang pré -- ci -- eux.  
}

verseFour = \lyricmode {
  % \set ignoreMelismata = ##t
  \set stanza = "4 "
  \l Il re -- vien -- dra plein de gloi -- re
  \l dans ce monde é -- pou -- van -- té
  \l nous pren -- dre dans sa vic -- toi -- re,
  \l con -- quis par sa cha -- ri -- té.
}

verseFive = \lyricmode {
  % \set ignoreMelismata = ##t
  \set stanza = "5 "
  \l Gloire, hon -- neur, dans tous les â -- ges
  \l au Père, au Fils Ré -- demp -- teur,
  \l à la lu -- miè -- re des sa -- ges,
  \l à l'Es -- prit con -- so -- la -- teur.
}


\score {
  <<
    \new Staff  <<
      \new Voice = "soprano" { \voiceOne \melody }
      \new Voice = "alto" { \voiceTwo \alto }
    >>
    \new Lyrics \with{\lyricspace} \lyricsto soprano \verseOne
    \new Lyrics  \lyricsto soprano \verseTwo
    \new Lyrics  \lyricsto soprano \verseThree
    \new Lyrics  \lyricsto soprano \verseFour
    \new Lyrics  \lyricsto soprano \verseFive
    \new Staff  <<
      \clef bass
      \new Voice = "tenor" { \voiceOne \tenor }
      \new Voice = "bass" { \voiceTwo \bass }
    >>
  >>
}


