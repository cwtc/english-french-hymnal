\include "../common_liturgy.ly"

global = {
  \key c \major
  \time 4/4
  \autoBeamOff
  \override Staff.TimeSignature #'stencil = ##f
}

\layout {
  \context {
    \Score
    \omit BarNumber
  }
}

melody = \relative c' {
  \global
  c'2 g4 a g4. (f8) e2 a4. b8 g4 c c4(b) c2 \bar "|" 
  c2 d4 d e4(c) a2 d4. a8 b4 c b4(a) g2 \bar "|" 
  e2 f4 g f4(e) d2 a'4. b8 c4 a d4(c) b2 \bar "|" 
  g4. (f8) e4 f e2 d c1 
  \bar "|."
} 

alto = \relative c' {
  \global
  e2 c4 f d2 c c4. b8 c4 c g'2 e \bar "|"
  e2 g4 g \bar "|" \break
g4 a fis2 g4. fis8 g4 e g4 fis d2 \bar "|" c2. b4 a4 cis d2 \bar "|" \break
c4. d8 c4 c f4 e e2 \bar "|" b2 c4 a c2 b g1
}

tenor = \relative c {
  \global
  g'2 g4 c b2 c2
  a4. f8 g4 a d2 c \bar "|" c2 b4 b \bar "|" \break
c4 e4 d2 d2 d4 c4 d2 b2 \bar "|" g2 f4 d f4 a f2 \bar "|" \break
 f4. f8 g4 a a4 a g2 \bar "|" g2 c4 f, g2 g e1
}

bass = \relative c {
  \global
  c2 e4 f  g,2 a  { f'4. d8 e4 f g2 c, \bar "|" a'2 g4 g } \bar "|" \break
c,4 a d2 b4. d8 g,4 a b4 d g,2 \bar "|" c2 a4 g d'4 a d2 \bar "|" \break   f4. d8 e4 f d4 a' e2 \bar "|"  { g2 a4 d, e4 f g2 } c,1
}

verseOne = \lyricmode {
  % \set ignoreMelismata = ##t
  \set stanza = "1 "
  \l Jé -- sus, la gloire et l'hon -- _ neur des an -- ges,
  \l toi qui nous cré -- a, toi qui sur nous rè -- gne:
  \l ô, de ta bon -- té, donne à tes ser -- vi -- teurs
  \l de s'ap -- pro -- cher au ciel.
  % qui donna au sommet la Loi
  % tu es mon ?? et mon Roi
  % tout ?? de tes louanges 
  % et tout ?? aux bas lieux
  % dans l? ?? de tes beaux yeux
}

verseTwo = \lyricmode {
  % \set ignoreMelismata = ##t
  \set stanza = "2 "
  % Tes anges font toujours la ronde
  % au tour du point(?) de ta beauté(?)
  % ministres de ta volonté
  % ce sont petits ? du monde 
  % qui nous conduisent ici bas
  % au ? du ciel pas à pas
  \l En -- voi -- e ton saint Mi -- _ chel l'arch -- an -- ge,
  \l le con -- ci -- lia -- teur; qu'il ex -- pul -- se de nous
  \l tout pro -- pos hain -- eux, pour que la paix é -- clore,
  \l et que tout pros -- pè -- re.
}

verseThree = \lyricmode {
  % \set ignoreMelismata = ##t
  \set stanza = "3 "
  % Sur cette petite famille 
  % 
  \l En -- voi -- e ton saint Ga -- bri -- el l'arch -- an -- ge,
  \l le fort hé -- rault; qu'il pro -- tè -- ge les mor -- tels,
  \l chas -- sant le ser -- pent, sur -- veil -- lant les tem -- ples
  \l où tu es lou -- _ é.
  
}

verseFour = \lyricmode {
  % \set ignoreMelismata = ##t
  \set stanza = "4 "
  \l En -- voi -- e ton saint Ra -- pha -- ël l'arch -- an -- ge,
  \l le res -- tau -- ra -- teur de tous qui s'é -- ga -- rent,
  \l qui, à ton or -- dre, for -- ti -- fi -- e nos corps
  \l par ton sainte onc -- ti -- on.
}


\score {
  <<
    \new Staff  <<
      \new Voice = "soprano" { \voiceOne \melody }
      \new Voice = "alto" { \voiceTwo \alto }
    >>
    \new Lyrics \with{\lyricspace} \lyricsto soprano \verseOne
    \new Lyrics  \lyricsto soprano \verseTwo
    \new Lyrics  \lyricsto soprano \verseThree
    \new Lyrics  \lyricsto soprano \verseFour
    \new Staff  <<
      \clef bass
      \new Voice = "tenor" { \voiceOne \tenor }
      \new Voice = "bass" { \voiceTwo \bass }
    >>
  >>
}

\markup { \vspace #0.5 } 

\markup {
  \fill-line {
    \hspace #0.1 % moves the column off the left margin;
     % can be removed if space on the page is tight
     \column {
      \line { \bold "5 "
        \column {
          % May the blest Mother of our God and Saviour,
          % may the assembly of the saints in glory,
          % may the celestial companies of angels
          % ever assist us. 
          "Que la Vierge Mère du Dieu Sauveur,"
          "que l'assemblée de tous les saints triomphants,"
          "que les nombreuse armées célestes des anges"
          "nous aident à jamais."
        }
      }
    }
    \hspace #0.1 % adds horizontal spacing between columns;
    \column {
      \line { \bold "6 "
        \column {
          % Father almighty, Son and Holy Spirit,
          % God ever blessèd, be thou our preserver;
          % thine is the glory which which the angels worship
          % veiling their faces.
          "Père tout-puissant, Fils, Saint-Esprit égal,"
          "Dieu saint et fort, sois toujours notre gardien;"
          "à toi la gloire que les anges louent,"
          "voilant leurs visages."
        }
      }
    }
  \hspace #0.1 % gives some extra space on the right margin;
  % can be removed if page space is tight
  }
}
