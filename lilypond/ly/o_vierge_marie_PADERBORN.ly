\include "../common_liturgy.ly"

% https://hymnary.org/media/fetch/106272/hymnary/score/CYBER/pdf/p/a/d/Paderborn.pdf
global = {
  \key g \major
  \time 3/4
  \autoBeamOff
  \override Staff.TimeSignature #'stencil = ##f
}

\layout {
  \context {
    \Score
    \omit BarNumber
  }
}

melody = \relative c' {
  \global
  \partial 4 d4 |
  g4 g4 a4 | b4 b4 b8[ c8] | d4 b4 a4 | g2 \bar "|" \break
  d4 | g4 g4 a4 | b4 b4 b8[ c8] | d4 b4 a4 | g2 \bar "|" \break
  b8[ c8] | d4 b4 d4 | c8[ b8] a4 c4 | b8[ a8] g4 b4 | a2 \bar "|" \break
  d,4 | g4 g4 a4 | b4 b4 b8[ c8] | d4 b4 a4 | g2.
  \bar "|."
} 

alto = \relative c' {
  \global
  b4 | d4 e4 fis4 | g4 g4 g4 | g4 g4 fis4 | d2
  d4 | b4 e4 e4 | e4 dis4 e4 | d4 e4 fis4 | g2
  g4 | d4 g8[ f8] e4 | e4 e4 d4 | d4 d4 d8[ e8] | fis2
  d4 | d8[ c8] b4 d4 | d4 e4 fis8[ g8] | g4 g4 fis4 | g2.
}

tenor = \relative c' {
  \global
  b4 | b4 b4 d4 | d4 d4 e4 | d4 d4 c4 | b2
  a4 | g4 b4 a4 | fis4 fis4 g4 | g4 g4 c4 | c4 b4 
  d4 | d4 d4 b4 | c4. b8 a4 | b8[ c8] d8[ c8] b4 | d2
  fis,4 | g4 g4 g8[ fis8] | g8[ e8] g4 d'8[ e8] | d4 d4 d4 | b2.
}

bass = \relative c {
  \global
  g4 | g'4 e4 d4 | g4 fis4 e4 | b4 b8[ c8] d4 | g2
  fis4 | e4 e8[ d8] c4 | b4 b4 e4 | b4 c4 d4 | g2
  g8[ a8] | b4 g4 gis4 | a4. f8 fis4 | g8[ a8] b8[ a8] g4 | d2
  d8[ c8] | b8[ a8] g4 b8[ d8] | g8[ e8] e4 d8[ c8] |
  b4 b8[ c8] d4 | g2.
}

verseOne = \lyricmode {
  % \set ignoreMelismata = ##t
  \set stanza = "1 "
  \l Ô Vier -- ge Ma -- ri -- e, nos voix et nos cœurs
  \l vou -- draient de ta vi -- e chan -- ter les gran -- deurs.
  \l À tou -- te la ter -- re, ta na -- ti -- vi -- té
  \l pré -- sage une autre è -- re de fé -- li -- ci -- té.

}

verseTwo = \lyricmode {
  % \set ignoreMelismata = ##t
  \set stanza = "2 "
  \l E -- mue et ra -- vi -- e de vant Ga -- bri -- el,
  \l ac -- cepte, ô Ma -- ri -- e, les or -- dres du Ciel.
  \l Pour sau -- ver la ter -- re du joug de Sa -- tan,
  \l oui, tu se -- ras Mè -- re du Dieu Tout -- Puis -- sant.
}

verseThree = \lyricmode {
  % \set ignoreMelismata = ##t
  \set stanza = "3 "
  \l Bien -- tôt dans l'é -- ta -- ble naît sur tes ge -- noux
  \l le Verbe a -- do -- ra -- ble fait Hom -- me pour nous.
  \l Oh! Qui pour -- ra di -- re l'a -- mour de ton Fils?
  \l À ton doux em -- pi -- re un Dieu s'est sou -- mis.
  
}

verseFour = \lyricmode {
  % \set ignoreMelismata = ##t
  % TO ADAPT
  \set stanza = "4 "
  \l Jé -- sus te cou -- ron -- ne de sa ma -- jes -- té;
  \l prends place à son trô -- ne pour l'é -- ter -- ni -- té.
  \l Et nous de ta vi -- e, chan -- tant les gran -- deurs,
  \l t'of -- frons, ô Ma -- ri -- e, nos voix et nos cœurs.

}


\score {
  <<
    \new Staff  <<
      \new Voice = "soprano" { \voiceOne \melody }
      \new Voice = "alto" { \voiceTwo \alto }
    >>
    \new Lyrics \with{\lyricspace} \lyricsto soprano \verseOne
    \new Lyrics  \lyricsto soprano \verseTwo
    \new Lyrics  \lyricsto soprano \verseThree
    \new Lyrics  \lyricsto soprano \verseFour
    \new Staff  <<
      \clef bass
      \new Voice = "tenor" { \voiceOne \tenor }
      \new Voice = "bass" { \voiceTwo \bass }
    >>
  >>
}

