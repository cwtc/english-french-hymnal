\include "../common_liturgy.ly"

global = {
  \key c \major
  \time 3/4
  \autoBeamOff
  \override Staff.TimeSignature #'stencil = ##f
}

\layout {
  \context {
    \Score
    \omit BarNumber
  }
}

melody = \relative c' {
  \global
  \partial 4
  \partial 4 c4 |
  e2 g4 | c2 g4 |
  f4( e4) d4 |
  c2 \bar "|"

  c'4 |
  e2 c4 |
  a4( b) c |
  g2 fis4 |
  g2 \bar "|"  \break

  g4 |
  a2 b4 |
  c2 g4 |
  f4( e) d |
  e2 \bar "|"

  g4 |
  a4( b) c |
  g( f) e |
  d2 c4 |
  c2 \bar "|."
} 

alto = \relative c' {
  \global
  \partial 4
  g4 |
  c2 b4 |
  e2 e4 |
  c4 g b |
  g2 \bar "|"

  g'4 |
  a2 a4 |
  fis2 e4 |
  b2 d4 |
  d2 \bar "|"

  e4 |
  f2 f4 |
  e2 c4 |
  c2 b4 |
  b2 \bar "|"

  c4 |
  e4( g4) g4 |
  d4 c c |
  b2 g4 |
  g2 
}

tenor = \relative c {
  \global
  \partial 4
  e4 | 
  g2 b4 |
  c2 c4 |
  a4 g4 b4 |
  e2 \bar "|"

  e4 |
  c2 a4 |
  d2 g,4 |
  g2 a4 |
  b2 \bar "|"

  c4 |
  c2 b4 |
  a2 g4 |
  a g g |
  g2 \bar "|"

  e4 |
  c4 d4 e4 |
  b'4 a4 g |
  g2 e4 |
  e2 
}

bass = \relative c {
  \global
  \partial 4
  c4 |
  c2 e4 |
  a,2 c4 |
  f4 c4 g'4 |
  c,2 \bar "|"

  c'4 |
  a2 c,4 |
  d2 c4 |
  e2 d4 |
  g2 \bar "|"

  c,4 |
  f2 d4 |
  a2 e'4 |
  f c g' |
  e2 \bar "|"

  c4 |
  a g e |
  g a c |
  g2 c4 |
  c2 
}

verseOne = \lyricmode {
  % \set ignoreMelismata = ##t
  \set stanza = "1 "
  % O love, how deep, how broad, how high,
  % O great, O wondrous mystery,
  % That God, the Son of God, should take
  % Our mortal form for mortals' sake!
  \l A -- mour pro -- fond ma -- jes -- tu -- eux:
  \l ô grand my -- stè -- re mer -- veil -- leux,
  \l que pour nous, Dieu, par Dieu le Fils
  \l la fai -- ble chair mor -- tel -- le prit!
}

verseTwo = \lyricmode {
  % \set ignoreMelismata = ##t
  \set stanza = "2 "
  % He sent no angel to our race,
  % Of higher or of lower place,
  % But He Himself to this world came
  % And wore the robe of human frame.
  \l Un ange il n'a pas en -- voy -- é
  \l pour que nous so -- yons ra -- bais -- sé:
  \l Ce Dieu lui -même est ve -- nu
  \l de la ro -- be hu -- mai -- ne vê -- tu. 
}

verseThree = \lyricmode {
  % \set ignoreMelismata = ##t
  \set stanza = "3 "
  % For us baptized! then Spirit-led
  % He fasted,--Who the thousands fed;
  % For us temptation sharp He knew,
  % For us the Tempter overthrew.
  \l Pour nous il s'est fait bap -- ti -- sé!
  \l Lui qui nour -- rit, il a jeû -- né;
  \l il a la ten -- ta -- tion con -- nu
  \l et a le ten -- ta -- teur bat -- tu.
}

verseFour = \lyricmode {
  % \set ignoreMelismata = ##t
  \set stanza = "4 "
  % For us He prayed, for us He taught,
  % For us His every work He wrought,
  % By words, and signs, and actions, thus
  % Still seeking not Himself, but us.
  \l Pour nous il par -- la et pri -- a,
  \l cha -- que la -- beur il tra -- vail -- la;
  \l cha -- que bien -- fait cher -- chait le bien
  \l de ses bré -- bis, et non le sien.
}

\score {
  <<
    \new Staff  <<
      \new Voice = "soprano" { \voiceOne \melody }
      \new Voice = "alto" { \voiceTwo \alto }
    >>
    \new Lyrics \with{\lyricspace} \lyricsto soprano \verseOne
    \new Lyrics  \lyricsto soprano \verseTwo
    \new Lyrics  \lyricsto soprano \verseThree
    \new Lyrics  \lyricsto soprano \verseFour
    \new Staff  <<
      \clef bass
      \new Voice = "tenor" { \voiceOne \tenor }
      \new Voice = "bass" { \voiceTwo \bass }
    >>
  >>
}


\markup { \vspace #0.5 } 

\markup {
  \fill-line {
    \hspace #0.1 % moves the column off the left margin;
     % can be removed if space on the page is tight
     \column {
      \line { \bold "5 "
        \column {
          % For us by wicked men betrayed,
          % Scourged, mocked, in crown of thorns arrayed;
          % For us He bore the Cross's death,
          % For us at length gave up His breath.
          "Pour nous trahi par le péché,"
          "flagellé, des plaies paré;"
          "pour nous il la dure croix prit"
          "et il y remettra l'esprit."
        }
      }
    }
    \hspace #0.1 % adds horizontal spacing between columns;
    \column {
      \line { \bold "6 "
        \column {
          % For us He rose from death again,
          % For us He went on high to reign,
          % For us He sent His Spirit here
          % To guide, to strengthen, and to cheer. 
          "Pour nous il est ressuscité;"
          "pour nous il monta pour régner;"
          "pour nous il envoya l'Esprit"
          "qui guide et qui refortifie."
        }
      }
    }
  \hspace #0.1 % gives some extra space on the right margin;
  % can be removed if page space is tight
  }
}
