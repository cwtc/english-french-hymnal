\include "../common_liturgy.ly"

% https://hymnary.org/hymn/CHT1907/page/88
% TRANSLATION NEEDS ADAPTING (the god of abraham praise)
% https://hymnary.org/text/the_god_of_abraham_praise_who_reigns/fulltexts

global = {
  \key bes \major
  \time 4/4
  \autoBeamOff
  \override Staff.TimeSignature #'stencil = ##f
}

\layout {
  \context {
    \Score
    \omit BarNumber
  }
}

melody = \relative c' {
  \global
  \partial 4 d4 | g4 a4 bes4 c4 | d2.
  bes4 | c4 d4 ees4 f4 | d2. \bar "|" \break
  a4 | bes4 c4 d4 ees4 | f4 a,4 bes4 
  ees4 | d2 c2 bes2. \bar "|" \break

  bes4 | d4 d4 d4 d4 | c2. 
  bes8[ a8] | g8[ a8] bes8[ c8] d4 g,4 | fis2. \bar "|" \break
  d4 | g4 a4 bes4 c4 | d4 c8[ d8] ees4
  d8[ c8] | bes2 a2 | g1
  \bar "|."
} 

alto = \relative c' {
  \global
  d4 | d4 d4 d4 g4 | fis2.
  f4 | g4 f4 g4 f4 | f2.
  d4 | d4 f4 f4 g4 | f4 ees4 d4 
  g4 | f2 f4( ees4) d2.

  f4 | f4 a4 g4 f4 | f2. 
  d4 | d4 g4 d4 e4 | d2.
  d4 | d4 d4 d4 f4 | f4 g8[ f8] g4
  a4 | g2 fis2 | s1
}

tenor = \relative c' {
  \global
  bes4 | bes4 bes4 g4 g4 | a2.
  bes4 | g8[ a8] b4 c4 a4 | bes2.
  fis4 | g4 a4 bes4 bes4 | bes4 c4 bes4 
  bes4 | bes2 a2 bes2.

  d4 | bes4 c4 bes4 bes4 | a2. 
  c4 | bes4 g8[ a8] bes4 a4 | a2.
  fis4 | g4 fis4 g4 a4 | bes4 g8[ b8] c4
  ees4 | d2 d4( c4) | bes1
}

bass = \relative c {
  \global
  g4 | g'4 fis4 g4 ees4 | d2.
  d4 | ees4 d4 c4 f,4 | bes2.
  d4 | g4 f4 bes4 g4 | d4 f4 g4 
  ees4 | f2 f,2 bes2.

  bes4 | bes'4 fis4 g4 d4 | f2. 
  fis4 | g4 ees4 bes4 c4 | d2.
  d8[ c8] | bes4 d4 g4 f4 | bes,4 ees8[ d8] c4
  c4 | d2 d2 | g1
}

verseOne = \lyricmode {
  % \set ignoreMelismata = ##t
  \set stanza = "1 "
  % The God of Abraham praise,
  % Who reigns enthroned above;
  % Ancient of everlasting days,
  % And God of love;
  % Jehovah, Great I AM!
  % By earth and heaven confess'd;
  % I bow and bless the sacred Name,
  % For ever bless'd.
  % \l Lou -- é soit à ja -- mais
  \l Le grand Dieu d'A -- bra -- ham,
  % \l le Dieu de nos aï -- eux,
  \l le vrai An -- cien des Jours,
  \l il rè -- gne pour l'é -- ter -- ni -- té,
  % \l tout ê -- tre est fait de sa main,
  % \l Dans les hauts lieux.
  \l ce Dieu d'a -- mour.
  % \l Je suis grand, dit Yah -- vé,
  \l Il est le grand Je Suis!
  % \l L'u -- ni -- vers est à moi.
  % \l L'u -- ni -- vers est à lui;
  \l Sei -- gneur, A -- do -- na -- ï!
  % \l A -- do -- rons le nom vé -- né -- ré
  \l A -- do -- rons donc le Nom bé -- ni
  % \l de no -- tre Roi.
  \l à l'in -- fi -- ni.
}

verseTwo = \lyricmode {
  % \set ignoreMelismata = ##t
  \set stanza = "2 "
  % The God of Abraham praise,
  % At Whose supreme command
  % From earth I rise, and seek the joys
  % At His right hand:
  % I all on earth forsake,
  % Its wisdom, fame, and power;
  % And Him my only portion make,
  % My shield and tower.
  \l Le grand Dieu d'A -- bra -- ham
  \l don -- na son mot su -- prême,
  \l et je fus for -- mé de la terre, et
  \l donc je l'aime.
  \l Le pou -- voir j'a -- ban -- donne,
  \l et tou -- te sa sa -- gesse;
  \l j'ac -- cep -- te tout ce qu'il me donne,
  \l ma for -- ter -- esse.
}

verseThree = \lyricmode {
  % \set ignoreMelismata = ##t
  \set stanza = "3 "
  % He by Himself hath sworn,
  % I on His oath depend,
  % I shall, on angel-wings upborne,
  % To heaven ascend:
  % I shall behold his face,
  % I shall His power adore,
  % And sing the wonders of His grace
  % For evermore.
  \l Mon Sau -- veur et mon Dieu
  \l m'a ju -- ré son a -- mour.
  \l Je mon -- te -- rai jus -- ques aux cieux
  \l plus haut tou -- jours.
  \l Là je ver -- rai mon Roi
  \l et, lou -- ant ses bien -- faits,
  \l chan -- te -- rai sa gloire a -- vec foi
  \l à tout ja -- mais.
}

verseFour = \lyricmode {
  % \set ignoreMelismata = ##t
  \set stanza = "4 "
  % There dwells the Lord our King,
  % The Lord our righteousness,
  % Triumphant o'er the world and sin,
  % The Prince of Peace;
  % On Sion's sacred height
  % His kingdom He maintains,
  % And, glorious with His saints in light,
  % For ever reigns.
  \l Sur les monts de Si -- on
  \l il de -- meure à ja -- mais,
  \l vic -- to -- ri -- eux des pas -- si -- ons
  \l en tou -- te paix:
  \l Il vit a -- vec les saints,
  \l il a la ro -- yau -- té;
  \l il ac -- com -- plit tous ses des -- seins
  \l dans la clar -- té.
}

\score {
  <<
    \new Staff  <<
      \new Voice = "soprano" { \voiceOne \melody }
      \new Voice = "alto" { \voiceTwo \alto }
    >>
    \new Lyrics \with{\lyricspace} \lyricsto soprano \verseOne
    \new Lyrics  \lyricsto soprano \verseTwo
    \new Lyrics  \lyricsto soprano \verseThree
    \new Lyrics  \lyricsto soprano \verseFour
    % \new Lyrics  \lyricsto soprano \verseFive
    \new Staff  <<
      \clef bass
      \new Voice = "tenor" { \voiceOne \tenor }
      \new Voice = "bass" { \voiceTwo \bass }
    >>
  >>
}

\markup { \vspace #0.5 } 

\markup {
  \fill-line {
    \hspace #0.1 % moves the column off the left margin;
     % can be removed if space on the page is tight
     \column {
      \line { \bold "5 "
        \column {
          % The God Who reigns on high
          % The great archangels sing;
          % And, "Holy, holy, holy," cry,
          % "Almighty King,
          % Who was, and is the same,
          % And evermore shall be;
          % Jehovah, Father, Great I AM!
          % We worship Thee."
          "Au Dieu qui règne en haut"
          "chantent tous les archanges:"
          "“À toi, grand Dieu, toi, trois fois saint,"
          "Roi tout-puissant;"
          "Comme au commencement,"
          "à jamais, maintenant,"
          "O Père, Seigneur, grand Je Suis,"
          "Nous te louons!”"
        }
      }
    }
    \hspace #0.1 % adds horizontal spacing between columns;
    \column {
      \line { \bold "6 "
        \column {
          % The whole triumphant host
          % Give thanks to God on high;
          % "Hail, Father, Son, and Holy Ghost,"
          % They ever cry:
          % Hail, Abraham's God and mine!
          % I join the heavenly lays;
          % All might and majesty are Thine,
          % And endless praise.
          "Le corps des anges uni"
          "remercie leur Dieu:"
          "“Au Père, Fils, et Saint-Esprit,”"
          "dans chaque lieu."
          "Il est Dieu, saint et fort,"
          "d'Abraham et de moi;"
          "je loue donc pour tous mes jours"
          "ce Dieu mon Roi."
        }
      }
    }
  \hspace #0.1 % gives some extra space on the right margin;
  % can be removed if page space is tight
  }
}

