\include "../common_liturgy.ly"

global = {
  \key g \major
  \time 3/4
  \autoBeamOff
  \override Staff.TimeSignature #'stencil = ##f
}

\layout {
  \context {
    \Score
    \omit BarNumber
  }
}

melody = \relative c'' {
  \global
  g4 g d' |
  b4. a8 g4 |
  fis e d |
  e fis g |
  a2. |
  g2. | \break

  g4 g d' |
  b4. a8 g4 |
  fis e d |
  e fis g |
  a2. |
  g2. | \break

  d'4 d d |
  e2. 

  b4 c d |
  d c b |
  a2. |  \break

  d,4 e fis |
  g a b |
  a2. |
  g2. \bar "|."
} 

alto = \relative c' {
  \global
  d4 e d |
  d4. fis8 e4 |
  d c b |
  c c b |
  e( d c)
  b2. |

  d4 e d |
  d4. fis8 e4 |
  d c b |
  c c b |
  e( d c)
  b2. |

  g'4 a g |
  g2. |

  g4 g a |
  g e8[ fis] g4 |
  fis2. |

  d4 c c |
  b e dis |
  e2( d!4) |
  b2. 
}

tenor = \relative c' {
  \global
  b4 b a |
  d4. d8 b4 |
  b g g |
  g a g |
  g( fis2) |
  g2. |

  b4 b a |
  d4. d8 b4 |
  b g g |
  g a g |
  g( fis2) |
  g2. |

  b4 a b |
  c2. |

  d4 c a |
  b c d |
  d2. |

  g,4 g a |
  g e fis |
  e( a8[ g] fis4) |
  g2.
}

bass = \relative c' {
  \global
  g4 e fis |
  g4. d8 e4 |
  b c g |
  c a e' |
  c( d2) |
  g,2. |

  g'4 e fis |
  g4. d8 e4 |
  b c g |
  c a e' |
  c( d2) |
  g,2. |

  g'4 fis g |
  c,2. |

  g'4 e fis |
  g a b8[ g] |
  d2. |

  b4 c a |
  e' c b |
  c2( d4) |
  g,2.
}

verseOne = \lyricmode {
  % \set ignoreMelismata = ##t
  \set stanza = "1 "
  % Praise to the Lord! the Almighty, the King of creation!
  % O my soul, praise Him, for He is thy health and salvation!
  % All ye who hear,
  % Now to His temple draw near,
  % Join me in glad adoration!
  \l Lou -- ez le Dieu puis -- sant, vrai Sei -- gneur, vrai Roi du mon -- de!
  \l C'est Dieu qui fit le ciel ra -- di -- eux, la terre et l'on -- de.
  \l Or -- gues, chan -- tez!
  \l Cors et trom -- pet -- tes, son -- nez:
  \l Et que les an -- ges ré -- pon -- dent!
}

verseTwo = \lyricmode {
  \set ignoreMelismata = ##t
  \set stanza = "2 "
  % Praise to the Lord! who o'er all things so wondrously reigneth,
  % Shelters thee under His wings, yea so gently sustaineth;
  % Hast thou not seen
  % How thy desires have been
  % Granted in what He ordaineth?
  \l Lou -- ez le Dieu qui sur tou -- te la cré -- a -- tion rè -- gne!
  \l Qu'il nous cache à l'om -- bre de ses ailes et nous sou -- tien -- ne!
  \l N'as -tu pas vu
  \l que tes dé -- sirs sont ren -- dus
  \l com -- plé -- tés s'il in -- ter -- vien -- ne?
}

verseThree = \lyricmode {
  \set ignoreMelismata = ##t
  \set stanza = "3 "
  % Praise to the Lord! who doth prosper thy work and defend thee,
  % Surely His goodness and mercy here daily attend thee;
  % Ponder anew
  % What the Almighty can do,
  % If with His love He befriend thee!
  \l Lou -- ez le Dieu qui vient nous bé -- nir et nous dé -- fen -- dre,
  \l qui fait sur nous ses dons, ses fa -- veur sans fin de -- scen -- dre!
  \l Et son -- gez tous
  \l à ce que Dieu fait pour vous!
  \l Sourds qui pas -- sez sans l'en -- ten -- dre!
}

verseFour = \lyricmode {
  \set ignoreMelismata = ##t
  \set stanza = "4 "
  % Praise to the Lord! Oh let all that is in me adore Him!
  % All that hath life and breath, come now with praises before Him!
  % Let the Amen
  % Sound from His people again,
  % Gladly for aye we adore Him!
  \l Hon -- neur et gloire à toi, Roi du Ciel; le ciel t'a -- do -- re!
  \l Et l'u -- ni -- vers en -- tier, pro -- ster -- né, t'ac -- clame en -- co -- re!
  \l L'a -- stre qui luit
  \l chan -- te ton Nom à la nuit;
  \l la nuit le dit à l'au -- ro -- re!
}


\score {
  <<
    \new Staff  <<
      \new Voice = "soprano" { \voiceOne \melody }
      \new Voice = "alto" { \voiceTwo \alto }
    >>
    \new Lyrics \with{\lyricspace} \lyricsto soprano \verseOne
    \new Lyrics  \lyricsto soprano \verseTwo
    \new Lyrics  \lyricsto soprano \verseThree
    \new Lyrics  \lyricsto soprano \verseFour
    \new Staff  <<
      \clef bass
      \new Voice = "tenor" { \voiceOne \tenor }
      \new Voice = "bass" { \voiceTwo \bass }
    >>
  >>
}
