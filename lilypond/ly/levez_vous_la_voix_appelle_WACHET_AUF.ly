\include "../common_liturgy.ly"

global = {
  \key d \major
  \time 4/4
  \autoBeamOff
  \set Staff.midiInstrument = "church organ"
}

\layout {
  \context {
    \Score
    \omit BarNumber
  }
}

melody = \relative c' {
  \global
  d4 fis a a |
  a a b2 |
  a2. \bar "||"

  a4 |
  d a d8[ e] fis4 \break
  e d cis( b) |
  a2. \bar "||"

  a4 |
  d a b fis |
  g8[ fis] e4 d2 \bar "||"

  %%repeat
  d4 fis a a |
  a a b2 |
  a2. \bar "||"

  a4 |
  d a d8[ e] fis4 |
  e d cis( b) |
  a2. \bar "||"

  a4 |
  d a b fis |
  g8[ fis] e4 d \bar "||" \break

  %%b section
  a'4 |
  a4. g8 fis4 e |
  d2. \bar "||"

  a'4 |
  a4. g8 fis4 e |
  d2 \bar "||" \break

  e4 fis |
  g2 fis4 \bar "||"

  a4 |
  b cis d d8[ e] |
  fis4 e d \bar "||" \break

  a4 |
  d a b fis |
  g8[ fis] e4 d2 \bar "|."
}

alto = \relative c' {
  \global
  a4 d fis e |
  fis e fis( e) |
  e2.

  e4 |
  a4. g8 fis8[ g] a4 |
  a b b8( a4 gis8) |
  e2.

  fis4 |
  g fis d d |
  d cis a2 \bar "||"

  %%repeat
  a4 d fis e |
  fis e fis( e) |
  e2.

  e4 |
  a4. g8 fis8[ g] a4 |
  a b b8( a4 gis8) |
  e2.

  fis4 |
  g fis d d |
  d cis a \bar "||"

  %% b section
  e'4 |
  fis4. e8 a,[ b] cis4 |
  b2.

  cis8[ d] |
  e4 e e8( d4 cis8) |
  a2

  cis4 d |
  d( cis) d

  fis |
  fis8[ e] e[ fis] fis4 fis8[ g] |
  a4. g8 fis4

  fis8[ g] |
  a[ g] a[ fis] g4 d |
  d cis a2 \bar "|."
}

tenor = \relative c {
  \global
  fis4 a d cis |
  d a a( gis) |
  cis2.

  cis4 |
  d d a d |
  e e e4.( b8) |
  cis2.

  d4 |
  d d8[ c] b4 b |
  b8[ a] g4 fis2 |

  %% repeat
  fis4 a d cis |
  d a a( gis) |
  cis2.

  cis4 |
  d d a d |
  e e e4.( b8) |
  cis2.

  d4 |
  d d8[ c] b4 b |
  b8[ a] g4 fis

  %% b section
  a4 |
  d8[ cis] b[ cis] d4 a8[ g] |
  fis2.

  a4 |
  a8[ b] cis4 a8( b4 a8) |
  fis2

  a4 a |
  b( g) a

  d4 |
  d cis b d |
  d cis d

  d |
  a d d4. b8 |
  b[ a] g4 fis2 \bar "|."
}

bass = \relative c {
  \global
  d4 d d8[ fis] a[ g] |
  fis[ e] d[ cis] d[ b] e4 |
  a,2.

  a'8[ g] |
  fis[ g] fis[ e] d4 d'4 |
  cis8[ b] a[ gis] a4( e) |
  a,2.

  d8[ c] |
  b[ cis!] d4 g8[ a] b4 |
  e, a, d2 |

  %% repeat
  d4 d d8[ fis] a[ g] |
  fis[ e] d[ cis] d[ b] e4 |
  a,2.

  a'8[ g] |
  fis[ g] fis[ e] d4 d'4 |
  cis8[ b] a[ gis] a4( e) |
  a,2.

  d8[ c] |
  b[ cis!] d4 g8[ a] b4 |
  e, a, d

  %% b section
  cis4 |
  d e fis8[ g] a[ a,] |
  b2.

  fis'4 |
  cis8[ b] a4 d8[ b] g[ a] |
  d2

  a'8[ g] fis4 |
  e2 d4

  d'4 |
  gis, ais b b |
  fis8[ g] a4 b,

  d8[ e] |
  fis[ e] fis[ d] g,[ a] b4 |
  e a, d2 \bar "|."
}

verseOne = \lyricmode {
  \set stanza = "1"
  \l “Le -- vez -vous!” la voix ap -- pel -- le; 
  \l les veil -- leurs crient des cit -- a -- del -- les:
  \l “Jé -- ru -- sa -- lem, ouv -- re les yeux.”
  % Jerusalem, reveillez vous
  % Jerusalem se leve
  % Reveillez vous jerusalem
  \l À mi -- nuit, l'heure é -- cla -- tan -- te,
  \l el -- les ap -- pellent, ces voix bril -- lan -- tes;
  % Les filles sages, elles sont ou?
  % Ou sont les filles sages?
  \l où sont les fil -- les sa -- _ ges?
  \l “Le fi -- an -- cé, il vient:
  \l ô, pré -- pa -- rez vos lampes.”
  \l Al -- lé -- lu -- ia!
  \l “De -- bout, de -- bout, pré -- pa -- rez -vous
  \l de le voir de -- ve -- nir É -- poux.”
}

verseTwo = \lyricmode {
  \set stanza = "2"
  % \l Zi -- on hears the watch -- men sing -- ing,
  % \l And all her heart with joy is spring -- ing,
  % \l She wakes, she ris -- es from her gloom;
  % \l For her Lord comes down all -- glo -- rious,
  % \l The strong in grace, in truth vic -- to -- rious,
  % \l Her Star is ris'n, her Light is come!
  % \l Ah come, Thou bles -- sed One,
  % \l God's own Be -- lov -- èd Son,
  % \l Hal -- le -- lu -- jah!
  % \l We fol -- low till the halls we see
  % \l Where Thou hast bid us sup with Thee!

  \l Elle en -- tend les veil -- leurs chan -- ter;
  \l l'â -- me de Si -- on est en -- chan -- tée;
  \l sa joie la fait par -- tir en hâte.
  \l Son a -- mant de -- scend en splen -- deur,
  \l la grâce et vé -- ri -- té sa gran -- deur;
  \l cet As -- tre ma -- tin -- al  é -- pate.
  \l Viens, cou -- ron -- ne des cieux,
  \l ô Jé -- sus, Fils de Dieu!
  \l Ho -- si -- an -- na!
  \set ignoreMelismata = ##t
  \l On est me -- né par sa cha -- ri -- té
  \l à cé -- lé -- brer son pro -- pre ban -- quet.

}

verseThree = \lyricmode {
  \set stanza = "3"
  % \l Now let all the heav'ns a -- dore Thee,
  % \l And men and an -- gels sing be -- fore Thee
  % \l With harp and cym -- bal's clear -- est tone;
  % \l Of one pearl each shin -- ing por -- tal,
  % \l Where we are with the choir im -- mor -- tal
  % \l Of an -- gels round Thy dazz -- ling throne;
  % \l Nor eye hath seen, nor ear
  % \l Hath yet at -- tain'd to hear
  % \l What there is ours,
  % \l But we re -- joice, and sing to Thee
  % \l Our hymn of joy e -- ter -- nal -- ly.

  \l Nous chan -- tons tous ses lou -- an -- ges,
  \l en nos langues et cel -- les des an -- ges,
  \l sur harpe et cym -- ba -- le bien -- tôt.
  \l Dou -- ze per -- les font les por -- tes;
  \set ignoreMelismata = ##t
  \l chez toi, nous som -- mes la rei -- ne con -- _ sort,
  \l les an -- ges à ton trô -- ne très haut.
  \l Ja -- mais ni l'œil n'a vu, 
  \l ni l'o -- reille en -- ten -- du
  \l un tel bon -- heur
  \l que no -- tre joie: l’É -- ter -- nel, le Roi,
  \l que nos lou -- an -- ges soient _ à toi.

}



\score {
  % \new ChoirStaff <<
  <<
    \new Staff  <<
      \new Voice = "soprano" { \voiceOne \melody }
      \new Voice = "alto" { \voiceTwo \alto }
    >>
    \new Lyrics \with{\lyricspace} \lyricsto soprano \verseOne
    \new Lyrics  \lyricsto soprano \verseTwo
    \new Lyrics  \lyricsto soprano \verseThree
    \new Staff  <<
      \clef bass
      \new Voice = "tenor" { \voiceOne \tenor }
      \new Voice = "bass" { \voiceTwo \bass }
    >>
  >>
  % \midi {
  %   \context {
  %     \Score
  %     tempoWholesPerMinute = #(ly:make-moment 60 4)
  %   }
  % }
}

