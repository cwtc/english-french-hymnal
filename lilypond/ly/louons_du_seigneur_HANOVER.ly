\include "../common_liturgy.ly"

global = {
  \key g \major
  \time 3/4
  \autoBeamOff
  \override Staff.TimeSignature #'stencil = ##f
}

\layout {
  \context {
    \Score
    \omit BarNumber
  }
}

melody = \relative c' {
  \global
  \partial 4
  d4 |
  g g a |
  b2 d4 |
  g, a fis |
  g2 \bar "|" \break

  a4 |
  b a g |
  fis2 g4 |
  a g8[ fis] e4 |
  d2 \bar "|" \break

  fis4 |
  g a b |
  g e c' |
  b a g |
  d'2 \bar "|" \break

  d,4 |
  e fis g |
  a d, b' |
  e, a fis |
  g2 \bar "|."
} 

alto = \relative c' {
  \global
  \partial 4
  b4 |
  b d d |
  d2 d4 |
  e e d |
  d2

  d4 |
  d d d8[ e] |
  a,2 b4 |
  d e8[ d] cis4 |
  d2

  dis4 |
  e e dis |
  b b d |
  d d g |
  fis2

  d4 |
  d c d |
  d b d |
  c e d |
  d2
}

tenor = \relative c' {
  \global
 \partial 4
  g4 |
  g g fis |
  g2 a4 |
  b c a |
  b2

  fis4 |
  g a b8[ cis] |
  d2 d4 |
  a b e, |
  fis2

  b4 |
  b e, fis |
  g g a |
  g d'4. cis8 |
  d2

  g,4 |
  g c b |
  a fis g |
  g c a |
  b2 
}

bass = \relative c {
  \global
  \partial 4
  g4 |
  g b d |
  g2 fis4 |
  e c d |
  g,2

  d'4 |
  g fis e |
  d2 b4 |
  fis g a |
  d2

  b4 |
  e c b |
  e e fis |
  g fis e |
  d2

  b4 |
  c a g |
  fis b g |
  c a d |
  g,2
}

verseOne = \lyricmode {
  % \set ignoreMelismata = ##t
  \set stanza = "1 "
  % O worship the King, all-glorious above,
  % And gratefully sing His wonderful love;
  % Our Shield and Defender, the Ancient of Days,
  % Pavilioned in splendor and girded with praise.

  \l Lou -- ons du Sei -- gneur le Nom glo -- ri -- eux,
  \l et pour son a -- mour ren -- dons grâce à Dieu;
  \l Il est mon re -- fu -- ge, mon seul dé -- fen -- seur,
  \l et dans les cieux rè -- gne, nim -- bé de splen -- deur.
}

verseTwo = \lyricmode {
  % \set ignoreMelismata = ##t
  \set stanza = "2 "
  % O tell of His might and sing of His grace,
  % Whose robe is the light, whose canopy space;
  % His chariots of wrath the deep thunder-clouds form,
  % And dark is His path on the wings of the storm.

  \l Van -- tons son pou -- voir; chan -- tons sa bon -- té;
  \l il vit dans les cieux, vê -- tu de beau -- té.
  \l L'ou -- ra -- gan fait ra -- ge; tout est dé -- chaî -- né,
  \l et l'o -- ra -- ge gron -- de, s'il est ir -- ri -- té.
}

verseThree = \lyricmode {
  % \set ignoreMelismata = ##t
  \set stanza = "3 "
  % The earth with its store of wonders untold,
  % Almighty, Thy power hath founded of old:
  % Hast stablished it fast by a changeless decree,
  % And round it hast cast like a mantle the sea.
  
  \l Le Dieu tout puis -- sant cré -- a l'u -- ni -- vers,
  \l puis il l'en -- tou -- ra du man -- teau des mers.
  \l Il ré -- git le mon -- de; tout lui o -- bé -- it;
  \l ses lois im -- mu -- a -- bles règ -- lent l'in -- fi -- ni.
}

verseFour = \lyricmode {
  % \set ignoreMelismata = ##t
  \set stanza = "4 "
  % Thy bountiful care what tongue can recite?
  % It breathes in the air, it shines in the light,
  % It streams from the hills, it descends to the plain
  % And sweetly distills in the dew and the rain.

  \l Qui chan -- te -- ra donc sa gran -- de bon -- té?
  \l Elle im -- prè -- gne l'air, luit dans la clar -- té,
  \l et dans les mon -- ta -- gnes, dans tous les val -- lons,
  \l s'ex -- hale en -- ro -- sé -- e, nous com -- ble de dons.
}

verseFive = \lyricmode {
  % \set ignoreMelismata = ##t
  \set stanza = "5 "
  % Frail children of dust, and feeble as frail,
  % In Thee do we trust, nor find Thee to fail;
  % Thy mercies how tender! how firm to the end
  % Our Maker, Defender, Redeemer, and Friend.
  \l Fait de la pous -- sière, et fai -- bles aus -- si,
  \l à ton fer -- me sou -- tien on se con -- fie;
  \l à ta mi -- sé -- ri -- cor -- de, toi, notre ap -- pui!
  \l Cré -- a -- teur, Dé -- fen -- deur, Ré -- demp -- teur, A -- mi.
}

\score {
  <<
    \new Staff  <<
      \new Voice = "soprano" { \voiceOne \melody }
      \new Voice = "alto" { \voiceTwo \alto }
    >>
    \new Lyrics \with{\lyricspace} \lyricsto soprano \verseOne
    \new Lyrics  \lyricsto soprano \verseTwo
    \new Lyrics  \lyricsto soprano \verseThree
    \new Lyrics  \lyricsto soprano \verseFour
    \new Lyrics  \lyricsto soprano \verseFive
    \new Staff  <<
      \clef bass
      \new Voice = "tenor" { \voiceOne \tenor }
      \new Voice = "bass" { \voiceTwo \bass }
    >>
  >>
}

