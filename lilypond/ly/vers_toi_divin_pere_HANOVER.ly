\include "../common_liturgy.ly"

% http://www.hymntime.com/tch/non/fr/v/t/o/i/vtoidper.htm

global = {
  \key g \major
  \time 3/4
  \autoBeamOff
  \override Staff.TimeSignature #'stencil = ##f
}

\layout {
  \context {
    \Score
    \omit BarNumber
  }
}

melody = \relative c' {
  \global
  \partial 4
  d4 |
  g g a |
  b4 b4 d4 |
  g, a fis |
  g2 \bar "|" \break

  a4 |
  b a g |
  fis4 fis4 g4 |
  a g8[ fis] e4 |
  d2 \bar "|" \break

  fis4 |
  g a b |
  g e c' |
  b a g |
  d'2 \bar "|" \break

  d,4 |
  e fis g |
  a d, b' |
  e, a fis |
  g2 \bar "|."
} 

alto = \relative c' {
  \global
  \partial 4
  b4 |
  b d d |
  d4 d4 d4 |
  e e d |
  d2

  d4 |
  d d d8[ e] |
  a,4 a4 b4 |
  d e8[ d] cis4 |
  d2

  dis4 |
  e e dis |
  b b d |
  d d g |
  fis2

  d4 |
  d c d |
  d b d |
  c e d |
  d2
}

tenor = \relative c' {
  \global
 \partial 4
  g4 |
  g g fis |
  g4 g4 a4 |
  b c a |
  b2

  fis4 |
  g a b8[ cis] |
  d4 d4 d4 |
  a b e, |
  fis2

  b4 |
  b e, fis |
  g g a |
  g d'4. cis8 |
  d2

  g,4 |
  g c b |
  a fis g |
  g c a |
  b2 
}

bass = \relative c {
  \global
  \partial 4
  g4 |
  g b d |
  g4 g4 fis4 |
  e c d |
  g,2

  d'4 |
  g fis e |
  d4 d4 b4 |
  fis g a |
  d2

  b4 |
  e c b |
  e e fis |
  g fis e |
  d2

  b4 |
  c a g |
  fis b g |
  c a d |
  g,2
}

verseOne = \lyricmode {
  % \set ignoreMelismata = ##t
  \set stanza = "1 "
  % O worship the King, all-glorious above,
  % And gratefully sing His wonderful love;
  % Our Shield and Defender, the Ancient of Days,
  % Pavilioned in splendor and girded with praise.

  \l Vers Toi, di -- vin Pè -- re, s'é -- lè -- vent mes yeux:
  \l en -- tends ma pri -- è -- re; ex -- au -- ce mes voeux;
  \l Du fond de la ter -- re, mon cœur mal -- heu -- reux
  \l t'in -- voque, ô lu -- miè -- re, puis -- sant Roi des Cieux!
}

verseTwo = \lyricmode {
  % \set ignoreMelismata = ##t
  \set stanza = "2 "
  % O tell of His might and sing of His grace,
  % Whose robe is the light, whose canopy space;
  % His chariots of wrath the deep thunder-clouds form,
  % And dark is His path on the wings of the storm.

  \l Mon âme est jo -- yeu -- se, Jé -- sus, près de toi:
  \l ma vie est heu -- reu -- se sou -- mise à ta Loi.
  \l Du monde ou -- bli -- eu -- se, fi -- dèle à sa foi,
  \l mon âme a -- mou -- reu -- se t'ac -- clame, ô mon Roi. 
}

verseThree = \lyricmode {
  % \set ignoreMelismata = ##t
  \set stanza = "3 "
  % The earth with its store of wonders untold,
  % Almighty, Thy power hath founded of old:
  % Hast stablished it fast by a changeless decree,
  % And round it hast cast like a mantle the sea.
  
  \l Ta main re -- dou -- ta -- ble, ter -- rible au pé -- cheur,
  \l me fut se -- cou -- ra -- ble, rem -- part pro -- tec -- teur.
  \l A -- mour in -- ef -- fa -- ble! Ce ten -- dre Pas -- teur
  \l in -- vite à sa Ta -- ble l'en -- fant vo -- ya -- geur.
}

verseFour = \lyricmode {
  % \set ignoreMelismata = ##t
  \set stanza = "4 "
  % Thy bountiful care what tongue can recite?
  % It breathes in the air, it shines in the light,
  % It streams from the hills, it descends to the plain
  % And sweetly distills in the dew and the rain.

  \l La ferme as -- su -- ran -- ce d'un cœur pa -- ter -- nel,
  \l a -- vec ta puis -- san -- ce, ras -- sure un mor -- tel;
  \l J'ai douce es -- pé -- ran -- ce de voir dans le ciel
  \l ta chè -- re pré -- sen -- ce, ô Christ é -- ter -- nel.
}

verseFive = \lyricmode {
  % \set ignoreMelismata = ##t
  \set stanza = "5 "
  \l Je dis à l'au -- ro -- re ton im -- men -- si -- té;
  \l sans ces -- se j'a -- do -- re, Sei -- gneur, ta beau -- té;
  \l le soir vient; j'im -- plo -- re ta dou -- ce bon -- té:
  \l Ma voix chante en -- co -- re ton é -- ter -- ni -- té.
}

\score {
  <<
    \new Staff  <<
      \new Voice = "soprano" { \voiceOne \melody }
      \new Voice = "alto" { \voiceTwo \alto }
    >>
    \new Lyrics \with{\lyricspace} \lyricsto soprano \verseOne
    \new Lyrics  \lyricsto soprano \verseTwo
    \new Lyrics  \lyricsto soprano \verseThree
    \new Lyrics  \lyricsto soprano \verseFour
    \new Lyrics  \lyricsto soprano \verseFive
    \new Staff  <<
      \clef bass
      \new Voice = "tenor" { \voiceOne \tenor }
      \new Voice = "bass" { \voiceTwo \bass }
    >>
  >>
}

