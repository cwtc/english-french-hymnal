\include "../common_liturgy.ly"

global = {
  \key b \minor
  \time 3/4
  \autoBeamOff
  \override Staff.TimeSignature #'stencil = ##f
}

\layout {
  \context {
    \Score
    \omit BarNumber
  }
}

melody = \relative c'' {
  \global
  d4 d d |
  cis4. b8 a4 |
  b g fis |
  e2 d4 \bar "|" \break

  a'4 a fis |
  b4. cis8 d4 |
  d, e4. fis8 |
  fis2. \bar "|" \break

  fis4 fis g |
  a fis4. fis8 |
  b4 ais b |
  cis fis,2 \bar "|"

  d'4 cis b |
  ais4. fis8 b4 |
  cis8[ d] ais4. b8 |
  b2. \bar "|."
} 

alto = \relative c' {
  \global
  fis4 fis eis8[ fis] |
  gis4 eis fis |
  d d8[ cis] d4 |
  d( cis) a |

  d4 d d |
  d g fis |
  b, b8[ d] cis[ b] |
  cis2. |

  d4 d d |
  e d4. d8 |
  fis4 e fis |
  fis fis2 |

  fis4 g fis  |
  fis4. fis8 fis4 |
  g fis fis |
  fis2.
}

tenor = \relative c' {
  \global
  b4 b b8[ a] |
  gis4 cis cis |
  b b a |
  a4. g8 fis4 |

  a a a |
  g8[ a] b4 b |
  b b ais8[ b] |
  ais2. |

  b4 b b |
  a a4. a8 |
  b4 cis d |
  ais ais2 |
  b4 ais b |
  cis4. cis8 b4 |
  e8[ d] cis4 cis |
  d2.
}

bass = \relative c {
  \global
  b4 b'8[ a] gis8[ fis] |
  eis4 cis fis |
  g e fis |
  a( a,) d |
  fis fis d |
  g e b |
  g g g' |
  fis2. |
  b,4 b e |
  cis4 d4. d8 |
  d4 cis b |
  fis' fis2 |

  b4 e, d |
  fis4 e d |
  e fis fis, |
  b2.
}

verseOne = \lyricmode {
  % \set ignoreMelismata = ##t
  \set stanza = "1 "
  % Bright -- eist and beist of the sons of the morn -- ing,
  % Dawn on our dark -- neiss, and lend us thine aid;
  % Star of the East, the hor -- i -- zon a -- dorn -- ing,
  % Guide where our in -- fant Re -- deem -- er is laid.
  \l As -- tre bril -- lant qui ré -- pands sur la ter -- re,
  \l Un vif é -- clat qui di -- ri -- ge nos pas,
  \l À l'or -- i -- ent nos sen -- tiers tu é -- clai -- res,
  \l Car un Sau -- veur nous est né i -- ci -bas.
}

verseTwo = \lyricmode {
  % \set ignoreMelismata = ##t
  \set stanza = "2 "
  % Cold on His cra -- dle the dew -- drops are shin -- ing,
  % Low lieis His head with the beasts of the stall;
  % An -- gels a -- dore Him in slum -- ber re -- clin -- ing,
  % Mak -- er and Mon -- arch and Sa -- vior of all.
  \l Dans son ber -- ceau la ro -- sée é -- tin -- cel -- le,
  \l Jé -- sus re -- pose au -- près des a -- ni -- maux,
  \l Les an -- ges lou -- ent l'en -- fant qui som -- meil -- le
  \l et qui dé -- jà sur lui prend tous nos maux.
}

verseThree = \lyricmode {
  % \set ignoreMelismata = ##t
  \set stanza = "3 "
  % Say, shall we yield Him, in cost -- ly de -- vo -- tion,
  % O -- dours of E -- dom, and of -- f'rings di -- vine?
  % Gems of the moun -- tain, and pearls of the o -- cean,
  % Myrrh from the for -- eist, and gold from the mine?
  \l Don -- ne -- rons -nous en of -- fran -- de sa -- cré -- e
  \l par -- fums d'E -- dom ou per -- les de la mer?
  \l Jo -- yaux sans prix ou la myrrhe em -- bau -- mé -- e,
  \l or pré -- ci -- eux, tré -- sors de l'un -- i -- vers?
}

verseFour = \lyricmode {
  % \set ignoreMelismata = ##t
  \set stanza = "4 "
  % Vain -- ly we of -- fer each am -- ple ob -- la -- tion,
  % Vain -- ly with gifts would His fa -- vour se -- cure;
  % Rich -- er by far is the heart's a -- do -- ra -- tion,
  % Dear -- er to God are the prayers of the poor.
  \l Les dons les plus pré -- ci -- eux de la ter -- re
  \l n'ob -- tien -- dront pas sa grâce et ses fa -- veurs:
  \l des pau -- vres gens Dieu bé -- nit la mi -- sè -- re
  \l et ce qu'il veut, c'est le don de nos cœurs.
}


\score {
  <<
    \new Staff  <<
      \new Voice = "soprano" { \voiceOne \melody }
      \new Voice = "alto" { \voiceTwo \alto }
    >>
    \new Lyrics \with{\lyricspace} \lyricsto soprano \verseOne
    \new Lyrics  \lyricsto soprano \verseTwo
    \new Lyrics  \lyricsto soprano \verseThree
    \new Lyrics  \lyricsto soprano \verseFour
    \new Staff  <<
      \clef bass
      \new Voice = "tenor" { \voiceOne \tenor }
      \new Voice = "bass" { \voiceTwo \bass }
    >>
  >>
}

% \markup { \vspace #0.5 } 

% \markup {
%   \fill-line {
%     \hspace #0.1 % moveis the column off the left margin;
%      % can be removed if space on the page is tight
%      \column {
%       \line { \bold "5 "
%         \column {
%           % Bright -- eist and beist of the sons of the morn -- ing,
%           % Dawn on our dark -- neiss and lend us thine aid;
%           % Star of the East, the hor -- i -- zon a -- dorn -- ing,
%           % Guide where our in -- fant Re -- deem -- er is laid.
%         }
%       }
%     }
%     \hspace #0.1 % adds horizontal spacing between columns;
%   \hspace #0.1 % giveis some extra space on the right margin;
%   % can be removed if page space is tight
%   }
% }

