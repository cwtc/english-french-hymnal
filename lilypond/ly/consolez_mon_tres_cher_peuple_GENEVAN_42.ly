\include "../common_liturgy.ly"

global = {
  \key f \major
  \cadenzaOn
  \autoBeamOff
  \override Staff.TimeSignature #'stencil = ##f
}

\layout {
  \context {
    \Score
    \omit BarNumber
  }
}

melody = \relative c' {
  \global
  f2 g4 a2 g4 \bar "|"
  f e d2 c \bar "|"
  f2 g4 a2 bes4 \bar "|"
  a2 g2 f1 \bar "|" \break

  f2 g4 a2 g4 \bar "|"
  f e d2 c \bar "|"
  f2 g4 a2 bes4 \bar "|"
  a2 g2 f1 \bar "|" \break

  a2 a4 c2 bes4 \bar "|"
  a g a1 \bar "|"
  c2 c4 d2 c4 \bar "|"
  bes4 a g1 \bar "|" \break

  a2 c4 bes2 a4 \bar "|"
  f4 g a2 f \bar "|"
  a2 a4 bes2 a4 \bar "|"
  g f e2 f2 \bar "|."
} 

alto = \relative c' {
  \global
  c2 e4 f2 e4
  d c bes2 a
  c2 e4 f2 f4
  f2 e c1

  c2 e4 f2 e4
  d c bes2 a
  c e4 f2 f4
  f2 e c1

  f2 f4 f2 f4
  f e f1
  f2 f4 f2 f4
  f f e1

  a2 g4 g2 f4
  d c c2 d
  c2 f4 f2 f4
  e a, c2 c2
}

tenor = \relative c' {
  \global
  a2 c4 c2 c4
  a a f2 f
  a2 c4 c2 d4
  c2 c a1

  a2 c4 c2 c4
  a a f2 f
  a2 c4 c2 d4
  c2 c a1

  c2 c4 c2 d4
  c c c1

  a2 a4 bes2 c4
  d c c1
  c2 ees4 d2 d4
  a g f2 f

  f c'4 d2 c4
  c f, g2 a
}

bass = \relative c {
  \global
  f2 c4 f2 c4
  d a bes2 f
  f'2 c4 f2 bes,4
  f'2 c f,1

  f'2 c4 f2 c4
  d a bes2 f
  f' c4 f2 bes,4
  f'2 c f,1

  f'2 f4 a,2 bes4
  f' c f1
  f2 f4 bes,2 a4
  bes4 f' c1

  f2 c4 g'2 d4
  d e f2 bes,
  f'2 f4 bes,2 f'4
  c d c2 f
}

verseOne = \lyricmode {
  % \set ignoreMelismata = ##t
  \set stanza = "1 "
  % Comfort, comfort ye my people,
  % Speak ye peace, thus saith our God;
  % Comfort those who sit in darkness,
  % Mourning 'neath their sorrow’s load;
  % Speak ye to Jerusalem
  % Of the peace that waits for them;
  % Tell her that her sins I cover,
  % And her warfare now is over.
  \l “Con -- so -- lez mon très cher peu -- ple,
  \l con -- so -- lez,” dit no -- tre Dieu;
  \l “Con -- so -- lez ceux aux té -- nè -- bres
  \l sous leur far -- deau en tout lieu;
  \l A Jé -- ru -- sa -- lem par -- lez:
  \l elle au -- ra en -- fin sa paix;
  \l cha -- que pé -- ché je vais cou -- vrir,
  \l et sa guer -- re je vais fi -- nir.”
}

verseTwo = \lyricmode {
  % \set ignoreMelismata = ##t
  \set stanza = "2 "
  % Yea, her sins our God will pardon,
  % Blotting out each dark misdeed;
  % That which well deserved His anger
  % He will not more see or heed.
  % She hath suffer'd many a day,
  % Now her griefs have passed away,
  % God will change her pining sadness
  % Unto ever springing gladness.
  \l Ses pé -- chés se -- ront par -- don -- né,
  \l cha -- cun se -- ra ef -- fa -- cé;
  \l ce qui mé -- ri -- te son i -- re
  \l ne se -- ra plus é -- cou -- té.
  \l Son cha -- grin qu'el -- le vé -- cut
  \l est main -- te -- nant dis -- pa -- ru;
  \l Dieu chan -- ge -- ra sa tris -- tes -- se
  \l en é -- ter -- nelle al -- lé -- gres -- se.
}

verseThree = \lyricmode {
  % \set ignoreMelismata = ##t
  \set stanza = "3 "
  % For Elijah's voice is crying
  % In the desert far and near,
  % Bidding all men to repentance,
  % Since the Kingdom now is here.
  % Oh, that warning cry obey,
  % Now prepare for God a way!
  % Let the valleys rise to meet Him,
  % And the hills bow down to greet Him.
  \l Car c'est la voix d'E -- sa -- ï -- e;
  \l le de -- sert en -- tend son cri:
  \l l'ap -- pel à la re -- pen -- ten -- ce,
  \l car le Roy -- aume est i -- ci.
  \l Ce con -- seil nous é -- cou -- tons;
  \l son che -- min nous pré -- pa -- rons:
  \l tou -- te val -- lée ex -- haus -- sé -- e,
  \l tou -- te col -- line a -- bais -- sé -- e.
}

verseFour = \lyricmode {
  % \set ignoreMelismata = ##t
  \set stanza = "4 "
  % Make ye straight what long was crooked,
  % Make the rougher places plain,
  % Let your hearts be true and humble,
  % As befits His holy reign;
  % For the glory of the Lord
  % Now o'er earth is shed abroad,
  % And all flesh shall see the token
  % That His Word is never broken. 
  \l Ap -- pla -- ti -- sez le tor -- tu -- eux;
  \l ren -- dez lis -- se le ru -- gueux;
  \l ren -- dez vos cœurs purs et hum -- bles,
  \l sé -- ant le rè -- gne de Dieu.
  \l Car la gloi -- re du Sei -- gneur
  \l est ré -- vé -- le aux pé -- cheurs:
  \l on voit de cet -- te pa -- ru -- re
  \l que cet -- te Pa -- ro -- le du -- re.
}


\score {
  <<
    \new Staff  <<
      \new Voice = "soprano" { \voiceOne \melody }
      \new Voice = "alto" { \voiceTwo \alto }
    >>
    \new Lyrics \with{\lyricspace} \lyricsto soprano \verseOne
    \new Lyrics  \lyricsto soprano \verseTwo
    \new Lyrics  \lyricsto soprano \verseThree
    \new Lyrics  \lyricsto soprano \verseFour
    \new Staff  <<
      \clef bass
      \new Voice = "tenor" { \voiceOne \tenor }
      \new Voice = "bass" { \voiceTwo \bass }
    >>
  >>
}
