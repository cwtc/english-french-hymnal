\include "../common_liturgy.ly"

global = {
  \key e \minor
  \time 6/8
  \autoBeamOff
  \override Staff.TimeSignature #'stencil = ##f
}

\layout {
  \context {
    \Score
    \omit BarNumber
  }
}

melody = \relative c' {
  \global
  \partial 8
  e8 |
  g4 a8 b8.[ c16] b8 |
  a4 fis8 d8.[ e16] fis8 |
  g4 e8 e8.[ dis16] e8 |
  fis4. b,4 \bar "|" \break
  e8 |
  g4 a8 b8.[ c16] b8 |
  a4 fis8 d8.[ e16] fis8 |
  g8.[ fis16] e8 dis8.[ cis16] dis8 |
  e4. e4. \bar "||" \break

  d'4.^\markup { \italic "Refrain" } d8.[ cis16] b8 |
  a4 fis8 d8.[ e16] fis8 |
  g4 e8 e8.[ dis16] e8 |
  fis4 dis8 b4. |
  d'4. d8.[ cis16] b8 |
  a4 fis8 d8.[ e16] fis8 |
  g8.[ fis16] e8 dis8.[ cis16] dis8 |
  e4. e4 \bar "|."
} 

alto = \relative c' {
  \global
  \partial 8
  \mergeDifferentlyDottedOn
  b8 |
  e4 d8 d4 g8 |
  fis4 d8 d4 c8 |
  b4 b8 a4 e'8 |
  dis4. b4 b8 |
  e4 d8 d4 g8 |
  fis4 d8 d4 c8 |
  b4 c8 b4 b8 |
  b4. b4.

  fis'4. b8.[ a16] g8 |
  fis4 d8 d4 c8 |
  b4 b8 a4 e'8 |
  dis4 b8 b4. |
  fis'4. b8.[ a16] g8 |
  fis4 d8 d4 c8 |
  b4 c8 b4 b8 |
  b4. b4
}

tenor = \relative c' {
  \global
  \partial 8
  g8 |
  b4 a8 g4 b8 |
  d4 a8 fis4 a8 |
  g4 g8 e4 e8 |
  b'4. b4 g8 |
  b4 a8 g4 b8 |
  d4 a8 fis4 a8 |
  g4 a8 fis4 fis8 |
  g4. g4.

  b4. d4 d8 |
  d4 a8 fis4 a8 |
  g4 g8 a4 e8 |
  b'4 fis8 b4. |
  b4. d4 d8 |
  d4 a8 fis4 a8 |
  g4 a8 fis4 fis8 |
  g4. g4
}

bass = \relative c {
  \global
  \partial 8
  e8 |
  e4 fis8 g4 g8 |
  d4 d8 d4 dis8 |
  e4 e8 c4 c8 |
  b4. b'4 e,8 |
  e4 fis8 g4 g8 |
  d4 d8 d4 dis8 |
  e4 a,8 b4 b8
  e4. e4.

  b'4. g4 g8 |
  d4 d8 d4 dis8 |
  e4 e8 c4 c8 |
  b4 b8 b4. |
  b'4. g4 g8 |
  d4 d8 d4 dis8 |
  e4 a,8 b4 b8 |
  e4. e4
}

verseOne = \lyricmode {
  % \set ignoreMelismata = ##t
  \set stanza = "1 "
  % What Child is this, who, laid to rest,
  % On Mary's lap is sleeping?
  % Whom angels greet with anthems sweet,
  % While shepherds watch are keeping?
  \l Qui est cet En -- fant en -- dor -- mi
  \l aux ge -- noux de sa Mè -- re?
  \l Chan -- tent les an -- ges ses lou -- anges
  \l aux ber -- gers sur la ter -- re.

  % This, this is Christ the King,
  % Whom shepherds guard and angels sing:
  % Haste, haste to bring Him laud,
  % The Babe, the Son of Mary.
  \l Voi -- ci le Roi, le Christ,
  \l qui les ber -- gers ont pré -- mu -- ni:
  \l Vite! A -- mè -- nez le los
  \l du cher Fils de Ma -- ri -- e. 
}

verseTwo = \lyricmode {
  % \set ignoreMelismata = ##t
  \set stanza = "2 "
  % Why lies He in such mean estate
  % Where ox and ass are feeding?
  % Good Christian, fear: for sinners here
  % The silent Word is pleading.
  \l Pour -- quoi dort -il si hum -- ble -- ment,
  \l en paix par -- mi les bê -- tes?
  \l Fin des ter -- reurs: pour nous pê -- cheurs
  \l le Ver -- be sans mot plai -- de.
}

verseThree = \lyricmode {
  % \set ignoreMelismata = ##t
  \set stanza = "3 "
  % So bring Him incense, gold, and myrrh,
  % Come, peasant, king to own Him;
  % The King of Kings salvation brings,
  % Let loving hearts enthrone Him.
  \l De l'or, de l'en -- cens, de la myrrhe:
  \l of -- frons nos choses ex -- quis -- ses;
  \l le Roi Jé -- sus rend le sa -- lut;
  \l que nos cœurs l'in -- tro -- ni -- se.
}

\score {
  <<
    \new Staff  <<
      \new Voice = "soprano" { \voiceOne \melody }
      \new Voice = "alto" { \voiceTwo \alto }
    >>
    \new Lyrics \with{\lyricspace} \lyricsto soprano \verseOne
    \new Lyrics  \lyricsto soprano \verseTwo
    \new Lyrics  \lyricsto soprano \verseThree
    \new Staff  <<
      \clef bass
      \new Voice = "tenor" { \voiceOne \tenor }
      \new Voice = "bass" { \voiceTwo \bass }
    >>
  >>
}
