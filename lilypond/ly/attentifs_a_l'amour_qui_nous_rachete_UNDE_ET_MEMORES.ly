\include "../common_liturgy.ly"

global = {
  \key d \major
  \time 4/4
  \autoBeamOff
  \override Staff.TimeSignature #'stencil = ##f
}

\layout {
  \context {
    \Score
    \omit BarNumber
  }
}

melody = \relative c' {
  \global
  \partial 4
  fis4 |
  fis4. d8 d4 e |
  fis4. fis8 fis4 g |
  a2. \bar "|" \break
  
    fis4 |
  e fis g4. g8 | 
  g4 fis a fis |
  e2. \bar "|" \break

  fis4 |
  fis4. d8 d4 e |
  fis4. fis8 fis4 g |
  a2. \bar "|" \break
    
    fis4 |
  b a gis4. gis8 |
  a4 b cis b |
  a2. \bar "|" \break

  a4 |
  d a g fis |
  b b d b | 
  a2. \bar "|"  \break 
  
    a4 | 
  g fis e d |
  b'4. g8 fis4 e | 
  d2. \bar "|."
} 

alto = \relative c' {
  \global
  d4 |
  d4. d8 d4 d |
  d4. d8 d4 cis |
  d2. d4 |
  cis d e4. e8 |
  e4 d d d |
  cis2.

  d4 |
  d4. d8 d4 d |
  d4. d8 d4 d |
  d2. d4 |
  d4 d d4. d8 |
  cis4 fis e d |
  cis2.

  cis4 |
  d d cis d |
  d4 d d g |
  fis2. d4 |
  cis d cis d |
  d4. e8 d4 cis |
  d2.
}

tenor = \relative c' {
  \global
  a4 |
  a4. fis8 fis4 g |
  a4. a8 a4 a |
  a2. a4 |
  a a b4. b8 |
  a4 a a a |
  a2.

  a4 |
  a4. fis8 fis4 g4 |
  a4. d,8 d4 e |
  fis2. a4 |
  g a b4. b8 |
  a4 a a gis |
  a2.

  a4 |
  a a a a |
  g g g d' |
  d2. a4 |
  a a g fis |
  g4. b8 a4 g |
  fis2.
}

bass = \relative c {
  \global
  \partial 4
  d4 |
  d4. d8 d4 d |
  d4. d8 d4 e |
  fis2. d4 |
  g fis e4. d8 |
  cis4 d fis,4. g8 |
  a2.

  d4 |
  d4. d8 d4 d |
  d4. b8 b4 b |
  fis2. d'4 |
  g fis e4. e8 |
  fis4 d e e |
  a,2.

  g'4 |
  fis4 fis e d |
  g, g b g |
  d'2. fis4 |
  e d a b |
  g4. g8 a4 a |
  d2.
}

verseOne = \lyricmode {
  \set ignoreMelismata = ##t
  \set stanza = "1 "
  % And now, O Father, mindful of the love
  % That bought us, once for all, on Calvary's Tree,
  % And having with us Him that pleads above,
  % We here present, we here spread forth to Thee,
  % That only Off'ring perfect in Thine eyes,
  % The one true, pure immortal Sacrifice.
  \l At -- ten -- tifs à l'a -- mour qui nous ra -- chète,
  \l ac -- com -- pli u -- ne fois sur le Cal -- vaire,
  \l et a -- yant i -- ci lui qui pour nous plaide,
  \l nous nous ca -- chons dans ton saint Fils, ô Père:
  \l Il s'of -- fra en sa -- cri -- fi -- ce com -- plet,
  \l im -- mor -- tel, pur, suf -- fi -- sant, et par -- fait.
}

verseTwo = \lyricmode {
  \set ignoreMelismata = ##t
  \set stanza = "2 "
  % Look, Father, look on His anointed face,
  % And only look on us as found in Him;
  % Look not on our misusings of Thy grace,
  % Our prayer so languid, and our faith so dim;
  % For lo! between our sins and their reward,
  % We set the Passion of Thy Son our Lord.
  \l Re -- garde, ô Pè -- re, son par -- fait vi -- sage;
  \l ne nous re -- gar -- de que com -- me ton Fils;
  \l I -- gno -- re nos i -- gno -- rances et nos rages,
  \l nos é -- checs et no -- tre foi af -- fai -- blie;
  \l Car en -- tre nos pé -- chés et leur sa -- laire,
  \l on met la Pas -- sion de no -- tre Sei -- gneur.
}

verseThree = \lyricmode {
  \set ignoreMelismata = ##t
  \set stanza = "3 "
  % And then for those, our dearest and our best,
  % By this prevailing presence we appeal;
  % O fold them closer to Thy mercy's breast!
  % O do Thine utmost for their souls' true weal!
  % From tainting mischief keep them white and clear,
  % And crown Thy gifts with strength to persevere.
  \l En -- sui -- te, pour tous ceux que l'on ché -- rit,
  \l nous lan -- çons ap -- pel par cet -- te Pré -- sence;
  \l Ra -- mène -les plus proche à ton sein bé -- ni!
  \l Ô gar -- de leurs â -- mes de la souf -- france!
  \l Qu'ils soi -- ent la -- vés, blancs com -- me la neige,
  \l qu'ils soi -- ent pro -- té -- gés de cha -- que piège.
}

verseFour = \lyricmode {
  \set ignoreMelismata = ##t
  \set stanza = "4 "
  % And so we come; O draw us to Thy feet,
  % Most patient Saviour, Who canst love us still!
  % And by this Food, so awful and so sweet,
  % Deliver us from every touch of ill:
  % In Thine own service make us glad and free,
  % And grant us never more to part with Thee.
  \l Donc nous ve -- nons se met -- tre à ge -- noux,
  \l ô Sau -- veur pa -- tient, qui nous aime en -- core!
  \l Et par cet A -- li -- ment ter -- rible et doux,
  \l de -- li -- vre -nous de tout ty -- pe de mort!
  \l Dans ton ser -- vice on est tous li -- bé -- ré:
  \l fais que l'on reste a -- vec toi à ja -- mais.
}


\score {
  <<
    \new Staff  <<
      \new Voice = "soprano" { \voiceOne \melody }
      \new Voice = "alto" { \voiceTwo \alto }
    >>
    \new Lyrics \with{\lyricspace} \lyricsto soprano \verseOne
    \new Lyrics  \lyricsto soprano \verseTwo
    \new Lyrics  \lyricsto soprano \verseThree
    \new Lyrics  \lyricsto soprano \verseFour
    \new Staff  <<
      \clef bass
      \new Voice = "tenor" { \voiceOne \tenor }
      \new Voice = "bass" { \voiceTwo \bass }
    >>
  >>
}

