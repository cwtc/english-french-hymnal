\include "../common_liturgy.ly"

global = {
  \key f \major
  \time 3/4
  \autoBeamOff
  \override Staff.TimeSignature #'stencil = ##f
}

\layout {
  \context {
    \Score
    \omit BarNumber
  }
}

melody = \relative c'' {
  \global
  \partial 4
  c4 |
  c4. bes8 a4 |
  a g f |
  f e d |
  c2 \bar "|" \break

  c4 |
  c4. d8 c4 |
  c g' e |
  d c f |
  a2 \bar "|" \break

  c4 |
  c4. bes8 a4 |
  % \set melismaBusyProperties = #'()
  % \slurDashed 
  a g f 
  % \unset melismaBusyProperties |
  f e d |
  c2 \bar "|" \break

  c4 |
  bes'4. a8 g4 |
  a g f |
  g d e |
  f2 \bar "|."
} 

alto = \relative c' {
  \global
  \partial 4
  f4 |
  f4. g8 f4 |
  f c c |
  d c bes |
  a2

  a4 |
  bes4. bes8 bes4 |
  bes bes bes |
  bes a a |
  f'2

  f4 |
  f4. g8 f4 |
  % \slurDashed 
  f c c |
  d c bes |
  a2

  c4 |
  e4. f8 e4 |
  f4 c c |
  d bes bes |
  c2 \bar "|."
}

tenor = \relative c' {
  \global
  \partial 4
  a4 |
  a4. d8 c4 |
  c bes a |
  bes f f |
  f2

  f4 |
  e4. e8 e4 |
  e e g |
  f f a |
  c2

  a4 |
  a4. d8 c4 |
  % \slurDashed 
  c bes a |
  bes f f |
  f2

  f4 |
  g4. c8 c4 |
  c bes a |
  g g bes |
 
 a2 \bar "|."

}

bass = \relative c {
  \global
  \partial 4
  \partial 4
  f4 |
  f4. f8 f4 |
  f f f |
  bes, bes bes |
  f2

  f4 |
  c'4. c8 c4 |
  c c c |
  f f f |
  f2

  f4 |
  f4. f8 f4 |
  % \tieDashed 
  f4 f4 f4 |
  bes, bes bes |
  f2

  a4 |
  c4. c8 c4 |
  f f f |
  bes, g c |
  f2 \bar "|."
}

verseOne = \lyricmode {
  % \set ignoreMelismata = ##t
  \set stanza = "1 "
  % Away in a manger, no crib for a bed,
  % The little Lord Jesus laid down His sweet head;
  % The stars in the bright sky looked down where He lay,
  % The little Lord Jesus asleep on the hay.
  \l Dans l'hum -- ble mange -- oi -- re
  qui fai -- sait son lit,
  \l pe -- tit Sei -- gneur Jé -- sus,
  là, il s'en -- dor -- mit;
  \l une é -- toi -- le dans le ciel
  l'il -- lu -- mi -- nait:
  \l pe -- tit Sei -- gneur Jé -- sus,
  sur la paille en paix.
}

verseTwo = \lyricmode {
  % \set ignoreMelismata = ##t
  \set stanza = "2 "
  % The cattle are lowing, the Baby awakes,
  % But little Lord Jesus, no crying He makes.
  % I love Thee, Lord Jesus, look down from the sky,
  % And stay by my cradle till morning is nigh.
  \l Les bes -- ti -- aux beug -- lent,
  puis il les en -- tend;
  \l pe -- tit Sei -- gneur Jé -- sus,
  il ne pleu -- re point.
  \l J'ai -- me Sei -- gneur Jé -- sus:
  que tu me sur -- veilles, 
  \l res -- tant à mes cô -- tés
  jus -- qu'à mon ré -- veil.
}

verseThree = \lyricmode {
  % \set ignoreMelismata = ##t
  \set stanza = "3 "
  % Be near me, Lord Jesus; I ask Thee to stay
  % Close by me forever, and love me, I pray.
  % Bless all the dear children in Thy tender care,
  % And fit us for heaven to live with Thee there.
  \l Donc, Sei -- gneur Jé -- sus, j'o -- se te de -- man -- der:
  \l que tu restes a -- vec moi aus -- si pour m'ai -- mer;
  \l que tout en -- fant ve -- nant vers toi soit bé -- ni;
  \l que l'on soit fait prêt pour ton règne in -- fi -- ni.
}

\score {
  <<
    \new Staff  <<
      \new Voice = "soprano" { \voiceOne \melody }
      \new Voice = "alto" { \voiceTwo \alto }
    >>
    \new Lyrics \with{\lyricspace} \lyricsto soprano \verseOne
    \new Lyrics  \lyricsto soprano \verseTwo
    \new Lyrics  \lyricsto soprano \verseThree
    \new Staff  <<
      \clef bass
      \new Voice = "tenor" { \voiceOne \tenor }
      \new Voice = "bass" { \voiceTwo \bass }
    >>
  >>
}
