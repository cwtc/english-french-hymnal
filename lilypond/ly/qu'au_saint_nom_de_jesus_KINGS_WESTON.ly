\include "../common_liturgy.ly"

global = {
  \key e \minor
  \time 3/2
  \autoBeamOff
  \override Staff.TimeSignature #'stencil = ##f
}

\layout {
  \context {
    \Score
    \omit BarNumber
  }
}

melody = \relative c' {
  \global
  e4 fis g2 fis |
  e1 d2 |
  e4 fis g2 a |
  e1. | \break

  g4 a b2 b |
  b2.( a4) g2 |
  e4 g a2 g |
  a1. | \break

  fis4 g a2 fis |
  b2.( a4) g2 |
  a4 b d2 cis |
  d1. | \break

  e2 d b4 a |
  d2 b a4 b |
  g1 fis2 |
  e1. \bar "|."
} 

alto = \relative c' {
  \global
  e4 fis4 d2 d |
  s1 d2

  e4 fis e2 e |
  b1 c2

  d2 b d |
  fis1 b,2
  e4 g4 e2 d |
  c d e |

  fis e d |
  d fis b, |

  fis' fis e |
  fis g a |

  b a fis |
  d fis
  e4 d |
  d1 d2 |
  b1.
}

tenor = \relative c' {
  \global
  g2 b a |
  g a b |
  g4 a b2 c |
  g1. |

  g2 g g |
  b2.( a4) g2 |

  g2 c b |
  a b c |

  d c d |
  b2.( a4) g2 |

  a2 d2 cis |
  d1.

  e2 d b4 a |
  b2 d c4 g |

  b1 a2 |
  g1.
}

bass = \relative c {
  \global
  e2 b d |
  e fis g |

  e d c |
  e d c |

  b g b |
  d1 e2 |

  d2 c b |
  a b c |

  d c d |
  d2 d e |

  fis2 g a |
  d, e fis |

  g fis d |
  b2 d c4 g |

  b2. c4 d2 |
  e1.
}

verseOne = \lyricmode {
  % \set ignoreMelismata = ##t
  \set stanza = "1 "
  % At the Name of Jesus
  % Every knee shall bow,
  % Every tongue confess Him
  % King of glory now;
  % 'Tis the Father's pleasure
  % We should call Him Lord,
  % Who from the beginning
  % Was the mighty Word.
  \l Qu'au saint Nom de Jé -- sus,
  \l tout ge -- nou flé -- chisse,
  \l tou -- te langue en -- ton -- ne
  \l qu'il est bien le Christ:
  \l Il est Roi de gloi -- re,
  \l no -- tre vrai Sei -- gneur;
  \l l'é -- ter -- nel Ver -- be
  \l du Pè -- re cré -- a -- teur.
}

verseTwo = \lyricmode {
  % \set ignoreMelismata = ##t
  \set stanza = "2 "
  % At His voice creation
  % Sprang at once to sight,
  % All the angel faces,
  % All the hosts of light,
  % Thrones and dominations,
  % Stars upon their way,
  % All the heavenly orders,
  % In their great array.
  \l De cet -- te Pa -- ro -- le
  \l tout é -- tait cré -- é:
  \l les sé -- ra -- phins bril -- lants,
  \l la cé -- leste ar -- mée,
  \l les Trônes, Au -- to -- ri -- tés,
  \l Pou -- voirs or -- don -- nés,
  \l les é -- toi -- les qui sont
  \l par sa main ran -- gées.
}

verseThree = \lyricmode {
  % \set ignoreMelismata = ##t
  \set stanza = "3 "
  % Humbled for a season,
  % To receive a Name
  % From the lips of sinners
  % Unto whom He came,
  % Faithfully he bore it
  % Spotless to the last,
  % Brought it back victorious,
  % When from death He passed;
  \l De -- ve -- nant ser -- vi -- teur,
  \l il s'est dé -- pouil -- lé;
  \l com -- me sim -- ple homme il
  \l s'est hu -- mi -- li -- é:
  \l Le Christ s'est ren -- du pour
  \l nous o -- bé -- is -- sant,
  \l jusqu' -- à la mort, mê -- me
  \l la mort sur la croix.
}

verseFour = \lyricmode {
  % \set ignoreMelismata = ##t
  \set stanza = "4 "
  % Bore it up triumphant,
  % With its human light,
  % Through all ranks of creatures,
  % To the central height;
  % To the throne of Godhead,
  % To the Father's breast,
  % Filled it with the glory
  % Of that perfect rest.
  \l Donc Dieu l'a é -- le -- vé,
  \l le vic -- to -- ri -- eux,
  \l son corps et son saint Nom
  \l mê -- me jus -- qu'aux cieux:
  \l au -des -- sus des an -- ges,
  \l au plus grande hau -- teur,
  \l as -- sis à la droi -- te
  \l du Père en splen -- deur. 
}


\score {
  <<
    \new Staff  <<
      \new Voice = "soprano" { \voiceOne \melody }
      \new Voice = "alto" { \voiceTwo \alto }
    >>
    \new Lyrics \with{\lyricspace} \lyricsto soprano \verseOne
    \new Lyrics  \lyricsto soprano \verseTwo
    \new Lyrics  \lyricsto soprano \verseThree
    \new Lyrics  \lyricsto soprano \verseFour
    \new Staff  <<
      \clef bass
      \new Voice = "tenor" { \voiceOne \tenor }
      \new Voice = "bass" { \voiceTwo \bass }
    >>
  >>
}

\markup { \vspace #0.5 } 

\markup {
  \fill-line {
    \hspace #0.1 % moves the column off the left margin;
     % can be removed if space on the page is tight
     \column {
      \line { \bold "5 "
        \column {
          % In your hearts enthrone Him;
          % There let Him subdue
          % All that is not holy,
          % All that is not true:
          % Crown Him as your Captain
          % In temptation's hour;
          % Let His will enfold you
          % In its light and power.
          "Que l'on l'intronise Roi de tous nos cœurs,"
          "afin qu'il maîtrise nos désirs pécheurs:"
          "Notre Capitaine à notre tentation!"
          "Que sa volonté soit la libération!"
        }
      }
    }
    \hspace #0.1 % adds horizontal spacing between columns;
    \column {
      \line { \bold "6 "
        \column {
          % Brothers, this Lord Jesus
          % Shall return again,
          % With His Father's glory,
          % With His angel train;
          % For all wreaths of empire
          % Meet upon His brow,
          % And our hearts confess Him
          % King of Glory now.
          "Et ce Seigneur Jésus, il retournera:"
          "dans la gloire immense, il nous jugera;"
          "et son règne parfait n'aura pas de fin:"
          "C'est le Roi de gloire; louons son Nom saint!"
        }
      }
    }
  \hspace #0.1 % gives some extra space on the right margin;
  % can be removed if page space is tight
  }
}
