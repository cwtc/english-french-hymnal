\include "../common_liturgy.ly"

global = {
  \key g \major
  \time 4/4
  \autoBeamOff
  \override Staff.TimeSignature #'stencil = ##f
}

\layout {
  \context {
    \Score
    \omit BarNumber
  }
}

melody = \relative c' {
  \global
  \partial 4 d4 | g4 a4 fis4 d4 | g4 a4 b4 a4 |
  b4 g4 b4 cis4 | d2. \bar "|"\break
  a4 |
  b4 a4 g4 fis | e4 a4 fis4 d4 |
  d'4 d8[ c8] b4 a4 | g1
  \bar "|."
} 

alto = \relative c' {
  \global
  b4 | b4 e4 d4 d4 | d4 d4 d4 d4 |
  d4 d4 d4 g4 | fis2. fis4 |
  g4 fis4 e4 d4 | c4 e4 d4 d4 |
  g8[ fis8] e4 d4. c8 | b1
}

tenor = \relative c' {
  \global
  g4 | g4 c4 a4 fis4 | g4 fis4 g4 fis4 |
  g4 b4 b8[ a8] g4 | a2. d4 |
  d4 d4 b4 b8[ a8] | g4 c4 a4 fis4 |
  g4 g4 g4 fis4 | g1
}

bass = \relative c' {
  \global
  g4 | e4 c4 d4. c8 | b4 d4 g,4 d'4 |
  g,4 g'4 g8[ fis8] e4 | d2. d4 |
  g4 d4 e4 b4 | c4 a4 d4 d8[ c8] |
  b4 c4 d4 d4 | g,1
}

verseOne = \lyricmode {
  % \set ignoreMelismata = ##t
  \set stanza = "1 "
  % The Head that once was crowned with thorns,
  % Is crowned with glory now;
  % A royal diadem adorns
  % The mighty Victor's brow.
  \l Le front d'é -- pi -- nes cou -- ron -- né Jé -- sus pour nous est mort;
  \l Mais glo -- ri -- eux il va re -- gner; il chan -- ge no -- tre sort.
}

verseTwo = \lyricmode {
  % \set ignoreMelismata = ##t
  \set stanza = "2 "
  % The highest place that heaven affords
  % Is His, is His by right,
  % The King of kings, and Lord of lords,
  % And heaven's eternal Light:
  \l Car main -- ten -- ant, ô Roi des rois, tu rè -- gnes dans les cieux;
  \l C'est le tri -- omphe a -- près la croix du Christ vic -- to -- ri -- eux.
}

verseThree = \lyricmode {
  % \set ignoreMelismata = ##t
  \set stanza = "3 "
  % The joy of all who dwell above;
  % The joy of all below,
  % To whom He manifests His love
  % And grants His Name to know.
  \l A tous les ha -- bi -- tants des cieux, à nous pau -- vres hu -- mains,
  \l il donne un bon -- heur ra -- di -- eux par son a -- mour di -- vin.
}

verseFour = \lyricmode {
  % \set ignoreMelismata = ##t
  \set stanza = "4 "
  % To them the cross, with all its shame,
  % With all its grace, is given;
  % Their name, an everlasting name,
  % Their joy, the joy of heaven.
  \l Pour les hu -- mains tu t'a -- bai -- ssas, puis tu mou -- rus, Sei -- gneur.
  \l Et tu nous sau -- ves par ta croix! Ne ver -- sons plus de pleurs!
}


\score {
  <<
    \new Staff  <<
      \new Voice = "soprano" { \voiceOne \melody }
      \new Voice = "alto" { \voiceTwo \alto }
    >>
    \new Lyrics \with{\lyricspace} \lyricsto soprano \verseOne
    \new Lyrics  \lyricsto soprano \verseTwo
    \new Lyrics  \lyricsto soprano \verseThree
    \new Lyrics  \lyricsto soprano \verseFour
    \new Staff  <<
      \clef bass
      \new Voice = "tenor" { \voiceOne \tenor }
      \new Voice = "bass" { \voiceTwo \bass }
    >>
  >>
}

\markup { \vspace #0.5 } 

\markup {
  \fill-line {
    \hspace #0.1 % moves the column off the left margin;
     % can be removed if space on the page is tight
     \column {
      \line { \bold "5 "
        \column {
          % They suffer with their Lord below,
          % They reign with Him above,
          % Their profit and their joy to know
          % The mystery of His love.
          "Avec le Christ portons la croix,"
          "car un jour dans les cieux,"
          "tous ceux qui vivent dans la foi"
          "verront enfin leur Dieu."
        }
      }
    }
    \hspace #0.1 % adds horizontal spacing between columns;
    \column {
      \line { \bold "6 "
        \column {
          % The cross He bore is life and health,
          % Though shame and death to Him:
          % His people's hope, His people's wealth,
          % Their everlasting theme.
          "Ta croix, ô Christ, est le salut;"
          "ta mort sauva les tiens;"
          "T'aimer, Seigneur, voilà mon but;"
          "tu es mon seul vrai bien."

        }
      }
    }
  \hspace #0.1 % gives some extra space on the right margin;
  % can be removed if page space is tight
  }
}