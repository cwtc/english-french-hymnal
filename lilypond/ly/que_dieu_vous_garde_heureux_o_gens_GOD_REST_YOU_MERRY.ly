\include "../common_liturgy.ly"

global = {
  \key g \major
  \time 2/2
  \autoBeamOff
  \override Staff.TimeSignature #'stencil = ##f
}

\layout {
  \context {
    \Score
    \omit BarNumber
  }
}

melody = \relative c' {
  \global
    \partial 4 e4 |
    e b' b a |
    \slurDashed g( fis) e d |
    \slurSolid e fis g a |
    \slurDashed b2( b4) \slurSolid \bar"|"\break

    e,4 |
    e b' b a |
    \slurDashed g( fis) e d |
    \slurSolid
    e fis g a |
    \slurDashed b2( b4) \slurSolid \bar"|"\break b |
    
    c a b c |
    \slurDashed d( e) b a |
    \slurSolid
    g e fis g |
    \partial 2 \slurDashed a4( a4) \slurSolid \bar "||" \break
    
    \partial 2 g4^\markup { \italic "Refrain" } a |
    b2 c4 b |
    b a g fis |
    e2 g8\noBeam fis e4 |
    a2 g4 a |
    b( c) d e |
    b a g fis |
    \partial 4*3 e2. \bar "|."
} 

alto = \relative c' {
  \global
  e4 |
  e g fis dis |
  \slurDashed e( d?) c b |
  \slurSolid e dis e e |
  \slurDashed dis2( dis4) \slurSolid e4 |
  
  e g fis dis |
  \slurDashed e( d?) c b |
  \slurSolid
  e dis e e |
  \slurDashed dis2( dis4) \slurSolid e |
  
  e d d g |
  \slurDashed f( e) d dis |
  \slurSolid
  e cis d g |
  \slurDashed fis4( fis4) \slurSolid |
  
  g4 d |
  d2 e4 d |
  g fis e dis |
  e2 b8\noBeam b cis4 |
  d2 e4 fis |
  g2 g4 g |
  g fis e dis |
  e2.
}

tenor = \relative c {
  \global
  e4 |
  g e fis b |
  \slurDashed b( b) g g |
  \slurSolid
  g b b e, |
  \slurDashed fis2( fis4) \slurSolid e4 |
  
  g e fis b |
  \slurDashed b( b) g g |
  \slurSolid
  g b b e, |
  \slurDashed fis2( fis4) \slurSolid g |
  
  a a g g |
  \slurDashed g( g) g fis |
  \slurSolid
  g g a d |
  d( c) |
  
  b a |
  g2 g4 g |
  d' c b b |
  g2 g8\noBeam g g4 |
  fis( a) d c |
  b( g) d' c |
  d c b b |
  g2.
}

bass = \relative c {
  e4 |
  e e dis b |
  \slurDashed e( b) c g |
  \slurSolid
  c b e c |
  \slurDashed b2( b4) \slurSolid e4 |
  
  e e dis b |
  \slurDashed e( b) c g |
  \slurSolid
  c b e c |
  \slurDashed b2( b4) \slurSolid e |
  
  a fis g e |
  \slurDashed b( c) g b |
  \slurSolid
  e e d b |
  \slurDashed d4( d4) \slurSolid |
  
  e4 fis |
  g2 c,4 g' |
  g, a b b |
  e2 e8\noBeam e e4 |
  d( c) b a |
  g( e') b c |
  g a b b |
  e2.
}

verseOne = \lyricmode {
  \set ignoreMelismata = ##t
  \set stanza = "1 "
  % God rest you merry, gentlemen,
  % Let nothing you dismay,
  % For Jesus Christ our Saviour
  % Was born upon this day,
  % To save us all from Satan's power
  % When we were gone astray:
  %     O tidings of comfort and joy,
  %     comfort and joy,
  %     O tidings of comfort and joy.

  % ADAPTED
  \l Que Dieu vous garde heur -- eux, ô gens;
  \l No -- ël a son mes -- sa -- ge:
  \l Sou -- ven -- ez -- vous que Jé -- sus Christ
  \l jus -- qu'à la croix s'en -- ga -- ge;
  \l à sau -- ver l'Hom -- me de Sa -- tan
  \l quand le pé -- ché ra -- va -- ge.

  \l Ô nou -- vel -- les de la con -- so -- la -- tion,
  \l con -- so -- la -- tion!
  \l Ô nou -- vel -- _ les de la con -- so -- la -- tion!
}

verseTwo = \lyricmode {
  \set ignoreMelismata = ##t
  \set stanza = "2 "
  % From God our heav'nly Father,
  % a blessed angel came;
  % and unto certain shepherds
  % brought tidings of the same:
  % how that in Bethlehem was born
  % the son of God by name,

  % ADAPTED
  \l De Dieu le Pè -- re cé -- les -- te,
  \l un ange est ap -- pa -- ru; __ _
  \l Et jus -- qu'à cer -- tains ber -- _ gers
  \l pro -- cla -- me la ve -- nu -- e
  \l D'un en -- fant dans un -- e crè -- che,
  \l le Fils de Dieu, Jé -- sus. __ _
}

verseThree = \lyricmode {
  \set ignoreMelismata = ##t
  \set stanza = "3 "
  % "Fear not, then," said the angel,
  % "let nothing you affright;
  % this day is born a Savior
  % of a pure virgin bright,
  % to free all those who trust in him
  % from Satan's pow'r and might."

  % ADAPTED
  \l N'ay -- ez crain -- te, dit l'ange _ __ aux
  \l ber -- gers qui pri -- rent peur; __ _
  \l En ce jour de la Vier -- _ ge
  \l est né l'En -- fant Sau -- veur, __ _
  \l Pour li -- bé -- rer ses fi -- dè -- les 
  \l du ma -- lin Ten -- ta -- teur. __ _
  
}

verseFour = \lyricmode {
  \set ignoreMelismata = ##t
  \set stanza = "4 "
  % But when to Bethlehem they came,
  % Whereat this Infant lay,
  % They found Him in a manger,
  % Where oxen feed on hay;
  % His mother Mary kneeling,
  % Unto the Lord did pray: 

  % ADAPTED
  \l En ar -- ri -- vant à Beth -- lé -- hem,
  \l leur tra -- jet ac -- com -- pli, __ _
  \l Les ber -- gers vi -- rent le Sau -- veur;
  \l le foin é -- tait son lit; __ _
  \l À son cô -- té, à ge -- noux, pri -- ait sa Mè -- re Ma -- ri -- e.
}

verseFive = \lyricmode {
  \set ignoreMelismata = ##t
  \set stanza = "5 "
  % ADAPTED TODO UPDATE MARKINGS
  \l Ô chan -- tez gloire au Sei -- _ gneur,
  \l car grande est sa jus -- ti -- ce.
  \l Ai -- mez -vous, ô chèrs frè -- _ res;
  \l trou -- vez la source en Christ. _ __
  \l Pour nous ra -- che -- ter de la mort,
  \l Dieu a don -- né son Fils. __ _
}


\score {
  <<
    \new Staff  <<
      \new Voice = "soprano" { \voiceOne \melody }
      \new Voice = "alto" { \voiceTwo \alto }
    >>
    \new Lyrics \with{\lyricspace} \lyricsto soprano \verseOne
    \new Lyrics  \lyricsto soprano \verseTwo
    \new Lyrics  \lyricsto soprano \verseThree
    \new Lyrics  \lyricsto soprano \verseFour
    \new Lyrics  \lyricsto soprano \verseFive
    \new Staff  <<
      \clef bass
      \new Voice = "tenor" { \voiceOne \tenor }
      \new Voice = "bass" { \voiceTwo \bass }
    >>
  >>
}
