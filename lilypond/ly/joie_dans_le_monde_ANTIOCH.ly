\include "../common_liturgy.ly"

global = {
  \key d \major
  \time 2/4
  \autoBeamOff
  \override Staff.TimeSignature #'stencil = ##f
}

\layout {
  \context {
    \Score
    \omit BarNumber
  }
}

melody = \relative c'' {
  \global
  d4 cis8. b16 |
  a4. g8 |
  fis4 e |
  d4. a'8 |
  b4. b8 |
  cis4. cis8 |
  d4. \bar "|" \break

  d8 |
  d8[ cis] b[ a] |
  a8.[ g16 fis8] d'8 |
  d8[ cis] b[ a] |
  a8.[ g16 fis8] \bar "|" \break

  fis8 |
  fis fis fis fis16[ g] |
  a4. g16[ fis] |
  e8 e e e16[ fis] |
  g4. \bar "|" \break
  
  fis16[ e] |
  d8( d'4) b8 |
  a8.[ g16 fis8] g8 |
  fis4 e |
  d2 \bar "|."
} 

alto = \relative c' {
  \global
  fis4 a8. g16 |
  fis4. e8 |
  d4 cis |
  d4. a'8 |
  g4. g8 |
  e4. e8 |
  fis4.

  fis8 |
  fis8[ a] g8[ fis] |
  fis8.[ e16 d8] fis |
  fis8[ a] g8[ fis] |
  fis8.[ e16 d8]

  d |
  d8 d d d16[ e] |
  fis4. e16[ d] |
  cis8 cis cis cis16[ d] |
  e4. d16[ cis] |
  d8( fis4) g8 |
  fis8.[ e16 d8] e8 |
  d4 cis |
  d2
}

tenor = \relative c' {
  \global
  d4 d8. d16 |
  d4. b8 |
  a4. g8 |
  fis4. d'8 |
  d4. d8 |
  a4. a8 |
  a4.

  a8 |
  a4 d |
  d( a8) a |
  a4 d |
  d4( a8)

  d,8\rest |
  d4\rest d8\rest a'8 |
  a a a a |
  a2~ |
  a4. a8 |
  fis8( a4) d8 |
  d4. b8 |
  a4 a8[ g] |
  fis2
}

bass = \relative c {
  \global
  d4 d8. d16 |
  d4. g,8 |
  a4 a |
  d4. fis8 |
  g4. g8 |
  a4. a8 |
  d,4.

  d8 |
  d4 d |
  d4. d8 |
  d4 d |
  d4.

  s8 |
  s4 s8 d8 |
  d d d d |
  a'4. a,8 |
  a8 a a a |
  d4. d8 |
  d4. g,8 |
  a4 a |
  d2
}

verseOne = \lyricmode {
  % \set ignoreMelismata = ##t
  \set stanza = "1 "
  % Joy to the World; the Lord is come;
  % Let Earth receive her King;
  % Let ev'ry Heart prepare him Room,
  % And Heav'n and Nature sing.
  \l Joie dans le monde! Il est ve -- nu!
  \l La terre, elle a son Roi!
  \l Que tout cœur lui pré -- pare un lieu:
  \l Le ciel chan -- te sa joie,
  \l le ciel chan -- te sa joie,
  \l le ciel, le ciel chan -- te sa joie.
}

verseTwo = \lyricmode {
  % \set ignoreMelismata = ##t
  \set stanza = "2 "
  % Joy to the Earth, the Saviour reigns,
  % Let Men their Songs employ;
  % While Fields and Floods, Rocks, Hills and Plains,
  % Repeat the sounding Joy.
  \l Joie sur la terre! Le Sau -- veur règne!
  \l Chan -- tons donc du Sei -- gneur!
  \l Les champs, les fleuves, les rocs, les plaines
  \l pro -- cla -- ment leur bon -- heur,
  \l pro -- cla -- ment leur bon -- heur,
  \l pro -- clament, pro -- cla -- ment leur bon -- heur.
}

verseThree = \lyricmode {
  % \set ignoreMelismata = ##t
  \set stanza = "3 "
  % No more let Sins and Sorrows grow,
  % Nor Thorns infest the Ground;
  % He comes to make his Blessings flow
  % Far as the Curse is found.
  \l Fi -- ni les ron -- ces, les é -- pines:
  \l C'est sa bé -- né -- dic -- tion! 
  \l Chas -- sant le ser -- pent, il ar -- rive
  \l sau -- ver sa cré -- a -- tion,
  \l sau -- ver sa cré -- a -- tion,
  \l sau -- ver, sau -- ver sa cré -- a -- tion.
}

verseFour = \lyricmode {
  % \set ignoreMelismata = ##t
  \set stanza = "4 "
  % He rules the World with Truth and Grace,
  % And makes the Nations prove
  % The Glories of his Righteousness,
  % And Wonders of his Love.
  \l Il est le Roi de Vé -- ri -- té;
  \l sa grâ -- ce rem -- plit tout!
  \l Donc cha -- que na -- tion va chan -- ter:
  \l si grand est son a -- mour,
  \l si grand est son a -- mour,
  \l si grand, si grand est son a -- mour.
}


\score {
  <<
    \new Staff  <<
      \new Voice = "soprano" { \voiceOne \melody }
      \new Voice = "alto" { \voiceTwo \alto }
    >>
    \new Lyrics \with{\lyricspace} \lyricsto soprano \verseOne
    \new Lyrics  \lyricsto soprano \verseTwo
    \new Lyrics  \lyricsto soprano \verseThree
    \new Lyrics  \lyricsto soprano \verseFour
    \new Staff  <<
      \clef bass
      \new Voice = "tenor" { \voiceOne \tenor }
      \new Voice = "bass" { \voiceTwo \bass }
    >>
  >>
}
