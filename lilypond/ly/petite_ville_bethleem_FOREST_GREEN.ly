\include "../common_liturgy.ly"

% TO CONSIDER:
% http://www.hymntime.com/tch/non/fr/p/v/b/e/pvbethlm.htm
% http://www.hymntime.com/tch/non/fr/p/e/v/i/pevibeth.htm

global = {
  \key f \major
  \time 4/2
  \autoBeamOff
  \override Staff.TimeSignature #'stencil = ##f
}

\layout {
  \context {
    \Score
    \omit BarNumber
  }
}

melody = \relative c' {
  \global 
  \partial 2 c2 | f2 f2 f2 g2 | a4 ( g4) a4(  bes4) c2 \bar "|"
  a2 | bes2 a4( f4) g2 g2 | f1. \bar "|" \break
  \partial 2 c2 | f2 f2 f2 g2 | a4 ( g4) a4(  bes4) c2 \bar "|"
  a2 | bes2 a4( f4) g2 g2 | f1. \bar "|" \break
  f4( a4) | c2. d4 c4 ( bes4) a4( g4) | f4( g4 a4 bes4) c2 \bar "|"
  c,2 | f2 a2 g2 f2 | c1 \bar "|" \break
  c1 | f2 f2 f2 g2 | a4( g4 -) a4( bes4) c2 \bar "|"
  a2 | bes2 a4( f4 ) g2 g2 | f1. \bar "|."
}  

alto = \relative c' {
  \global 
  c2 | c2 d2 c2 d4( e4) | f2 f2 e2 
  d2 | d2 f2 f2 e2 | s1.
  c2 | c2 d2 c2 d4( e4) | f2 f2 e2 
  d2 | d2 f2 f2 e2 | s1.
  f2 | e2. d4 e2 c2 | c2( f2) e2 
  c2 | c2 c2 bes2 a4( bes4) | s1 
  s1 | c2 d2 c2 d4( e4) | f2 f2 e2 
  d2 | d2 f2 f2 e2 | s1. 
}

tenor = \relative c {
  \global
  g'2 | a2 bes2 c2 bes2 | c2 f,2 g2 
  f2 | bes2 c2 d2 c4( bes4) | a1.
  g2 | a2 bes2 c2 bes2 | c2 f,2 g2 
  f2 | bes2 c2 d2 c4( bes4) | a1. 
  a2 | a2. f4 c'2 c4( bes4) | a2( f2) g2 
  e2 | f2 f2 d4( e4) f2 | e1 
  f2( g2) | a2 bes2 c2 bes2 | c2 f,2 g2 
  f2 | bes2 c4( a4) c2 c4( bes4) | a1. 
}

bass = \relative c {
  \global
  e2 | f2 bes2 a2 g2 | f2 d2 c2 
  d2 | g,2 a2 bes2 c2 | f1.
  e2 | f2 bes2 a2 g2 | f2 d2 c2 
  d2 | g,2 a2 bes2 c2 | f1.
  d2 | a2. bes4 c2 d4( e4) | f2( d2) c2 
  c4( bes4) | a2 f2 bes2 d2 | c1  
  d2( e2) | f2 bes2 a2 g2 | f2 d2 c2 
  d2 | g,2 a4( d4) c2 c2 | f1
}

verseOne = \lyricmode {
  \set stanza = "1 "
   \set ignoreMelismata = ##t
  % \l O lit -- tle town of Beth -- le -- hem,
  \l Pe -- ti -- te vil -- le Beth -- _ lé -- _ em,
  % \l How still we _ see thee lie!
  \l tu te re -- po -- ses en paix!
  %% TL: que tu nous parais tranquille! (2)
  % \l tu dors tran -- _ quil -- le -- ment;
  % \l A -- bove thy deep and dream -- less sleep
  \l Là quand tu dors, l'é -- toi -- _ le _ d'or
  %% TL: Tu dors la nuit dans l'infini
  % \l Sur ton som -- meil, l'é -- toi -- le d'or 
  % \l The si -- lent _ \set associatedVoice = "altos" stars go by; \unset associatedVoice
  \l en si -- len -- _ ce sur -- "viel - le."
  %% TL: où les étoiles défilent. (2)
  % \l se lève au _ fir -- ma -- ment.
  % \l Yet in thy dark streets shin -- eth
  %% TL: Caché dans tes rues sombres
  \l Sa _ lu -- mière é -- _ ter -- _ nel -- _ _ _ le
  % \l The ev -- er -- last -- ing Light;
  \l é -- clai -- re le ciel noir;
  %% TL: l'éternelle lumière, (2)
  % \l nous ap -- por -- te la joie;
  % \l The hopes and fears of all the years
  \l Nos peurs, nos es -- poirs sont, _ en _ toi,
  %% TL: Espoir des sages de tous les âges,
  % \l Oui, la ré -- ponse à nos ap -- pels
  % \l Are met in \set associatedVoice = "altos" thee to -- night.
  \l bien ex -- au -- _ cés ce soir.
  %% TL: le Christ vient sur la terre. (2)
  % \l ce soir se trouve en toi.
}

verseTwo = \lyricmode {
  \set stanza = "2 "
  \set ignoreMelismata = ##t
  % \l For Christ is born of Ma -- _ ry,
  \l Car Jé -- sus de Ma -- rie _ est _ né,
  % \l And gath -- er’d _ all a -- bove,
  \l et tan -- dis _ que tout dort,
  % \l While mor -- tals sleep, the an -- gels keep
  \l Les an -- ges sur -- veillent, as -- _ sem -- _ blés,
  % \l les an -- ges là -- haut as -- sem -- blés
  % \l Their watch of _ \set associatedVoice = "altos" won -- d’ring love. \unset associatedVoice
  \l chan -- tent leurs _ doux ac -- cords.
  % \l O morn -- ning stars, to -- geth -- er
  \l Pro -- _ cla -- mez sa _ nai -- _ san -- _ _ _ ce,
  % \l Pro -- claim the ho -- ly birth!
  \l é -- toi -- les du ma -- tin!
  % \l And prais -- es sing to God the King,
  \l Chan -- tez sa gloi -- re, c'est _ le _ Roi;
  % \l Ap -- por -- tant la paix et l'a -- mour,
  % \l And peace to \set associatedVoice = "altos" men on earth!
  \l voi -- ci l'En -- _ fant di -- vin.
}

verseThree = \lyricmode {
  \set stanza = "3 "
  \set ignoreMelismata = ##t
  % \l How si -- lent -- ly, how si -- lent -- ly
  %% TL: Le Messie vient nous rejoindre
  \l Ah! Qu'il fut don -- né sim -- _ ple -- _ ment
  % \l The won -- drous _ gift is giv’n!
  %% TL: sans fanfare et sans bruit.
  \l ce pré -- sent _ mer -- veil -- leux!
  % \l So God im -- parts to hu -- man hearts
  %% TL: Dieu, par son don, donne son pardon
  \l Et Dieu bé -- nit l'homme ain -- _ _ _ si:
  % \l Ain -- si Dieu bé -- nit les hu -- mains
  % \l The bless -- ings _ \set associatedVoice = "altos" of His Heav’n. \unset associatedVoice
  %% À ceux qui ont foi en lui.
  \l Il les ouv -- _ re les cieux.
  % \l en leur ouv -- _ rant les cieux.
  % \l No ear may hear His com -- ing,
  \l Nul _ n'en -- tend sa _ ve -- _ nu -- _ _ _ e
  % \l But in this world of sin;
  \l dans ce mon -- de pé -- cheur.
  % \l Where meek souls will re -- ceive Him still,
  \l Mais, bien -- ve -- nu, en -- tre _ Jé -- _ sus,
  % \l Mais Jé -- sus en -- tre, bien -- ve -- nu,
  % \l The dear Christ \set associatedVoice = "altos" en -- ters in.
  \l dans les plus _ hum -- bles cœurs.
}

verseFour = \lyricmode {
  \set stanza = "4 "
  \set ignoreMelismata = ##t
  %% AC
  % \l Where chil -- dren pure and hap -- py
  \l Où les en -- fants heu -- reux _ et _ purs
  % \l pray to the bless -- ed Child,
  \l se tournent à _ l'En -- fant Saint,
  % \l Where mis -- e -- ry cries out to Thee,
  \l Où la mi -- sè -- re voit _ la _ Mère
  % \l Son of the \set associatedVoice = "altos" mo -- ther mild; \unset associatedVoice
  \l a -- vec son _ Fils di -- vin,
  % \l Where cha -- ri -- ty stands watch -- ing
  \l Où _ l'on _ voit _ la _  cha -- _ ri -- _ té,
  % \l and faith holds wide the door,
  \l on sait que Dieu est là.
  % \l The dark night wakes, the glo -- ry breaks,
  \l Comme au ci -- el, fê -- tons _ No -- _ ël
  % \l and Christ -- \set associatedVoice = "altos" mas comes once more.
  \l en -- co -- re _ un -- e fois.
}

verseFive = \lyricmode {
  \set stanza = "5 "
  \set ignoreMelismata = ##t
  % TL, ad. AC:
  % O Holy Child of Bethlehem,
  \l Cher En -- fant Saint de Beth -- _ lé -- _ em,
  % Descend to us, we pray;
  \l Des -- cends nous _ voir ce jour;
  % Cast out our sin, and enter in,
  %% TL: Tu brises les chaînes qui nous entrâinent
  \l Verbe in -- car -- né, ô viens _ en -- _ trer,
  % Be born in us today.
  %% TL: vers la mort aux alentours.
  \l Sau -- ve -nous _ de la mort.
  % We hear the Christmas angels
  \l Nous _ en -- ten -- dons _ les _ an -- _ _ _ ges
  % The great glad tidings tell;
  \l qui chan -- tent pour No -- ël;
  % O come to us, abide with us,
  % Groupons nos voix, trouvons la joie
  \l Ô viens vers nous, reste a -- _ vec _ nous,
  % Our Lord Emmanuel!
  % avec Emmanuel.
  \l Sei -- gneur Em -- _ man -- u -- el!
}


\score {
  <<
    \new Staff  <<
      \new Voice = "soprano" { \voiceOne \melody }
      \new Voice = "alto" { \voiceTwo \alto }
    >>
    \new Lyrics \with{\lyricspace} \lyricsto soprano \verseOne
    \new Lyrics  \lyricsto soprano \verseTwo
    \new Lyrics  \lyricsto soprano \verseThree
    \new Lyrics  \lyricsto soprano \verseFour
    \new Lyrics  \lyricsto soprano \verseFive
    \new Staff  <<
      \clef bass
      \new Voice = "tenor" { \voiceOne \tenor }
      \new Voice = "bass" { \voiceTwo \bass }
    >>
  >>
}

