\include "../common_liturgy.ly"
% https://github.com/magdalenesacredmusic/English-Hymns/blob/master/A-C/Bread%20of%20the%20World%20in%20Mercy%20Broken%20(RENDEZ%20%C3%80%20DIEU).ly
% https://www.hymnologyarchive.com/rendez-dieu

global = {
  \key g \major
  % \time 4/4
  \cadenzaOn
  \autoBeamOff
  \override Staff.TimeSignature #'stencil = ##f
}

\layout {
  \context {
    \Score
    \omit BarNumber
  }
}

melody = \relative c'' {
  \global
  g2 e4 d \bar "|"
  g g a c \bar "|"
  b2 a \bar "|"

  d2 b4 b \bar "|"
  g4 c b2 a \bar "|"
  g1 \bar "|"
  \break

  g2 e4 d \bar "|"
  g4 g a c \bar "|"
  b2 a \bar "|"

  d2 c4 b \bar "|"
  a g g fis \bar "|"
  g1 \bar "|"
  \break

  d'2 d4 c \bar "|"
  b2 a g4 fis \bar "|"
  e2 d \bar "|"

  d2 g4 g fis e \bar "|"
  g2 a \bar "|"
  b1 \bar "|"
  \break

  g2 g4 a \bar "|"
  b g c c \bar "|"
  b2 a \bar "|"

  d2 c4 b \bar "|"
  a g g fis \bar "|"
  g1 \bar "|."
} 

alto = \relative c' {
  \global
  b4( d) c a |
  e' d e e |
  g2 fis |

  fis2 fis4 fis |
  g a g( e) fis2 |
  s1 |

  b,2 c4 a |
  d e fis g |
  g2 fis |

  g2 fis4 g |
  e e d d |
  d1 |

  g2 g4 fis |
  d( e) fis2 e4 d |
  d( cis) d2 |

  a2 d4 e d b |
  e2. fis4 |
  gis1 |

  e2 e4 d |
  d e e4.( fis8) |
  gis2 e4( fis) |

  g2 g4 g |
  e e d d |
  d1
}

tenor = \relative c' {
  \global
  g2 g4 fis |
  b g c c |
  d( e) a,2 |

  b2 b4 b |
  b e d2. c4 |
  b1 |

  g2 g4 fis |
  g b d e |
  d2 d |
  b2 c4 d |
  c b a a |
  b1 |

  b2 b4 c |
  g2 d' b4 a |
  b( a) fis2 |

  fis2 g4 c a g |
  g2 c |
  b1 |

  b2 b4 a |
  g b e e |
  e( d) c2 |

  d2 e4 d |
  c b a a |
  b1
}

bass = \relative c {
  \global
  g4( b) c d |
  e b c a |
  b( c) d2 |

  b2 d4 d |
  e a, b( c) d2 |
  g,1 |

  e'4( d) c2 |
  b4 e d c |
  g( b) d2 |
  g,2 a4 b |
  c c d d |
  g,1 |

  g2 g4 a |
  b( c) d2 e4 fis |
  g( a) d,2 |

  d2 b4 c d e |
  c( b) a2 |
  e'1 |

  e2 e4 fis |
  g e c4. d8 |
  e2 a,2 |

  b2 c4 g |
  a8[ b] c4 d d |
  g,1 |
}

verseOne = \lyricmode {
  \set ignoreMelismata = ##t
  \set stanza = "1 "
  \l Ren -- dez à Dieu lou -- ange et gloi -- re,
  \l car il est bé -- nin et clé -- ment:
  \l Qui plus est, sa bon -- té no -- toi -- re
  \l du -- re per -- pé -- tu -- el -- le -- ment.
  \l Qu'Is -- ra -- ël o -- res se re -- cor -- de
  \l de chan -- ter so -- lon -- nel -- le -- ment:
  \l Que sa gran -- de mi -- sé -- ri -- cor -- de 
  \l du -- re per -- pé -- tu -- el -- le -- ment.
}

verseTwo = \lyricmode {
  \set ignoreMelismata = ##t
  \set stanza = "2 "
  \l La mai -- son d'A -- ron an -- ci -- en -- ne
  \l vien -- ne tout haut pré -- sen -- te -- ment
  \l Con -- fes -- ser que la bon -- té sien -- ne
  \l du -- re per -- pé -- tu -- el -- le -- ment.
  \l Tous ceux qui du Sei -- gneur ont crain -- te,
  \l vien -- nent aus -- si chan -- ter com -- ment
  \l sa bon -- té pi -- toy -- able et sain -- te
  \l du -- re per -- pé -- tu -- el -- le -- ment.
}

verseThree = \lyricmode {
  \set ignoreMelismata = ##t
  \set stanza = "3 "
  \l Ain -- si que je sois en dé -- tres -- se,
  \l in -- vo -- quant sa _ ma -- je -- sté,
  \l il m'ou -- it et de cet -- te pres -- se
  \l me met au large, à sau -- ve -- té.
  \l Le Tout -Puis -- sant, qui m'ou -- it plain -- dre,
  \l mon par -- ti tou -- jours te -- nir veut:
  \l Qu'ai -- je donc que fai -- re de crain -- dre
  \l tout ce que l'hom -- me fai -- re peut?
}

verseFour = \lyricmode {
  \set ignoreMelismata = ##t
  \set stanza = "4 "
  \l Tu es le seul Dieu que j'ho -- no -- re,
  \l au -- si sans fin je chan -- te -- rai:
  \l Tu es le seul Dieu que j'a -- do -- re,
  \l aus -- si sans fin t'ex -- al -- te -- rai.
  \l Ren -- dez à Dieu lou -- ange et gloi -- re,
  \l Car il est bé -- nin et clé -- ment:
  \l Qui plus est, sa bon -- té no -- toi -- re
  \l du -- re per -- pé -- tu -- el -- le -- ment.
}


\score {
  <<
    \new Staff  <<
      \new Voice = "soprano" { \voiceOne \melody }
      \new Voice = "alto" { \voiceTwo \alto }
    >>
    \new Lyrics \with{\lyricspace} \lyricsto soprano \verseOne
    \new Lyrics  \lyricsto soprano \verseTwo
    \new Lyrics  \lyricsto soprano \verseThree
    \new Lyrics  \lyricsto soprano \verseFour
    \new Staff  <<
      \clef bass
      \new Voice = "tenor" { \voiceOne \tenor }
      \new Voice = "bass" { \voiceTwo \bass }
    >>
  >>
}

