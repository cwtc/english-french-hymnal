\include "../common_liturgy.ly"

global = {
  \key g \major
  \time 4/4
  \autoBeamOff
  \override Staff.TimeSignature #'stencil = ##f
}

\layout {
  \context {
    \Score
    \omit BarNumber
  }
}

melody = \relative c' {
  \global
  \partial 4 e4 |
  g4 b4 b4 b4 | a4( c4 b4) a4 | g2.
  a4 | b4 g4 e4 g4 | a4( fis4 e4) d4 | e2. \bar "|" \break
  a4 | a4 e4 e4 fis4 | g2( fis4) e4 | d2.
  g4 | a4 b4 b4 b4 | a4( c4 b4) a4 | g2. \bar "||" \break

  d'4^\markup { \italic "Refrain" } | d2. b4 | b2.
  b4 | a4( c4 b4) a4 | g2.
  a4 | b4 g4 e4 g4 | a4( fis4 e4) d4 | e1
  \bar "|."
} 

alto = \relative c' {
  \global
  b4 |
  e4 b4 d4 g4 | e2(g4) fis4 | d2.
  d4 | d4 d4 c4 d4 | c2( b4) b4 | b2. 
  e8[ d8] | c4 c4 e4 d4 | e2( d4) c4 | d2.
  b4 | d4 d4 d4 g4 | e2( g4) fis4 | d2. \bar "||" \break

  g4 | fis2. g4 | fis2.
  g4 | e2( g4) fis4 | e2.
  d4 | d4 d4 c4 d4 | c2(  b4) b4 | b1
}

tenor = \relative c' {
  \global
  g4 |
  b4 fis4 g4 d'4 | c4( e4 d4) d4 | b2.
  fis4 | fis4 g4 g4 g4 | e4( fis4 g4) fis4 | g2. 
  e4 | e4 a4 b4 a4 | b2. g4 | fis2.
  g4 | fis4 fis4 g4 d'4 | c4( e4 d4) d4 | b2. \bar "||" \break

  b4 | a2. e'4 | d2.
  d4 | c4( e4 d4) d4 | b2.
  a4 | g4 g4 g4 d4 | e4( fis4 g4) fis4 | g1
}

bass = \relative c {
  \global
  e4 |
  e4 d4 b4 g4 | c4( a4 b4) d4 | g2.
  d4 | b4 b4 c4 b4 | a2( b4) b4 | e2. 
  c8[ b8] | a4 a'4 g4 fis4 | e2( b4) c4 | d2.
  e4 | d4 b4 g4 b4 | c4 ( a4 b4) d4 | g2. \bar "||" \break

  g4 | d2. e4 | b2.
  g4 | c4( a4 b4) d4 | e2.
  fis4 | g4 b,4 c4 b4 | a2 b4 b4 | e1
}

verseOne = \lyricmode {
  % \set ignoreMelismata = ##t
  \set stanza = "1 "
  % O come, O come, Immanuel,
  % and ransom captive Israel
  % that mourns in lonely exile here
  % until the Son of God appear.
  \l Ô viens, ô viens, Em -- man -- u -- el,
  \l viens pour dé -- liv -- rer Is -- ra -- ël:
  \l en deuil, en ex -- il dé -- mu -- ni
  \set ignoreMelismata = ##t
  \l jus -- qu'à ce que le Fils de Dieu ar -- rive.

  \set ignoreMelismata = ##f
  \l Chan -- tez! Chan -- tez! Em -- man -- u -- el
  \l vien -- dra vers toi, ô Is -- ra -- ël.
}

verseTwo = \lyricmode {
  \set ignoreMelismata = ##t
  \set stanza = "2 "
  %  O come, O Wisdom from on high,
  % who ordered all things mightily;
  % to us the path of knowledge show
  % and teach us in its ways to go.
  \l Ô viens, ô viens, ô sain -- _ te Sa -- gesse,
  \l qui or -- don -- na le mon -- de sans fai -- blesse;
  \set ignoreMelismata = ##f
  \l ré -- vè -- le -nous ta con -- nais -- sance;
  \l que l'on la suive en es -- pér -- ance.
}

verseThree = \lyricmode {
  \set ignoreMelismata = ##t
  \set stanza = "3 "
  % O come, O come, great Lord of might,
  % who to your tribes on Sinai's height
  % in ancient times did give the law
  % in cloud and majesty and awe.
  \l Ô viens, ô viens, Sei -- gneur _ de pou -- voir,
  \l qui de -- mon -- tra sur Si -- na -- ï sa gloire;
  \l aux temps an -- ciens ses dou -- ze tri -- bus
  \l la loi en ré -- vé -- ren -- _ ce re -- çurent.
}

verseFour = \lyricmode {
  \set ignoreMelismata = ##t
  \set stanza = "4 "
  % O come, O Branch of Jesse's stem,
  % unto your own and rescue them!
  % From depths of hell your people save,
  % and give them victory o'er the grave.
  \l Ô viens, Ra -- meau du tronc _ de Jes -- sé;
  \l viens vers ton peu -- ple pour _ les sau -- ver
  \l de l'en -- fer af -- freux et ses tré -- fonds,
  \l et ga -- gne la vic -- toi -- re sur la tombe.
}

verseFive = \lyricmode {
  \set ignoreMelismata = ##t
  \set stanza = "5 "
  % O come, O Key of David, come
  % and open wide our heavenly home.
  % Make safe for us the heavenward road
  % and bar the way to death's abode.
  \l Ô viens, ô clé de Da -- vid pré -- ci -- eux;
  \l dé -- ver -- rou -- ille le Roy -- au -- me des Cieux.
  \l Au che -- min vi -- vant ou -- vre la porte,
  \l et blo -- que la de -- meu -- re de la mort.
}


\score {
  <<
    \new Staff  <<
      \new Voice = "soprano" { \voiceOne \melody }
      \new Voice = "alto" { \voiceTwo \alto }
    >>
    \new Lyrics \with{\lyricspace} \lyricsto soprano \verseOne
    \new Lyrics  \lyricsto soprano \verseTwo
    \new Lyrics  \lyricsto soprano \verseThree
    \new Lyrics  \lyricsto soprano \verseFour
    \new Lyrics  \lyricsto soprano \verseFive
    \new Staff  <<
      \clef bass
      \new Voice = "tenor" { \voiceOne \tenor }
      \new Voice = "bass" { \voiceTwo \bass }
    >>
  >>
}

% \markup { \vspace #0.5 } 

\markup {
  \fill-line {
    \hspace #0.1 % moves the column off the left margin;
     % can be removed if space on the page is tight
     \column {
      \line { \bold "6 "
        \column {
          % O come, O Bright and Morning Star,
          % and bring us comfort from afar!
          % Dispel the shadows of the night
          % and turn our darkness into light.
          "Ô viens, Étoile brillant du matin;"
          "et réconforte-nous de loin!"
          "Chasse les ombres de la nuit"
          "et fais que les ténèbres luisent."
        }
      }
    }
    \hspace #0.1 % adds horizontal spacing between columns;
    \column {
      \line { \bold "7 "
        \column {
          % O come, O King of nations, bind
          % in one the hearts of all mankind.
          % Bid all our sad divisions cease
          % and be yourself our King of Peace.
          "Ô viens, ô Roi de toute nation,"
          "unis les cœurs de ta création."
          "Guéris nos fractures aggravées,"
          "et sois toi-même le Roi de la Paix."
        }
      }
    }
  \hspace #0.1 % gives some extra space on the right margin;
  % can be removed if page space is tight
  }
}
