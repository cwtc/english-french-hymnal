\include "../common_liturgy.ly"

global = {
  \key f \major
  \time 4/4
  \autoBeamOff
  \override Staff.TimeSignature #'stencil = ##f
}

\layout {
  \context {
    \Score
    \omit BarNumber
  }
}

melody = \relative c' {
  \global
  f4 f c c |
  f g a2 |
  \time 6/4 d2 c bes4 a |
  \time 4/4 g2 f \bar "|" \break

  f4 f c c |
  f g a2 |
  \time 6/4 d2 c bes4 a |
  \time 4/4 g2 f \bar "|" \break

  \time 6/4 c'2 d c4 bes |
  \time 4/4 a4 a g2 |
  \time 6/4 a2 g4 f2 e4 |
  \time 4/4 d2 c2 \bar "|" \break

  f4 f c c |
  f g a2 |
  \time 6/4 d2 c bes4 a |
  \time 4/4 g2 f \bar "|."
} 

alto = \relative c' {
  \global
  c4 c c c |
  d e f2 |
  f e f4 f |
  e2 f | 

  c4 c c c |
  d e f2 |
  f f d4 f |
  d( e) f2 |

  f2 f e4 f |
  f f e2 |
  c2 c4 a2 c4 |
  a( b) c2 |

  a4 a c c |
  d e f2 |
  f f bes,4 c |
  d( c) a2 |
}

tenor = \relative c' {
  \global
  a4 a g g |
  a c c2 |
  bes g bes4 c |
  c2 a |

  a4 a g g |
  a c c2 |
  bes a bes4 c |
  d( c) a2 |

  a bes g4 bes |
  c c c2 |
  f,2 e4 f2 a4 |
  a( g) e2 |
  
  f4 f g g |
  a c c2 |
  bes c f,4 f |
  f( e) f2 |
}

bass = \relative c {
  \global
  f4 f e e |
  d c f2 |
  bes,2 c d4 f |
  c2 f2 |

  f4 f e e |
  d c f2 |
  bes, f g4 a |
  bes( c) f2 |

  f bes, c4 d |
  f a, c2 |
  f2 c4 d2 a4 |
  f( g) c2 |
  
  d4 d e e |
  d c f2 |
  bes, a d4 c |
  bes( c) f,2 |  
}

verseOne = \lyricmode {
  % \set ignoreMelismata = ##t
  \set stanza = "1 "
  % Come, ye faithful, raise the strain
  % Of triumphant gladness:
  % God hath brought His Israel
  % Into joy from sadness;
  % Loosed from Pharaoh’s bitter yoke
  % Jacob’s sons and daughters,
  % Led them with unmoistened foot
  % Thro' the Red Sea waters.
  \l Ve -- nez, fi -- dè -- les, chan -- tez
  \l dans votre al -- lé -- gres -- se;
  \l Dieu me -- na son Is -- ra -- ël
  \l hors de leur tris -- tes -- se:
  \l Pour les en -- fants de Ja -- cob
  \l la mer Rouge il ou -- vra;
  \l les cha -- riots de Pha -- ra -- on
  \l de ces eaux il cou -- vra.
}

verseTwo = \lyricmode {
  % \set ignoreMelismata = ##t
  \set stanza = "2 "
  % ’Tis the spring of souls today;
  % Christ hath burst His prison,
  % And from three days’ sleep in death,
  % As a sun hath risen:
  % All the winter of our sins,
  % long and dark, is flying
  % From His light, to whom we, give
  % Laud and praise undying.
  \l Des â -- mes c'est le prin -- temps;
  \l Christ a les fers bri -- sés,
  \l et a -- près trois jours, il s'est
  \l comme un so -- leil le -- vé:
  \l L'hi -- ver de cha -- que pé -- ché,
  \l en -- fin, il s'en -- vo -- le
  \l de sa Lu -- mière qu'on per -- çoit,
  \l don -- nant nos é -- lo -- ges.
}

verseThree = \lyricmode {
  % \set ignoreMelismata = ##t
  \set stanza = "3 "
  % Now the queen of seasons, bright
  % With the day of splendor,
  % With the royal feast of feasts,
  % Comes its joy to render;
  % Comes to glad Jerusalem,
  % Who with true affection
  % Welcomes in unwearied strains
  % Jesus' resurrection.
  \l Cet -- te rei -- ne des sai -- sons
  \l a -- mè -- ne sa splen -- deur;
  \l cet -- te fê -- te ap -- por -- te
  \l son bon -- heur à ren -- dre
  \l à l'heu -- reux Jé -- ru -- sa -- lem,
  \l qui, par son af -- fec -- tion,
  \l ca -- ril -- lon -- ne sur Jé -- sus 
  \l et sa re -- sur -- rec -- tion.
}

verseFour = \lyricmode {
  \set ignoreMelismata = ##t
  \set stanza = "4 "
  % Neither could the gates of death,
  % Nor the tomb’s dark portal,
  % Nor the watchers nor the seal,
  % Hold Thee as a mortal.
  % But today amidst Thine own
  % Thou didst stand bestowing,
  % Lord, Thy peace, which evermore
  % Passeth human knowing.
  \l Ni les por -- tes de la mort,
  \l ni la som -- bre tom -- be,
  \l ni les veil -- leurs, ni le sceau
  \l ne t'ont sous leur om -- bre!
  \l Ce jour, tu don -- nas, Sei -- gneur,
  \l par ta bien -- fai -- san -- ce,
  \l cet -- te paix qui dé -- pas -- se
  \l toute in -- tel -- li -- gen -- ce.
}


\score {
  <<
    \new Staff  <<
      \new Voice = "soprano" { \voiceOne \melody }
      \new Voice = "alto" { \voiceTwo \alto }
    >>
    \new Lyrics \with{\lyricspace} \lyricsto soprano \verseOne
    \new Lyrics  \lyricsto soprano \verseTwo
    \new Lyrics  \lyricsto soprano \verseThree
    \new Lyrics  \lyricsto soprano \verseFour
    \new Staff  <<
      \clef bass
      \new Voice = "tenor" { \voiceOne \tenor }
      \new Voice = "bass" { \voiceTwo \bass }
    >>
  >>
}
