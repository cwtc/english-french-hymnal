\include "../common_liturgy.ly"

% https://github.com/magdalenesacredmusic/English-Hymns/blob/master/A-C/A%20Mighty%20Fortress%20(EIN%20FESTE%20BURG).ly
% https://www.conducteurdelouange.com/chants/consulter/45

global = {
  \key c \major
  \time 4/4
  \autoBeamOff
  \override Staff.TimeSignature #'stencil = ##f
}

\layout {
  \context {
    \Score
    \omit BarNumber
  }
}

melody = \relative c'' {
  \global
  \partial 4
  c4 |
  c4 c g8[ a] b4 |
  c8[ b] a4 g c |
  b a g a |
  f8[( e] d4) c4 \bar "|" \break

  c'4 |
  c4 c g8[ a] b4 |
  c8[ b] a4 g c |
  b a g a |
  f8([ e] d4) c \bar "|" \break

  c4 |
  g' a g fis |
  g2. c,4 |
  g' g a b |
  c2.  \bar "|"

  b4 |
  c b a a |
  g2. a4 |
  a g a f |
  e2. \bar "|"

  c'4 |
  b a g a |
  f8[ e] d4 c \bar "|."
} 

alto = \relative c' {
  \global
  \partial 4
  e4 |
  c e d g |
  e fis g e |
  e c c c |
  a( b) c4

  e |
  c e d g |
  e fis g e |
  e c c c |
  a( b) c

  c |
  c e d d8[ c] |
  b2. c4 |
  d e f f |
  g2.

  e4 |
  c d e d8[ c] |
  b2. f'4 |
  f e e d |
  b2.

  c4 |
  f4 f f e |
  d8[ c] b4 c4
}

tenor = \relative c' {
  \global
  \partial 4
  g4 |
  g g b b |
  a d8[ c] b4 a |
  g f e e |
  f( g) e

  g4 |
  g g b b |
  a4 d8[ c8] b4 a4 |
  g f e e |
  f( g) e

  e |
  e c' b a |
  g2. e4 |
  g c c d |
  e2.

  g,4 |
  e8[ fis] g4 g fis |
  g2. c4 |
  c4. bes8 a4 a |
  gis2.

  a4 |
  a8[ b] c4 d a |
  a g8[ f] e4 \bar "|."
}

bass = \relative c {
  \global
  \partial 4
  c4 |
  e c g' e |
  a, d g, a |
  e' f c a |
  d( g,) c

  c |
  e c g' e |
  a, d g, a |
  e' f c a |
  d( g,) c

  c |
  c a b8[ c] d4 |
  g,2. a4 |
  b c f8[ e] d4 |
  c2. e4 |
  a, b c d |
  g2.

  f4 |
  f, c' cis d |
  e2. a,4 |
  d c b cis |
  d g, c
}

verseOne = \lyricmode {
  % \set ignoreMelismata = ##t
  \set stanza = "1 "
  \l C'est un rem -- part que no -- tre Dieu,
  \l Une in -- vin -- cible ar -- mu -- re,
  \l no -- tre dé -- li -- vrance en tout lieu,
  \l no -- tre dé -- fen -- se sû -- re.
  \l L'en -- ne -- mi con -- tre nous
  \l re -- dou -- ble de cour -- roux,
  \l Vai -- ne _ co -- lè -- re! Que pour -- rait l'ad -- ver -- saire?
  \l L'É -- ter -- nel dé -- tour -- ne ses coups.
}

verseTwo = \lyricmode {
  % \set ignoreMelismata = ##t
  \set stanza = "2 "
  \l Seuls, nous bron -- chons à cha -- que pas;
  \l no -- tre force est fai -- bles -- se;
  \l mais un hé -- ros dans les com -- bats
  \l pour nous lut -- te sans ces -- se.
  \l Quel est ce dé -- fen -- seur?
  \l C'est toi, di -- vin Sau -- veur!
  \l Dieu des _ ar -- _ mées, tes tri -- bus op -- pri -- mées
  \l con -- nais -- sent leur li -- bér -- a -- teur.
}

verseThree = \lyricmode {
  % \set ignoreMelismata = ##t
  \set stanza = "3 "
  \l Que les dé -- mons for -- gent des fers
  \l pour ac -- cab -- ler l'É -- gli -- se;
  \l Ta Si -- on bra -- ve les en -- fers,
  \l sur le Ro -- cher as -- si -- se.
  \l Con -- stant dans son ef -- fort,
  \l en vain a -- vec la mort
  \l Sa -- tan _ con -- spi -- re
  \l pour sa -- per son em -- pire:
  \l Il suf -- fit d'un mot du Dieu fort.
  
}

verseFour = \lyricmode {
  % \set ignoreMelismata = ##t
  \set stanza = "4 "
  \l Dis -le, ce mot vic -- to -- ri -- eux,
  \l dans tou -- tes nos dé -- tres -- ses;
  \l ré -- pands sur nous du haut des cieux
  \l tes di -- vi -- nes lar -- ges -- ses.
  \l Qu'on nous ô -- te nos biens,
  \l qu'on ser -- _ re nos liens,
  \l que nous _ im -- por -- te!
  \l Ta grâce est la plus forte,
  \l et ton Ro -- yaume est pour les tiens.
}


\score {
  <<
    \new Staff  <<
      \new Voice = "soprano" { \voiceOne \melody }
      \new Voice = "alto" { \voiceTwo \alto }
    >>
    \new Lyrics \with{\lyricspace} \lyricsto soprano \verseOne
    \new Lyrics  \lyricsto soprano \verseTwo
    \new Lyrics  \lyricsto soprano \verseThree
    \new Lyrics  \lyricsto soprano \verseFour
    \new Staff  <<
      \clef bass
      \new Voice = "tenor" { \voiceOne \tenor }
      \new Voice = "bass" { \voiceTwo \bass }
    >>
  >>
}
