\include "../common_liturgy.ly"

% http://chant.rezo509.com/mobile/MobileChantViewDetails.aspx?ID=233&tc=FR&ri=30&rwndrnd=0.28228481370024383
% https://www.conducteurdelouange.com/chants/consulter/1577

global = {
  \key e \minor
  \time 4/4
  \autoBeamOff
  \override Staff.TimeSignature #'stencil = ##f
}

\layout {
  \context {
    \Score
    \omit BarNumber
  }
}

melody = \relative c' {
  \global
  e4 e8[ fis] g[ a] b4 |
  g fis e2 |

  g4 fis e  dis |
  e8[ fis] g4 fis2 |

  e4 e8[ fis] g[ a] b4 |
  g fis e2 |

  fis4 g8[ a] b4 a |
  g fis e2 |

  e4 d e fis |
  g b8[ a] g2 |

  g4 fis g a |
  b d8[ cis] b2 |

  e4 e d b |
  g a b2 |

  e,4 e8[ fis] g[ a] b4 |
  g fis e2 \bar "|."
  \bar "|."
} 

alto = \relative c' {
  \global
  b4 e8[  dis] e[ fis] g4 |
  e  dis b2 |

  e4  dis e b |
  b e  dis2 |

  b4 e8[  dis] e[ fis] g4 |
  e  dis b2 |

   dis4 e8[ fis] g4 fis |
  e  dis b2 |

  c4 b c c |
  d fis d2 |

  e4  dis e fis8[ d!] |
  d4 fis8[ e] d2 |

  e4 g8[ fis] g4 d |
  e e fis2 |

  e4 e8[  dis] e[ fis] g4 |
  e  dis b2
}

tenor = \relative c' {
  \global
  g4 b b b |
  b b8[ a] g2 |

  b4 b b b |
  b b b2 |

  g8[ a] b4 b b |
  b fis8[ a] g2 |

  b4 b8[ d!] d4 c |
  b b8[ a] g2 |

  g4 g g a |
  b d8[ c] b2 |

  b4 b b d8[ fis,] |
  g8( b4) as8 b2 |

  g4 c d g, |
  b e  dis2 |

  b8[ c] b8[ a] b4 b |
  b fis16[ g a8] g2
}

bass = \relative c {
  \global
  e4 g,8[ b] e4 g,8[ a] |
  b4 b e2 |

  e4 fis g a |
  g8[ fis] e4 b2 |

  e8[ fis] g8[ fis] e4 g,8[ a] |
  b4 b e2 |

  b4 e8[ d!] g4 a |
  b4 b,4 e2 |

  c4 g c8[ b] a4 |
  g d' g,2 |

  e'4 b e d |
  g fis b,2 |

  c4 c' b g |
  e c b2 |
  g'8[ a] g[ fis] e4 g,8[ a] |
  b4 b e2
}

verseOne = \lyricmode {
  % \set ignoreMelismata = ##t
  \set stanza = "1 "
  % Je -- sus, lov -- er of my soul,
  % Let me to thy bos -- om fly,
  % While the near -- er wa -- ters roll,
  % While the tem -- pest still is high.
  % Hide me, O my Sa -- vior, hide,
  % Till the storm of life is past;
  % Safe in -- to the ha -- ven guide;
  % O re -- ceive my soul at last.

  \l Jé -- sus, ô nom qui sur -- passe
  \l tout nom qu’on puisse ex -- al -- ter!
  \l Que ja -- mais je ne me lasse,
  \l Nom bé -- ni, de te chan -- ter!
  \l Seu -- le clar -- té qui ra -- yonne
  \l sur les gloi -- res du saint lieu,
  \l seul nom dont l’é -- cho ré -- sonne
  \l dans le cœur mê -- me de Dieu!
}

verseTwo = \lyricmode {
  % \set ignoreMelismata = ##t
  \set stanza = "2 "
  % Oth -- er re -- fuge have I none,
  % Hangs my help -- less soul on thee;
  % Leave, ah! leave me not a -- lone,
  % Still sup -- port and com -- fort me.
  % All my trust on thee is stayed,
  % All my help from thee I bring;
  % Cov -- er my de -- fense -- less head
  % with the sha -- dow of thy wing.

  \l Jé -- sus, c’est l’A -- mour su -- prême
  \l de son trôn -- e de -- scen -- du,
  \l qui ceint de son di -- a -- dème
  \l le front de l’hom -- me per -- du.
  \l C’est le Roi qui s’hu -- mi -- lie
  \l pour vain -- cre le ré -- vol -- té;
  \l c’est la di -- vi -- ne fo -- lie,
  \l dans la di -- vi -- ne bon -- té.

}

verseThree = \lyricmode {
  % \set ignoreMelismata = ##t
  \set stanza = "3 "
  % Thou, O Christ, art all I want,
  % More than all in thee I find;
  % Raise the fal -- len, cheer the faint,
  % Heal the sick, and lead the blind.
  % Just and ho -- ly is thy Name,
  % I am all un -- righ -- teous -- ness;
  % False and full of sin I am;
  % Thou art full of truth and grace.
  
  \l Qui pleu -- ra sur ceux qui pleurent?
  \l C’est Lui, l’hom -- me mé -- pri -- sé!
  \l Qui mou -- rut pour ceux qui meurent?
  \l C’est Lui, l’homme au cœur bri -- sé!
  \l De son sang et de ses larmes
  \l il ar -- ro -- sa son che -- min,
  \l Et c’est par ces seu -- les armes
  \l qu’il sau -- va le genre hu -- main.
}

verseFour = \lyricmode {
  % \set ignoreMelismata = ##t
  \set stanza = "4 "
  % Plen -- teous grace with thee is found,
  % Grace to cov -- er all my sin;
  % Let the heal -- ing streams a -- bound;
  % Make and keep me pure with -- in.
  % Thou of life the foun -- tain art,
  % Free -- ly let me take of thee;
  % Spring thou up with -- in my heart;
  % Rise to all e -- ter -- ni -- ty.

  \l Jé -- sus, par qui Dieu par -- donne,
  \l Roi d’é -- pin -- es cou -- ron -- né,
  \l que le mon -- de t’a -- ban -- donne,
  \l à toi mon cœur s’est don -- né!
  \l Ta mort est ma dé -- li -- vrance;
  \l je suis heu -- reux sous ta loi;
  \l ô Jé -- sus, mon es -- pér -- ance,
  \l quelle autre au -- rais –je que toi?
}


\score {
  <<
    \new Staff  <<
      \new Voice = "soprano" { \voiceOne \melody }
      \new Voice = "alto" { \voiceTwo \alto }
    >>
    \new Lyrics \with{\lyricspace} \lyricsto soprano \verseOne
    \new Lyrics  \lyricsto soprano \verseTwo
    \new Lyrics  \lyricsto soprano \verseThree
    \new Lyrics  \lyricsto soprano \verseFour
    \new Staff  <<
      \clef bass
      \new Voice = "tenor" { \voiceOne \tenor }
      \new Voice = "bass" { \voiceTwo \bass }
    >>
  >>
}

\markup { \vspace #0.5 } 

\markup {
  \fill-line {
    \hspace #0.1 % moves the column off the left margin;
     % can be removed if space on the page is tight
     \column {
      \line { \bold "5 "
        \column {
          % TO ADAPT
          "Ô penche-toi sur ma couche"
          "lorsque je devrai mourir,"
          "et, ton doux nom sur la bouche,"
          "je verrai le ciel s'ouvrir."
          "Mais le ciel que je réclame,"
          "c'est ton regard, c'est ta voix,"
          "qui s'abaissent sur mon âme"
          "assise au pied de ta croix!"
        }
      }
    }
    \hspace #0.1 % adds horizontal spacing between columns;
    \column {
      \line { \bold "6 "
        \column {
          "Attire, ô Sauveur, attire"
          "sur ton sein et dans tes bras"
          "le cœur qui tremble et soupire"
          "parce qu'il ne te voit pas."
          "Mon frère, il t'appelle, écoute:"
          "il t'appelle, ne crains plus;"
          "suis ses pas, et sur la route"
          "chante ce beau nom: Jésus!"
        }
      }
    }
  \hspace #0.1 % gives some extra space on the right margin;
  % can be removed if page space is tight
  }
}

