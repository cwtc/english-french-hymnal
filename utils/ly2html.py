# import argparse
import glob
import os

if __name__ == '__main__':
    # parser = argparse.ArgumentParser()
    # parser.add_argument('in_ly_dir', type=str)
    # parser.add_argument('out_html_dir', type=str)
    # args = parser.parse_args()

    # if not os.path.exists(args.out_html_dir):
        # os.makedirs(args.out_html_dir)

    in_ly_dir = 'lilypond/ly'
    out_html_dir = 'lilypond/html'

    # For every input ly, write an output html
    # in_ly_dir = os.path.abspath(args.in_ly_dir)
    # out_html_dir = os.path.abspath(args.out_html_dir)
    for ly_path in glob.glob(os.path.join(in_ly_dir, '*.ly')):
        html_lines = []
        html_lines.append('<html>')
        html_lines.append('<lilypondfile line-width=20 staffsize=14>{0}</lilypondfile>'.format(ly_path))
        html_lines.append('</html>')
        basename = os.path.basename(ly_path).replace('.ly', '.html')
        html_path = os.path.join(out_html_dir, basename)

        with open(html_path, 'w') as fp:
            for line in html_lines:
                fp.write('{0}\n'.format(line))
        break
